<?php

namespace Exen\Support\Tests\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\Packer;
use Exen\Support\MessagePack\Type\Map;
use Exen\Support\MessagePack\TypeTransformer\MapTransformer;
use Exen\Support\TestCase;

class MapTransformerTest extends TestCase
{
    public function testPackMap(): void
    {
        $raw = ['abc' => 5];
        $packed = "\x81\xa3\x61\x62\x63\x05";

        $packer = $this->createMock(Packer::class);
        $packer->expects(self::any())->method('packMap')
            ->with($raw)
            ->willReturn($packed);

        $transformer = new MapTransformer();
        $map = new Map($raw);

        self::assertSame($packed, $transformer->pack($packer, $map));
    }

    public function testPackNonMap(): void
    {
        $raw = ['abc' => 5];
        $packed = "\x81\xa3\x61\x62\x63\x05";

        $packer = $this->createMock(Packer::class);
        $packer->expects(self::any())->method('packMap')
            ->with($raw)
            ->willReturn($packed);

        $transformer = new MapTransformer();

        self::assertNull($transformer->pack($packer, $raw));
    }
}
