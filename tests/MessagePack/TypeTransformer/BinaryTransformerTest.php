<?php

namespace Exen\Support\Tests\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\Packer;
use Exen\Support\MessagePack\Type\Binary;
use Exen\Support\MessagePack\TypeTransformer\BinaryTransformer;
use Exen\Support\TestCase;

final class BinaryTransformerTest extends TestCase
{
    public function testPackBinary(): void
    {
        $raw = 'abc';
        $packed = "\xc4\x03\x61\x62\x63";

        $packer = $this->createMock(Packer::class);
        $packer->expects(self::any())->method('packBin')
            ->with($raw)
            ->willReturn($packed);

        $transformer = new BinaryTransformer();
        $binary = new Binary($raw);

        self::assertSame($packed, $transformer->pack($packer, $binary));
    }

    public function testPackNonBinary(): void
    {
        $raw = 'abc';
        $packed = "\xc4\x03\x61\x62\x63";

        $packer = $this->createMock(Packer::class);
        $packer->expects(self::any())->method('packBin')
            ->with($raw)
            ->willReturn($packed);

        $transformer = new BinaryTransformer();

        self::assertNull($transformer->pack($packer, $raw));
    }
}
