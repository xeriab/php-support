<?php

namespace Exen\Support\Tests\MessagePack;

use Exen\Support\MessagePack\MessagePack;
use Exen\Support\MessagePack\PackOptions;
use Exen\Support\MessagePack\UnpackOptions;
use Exen\Support\TestCase;

final class MessagePackTest extends TestCase
{
    public function testPack(): void
    {
        self::assertSame("\x91\x01", MessagePack::pack([0 => 1]));
    }

    public function testPackWithOptions(): void
    {
        self::assertSame("\x81\x00\x01", MessagePack::pack([0 => 1], PackOptions::FORCE_MAP));
    }

    public function testUnpack(): void
    {
        self::assertSame('abc', MessagePack::unpack("\xa3\x61\x62\x63"));
    }

    public function testUnpackWithOptions(): void
    {
        $packed = "\xcf" . "\xff\xff\xff\xff" . "\xff\xff\xff\xff";
        $unpacked = '18446744073709551615';

        self::assertSame($unpacked, MessagePack::unpack($packed, UnpackOptions::BIGINT_AS_STR));
    }
}
