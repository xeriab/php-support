<?php

namespace Exen\Support\Tests\MessagePack;

use Exen\Support\MessagePack\Exception\InvalidOptionException;
use Exen\Support\MessagePack\UnpackOptions;
use Exen\Support\TestCase;

final class UnpackOptionsTest extends TestCase
{
    /**
     * @dataProvider provideIsserData
     */
    public function testFromBitmask(string $isserName, bool $expectedResult, int $bitmask): void
    {
        $options = UnpackOptions::fromBitmask($bitmask);

        self::assertSame($expectedResult, $options->{$isserName}());
    }

    public function provideIsserData(): array
    {
        return [
            ['isBigIntAsStrMode', true, 0],
            ['isBigIntAsStrMode', true, UnpackOptions::BIGINT_AS_STR],
            ['isBigIntAsStrMode', false, UnpackOptions::BIGINT_AS_GMP],
            ['isBigIntAsStrMode', false, UnpackOptions::BIGINT_AS_EXCEPTION],

            ['isBigIntAsGmpMode', false, 0],
            ['isBigIntAsGmpMode', false, UnpackOptions::BIGINT_AS_STR],
            ['isBigIntAsGmpMode', true, UnpackOptions::BIGINT_AS_GMP],
            ['isBigIntAsGmpMode', false, UnpackOptions::BIGINT_AS_EXCEPTION],

            ['isBigIntAsExceptionMode', false, 0],
            ['isBigIntAsExceptionMode', false, UnpackOptions::BIGINT_AS_STR],
            ['isBigIntAsExceptionMode', false, UnpackOptions::BIGINT_AS_GMP],
            ['isBigIntAsExceptionMode', true, UnpackOptions::BIGINT_AS_EXCEPTION],
        ];
    }

    /**
     * @dataProvider provideInvalidOptionsData
     */
    public function testFromBitmaskWithInvalidOptions(int $bitmask, string $errorMessage): void
    {
        try {
            UnpackOptions::fromBitmask($bitmask);
        } catch (InvalidOptionException $e) {
            self::assertSame($e->getMessage(), $errorMessage);

            return;
        }

        self::fail(InvalidOptionException::class . ' was not thrown.');
    }

    public function provideInvalidOptionsData(): iterable
    {
        yield [
            UnpackOptions::BIGINT_AS_GMP | UnpackOptions::BIGINT_AS_STR,
            'Invalid option bigint, use one of Exen\Support\MessagePack\UnpackOptions::BIGINT_AS_STR, Exen\Support\MessagePack\UnpackOptions::BIGINT_AS_GMP or Exen\Support\MessagePack\UnpackOptions::BIGINT_AS_EXCEPTION.',
        ];
    }
}
