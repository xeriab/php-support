<?php

namespace Exen\Support\Tests\MessagePack\Exception;

use Exen\Support\MessagePack\Exception\InvalidOptionException;
use Exen\Support\TestCase;

final class InvalidOptionExceptionTest extends TestCase
{
    /**
     * @dataProvider provideOutOfRangeData
     */
    public function testOutOfRange(string $invalidOption, array $validOptions, string $message): void
    {
        $exception = InvalidOptionException::outOfRange($invalidOption, $validOptions);

        self::assertSame($message, $exception->getMessage());
    }

    public function provideOutOfRangeData(): array
    {
        return [
            ['foobar', ['foo'], 'Invalid option foobar, use foo.'],
            ['foobar', ['foo', 'bar'], 'Invalid option foobar, use foo or bar.'],
            ['foobar', ['foo', 'bar', 'baz'], 'Invalid option foobar, use one of foo, bar or baz.'],
        ];
    }
}
