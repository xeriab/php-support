<?php

namespace Exen\Support\Tests\MessagePack\Exception;

use Exen\Support\MessagePack\Exception\PackingFailedException;
use Exen\Support\TestCase;

final class PackingFailedExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $value = (object) ['foo' => 'bar'];
        $errorMessage = 'Error message';
        $prevException = new \Exception();

        $exception = new PackingFailedException($value, $errorMessage, $prevException);

        self::assertSame($value, $exception->getValue());
        self::assertSame($errorMessage, $exception->getMessage());
        self::assertSame(0, $exception->getCode());
        self::assertSame($prevException, $exception->getPrevious());
    }
}
