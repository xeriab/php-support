<?php

namespace Exen\Support\Tests\MessagePack\Exception;

use Exen\Support\MessagePack\Exception\IntegerOverflowException;
use Exen\Support\TestCase;

final class IntegerOverflowExceptionTest extends TestCase
{
    public function testConstructor(): void
    {
        $value = -1;
        $exception = new IntegerOverflowException($value);

        self::assertSame($value, $exception->getValue());
        self::assertSame('The value is too big: 18446744073709551615.', $exception->getMessage());
    }
}
