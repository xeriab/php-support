<?php

declare(strict_types=1);

namespace Exen\Support\Tests;

use Exen\Support\Path;
use Exen\Support\TestCase;

class PathTest extends TestCase
{
    const TEST_CLASS = 'Exen\Support\Path';

    public function setUp(): void
    {
        //
    }

    public function testDirname()
    {
        $f = __FILE__;

        $this->assertEquals(\substr(Path::dirName($f), -5), 'tests');
        $this->assertEquals('/a', Path::dirName('/a/b/'));
        $this->assertEquals('/a', Path::dirName('/a/b'));
        $this->assertEquals('/', Path::dirName('/a'));
        $this->assertEquals('.', Path::dirName(''));
        $this->assertEquals('/', Path::dirName('/'));
        $this->assertEquals('/', Path::dirName('////'));
    }

    public function testExtname()
    {
        $f = __FILE__;

        $this->assertEquals('.php', Path::extName($f));
        $this->assertEquals('', Path::extName(''));
        $this->assertEquals('', Path::extName('/path/to/file'));
        $this->assertEquals('.ext', Path::extName('/path/to/file.ext'));
        $this->assertEquals('.ext', Path::extName('/path.to/file.ext'));
        $this->assertEquals('', Path::extName('/path.to/file'));
        $this->assertEquals('', Path::extName('/path.to/.file'));
        $this->assertEquals('.ext', Path::extName('/path.to/.file.ext'));
        $this->assertEquals('.ext', Path::extName('/path/to/f.ext'));
        $this->assertEquals('.ext', Path::extName('/path/to/..ext'));
        $this->assertEquals('', Path::extName('file'));
        $this->assertEquals('.ext', Path::extName('file.ext'));
        $this->assertEquals('', Path::extName('.file'));
        $this->assertEquals('.ext', Path::extName('.file.ext'));
        $this->assertEquals('', Path::extName('/file'));
        $this->assertEquals('.ext', Path::extName('/file.ext'));
        $this->assertEquals('', Path::extName('/.file'));
        $this->assertEquals('.ext', Path::extName('/.file.ext'));
        $this->assertEquals('.ext', Path::extName('.path/file.ext'));
        $this->assertEquals('.ext', Path::extName('file.ext.ext'));
        $this->assertEquals('.', Path::extName('file.'));
        $this->assertEquals('', Path::extName('.'));
        $this->assertEquals('', Path::extName('./'));
        $this->assertEquals('.ext', Path::extName('.file.ext'));
        $this->assertEquals('', Path::extName('.file'));
        $this->assertEquals('.', Path::extName('.file.'));
        $this->assertEquals('.', Path::extName('.file..'));
        $this->assertEquals('', Path::extName('..'));
        $this->assertEquals('', Path::extName('../'));
        $this->assertEquals('.ext', Path::extName('..file.ext'));
        $this->assertEquals('.file', Path::extName('..file'));
        $this->assertEquals('.', Path::extName('..file.'));
        $this->assertEquals('.', Path::extName('..file..'));
        $this->assertEquals('.', Path::extName('...'));
        $this->assertEquals('.ext', Path::extName('...ext'));
        $this->assertEquals('.', Path::extName('....'));
        $this->assertEquals('.ext', Path::extName('file.ext/'));
        $this->assertEquals('.ext', Path::extName('file.ext//'));
        $this->assertEquals('', Path::extName('file/'));
        $this->assertEquals('', Path::extName('file//'));
        $this->assertEquals('.', Path::extName('file./'));
        $this->assertEquals('.', Path::extName('file.//'));
    }
}
