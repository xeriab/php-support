<?php

declare(strict_types=1);

namespace Exen\Support\Tests;

use Exen\Support\Lang;
use Exen\Support\TestCase;

class LangTest extends TestCase
{
    const TEST_CLASS = 'Exen\Support\Lang';


    public function setUp(): void
    {
        //
    }

    public function testisNotEmpty()
    {
        $this->assertTrue(Lang::isNotEmpty([0]));
    }

    public function testisNotNull()
    {
        $this->assertTrue(Lang::isNotNull([0]));
    }

    public function testsomething()
    {
        $this->assertTrue(true);
    }
}
