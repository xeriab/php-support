<?php

namespace Exen\Support\Tests\Json;

use Exen\Support\TestCase;
use Exen\Support\Json\JsonPrettyPrinter;

class JsonPrettyPrinterTest extends TestCase
{
    public function testPrettyPrint()
    {
        $json = \file_get_contents(__DIR__ . '/source.json');
        $object = \json_decode($json);
        $jsonString = \json_encode($object);
        $prettyPrinter = new JsonPrettyPrinter();
        $result = $prettyPrinter
            ->setIndentationString('  ')
            ->prettyPrint($jsonString);
        $expected = \file_get_contents(__DIR__ . '/result.json');
        $this->assertEquals($expected, $result);
    }
}
