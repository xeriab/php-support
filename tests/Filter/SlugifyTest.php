<?php

namespace Exen\Support\Tests\Filter;

use Exen\Support\TestCase;
use Exen\Support\Filter\SlugifyFilter;

class SlugifyTest extends TestCase
{
    public function testFilter()
    {
        $filter = new SlugifyFilter;

        $values = [
            'PAGEKIT'                  => 'pagekit',
            ":#*\"@+=;!><&.%()/'\\|[]" => "",
            "  a b ! c   "             => "a-b-c",
        ];

        foreach ($values as $in => $out) {
            $this->assertEquals($out, $filter->filter($in));
        }

    }
}
