<?php

namespace Exen\Support\Tests\Filter;

use Exen\Support\TestCase;
use Exen\Support\Filter\StripNewlinesFilter;

class StripNewlinesTest extends TestCase
{
    /**
     * @dataProvider provideNewLineStrings
     */
    public function testFilter($input, $output)
    {
        $filter = new StripNewlinesFilter;

        $this->assertEquals($output, $filter->filter($input));
    }

    /**
     * @return array
     */
    public function provideNewLineStrings()
    {
        return [
            ['', ''],
            ["\n", ''],
            ["\r", ''],
            ["\r\n", ''],
            ['\n', '\n'],
            ['\r', '\r'],
            ['\r\n', '\r\n'],
            ["These newlines should\nbe removed by\r\nthe filter", 'These newlines shouldbe removed bythe filter']
        ];
    }
}
