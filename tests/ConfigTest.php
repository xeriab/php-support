<?php
/**
 * Config test.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.com so we can mail you a copy immediately.
 *
 * @category Tests
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.com>
 * @license  https://gitlab.com/xeriab/support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/support
 */

declare(strict_types=1);

namespace Exen\Support\Tests;

use Exen\Support\Config;
use Exen\Support\TestCase;

class ConfigTest extends TestCase
{
    /**
     * Config object.
     *
     * @var \Exen\Support\Config
     */
    protected $config;

    /**
     * @var array
     */
    protected $values;

    public function setUp(): void
    {
        $values = [
            'foo' => [
                'bar' => 'test'
            ]
        ];

        $this->config = new Config($values);
        $this->values = $values;
    }

    public function testHas()
    {
        $this->assertTrue($this->config->has('foo'));
        $this->assertTrue(!$this->config->has('none'));
    }

    public function testGet()
    {
        $this->assertEquals($this->config->get('foo.bar'), 'test');
    }

    public function testToArray()
    {
        $this->assertEquals($this->config->toArray(), $this->values);
    }

    public function testSet()
    {
        $this->config->set('foo.bar2', 'test2');
        $this->assertEquals($this->config->get('foo.bar2'), 'test2');
        $this->config->offsetSet('foo.bar', 'test3');
        $this->assertTrue($this->config->offsetExists('foo.bar'));
        $this->assertEquals($this->config->offsetGet('foo.bar'), 'test3');
        $this->config->offsetUnset('foo.bar');
        $this->assertEquals($this->config->offsetGet('foo.bar'), null);
    }

    // public function testDump()
    // {
    //     $this->config->dump();
    // }

    public function testGzipSerialize()
    {
        $this->assertEquals($this->values, $this->config->gzipUnserialize($this->config->gzipSerialize())->toArray());

        // // Uncompressed data.
        // $this->assertEquals($this->values, $this->config->gzipUnserialize($this->config->gzipSerialize()));
        // $this->assertEquals($this->values, $serializer->decode($serializer->encode($data)));
        // $this->assertEquals($data, $serializer->decode(serialize($data)));
    }
}
