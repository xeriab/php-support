<?php

declare(strict_types=1);

namespace Exen\Support\Tests;

use Exen\Support\TestCase;
use Exen\Support\MagicObject;

class MagicObjectTest extends TestCase
{
    const TEST_CLASS = 'Exen\Support\MagicObject';

    public function setUp(): void
    {
        //
    }

    /**
     *
     */
    public function testObjectInstanceOf()
    {
        $magicObject = new MagicObject();

        $this->assertInstanceOf(self::TEST_CLASS, $magicObject);
    }

    /**
     *
     */
    public function testConstruct()
    {
        $magicObject = new MagicObject('testing', 'construct');

        // test storage property
        $this->assertClassHasAttribute('storage', self::TEST_CLASS);

        // test ArrayAccess
        $this->assertIsString($magicObject[0]);
        $this->assertIsString($magicObject[1]);
        //
        $this->assertEquals('testing', $magicObject[0]);
        $this->assertEquals('construct', $magicObject[1]);

        // test ArrayObject
        $this->assertIsString($magicObject->{0});
        $this->assertIsString($magicObject->{1});
        //
        $this->assertEquals('testing', $magicObject->{0});
        $this->assertEquals('construct', $magicObject->{1});
    }

    /**
     *
     */
    public function testArrayAccessGetterSetter()
    {
        $magicObject = new MagicObject();

        // set something keyed
        $magicObject['foo'] = 'BarBaz';
        $this->assertIsString($magicObject['foo']);
        $this->assertEquals('BarBaz', $magicObject['foo']);

        // set something indexed
        $magicObject[] = 'BarBaz';
        $this->assertIsString($magicObject[0]);
        $this->assertEquals('BarBaz', $magicObject[0]);
    }

    /**
     *
     */
    public function testArrayAccessIsset()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject['foo'] = 'BarBaz';

        // test
        $this->assertTrue(isset($magicObject['foo']));
    }

    /**
     *
     */
    public function testArrayAccessUnset()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject['foo'] = 'BarBaz';

        // check isset
        $this->assertTrue(isset($magicObject['foo']));

        // unset and check
        unset($magicObject['foo']);
        $this->assertFalse(isset($magicObject['foo']));
    }

    /**
     *
     */
    public function testMagicGetterSetter()
    {
        $magicObject = new MagicObject();

        // set something keyed
        $magicObject->foo = 'BarBaz';
        $this->assertIsString($magicObject->foo);
        $this->assertEquals('BarBaz', $magicObject->foo);

        // set something indexed
        $magicObject[] = 'BarBaz';
        $this->assertIsString($magicObject->{0});
        $this->assertEquals('BarBaz', $magicObject->{0});

        // test not set
        $this->assertEquals(null, $magicObject->non_existent);
    }

    /**
     *
     */
    public function testMagicIsset()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->foo = 'BarBaz';

        // test
        $this->assertTrue(isset($magicObject->foo));
    }

    /**
     *
     */
    public function testMagicUnset()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->foo = 'BarBaz';

        // check isset
        $this->assertTrue(isset($magicObject->foo));

        // unset and check
        unset($magicObject->foo);
        $this->assertFalse(isset($magicObject->foo));
    }

    /**
     *
     */
    public function testMagicInvoke()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->string = 'BarBaz';

        // test
        $this->assertEquals('BarBaz', $magicObject('string'));
        $this->assertEquals(null, $magicObject('non_existent'));
    }

    /**
     *
     */
    public function testMagicToString()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->string = 'BarBaz';

        $expected = \json_encode(['string' => 'BarBaz'], \JSON_PRETTY_PRINT);

        // test
        $this->assertEquals($expected, \strval($magicObject));
    }

    /**
     *
     */
    public function testMagicDebugInfo()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->string = 'BarBaz';

        // test
        $this->assertEquals(
            "Exen\Support\MagicObject Object\n(\n    [string] => BarBaz\n)\n",
            \print_r($magicObject, true)
        );
    }

    /**
     *
     */
    public function testCount()
    {
        $magicObject = new MagicObject();

        // set something
        $magicObject->string = 'BarBaz';

        // test
        $this->assertEquals(1, $magicObject->count());
        $this->assertEquals(1, \count($magicObject));
    }
}
