<?php
/**
 * Headers test.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.com so we can mail you a copy immediately.
 *
 * @category Tests
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.com>
 * @license  https://gitlab.com/xeriab/support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/support
 */

declare(strict_types=1);

namespace Exen\Support\Tests;

use Exen\Support\TestCase;
use Exen\Support\Headers;

class HeadersTest extends TestCase
{
    public function testIsThereAnySyntaxError()
    {
        $var = new Headers();
        $this->assertTrue(\is_object($var));
        unset($var);
    }
}
