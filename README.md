<center>![Support](.gitlab/support-logo.png "Support")</center>

Support
=======

The PHP general purpose support library provides classes and utilities to ease developing PHP applications.

Resources
---------

  * [Documentation](https://xeriab.gitlab.com/docs/php-support/index.html)
  * [Contributing](https://xeriab.gitlab.com/docs/contributing/index.html)
  * [Changes log](CHANGES.md).
  * [Report issues](https://gitlab.com/xeriab/php-support/issues) and
    [Send Pull/Merge Requests](https://gitlab.com/xeriab/php-support/merge_requests)
    in the [main Support repository](https://gitlab.com/xeriab/php-support)

## Copyright and License

Copyright [Xeriab Nabil](#). under the [MIT license](LICENSE).
