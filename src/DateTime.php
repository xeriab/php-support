<?php

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Exception\InvalidArgumentException;
use Exen\Support\Traits\SmartObjectTrait;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\YamlSerializable;

/**
 * Class for Date and Time.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
final class DateTime extends \DateTime implements
    JsonSerializable,
    YamlSerializable
{
    use SmartObjectTrait;

    /**
     * Minute in seconds.
     */
    const MINUTE = 60;

    /**
     * Hour in seconds.
     */
    const HOUR = 60 * self::MINUTE;

    /**
     * Day in seconds.
     */
    const DAY = 24 * self::HOUR;

    /**
     * Week in seconds.
     */
    const WEEK = 7 * self::DAY;

    /**
     * Average month in seconds.
     */
    const MONTH = 2629800;

    /**
     * Average year in seconds.
     */
    const YEAR = 31557600;

    /**
     * DateTime object factory.
     *
     * @param string|int|\DateTimeInterface $time Time.
     *
     * @return static
     */
    public static function from($time = null): self
    {
        if ($time instanceof \DateTimeInterface) {
            return new static(
                $time->format('Y-m-d H:i:s.u'),
                $time->getTimezone()
            );
        } elseif (\is_numeric($time)) {
            if ($time <= self::YEAR) {
                $time += \time();
            }

            return (new static('@'.$time))->setTimezone(
                new \DateTimeZone(\date_default_timezone_get())
            );
        } else { // textual or null
            return new static($time);
        }
    }

    /**
     * Creates DateTime object.
     *
     * @param int|integer $year   Year.
     * @param int|integer $month  Month.
     * @param int|integer $day    Day.
     * @param int|integer $hour   Hour.
     * @param int|integer $minute Minute.
     * @param int|integer $second Second.
     *
     * @return static
     */
    public static function fromParts(
        $year = null,
        $month = null,
        $day = null,
        int $hour = 0,
        int $minute = 0,
        int $second = 0
    ): self {
        $s = \sprintf(
            '%04d-%02d-%02d %02d:%02d:%02.5f',
            $year,
            $month,
            $day,
            $hour,
            $minute,
            $second
        );

        if (!\checkdate($month, $day, $year) || $hour < 0 || $hour > 23
            || $minute < 0 || $minute > 59 || $second < 0 || $second >= 60
        ) {
            throw new InvalidArgumentException("Invalid date '$s'");
        }

        return new static($s);
    }

    /**
     * Returns new DateTime object formatted according to the specified format.
     *
     * @param string               $format   The format the $time parameter
     *                                       should be in.
     * @param string               $time     String representing the time
     * @param string|\DateTimeZone $timezone Desired timezone (default timezone
     *                                       is used if null is passed)
     *
     * @return static|false
     */
    public static function createFromFormat(
        $format,
        $time,
        $timezone = null
    ): self {
        if (null === $timezone) {
            $timezone = new \DateTimeZone(date_default_timezone_get());
        } elseif (is_string($timezone)) {
            $timezone = new \DateTimeZone($timezone);
        } elseif (!$timezone instanceof \DateTimeZone) {
            throw new InvalidArgumentException('Invalid timezone given');
        }

        $date = parent::createFromFormat($format, $time, $timezone);

        return $date ? static::from($date): false;
    }

    /**
     * Returns JSON representation in ISO 8601 (used by JavaScript).
     *
     * @return string
     */
    public function jsonSerialize(): string
    {
        return $this->format('c');
    }

    /**
     * Returns YAML representation in ISO 8601.
     *
     * @return string
     */
    public function yamlSerialize(): string
    {
        return $this->format('c');
    }

    /**
     * Modify clonned.
     *
     * @param string $modify Modify string.
     *
     * @return static
     */
    public function modifyClone(string $modify = ''): DateTime
    {
        $clonned = clone $this;

        return $modify ? $clonned->modify($modify): $clonned;
    }

    /**
     * Sets timestamp.
     *
     * @param int $timestamp Timestamp.
     *
     * @return static
     */
    public function setTimestamp($timestamp = null): DateTime
    {
        $zone = $this->getTimezone();
        $this->__construct('@'.$timestamp);

        return $this->setTimezone($zone);
    }

    /**
     * Gets timestamp.
     *
     * @return int|string
     */
    public function getTimestamp()
    {
        $timestamp = $this->format('U');

        return \is_float($tmp = $timestamp * 1) ? $timestamp : $tmp;
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        // return $this->format('Y-m-d H:i:s');

        return xprintf(
            '<%(className)s %(dateTime)s>',
            [
                'className' => parseClassName(static::class)['classname'],
                'dateTime'  => $this->format('Y-m-d H:i:s'),
            ]
        );
    }
}
