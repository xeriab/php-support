<?php /** @noinspection PhpComposerExtensionStubsInspection */
/** @noinspection PhpUndefinedNamespaceInspection */
/** @noinspection SpellCheckingInspection */
/** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * Lang.
 *
 * @category  Helpers
 * @package   Exen\Support
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types=1);

namespace Exen\Support;

use ArrayAccess;
use ArrayObject;
use Closure;
use Countable;
use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use DomNode;
use DOMXPath;
use Error;
use ErrorException;
use Exception;
use Exen\Support\Container\ArrObject;
use Exen\Support\Container\Collection;
use Exen\Support\Container\Interfaces\CollectionInterface;
use Exen\Support\Helpers\StringFormat\FormatterIndexed;
use Exen\Support\Helpers\StringFormat\FormatterNamed;
use Exen\Support\Helpers\StringFormat\FormatterPlaceholder;
use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Hashable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Serializable;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Interfaces\Xmlable;
use Exen\Support\Interfaces\XmlSerializable;
use Exen\Support\Interfaces\Yamlable;
use Exen\Support\Interfaces\YamlSerializable;
use Exen\Support\Json\Json;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Type;
use Exen\Support\Yaml\Yaml;
use InvalidArgumentException;
use LogicException;
use Monolog\Logger;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use stdClass;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\Yaml\Exception\ParseException;
use Throwable;
use Traversable;
use TypeError;
use function abs;
use function apache_get_modules;
use function array_filter;
use function array_key_exists;
use function array_keys;
use function array_map;
use function array_merge;
use function array_reduce;
use function array_reverse;
use function array_shift;
use function array_slice;
use function array_values;
use function array_walk;
use function boolval;
use function call_user_func;
use function call_user_func_array;
use function chr;
use function class_exists;
use function constant;
use function count;
use function crc32;
use function ctype_digit;
use function date_default_timezone_get;
use function doubleval;
use function end;
use function explode;
use function floatval;
use function fopen;
use function func_get_args;
use function func_num_args;
use function function_exists;
use function get_class;
use function get_class_methods;
use function get_declared_classes;
use function get_defined_constants;
use function get_loaded_extensions;
use function get_object_vars;
use function gettype;
use function glob;
use function hash;
use function hash_algos;
use function hash_hmac_algos;
use function implode;
use function in_array;
use function interface_exists;
use function intval;
use function is_a;
use function is_array;
use function is_bool;
use function is_callable;
use function is_dir;
use function is_double;
use function is_file;
use function is_finite;
use function is_float;
use function is_infinite;
use function is_int;
use function is_integer;
use function is_iterable;
use function is_long;
use function is_nan;
use function is_null;
use function is_numeric;
use function is_object;
use function is_readable;
use function is_resource;
use function is_scalar;
use function is_string;
use function is_subclass_of;
use function is_writable;
use function join;
use function json_decode;
use function json_encode;
use function json_last_error;
use function ksort;
use function libxml_clear_errors;
use function libxml_get_errors;
use function libxml_use_internal_errors;
use function mb_strlen;
use function md5;
use function method_exists;
use function mt_rand;
use function number_format;
use function ord;
use function password_hash;
use function password_verify;
use function pow;
use function preg_match;
use function preg_match_all;
use function preg_replace;
use function preg_replace_callback;
use function property_exists;
use function range;
use function reset;
use function round;
use function serialize;
use function setrawcookie;
use function simplexml_load_string;
use function sleep;
use function spl_object_hash;
use function sprintf;
use function str_replace;
use function str_split;
use function stream_get_contents;
use function stripos;
use function stripslashes;
use function strlen;
use function strpos;
use function strtolower;
use function strtoupper;
use function substr;
use function substr_count;
use function time;
use function time_nanosleep;
use function trim;
use function uniqid;
use function unserialize;
use function usleep;
use function var_export;
use function vsprintf;
use const ASTERISK;
use const COUNT_RECURSIVE;
use const E_ALL;
use const E_COMPILE_ERROR;
use const E_COMPILE_WARNING;
use const E_CORE_ERROR;
use const E_DEPRECATED;
use const E_ERROR;
use const E_NOTICE;
use const E_STRICT;
use const E_USER_DEPRECATED;
use const E_USER_ERROR;
use const E_USER_NOTICE;
use const E_USER_WARNING;
use const E_WARNING;
use const EMPTY_STRING;
use const JSON_ERROR_NONE;
use const PASSWORD_DEFAULT;
use const PHP_EOL;
use const PHP_INT_MAX;
use const PHP_SAPI;
use const PREG_SET_ORDER;
use const SINGLE_DOT;

// use GuzzleHttp\Psr7\Uri;

/**
 * Class Lang.
 *
 * This class includes various `PHP` helpers.
 *
 * @category Utilities
 * @package  Exen\Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 * @since    0.1.0
 */
class Lang extends PhpObject implements Singletonable
{
    use MacroableTrait;

    /* SERVER TYPE CONSTANT
     * ----------------------------- */
    const SERVER_TYPE_APACHE = 'apache';
    const SERVER_TYPE_HIAWATHA = 'hiawatha';
    const SERVER_TYPE_IIS = 'iis';
    const SERVER_TYPE_IIS7 = 'iis7';
    const SERVER_TYPE_LIGHTTPD = 'lighttpd';
    const SERVER_TYPE_LITESPEED = 'litespeed';
    const SERVER_TYPE_NGINX = 'nginx';
    const SERVER_TYPE_UNKNOWN = 'unknown';

    /**
     * An array of ignored method names.
     *
     * @var string[]
     */
    public static array $ignoredMethodNames = [
        'callMethod',
        'getPublicMethods',
        'hasMacro',
        'instance',
        'getInstance',
        'mixin',
        'registerMacro',
        'toString',
        '__invoke',
        '__toString',
        '__clone',
        '__call',
        '__callStatic',
    ];

    /**
     * An array of functions to be called X times.
     *
     * @var array
     */
    public static array $canBeCalledTimes = [];

    /**
     * An array of cached function results.
     *
     * @var array
     */
    public static array $cached = [];

    /**
     * An array tracking the last time a function was called.
     *
     * @var array
     */
    public static array $throttle = [];

    /**
     * Lang instance.
     *
     * @var Lang|null
     */
    private static ?Lang $instance = null;

    /**
     * Lang instances.
     *
     * @var Lang[]|array
     */
    private static array $instances = [];

    /**
     * Lang constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * Check if apache Server Base.
     *
     * @return bool
     */
    public static function isApache(): bool
    {
        return in_array(self::getWebServerSoftware(), [self::SERVER_TYPE_APACHE, self::SERVER_TYPE_LITESPEED]);
    }

    /**
     * Get Web Server Type.
     *
     * @return string
     */
    public static function getWebServerSoftware(): string
    {
        static $type;

        if (isset($type)) {
            return $type;
        }

        $software = $_SERVER['SERVER_SOFTWARE'] ?? EMPTY_STRING;

        $type = self::SERVER_TYPE_UNKNOWN;

        if (false !== stripos($software, 'lighttpd')) {
            $type = self::SERVER_TYPE_LIGHTTPD;
        }

        if (false !== strpos($software, 'Hiawatha')) {
            $type = self::SERVER_TYPE_HIAWATHA;
        }

        if (false !== strpos($software, 'Apache')) {
            $type = self::SERVER_TYPE_APACHE;
        } elseif (false !== strpos($software, 'Litespeed')) {
            $type = self::SERVER_TYPE_LITESPEED;
        }

        if (false !== strpos($software, 'nginx')) {
            $type = self::SERVER_TYPE_NGINX;
        }

        if (
            self::SERVER_TYPE_APACHE !== $type and self::SERVER_TYPE_LITESPEED !== $type
            and false !== strpos($software, 'Microsoft-IIS')
            and false !== strpos($software, 'ExpressionDevServer')
        ) {
            $type = self::SERVER_TYPE_IIS;
            if (intval(substr($software, strpos($software, 'Microsoft-IIS/') + 14)) >= 7) {
                $type = self::SERVER_TYPE_IIS7;
            }
        }
        if (function_exists('apache_get_modules')) {
            if (in_array('mod_security', apache_get_modules())) {
                $type = self::SERVER_TYPE_APACHE;
            }

            if (self::SERVER_TYPE_UNKNOWN == $type and function_exists('apache_get_version')) {
                $type = self::SERVER_TYPE_APACHE;
            }
        }

        return $type;
    }

    /**
     * Check if Litespeed Web Server Base.
     *
     * @return bool
     */
    public static function isLiteSpeed(): bool
    {
        return self::SERVER_TYPE_LIGHTTPD === self::getWebServerSoftware();
    }

    /**
     * Check if Nginx Web Server Base.
     *
     * @return bool
     */
    public static function isNginX(): bool
    {
        return self::SERVER_TYPE_NGINX === self::getWebServerSoftware();
    }

    /**
     * Check if Hiawatha Web Server Base.
     *
     * @return bool
     */
    public static function isHiawatha(): bool
    {
        return self::SERVER_TYPE_HIAWATHA === self::getWebServerSoftware();
    }

    /**
     * Check if Hiawatha Web Server Base.
     *
     * @return bool
     */
    public static function isLightHttpd(): bool
    {
        return self::SERVER_TYPE_LIGHTTPD === self::getWebServerSoftware();
    }

    /**
     * Check if IIS Web Server Base.
     *
     * @return bool
     */
    public static function isIIS(): bool
    {
        return in_array(self::getWebServerSoftware(), [self::SERVER_TYPE_IIS, self::SERVER_TYPE_IIS7]);
    }

    /**
     * Check if IIS7 Web Server Base.
     *
     * @return bool
     */
    public static function isIIS7(): bool
    {
        return self::SERVER_TYPE_IIS7 === self::getWebServerSoftware();
    }

    /**
     * Test if the current browser runs on a mobile device (smart phone, tablet, etc.).
     *
     * @return bool
     */
    public static function isMobile(): bool
    {
        if (empty($_SERVER['HTTP_USER_AGENT'])) {
            return false;
        }

        $userAgent = $_SERVER['HTTP_USER_AGENT'];

        if (
            false !== strpos($userAgent, 'Mobile') // many mobile devices (all iPhone, iPad, etc.)
            || false !== strpos($userAgent, 'Android')
            || false !== strpos($userAgent, 'Silk/')
            || false !== strpos($userAgent, 'Kindle')
            || false !== strpos($userAgent, 'BlackBerry')
            || false !== strpos($userAgent, 'Opera Mini')
            || false !== strpos($userAgent, 'Opera Mobi')
        ) {
            return true;
        }

        return false;
    }

    /**
     * Check If Using PhPass from
     * {@link http://www.openwall.com/phpass}.
     *
     * @param string|null $string $string has to check
     *
     * @return bool
     */
    public static function isMaybeOpenWallPasswordHash(string $string = null): bool
    {
        if (
            !is_string($string)
            || !in_array(($length = strlen($string)), [20, 34, 60])
            || preg_match('/[^a-zA-Z0-9.\/\$_]/', $string)
        ) {
            return false;
        }

        switch ((string)$length) {
            case '20':
                return !('_' != $string[0]
                    || false !== strpos($string, '$')
                    || false === strpos($string, '.'));
            case '34':
                return !(2 != substr_count($string, '$')
                    || !in_array(substr($string, 0, 3), ['$P$', '$H$']));
        }

        return !('$2a$' != substr($string, 0, 4)
            || '$' != substr($string, 6, 1)
            || !is_numeric(substr($string, 4, 2))
            || 3 != substr_count($string, '$'));
    }

    /**
     * Get Default Log Name By Code.
     *
     * @param integer|null $code
     *
     * @return string|null string as lowercase and returning null if not found
     * @noinspection PhpUndefinedClassInspection
     */
    public static function getLogStringByCode(int $code = null): ?string
    {
        $code = (is_numeric($code) and !is_float($code)) ? abs($code) : $code;

        if (!is_int($code)) {
            return null;
        }

        switch (self::getConvertAliasLogLevel($code)) {
            case Logger::NOTICE:
                return 'notice';
            case Logger::ERROR:
                return 'error';
            case Logger::WARNING:
                return 'warning';
            case Logger::INFO:
                return 'info';
            case Logger::DEBUG:
                return 'debug';
            case Logger::EMERGENCY:
                return 'emergency';
            case Logger::CRITICAL:
                return 'critical';
            case Logger::ALERT:
                return 'alert';
        }

        return null;
    }

    /**
     * Get Alias Log Level.
     *
     * @param integer $code
     *
     * @return integer if no match between code will be return integer 0
     * @noinspection PhpUndefinedClassInspection
     */
    public static function getConvertAliasLogLevel(int $code = 0): int
    {
        switch ($code) {
            case Logger::ALERT:
                return Logger::ALERT;
            case Logger::CRITICAL:
                return Logger::CRITICAL;
            case E_ALL:
            case Logger::DEBUG:
                return Logger::DEBUG;
            case E_NOTICE:
            case E_USER_NOTICE:
            case E_DEPRECATED:
            case E_USER_DEPRECATED:
            case Logger::NOTICE:
                return Logger::NOTICE;
            case E_ERROR:
            case E_CORE_ERROR:
            case E_COMPILE_ERROR:
            case Logger::ERROR:
                return Logger::ERROR;
            case Logger::INFO:
                return Logger::INFO;
            case Logger::EMERGENCY:
                return Logger::EMERGENCY;
            case E_WARNING:
            case E_USER_WARNING:
            case E_COMPILE_WARNING:
            case Logger::WARNING:
                return Logger::WARNING;
        }

        return 0;
    }

    public static function dumpAsString($variable = null)
    {
        $DEBUG_DUMP_AS_STRING = self::getConstant('DEBUG_DUMP_AS_STRING', null, false);

        $debugDumpAsString = self::isCli() and $DEBUG_DUMP_AS_STRING;

        $cloner = new VarCloner();
        $dumper = new CliDumper();

        $output = fopen('php://memory', 'r+b');

        if ($debugDumpAsString) {
            try {
                $output = $dumper->dump($cloner->cloneVar($variable), true);
            } catch (ErrorException $ex) {
                //
            }
        } else {
            try {
                $dumper->dump($cloner->cloneVar($variable), $output);
            } catch (ErrorException $ex) {
                //
            }

            $output = stream_get_contents($output, -1, 0);
        }

        // $dumper->dump($cloner->cloneVar($variable), true);

        // $dumper->dump($cloner->cloneVar($variable), $output);
        // $output = \stream_get_contents($output, -1, 0);

        return $output;

        // return (new CliDumper())->dump((new VarCloner())->cloneVar($variable, true));
    }

    /**
     * Gets constant.
     *
     * @param mixed ...$parameters
     *
     * @return mixed
     */
    public static function getConstant(...$parameters)
    {
        $input = null;
        $constantName = null;
        $default = null;

        $argc = count($parameters);

        $arrKey = function (?string $key = null, ?array $array = [], ?bool $returnValue = false) {
            $exists = array_key_exists($key, $array);

            if ($returnValue) {
                if ($exists) {
                    return $array[$key];
                } else {
                    return null;
                }
            }

            return $exists;
        };

        $result = null;

        if ($argc === 1) {
            $input = $parameters[0];
            $constantName = null;
            $default = null;
        } elseif ($argc === 2) {
            $input = $parameters[0];
            $constantName = $parameters[1] ?? null;
            $default = null;
        } elseif ($argc === 3) {
            $input = $parameters[0];
            $constantName = $parameters[1];
            $default = $parameters[2] ?? null;
        }

        if (
            self::isString($input) and (class_exists($input) or self::isInterface($input)) and
            self::isNotNull($constantName)
        ) {
            try {
                if (self::hasConstant($input, $constantName)) {
                    $refClass = new ReflectionClass($input);
                    $result = constant($refClass->getName() . "::$constantName");
                }
            } catch (ReflectionException $ex) {
                //
            }
        } elseif (self::isString($input) and self::isNull($constantName)) {
            $result = $arrKey($input, self::constants(), true) ?? $default;
        } elseif (self::isString($input) and self::isNull($constantName) and $default) {
            $result = $arrKey($input, self::constants(), true) ?? $default;
        }

        return $result;
    }

    /**
     * Determine whether the given value is a string.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isString($value = null): bool
    {
        // \preg_match(
        //     '/[A-ZA-za-z0-9\!\@\#\$\%\^\&\*(){}\[\]\.\s\t]/m',
        //     (string) $value,
        //     $matches
        // );

        // return \count($matches) > 0 || \is_string($value);

        return is_string($value);
    }

    /**
     * Checks that a `input` is an interface.
     *
     * @param mixed $input Input to check.
     *
     * @return boolean
     */
    public static function isInterface($input = null): bool
    {
        return interface_exists($input);
    }

    /**
     * Determine whether the given value is not null.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNotNull($value = null): bool
    {
        return !self::isNull($value);
    }

    /**
     * Determine whether the given value is null.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNull($value = null): bool
    {
        return is_null($value) or ($value == null);
    }

    /**
     * Determine whether the given `object` has the given constant `constantName`.
     *
     * @param mixed $object
     * @param string|null $constantName
     *
     * @return bool
     * @throws ReflectionException
     */
    public static function hasConstant($object = null, ?string $constantName = null): bool
    {
        $result = false;

        $classExists = false;

        if (self::isObject($object)) {
            $classExists = @class_exists(@get_class($object));
        } elseif (self::isString($object)) {
            $classExists = @class_exists($object);
        }

        if ($classExists) {
            if (
                self::isString($object) and (class_exists($object) or self::isInterface($object)) and
                self::isNotNull($constantName)
            ) {
                $result = (new ReflectionClass($object))->hasConstant($constantName);
            }
        }

        return $result;
    }

    /**
     * Checks that a value is an object if not throws an exception.
     *
     * @param mixed $input Input to check.
     * @param string|null $message Error message to throw.
     *
     * @return boolean
     */
    public static function isObject(
        $input = null,
        ?string $message = null
    ): bool
    {
        if (!is_object($input) or !($input instanceof ArrayObject)) {
            if (self::isNotNull($message)) {
                throw new InvalidArgumentException(
                    self::printf($message, $input)
                );
            }

            return gettype($input) === 'object';
            // return false;
        }

        return true;
    }

    /**
     * String format.
     *
     * @param string|null $inputString
     * @param mixed ...$arguments
     *
     * @return string|null
     */
    public static function printf(?string $inputString = null, ...$arguments): ?string
    {
        $args = null;
        $retVal = null;

        if (count($arguments) === 1) {
            if (Lang::isArray($arguments[0])) {
                $args = $arguments[0];
            } else {
                $args = [$arguments[0]];
            }
        } elseif (count($arguments) >= 1) {
            $args = $arguments;
        }

        // preg_match('@(%\([a-zA-Z0-9_-]+\)(\w))@mx', $inputString, $matches);
        // preg_match('/(%\d+)/i', $inputString, $matches, PREG_UNMATCHED_AS_NULL, 1);

        // DD($matches);

        if (preg_match('@(%+\w)@mx', $inputString)) {
            $retVal = sprintf($inputString, ...$args);
        } elseif (preg_match('@(%\d+)@mx', $inputString)) {
            $retVal = Lang::iprintf($inputString, $args);
        } else {
            if (Lang::isArray($args) and Lang::isAssociative($args)) {
                $retVal = Lang::nprintf($inputString, $args);

                if (preg_match('@(%\([a-zA-Z0-9_-]+\)(\w))@mx', $inputString)) {
                    $retVal = Lang::namedSprintf($inputString, $args);
                }
            } elseif (Lang::isArray($args) and Lang::isNotAssociative($args)) {
                if (preg_match('@({\d+})@mx', $inputString)) {
                    $retVal = Lang::phprintf($inputString, ...$args);
                } elseif (preg_match('@(%\d+)@mx', $inputString)) {
                    $retVal = Lang::phprintf($inputString, ...$args);
                } else {
                    $retVal = Lang::iprintf($inputString, $args);
                }
            } else {
                $retVal = Lang::iprintf($inputString, $args);
            }
        }

        if (preg_match('@({\d+})@mx', $inputString)) {
            $retVal = Lang::phprintf($inputString, ...$args);
        } elseif (preg_match('@(%\d+)@mx', $inputString)) {
            $retVal = Lang::phprintf($inputString, ...$args);
        }

        return $retVal;
    }

    /**
     * Determine whether the given value is an array.
     *
     * @param mixed $value Value to check.
     * @param bool|null $strict
     * @return boolean
     */
    public static function isArray($value = null, ?bool $strict = false): bool
    {
        if ($strict) {
            return is_array($value);
        }

        return is_array($value) or ($value instanceof ArrayAccess);
    }

    /**
     * Functional API for FormatterIndex. Instead of:
     *
     *   new FormatterIndex('some {} format')->compile('glorious');
     *
     * you can call:
     *
     *   Lang::iprintf('some {} format', ['glorious']);
     *
     * Few characters less :)
     *
     * @param string|null $inputString
     * @param array $arguments parameters used to fill placeholders
     *
     * @return string
     */
    public static function iprintf(string $inputString = null, array $arguments = []): string
    {
        $fmt = new FormatterIndexed($inputString);
        return call_user_func_array([$fmt, 'compile'], $arguments)->unfold();
    }

    /**
     * Determines if the given array is an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array Array to check.
     *
     * @return boolean
     */
    public static function isAssociative(?array $array = null): bool
    {
        return self::isAssoc($array);
    }

    /**
     * Determines if the given array is an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array Array to check.
     *
     * @return boolean
     */
    public static function isAssoc(?array $array = null): bool
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    /**
     * Functional API for FormatterNamed. Instead of:
     *
     *   new FormatterNamed('some {adjective} format')->compile(['adjective' => 'glorious']);
     *
     * you can call:
     *
     *   Lang::nprintf('some {adjective} format', ['adjective' => 'glorious']);
     *
     * Few characters less :)
     *
     * @param string|null $inputString
     * @param array $arguments parameters used to fill placeholders
     *
     * @return string
     */
    public static function nprintf(string $inputString = null, array $arguments = []): string
    {
        $fmt = new FormatterNamed($inputString);
        return $fmt->compile($arguments)->unfold();
    }

    protected static function namedSprintf(?string $inputString = null, array $arguments = []): ?string
    {
        $parsedExpression = self::pvtSprintf($inputString, $arguments);
        return $parsedExpression->format($arguments);
    }

    /**
     * Printf
     *
     * @param string|null $inputString
     * @param mixed|array $arguments
     * @return string
     * @noinspection PhpUnusedLocalVariableInspection
     */
    private static function pvtSprintf(string $inputString = null, $arguments = null)
    {
        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }

        $parameterMap = [];
        $replacementSets = [];
        $matches = [];

        $pattern = <<<'REGEX'
@
    (
        (?:^|[^%])
        (?:%%)*%
    ) # 1: The percent sign(s)
    \( # Literal open paren
        ([a-zA-Z_][a-zA-Z0-9_\-,:]*?) # 2: The name of the param
    \) # Literal close paren
    (
        (?:
            # Standard sprintf format. reference http://php.net/sprintf
            [+]?                # an optional +/- sign specifier
            (?:[ 0]|\'.)?       # an optional space/zero padding specifier or
                                # alternative padding character prefixed by a single quote
            [\-]?               # an optional alignment specifier
                                # ("-" for left-justified; right-justified otherwise)
            \d*                 # an optional width specifier
            (?:\.(?:.?\d+)?)?   # an optional precision specifier
                                # (a dot "." followed by an optional number, with an optional
                                # padding character in between the dot and the number)
            [bcdeEfFgGosuxX]    # the type specifier
        )?
    ) # 3: The standard sprintf format
@mx
REGEX;

        if (preg_match_all($pattern, $inputString, $matches, PREG_SET_ORDER)) {
            // self::d($matches);

            foreach ($matches as $match) {
                $percentSigns = $match[1];
                $namedParam = $match[2];
                $strFmt = $match[3];

                if (!$strFmt) {
                    $strFmt = 's';
                }

                $replacementSets[$match[0]] = "{$percentSigns}{$strFmt}";

                $parameterMap[] = [
                    'name' => $namedParam,
                ];
            }
        }

        $searches = array_keys($replacementSets);
        $replacements = array_values($replacementSets);
        $parsedFormat = str_replace($searches, $replacements, $inputString);

        // self::d($searches);
        // self::d($replacements);
        // self::d($parsedFormat);
        // self::d($parameterMap);

        return new SprintfParsedExpression($parsedFormat, $parameterMap);
    }

    /**
     * Determines if the given array is not an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array
     *
     * @return bool
     */
    public static function isNotAssociative(?array $array): bool
    {
        return !self::isAssociative($array);
    }

    /**
     * Functional API for FormatterPlaceholder. Instead of:
     *
     *   new FormatterPlaceholder('some %1 format')->compile('glorious');
     *
     * you can call:
     *
     *   Lang::phprintf('some %1 format', ['glorious']);
     *
     * Few characters less :)
     *
     * @param string|null $inputString
     * @param mixed ...$arguments parameters used to fill placeholders
     *
     * @return string
     */
    public static function phprintf(string $inputString = null, ...$arguments): string
    {
        $fmt = new FormatterPlaceholder($inputString);
        return $fmt->render([$inputString], $arguments);
        // return call_user_func_array([$fmt, 'compile'], $arguments)->unfold();
    }

    /**
     * Returns an associative array with the names of all the constants and their values.
     *
     * @param boolean $categorize Causing this function to return a multi-dimensional
     *                                 array with categories in the keys of the first dimension
     *                                 and constants and their values in the second dimension.
     *
     * @return array Returns the names and values of all the constants currently defined.
     *               This includes those created by extensions as well as those created with
     *               the `define()` function.
     */
    public static function constants(?bool $categorize = false): array
    {
        return get_defined_constants($categorize);
    }

    /**
     * Is it CLI mode or Web?
     *
     * @return bool
     */
    public static function isCli(): bool
    {
        return in_array(PHP_SAPI, ['cli', 'phpdbg']) or defined('STDIN');
    }

    /**
     * Functional API for FormatterIndex. Instead of:
     *
     *   new FormatterIndex('some {} format')->compile('glorious');
     *
     * you can call:
     *
     *   Lang::iprintfl('some {} format', 'glorious');
     *
     * Few characters less :)
     *
     * @param string|null $inputString
     * @return string
     */
    public static function iprintfl(string $inputString = null): string
    {
        $params = func_get_args();
        // $params = $arguments;
        array_shift($params);
        $fmt = new FormatterIndexed($inputString);
        return call_user_func_array([$fmt, 'compile'], $params)->unfold();
    }

    /**
     * Sprintf w/ named input, python style: %(name)x.
     *
     * NOTE: this is pretty crude, and doesn't support the full range of options
     * for printf.
     * @param string|null $inputString
     * @param null $arguments
     * @return string|null
     */
    public static function sprintf(string $inputString = null, $arguments = null): ?string
    {
        if (!is_array($arguments)) {
            $arguments = [$arguments];
        }

        // self::d(preg_match('/(\(+' . $arguments . '+\))(\w)/', $inputString));

        // Translate %(name)x -> positional arguments
        $x = 0;

        foreach ($arguments as $key => $value) {
            $inputString = preg_replace(
                '/(\(' . $key . '\))(\w)/',
                ++$x . '$$2',
                $inputString
            );
        }

        return vsprintf($inputString, array_values($arguments));
    }

    /**
     * Dumps the passed variables/arguments to the standard output then end the
     * script.
     *
     * @param mixed ...$variables Variables to dump.
     *
     * @return void
     * @throws ErrorException
     */
    public static function dd(...$variables): void
    {
        foreach ($variables as $var) {
            (new Dumper())->dump($var);
        }

        die(1);
    }

    /**
     * Creates a Unique ID if $callable is not string.
     *
     * @param callable|string $callable Function/Callable.
     *
     * @return string|bool|null
     */
    public static function uniqueID($callable = null)
    {
        if (self::isString($callable)) {
            return $callable;
        }

        if (self::isObject($callable)) {
            // Closures are currently implemented as objects
            $callable = [$callable, EMPTY_STRING];
        } elseif (!self::isArray($callable)) {
            $callable = [$callable];
        }

        $callable = array_values($callable);

        if (self::isObject($callable[0])) {
            return spl_object_hash($callable[0]) . $callable[1];
        } elseif (count($callable) > 1 || self::isString($callable[0])) {
            // Call as static
            return $callable[0] . '::' . $callable[1];
        }

        // Unexpected result
        return null;
    }

    /**
     * Generate a unique identifier.
     *
     * @param string|null $prefix Prefix.
     *
     * @return string
     */
    public static function id(?string $prefix = EMPTY_STRING): string
    {
        return uniqid($prefix, true);
    }

    /**
     * Return the value passed as the first argument.
     *
     * @param mixed $value Value.
     *
     * @return mixed
     * @noinspection PhpParameterByRefIsNotUsedAsReferenceInspection
     */
    public static function with(&$value = null)
    {
        return $value;
    }

    /**
     * Gets the current `UTC` timestamp.
     *
     * @return DateTimeInterface
     * @throws Exception
     */
    public static function utcNow(): DateTimeInterface
    {
        return self::now('UTC');
    }

    /**
     * Gets the current timestamp.
     *
     * @param string|null $timezone Timezone string.
     *
     * @return DateTimeInterface
     * @throws Exception
     */
    public static function now(?string $timezone = null): DateTimeInterface
    {
        $tz = ($timezone) ?: date_default_timezone_get();
        $tz = new DateTimeZone($tz);

        return new \Datetime('now', $tz);
    }

    /**
     * Takes a value and checks if it's a Closure or not, if it's a Closure it
     * will return the result of the closure, if not, it will simply return the
     * value.
     *
     * @param mixed $value The value to check
     *
     * @return mixed
     */
    public static function checkValue($value = null)
    {
        return self::value($value);
    }

    /**
     * Takes a value and checks if it's a Closure or not, if it's a Closure it
     * will return the result of the closure, if not, it will simply return the
     * value.
     *
     * @param mixed $value The value to check
     *
     * @return mixed
     */
    public static function value($value = null)
    {
        return ($value instanceof Closure) ? $value() : $value;
    }

    /**
     * Hashes a given `password` using the provided `algorithm`.
     *
     * @param string|null $plainText Password to hash.
     * @param string|null $algorithm Hashing algorithm.
     * @param array|null $options Hashing options.
     *
     * @return string|boolean Returns the hashed password, or `FALSE` on failure.
     *
     * @since  0.1.0
     */
    public static function hashPassword(
        ?string $plainText = null,
        ?string $algorithm = PASSWORD_DEFAULT,
        ?array  $options = []
    ): string
    {
        return password_hash($plainText, $algorithm, $options);
    }

    /**
     * Verifies that a `password` matches a `hash`.
     *
     * @param string|null $password The user's password.
     * @param string|null $hash A hash created by `Lang::hashPassword()`.
     *
     * @return bool Returns `TRUE` if the password and hash match, or `FALSE` otherwise.
     *
     * @since  0.1.0
     */
    public static function verifyPassword(
        ?string $password = null,
        ?string $hash = null
    ): bool
    {
        return password_verify($password, $hash);
    }

    /**
     * Invokes the given `$closure` `$number` of times.
     *
     * @param integer $number Number of times to execute/invoke.
     * @param Closure|null $closure Callback/Function to execute/invoke.
     *
     * @return void
     */
    public static function times(int $number = 0, Closure $closure = null): void
    {
        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach (range(1, $number) as $index) {
            $closure();
        }
    }

    /**
     * Access an array index, retrieving the value stored there if it exists or
     * a default if it does not.
     *
     * This function allows you to concisely access an index which may or may
     * not exist without raising a warning.
     *
     * @param array $var Array value to access
     * @param mixed $default Default value to return if the key is not
     *
     * @return array|mixed
     * @noinspection PhpParameterByRefIsNotUsedAsReferenceInspection
     */
    public static function get(array &$var, $default = null): array
    {
        if (isset($var)) {
            return $var;
        }

        return $default;
    }

    /**
     * Gets an item from an array or object using "dot" notation.
     *
     * @param mixed $target Array or Object to get item from.
     * @param string|array $key Array/Object key.
     * @param mixed $default Default value if the item does't exist.
     *
     * @return mixed
     */
    public static function arrayOrObjectGet(
        $target = null,
        $key = null,
        $default = null
    )
    {
        if (self::isNull($key)) {
            return $target;
        }

        $key = self::isArray($key) ? $key : Str::explode(SINGLE_DOT, $key);

        while (self::isNotNull($segment = Arr::shift($key))) {
            if (ASTERISK === $segment) {
                if ($target instanceof Collection) {
                    $target = $target->all();
                } elseif (!self::isArray($target)) {
                    return self::value($default);
                }

                $result = Arr::pluck($target, $key);

                return Arr::in(ASTERISK, $key) ? Arr::collapse($result) : $result;
            }

            if (Arr::isAccessible($target) && Arr::exists($target, $segment)) {
                $target = $target[$segment];
            } elseif (
                self::isObject($target)
                && self::isSet($target, $segment)
            ) {
                $target = $target->{$segment};
            } else {
                return self::value($default);
            }
        }

        return $target;
    }

    public static function isset($object = null, $element = null): bool
    {
        if (self::isArray($object)) {
            return isset($object[$element]);
        } elseif (self::isObject($object)) {
            return isset($object->$element) || isset($object->{$element});
        }

        return false;
    }

    /**
     * Sets an item on an array or object using dot notation.
     *
     * @param mixed $target Array or Object to get item from.
     * @param string|array $key Array/Object key.
     * @param mixed $value Value to set.
     * @param bool $overwrite Overwrite existing item.
     *
     * @return mixed
     * @noinspection PhpMissingReturnTypeInspection
     */
    public static function arrayOrObjectSet(
        &$target = null,
        $key = null,
        $value = null,
        bool $overwrite = true
    )
    {
        $segments = self::isArray($key) ? $key : Str::explode(SINGLE_DOT, $key);

        if (ASTERISK === ($segment = Arr::shift($segments))) {
            if (!Arr::isAccessible($target)) {
                $target = [];
            }

            if ($segments) {
                foreach ($target as &$inner) {
                    self::arrayOrObjectSet(
                        $inner,
                        $segments,
                        $value,
                        $overwrite
                    );
                }
            } elseif ($overwrite) {
                foreach ($target as &$inner) {
                    $inner = $value;
                }
            }
        } elseif (Arr::isAccessible($target)) {
            if ($segments) {
                if (!Arr::exists($target, $segment)) {
                    $target[$segment] = [];
                }

                self::arrayOrObjectSet(
                    $target[$segment],
                    $segments,
                    $value,
                    $overwrite
                );
            } elseif ($overwrite || !Arr::exists($target, $segment)) {
                $target[$segment] = $value;
            }
        } elseif (self::isObject($target)) {
            if ($segments) {
                if (!self::isSet($target, $segment)) {
                    $target->{$segment} = [];
                }

                self::arrayOrObjectSet(
                    $target->{$segment},
                    $segments,
                    $value,
                    $overwrite
                );
            } elseif ($overwrite || !self::isSet($target, $segment)) {
                $target->{$segment} = $value;
            }
        } else {
            $target = [];

            if ($segments) {
                self::arrayOrObjectSet(
                    $target[$segment],
                    $segments,
                    $value,
                    $overwrite
                );
            } elseif ($overwrite) {
                $target[$segment] = $value;
            }
        }

        return $target;
    }

    /**
     * Return `TRUE` if the number is within the `min` and `max`.
     *
     * @param integer|float $number Number to check.
     * @param integer|float $min Maximum value.
     * @param integer|float $max Minimum value.
     *
     * @return bool
     */
    public static function isIn($number = null, $min = null, $max = null): bool
    {
        return ($number >= $min && $number <= $max);
    }

    /**
     * Is the current value negative; less than zero.
     *
     * @param integer|null $number Number to check.
     *
     * @return bool
     */
    public static function isNegative(int $number = null): bool
    {
        return ($number < 0);
    }

    /**
     * Is the current value odd?
     *
     * @param integer|null $number Number to check.
     *
     * @return bool
     */
    public static function isOdd(int $number = null): bool
    {
        return !self::isEven($number);
    }

    /**
     * Is the current number even?
     *
     * @param integer|null $number Number to check.
     *
     * @return bool
     */
    public static function isEven(int $number = null): bool
    {
        return ($number % 2 === 0);
    }

    /**
     * Is the current value positive; greater than or equal to zero.
     *
     * @param integer|null $number Number to check.
     * @param bool $zero Is the number is greater than zero.
     *
     * @return bool
     */
    public static function isPositive(
        int  $number = null,
        bool $zero = true
    ): bool
    {
        return ($zero ? ($number >= 0) : ($number > 0));
    }

    /**
     * Limits the number between two bounds.
     *
     * @param integer|null $number Number to check.
     * @param integer|null $min Maximum value.
     * @param integer|null $max Minimum value.
     *
     * @return int
     */
    public static function limit(
        int $number = null,
        int $min = null,
        int $max = null
    ): integer
    {
        return self::max(self::min($number, $min), $max);
    }

    /**
     * Decrease the number to the maximum if above threshold.
     *
     * @param integer|null $number Number to check.
     * @param integer|null $max Maximum value.
     *
     * @return int
     */
    public static function max(int $number = null, int $max = null): int
    {
        if ($number > $max) {
            $number = $max;
        }
        return $number;
    }

    /**
     * Increase the number to the minimum if below threshold.
     *
     * @param integer|null $number Number to check.
     * @param integer|null $min Maximum value.
     *
     * @return int
     */
    public static function min(int $number = null, int $min = null): int
    {
        if ($number < $min) {
            $number = $min;
        }

        return $number;
    }

    /**
     * Return true if the number is outside the min and max.
     *
     * @param integer|null $number Number to check.
     * @param integer|null $min Maximum value.
     * @param integer|null $max Minimum value.
     *
     * @return bool
     */
    public static function out(
        int $number = null,
        int $min = null,
        int $max = null
    ): bool
    {
        return ($number < $min || $number > $max);
    }

    /**
     * Get the last element of an array. Useful for method chaining.
     *
     * @param array $array Array.
     *
     * @return mixed
     */
    public static function last(array &$array)
    {
        return self::end($array);
    }

    /**
     * Advances array's internal pointer to the last element, and returns its
     * value.
     *
     * @param array $array The array. This array is passed by reference because
     *                     it is modified by the function. This means you must
     *                     pass it a real variable and not a function returning
     *                     an array because only actual variables may be passed
     *                     by reference.
     *
     * @return mixed
     */
    public static function end(array &$array)
    {
        return end($array);
    }

    /**
     * Get the last element of an array. Useful for method chaining.
     *
     * @param array $array Array.
     *
     * @return mixed
     */
    public static function head(array &$array)
    {
        return self::first($array);
    }

    /**
     * Get the first element of an array. Useful for method chaining.
     *
     * @param array $array Array.
     *
     * @return mixed
     */
    public static function first(array &$array)
    {
        return self::start($array);
    }

    /**
     * Get the first element of an array. Useful for method chaining.
     *
     * @param array $array Array.
     *
     * @return mixed
     */
    public static function start(array &$array)
    {
        return reset($array);
    }

    /**
     * Get relative percent
     *
     * @param float|int $normal
     * @param float|int $current
     *
     * @return string
     */
    public static function relativePercent(
        $normal = null,
        $current = null
    ): string
    {
        $normal = (float)$normal;
        $current = (float)$current;

        if (!$normal || $normal === $current) {
            return '100';
        }

        $normal = abs($normal);
        $percent = round($current / $normal * 100);

        return number_format($percent, 0, SINGLE_DOT, ' ');
    }

    /**
     * Nice formatting for computer sizes (Bytes).
     *
     * @param integer $bytes The number in bytes to format
     * @param integer $decimals The number of decimal points to include
     *
     * @return string
     */
    public static function sizeFormat(int $bytes = 0, int $decimals = 0): string
    {
        $bytes = floatval($bytes);

        if ($bytes < 1024) {
            return $bytes . ' B';
        } elseif ($bytes < pow(1024, 2)) {
            return number_format($bytes / 1024, $decimals, SINGLE_DOT, EMPTY_STRING) . ' KiB';
        } elseif ($bytes < pow(1024, 3)) {
            return number_format(
                    $bytes / pow(1024, 2),
                    $decimals,
                    SINGLE_DOT,
                    EMPTY_STRING
                ) . ' MiB';
        } elseif ($bytes < pow(1024, 4)) {
            return number_format(
                    $bytes / pow(1024, 3),
                    $decimals,
                    SINGLE_DOT,
                    EMPTY_STRING
                ) . ' GiB';
        } elseif ($bytes < pow(1024, 5)) {
            return number_format(
                    $bytes / pow(1024, 4),
                    $decimals,
                    SINGLE_DOT,
                    EMPTY_STRING
                ) . ' TiB';
        } elseif ($bytes < pow(1024, 6)) {
            return number_format(
                    $bytes / pow(1024, 5),
                    $decimals,
                    SINGLE_DOT,
                    EMPTY_STRING
                ) . ' PiB';
        } else {
            return number_format(
                    $bytes / pow(1024, 5),
                    $decimals,
                    SINGLE_DOT,
                    EMPTY_STRING
                ) . ' PiB';
        }
    }

    /**
     * Sets cookie.
     *
     * @param string|null $name Cookie name.
     * @param mixed $value Cookie value.
     * @param bool $ignore Ignore already defined cookie and override
     *                              its value.
     * @param integer $time Cookie time.
     * @param string|null $path Path.
     * @param string|null $domain Domain.
     * @param bool $secure Register as a secure HTTP Cookie.
     * @param bool $httponly Register as a HTTP-ONLY Cookie.
     *
     * @return void
     */
    public static function setCookie(
        string $name = null,
               $value = null,
        bool   $ignore = false,
        int    $time = 3600,
        string $path = '/',
        string $domain = EMPTY_STRING,
        bool   $secure = false,
        bool   $httponly = false
    ): void
    {
        $time = ($time !== 3600) ? $time : time() + $time;

        if (!isset($_COOKIE[$name]) and $ignore) {
            setrawcookie(
                $name,
                $value,
                $time,
                $path,
                $domain,
                $secure,
                $httponly
            );
        } elseif (!isset($_COOKIE[$name]) and !$ignore) {
            setrawcookie(
                $name,
                $value,
                $time,
                $path,
                $domain,
                $secure,
                $httponly
            );
        }

        if (isset($_COOKIE[$name]) and $ignore) {
            setrawcookie(
                $name,
                $value,
                $time,
                $path,
                $domain,
                $secure,
                $httponly
            );
        } elseif (isset($_COOKIE[$name]) and !$ignore) {
            return;
        }
    }

    /**
     * Gets cookie.
     *
     * @param string|null $name cookie name
     *
     * @return mixed
     */
    public static function getCookie(string $name = null)
    {
        if (isset($_COOKIE[$name])) {
            return $_COOKIE[$name];
        }

        return null;
    }

    /**
     * Deletes cookie.
     *
     * @param string|null $name Cookie name.
     *
     * @return void
     */
    public static function deleteCookie(string $name = null): void
    {
        if (isset($_COOKIE[$name])) {
            unset($_COOKIE[$name]);
        }
    }

    public static function unset(&$object = null, $key = null): void
    {
        if (self::isSet($object, $key)) {
            unset($object[$key]);
        }
    }

    /**
     * Returns a formatted memory size string from the given number.
     *
     * @param integer $total_memory Total memory size
     *
     * @return string
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function memoryFormat(int $total_memory = 0): ?string
    {
        $format = null;

        if ($total_memory < 1024) {
            $format = $total_memory . ' B';
        } elseif ($total_memory < 1048576) {
            $format = round($total_memory / 1024, 2) . ' KB';
        } else {
            $format = round($total_memory / 1048576, 2) . ' MB';
        }

        return $format;
    }

    /**
     * Gets the count of the given Array or Object.
     *
     * @param array|object $input Array or Object to count.
     *
     * @return integer
     */
    public static function sizeOf($input = null): int
    {
        return self::countOf($input);
    }

    /**
     * Gets the count of the given Array or Object.
     *
     * @param null $arrayOrCountable Array or Object to count.
     * @param integer $mode Count mode.
     *
     * @return integer
     */
    public static function countOf(
        $arrayOrCountable = null,
        int $mode = COUNT_RECURSIVE
    ): integer
    {
        if (self::isEmpty($arrayOrCountable)) {
            return 0;
        } elseif (self::isObject($arrayOrCountable) and self::isCountable($arrayOrCountable)) {
            return $arrayOrCountable->count();
        } elseif (self::isArray($arrayOrCountable)) {
            return count($arrayOrCountable, $mode);
        } elseif (self::isString($arrayOrCountable)) {
            $str = (string)$arrayOrCountable;
            return Str::length($str);
        }

        return count($arrayOrCountable, $mode);
    }

    /**
     * Determine whether the given value is empty.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isEmpty($value = null): bool
    {
        if (self::isString($value)) {
            return Str::length($value) === 0 or $value === EMPTY_STRING;
        } elseif (self::isArray($value)) {
            return Arr::size($value) === 0;
        } else {
            return empty($value);
        }
    }

    /**
     * Determine whether the given value is countable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isCountable($value = null): bool
    {
        return self::isArray($value) or ($value instanceof Countable);
    }

    /**
     * Gets the count of arrays in the given Array or Object.
     *
     * @param array|object $input Array or Object to count.
     *
     * @return integer
     */
    public static function countArrays($input = null): int
    {
        $retVal = 0;

        if (self::isObject($input)) {
            $input = self::toArray($input);
        }

        // D($input);

        // if (self::isArray($input)) {
        //     if (self::isAssociative($input)) {
        //         foreach ($input as $key => &$value) {
        //             if (self::isArray($value)) {
        //                 ++$retVal;
        //             }
        //         }
        //     } else {
        //         foreach ($input as &$item) {
        //             if (self::isArray($item)) {
        //                 ++$retVal;
        //             }
        //         }
        //     }
        // }

        array_walk($input, function ($item) use (&$retVal) {
            if (self::isArray($item)) {
                foreach ($item as $key => $value) {
                    if (self::isArray($value)) {
                        $retVal += self::countArrays($value);
                    }
                }
            }

            ++$retVal;
        });

        return $retVal;
    }

    /**
     * Recursively converts the given value into an array.
     *
     * @param mixed $value Value to convert.
     *
     * @return array
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toArray($value = null): array
    {
        // if (self::isInstanceOf($value, Arrayable::class)) {
        //     $result = $value->toArray();
        // }

        if (self::isArrayable($value)) {
            $result = $value->toArray();
        } elseif (self::isObject($value)) {
            $data = @get_object_vars($value);
            $result = array_map([__CLASS__, 'toArray'], $data);
        } elseif (self::isStdClass($value)) {
            $result = (array)$value;
        } elseif (self::isArray($value)) {
            $result = array_map([__CLASS__, 'toArray'], $value);
        } else {
            $result = [$value];
        }

        return $result;
    }

    /**
     * Determine whether the given value is Arrayable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isArrayable($value = null): bool
    {
        return ($value instanceof Arrayable);
    }

    /**
     * Determine whether the given value is \stdClass.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isStdClass($value = null): bool
    {
        return ($value instanceof stdClass);
    }

    /**
     * Serialize data, if needed.
     *
     * @param mixed $data Data that might need to be serialized
     *
     * @return mixed
     */
    public static function maybeSerialize($data = null)
    {
        if (self::isArray($data) || self::isObject($data)) {
            return serialize($data);
        }

        return $data;
    }

    /**
     * Unserialize value only if it is serialized.
     *
     * @param string $data A variable that may or may not be serialized
     *
     * @return mixed
     */
    public static function maybeUnserialize($data = null)
    {
        // If it isn't a string, it isn't serialized
        if (!self::isString($data)) {
            return $data;
        }

        $data = trim($data);

        // Is it the serialized NULL value?
        if ($data === 'N;') {
            return null;
        }

        $length = strlen($data);

        // Check some basic requirements of all serialized strings
        if (
            $length < 4 || $data[1] !== ':' || ($data[$length - 1] !== ';'
                && $data[$length - 1] !== '}')
        ) {
            return $data;
        }

        // $data is the serialized false value
        if ($data === 'b:0;') {
            return false;
        }

        // Don't attempt to unserialize data that isn't serialized
        $uns = @unserialize($data);

        // Data failed to unserialize?
        if ($uns === false) {
            $uns = @unserialize(self::fixBrokenSerialization($data));

            if ($uns === false) {
                return $data;
            } else {
                return $uns;
            }
        } else {
            return $uns;
        }
    }

    /**
     * Unserializes partially-corrupted arrays that occur sometimes. Addresses
     * specifically the `unserialize(): Error at offset xxx of yyy bytes` error.
     *
     * NOTE: This error can *frequently* occur with mismatched character sets
     * and higher-than-ASCII characters.
     *
     * @param string|null $brokenSerializedData Serialized data.
     *
     * @return string
     */
    public static function fixBrokenSerialization(
        string $brokenSerializedData = null
    ): string
    {
        return preg_replace_callback(
            '!s:(\d+):"(.*?)";!',
            function ($matches) {
                $snip = $matches[2];
                return 's:' . strlen($snip) . ':"' . $snip . '";';
            },
            $brokenSerializedData
        );
    }

    /**
     * Get an item from an object using "dot" notation.
     *
     * @param object|null $object $object
     * @param string|null $key
     * @param mixed $default
     *
     * @return mixed
     * @noinspection PhpMissingReturnTypeInspection
     */
    public static function objectGet(
        object $object = null,
        string $key = null,
               $default = null
    )
    {
        if (self::isNull($key) || EMPTY_STRING === Str::trim($key)) {
            return $object;
        }

        foreach (Str::explode(SINGLE_DOT, $key) as $segment) {
            if (
                !self::isObject($object)
                || !self::isSet($object, $segment)
            ) {
                return self::value($default);
            }

            $object = $object->{$segment};
        }

        return $object;
    }

    public static function isNotSet($object = null, $element = null): bool
    {
        return !self::isSet($object, $element);
    }

    /**
     * Determine whether the given value is \Closure.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isClosure($value = null): bool
    {
        return ($value instanceof Closure);
    }

    /**
     * Determine whether the given value is traversable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isTraversable($value = null): bool
    {
        return is_array($value) or ($value instanceof Traversable);
    }

    /**
     * Determine whether the given `value` isn't an array.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNotArray($value = null): bool
    {
        return !self::isArray($value);
    }

    /**
     * Determine whether the given value is ArrayAccess.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isArrayAccess($value = null): bool
    {
        return ($value instanceof ArrayAccess);
    }

    /**
     * Determines if the given array is not an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array
     *
     * @return bool
     */
    public static function isNotAssoc(?array $array): bool
    {
        return !self::isAssoc($array);
    }

    /**
     * Determine whether the given input is IntArray.
     *
     * @param mixed $input Input to check.
     *
     * @return boolean
     */
    public static function isIntArray($input = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($input)) {
            if (self::isNotAssociativeArray((array)$input)) {
                foreach ($input as $value) {
                    if (self::isInteger($value)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($input instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determines if the given array is not an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array
     *
     * @return bool
     */
    public static function isNotAssociativeArray(?array $array): bool
    {
        return !self::isAssociativeArray($array);
    }

    /**
     * Determines if the given array is an associative array.
     *
     * NOTE: An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param array|null $array $array Array to check.
     *
     * @return boolean
     */
    public static function isAssociativeArray(?array $array = null): bool
    {
        return self::isArray($array) and self::isAssoc($array);
    }

    /**
     * Determine whether the given value is an integer.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isInteger($value = null): bool
    {
        if (!is_scalar($value) || is_bool($value)) {
            return false;
        }

        if (is_float($value + 0) && ($value + 0) > PHP_INT_MAX) {
            return false;
        }

        // return \is_float($value) ? false : (bool) \preg_match('~^((:?+|-)?[0-9]+)$~', $value);
        return is_float($value) ? false : (bool)preg_match('~^((:?+|-)?[0-9]+)$~', (string)$value);

        // $rv = null;

        // if (self::isString($value)) {
        //     $matches = [];
        //     $pattern = '/^-?(?:\d+)$/';

        //     if (\preg_match($pattern, $value, $matches)) {
        //         // self::d($matches);

        //         foreach ($matches as $match) {
        //             if (self::isNotNull($match)) {
        //                 $rv = self::toNumber($match);
        //                 break;
        //             }
        //         }
        //     }
        // }

        // return (\is_integer($rv) or \is_numeric($rv)) or \is_integer($value) or \is_numeric($value);
    }

    /**
     * Determine whether the given value is CharArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isCharArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isString($val)) {
                        if (Str::length($val) === 1) {
                            return true;
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is StringArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isStringArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isString($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is StringList.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isStringList($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isString($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is BoolArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isBoolArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isBool($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is a bool.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isBool($value = null): bool
    {
        return is_bool($value);
    }

    /**
     * Determine whether the given value is ObjectArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isObjectArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isObject($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is FloatArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isFloatArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isFloat($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is float.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isFloat($value = null): bool
    {
        // return \is_float($value);

        // return \is_float($value) ||
        //     \is_numeric($value) && ((float) $value != (int) $value);

        if (!is_scalar($value)) {
            return false;
        }

        if (self::isNumber($value)) {
            return is_float($value + 0);
        }

        return false;
    }

    /**
     * Determine whether the given value is a float or an integer.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNumber($value = null): bool
    {
        $rv = null;

        if (self::isString($value)) {
            $matches = [];
            $pattern = '/^-?(?:\d+|\d*\.\d+)$/';

            if (preg_match($pattern, $value, $matches)) {
                // self::d($matches);

                foreach ($matches as $match) {
                    if (self::isNotNull($match)) {
                        $rv = self::toNumber($match);
                        break;
                    }
                }
            }
        }

        return (is_integer($rv) or is_float($rv)) or is_integer($value) or is_float($value);
    }

    /**
     * Converts the given value into float.
     *
     * @param mixed $value Value to convert.
     *
     * @return int
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toNumber($value = null): int
    {
        if (Validator::isValidNumber($value)) {
            return (int)$value;
        } elseif (self::isString($value)) {
            return (int)$value;
        }

        return (int)$value;
    }

    /**
     * Determine whether the given value is DoubleArray.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isDoubleArray($value = null): bool
    {
        $mn = str_replace(__CLASS__ . '::', EMPTY_STRING, __METHOD__);
        $mn = str_replace('is', EMPTY_STRING, $mn);
        $cn = '\Exen\Support\Container';
        $cn .= '\\' . $mn;

        if (self::isArray($value)) {
            if (self::isNotAssociativeArray((array)$value)) {
                foreach ($value as $val) {
                    if (self::isDouble($val)) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } elseif ($value instanceof $cn) {
                return true;
            } else {
                return false;
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is double.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isDouble($value = null): bool
    {
        // return \is_double($value);

        // return \is_double($value) ||
        //     \is_numeric($value) && ((float) $value != (int) $value);

        if (!is_scalar($value)) {
            return false;
        }

        if (self::isNumber($value)) {
            return is_double($value + 0);
        }

        return false;
    }

    /**
     * Determine whether the given value is signed int.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isSignedInt($value = null): bool
    {
        return !ctype_digit((string)$value);
    }

    /**
     * Determine whether the given value is unsigned int.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isUnsignedInt($value = null): bool
    {
        return ctype_digit((string)$value);
    }

    /**
     * Determine whether the given value is numeric.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNumeric($value = null): bool
    {
        return is_numeric($value);
    }

    /**
     * Determine whether the given value is a true.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isTrue($value = null): bool
    {
        return ((self::isBool($value) or self::isBoolean($value)) and $value === true);
    }

    /**
     * Determine whether the given value is boolean.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isBoolean($value = null): bool
    {
        if (
            is_string($value)
            && ('false' === strtolower($value)
                || 'true' === strtolower($value))
        ) {
            $value = boolval($value);
        }

        return is_bool($value) || ($value === false || $value === true);
    }

    /**
     * Determine whether the given value is a false.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isFalse($value = null): bool
    {
        return ((self::isBool($value) or self::isBoolean($value)) and $value === false);
    }

    /**
     * Determine whether the given value is scalar.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isScalar($value = null): bool
    {
        return is_scalar($value);
    }

    /**
     * Determine whether the given value is long number.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isLong($value = null): bool
    {
        return is_long($value);
    }

    /**
     * Checks that two values are equals.
     *
     * @param mixed $firstValue
     * @param mixed $secondValue
     * @param string|null $message
     *
     * @return bool
     */
    public static function isEqual(
        $firstValue = null,
        $secondValue = null,
        string $message = null
    ): bool
    {
        if ($firstValue !== $secondValue) {
            throw new InvalidArgumentException($message);
        }

        return true;
    }

    /**
     * Determine whether the given value is tainted string.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isTainted($value = null): bool
    {
        return self::isString($value) and call_user_func('is_tainted', $value);
    }

    /**
     * Determine whether the given value is a string.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isBinary($value = null): bool
    {
        // \preg_match(
        //     '/[A-ZA-za-z0-9\!\@\#\$\%\^\&\*(){}\[\]\.\s\t]/m',
        //     (string) $value,
        //     $matches
        // );

        // return \count($matches) > 0 || \is_string($value);

        return self::isString($value) and preg_match('~[^\x20-\x7E\t\r\n]~', $value) > 0;
    }

    /**
     * Determine whether the given value is regex.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isRegex($value = null): bool
    {
        if (!self::isString($value)) {
            return false;
        }

        return (@preg_match($value, EMPTY_STRING) !== false);
    }

    /**
     * Determine whether the given string is a format string.
     *
     * @param mixed $string String to check.
     *
     * @return boolean
     */
    public static function isFormatString($string = null): bool
    {
        $pattern = <<<'REGEX'
@
    \( # Literal open paren
        ([a-zA-Z_][a-zA-Z0-9_\-,:]*?) # 2: The name of the param
    \) # Literal close paren
    (
        (?:
            # Standard sprintf format. reference http://php.net/sprintf
            [+]?                # an optional +/- sign specifier
            (?:[ 0]|'.)?       # an optional space/zero padding specifier or
                                # alternative padding character prefixed by a single quote
            [\-]?               # an optional alignment specifier
                                # ("-" for left-justified; right-justified otherwise)
            \d*                 # an optional width specifier
            (?:\.(?:.?\d+)?)?   # an optional precision specifier
                                # (a dot "." followed by an optional number, with an optional
                                # padding character in between the dot and the number)
            [bcdeEfFgGosuxX]    # the type specifier
        )?
    ) # 3: The standard sprintf format
    |
    (%+\w)
    |
    ({+%+%+\w+}|{+\d+}|{})
@mx
REGEX;
        // $pattern = '/(\%+\(+[A-Za-z0-9-_]+\)(\w))|(\(+[A-Za-z0-9-_]+\)(\w))|(\%+\w)/i';
        // $retVal = false;

        if (preg_match_all($pattern, $string, $matches, PREG_SET_ORDER)) {
            foreach ($matches as $match) {
                return self::isNotEmpty($match);
            }
        }

        return false;
    }

    /**
     * Determine whether the given value is not empty.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNotEmpty($value = null): bool
    {
        return !self::isEmpty($value);
    }

    /**
     * Determine whether the given string is valid YAML string.
     *
     * @param mixed $input String to check.
     *
     * @return boolean
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public static function isYaml($input = null): bool
    {
        $retVal = false;

        if (self::isString($input)) {
            try {
                $value = Yaml::parse($input);
                $retVal = true;
            } catch (ParseException $ex) {
                try {
                    // if (\function_exists('yaml_parse')) {
                    //     $value = \yaml_parse($input);
                    // }
                    $value = call_user_func('yaml_parse', $input);
                    $retVal = true;
                } catch (Exception $ex) {
                    $retVal = false;
                }
            } catch (Exception|TypeError $ex) {
                $retVal = false;
            }
        }

        return $retVal;
    }

    /**
     * Determine whether the given value is iterable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isIterable($value = null): bool
    {
        return is_iterable($value);
    }

    /**
     * Determine whether the given value is a resource.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isResource($value = null): bool
    {
        return is_resource($value);
    }

    /**
     * Determine whether the given value is callable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isCallable($value = null): bool
    {
        if (self::isString($value)) {
            if (self::isSet($GLOBALS, $value)) {
                $value = $GLOBALS[$value];
            }
        }

        return is_callable($value);
    }

    /**
     * Determine whether the given value is callable or an instance of \Closure.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     * @since  0.1.1
     */
    public static function isCallableOrClosure($value = null): bool
    {
        if (self::isString($value)) {
            if (self::isSet($GLOBALS, $value)) {
                $value = $GLOBALS[$value];
            }
        }

        return (is_callable($value) or $value instanceof Closure);
    }

    /**
     * Determine whether the given value is an instance of \Closure.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isFunction($value = null): bool
    {
        return is_callable($value) or ($value instanceof Closure);
    }

    /**
     * Determine whether the given value is NaN.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isNaN($value = null): bool
    {
        return is_nan($value);
    }

    /**
     * Determine whether the given value is finite.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isFinite($value = null): bool
    {
        return is_finite($value);
    }

    /**
     * Determine whether the given value is infinite.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isInfinite($value = null): bool
    {
        return is_infinite($value);
    }

    /**
     * Checks if the object is of this class or has this class as one of its
     * parents.
     *
     * @param mixed $input Object or class to check.
     * @param mixed $objectOrClass Object or class name.
     * @param string|null $message Error message to throw.
     *
     * @return boolean
     */
    public static function isInstanceOf(
        $input = null,
        $objectOrClass = null,
        ?string $message = 'Object or class "{object}" is not an instance of "{class}"'
    ): bool
    {
        // $object = null;
        $class = null;

        // if (self::isObject($input)) {
        //     $object = \get_class($input);
        // } else if (self::isString($input) or (\class_exists($input) or self::isInterface($input))) {
        //     $object = $input;
        // }

        if (self::isObject($objectOrClass)) {
            $class = get_class($objectOrClass);
        } elseif (
            self::isString($objectOrClass) or (class_exists($objectOrClass) or self::isInterface($objectOrClass))
        ) {
            $class = $objectOrClass;
        }

        // $interfaces = \array_values(\class_implements($object));

        // if (\in_array($class, $interfaces)) {
        //     D(($object instanceof $class));
        // }

        // $message = 'Object or class "{object}" is not an instance of "{class}"';

        if (false === ($input instanceof $class)) {
            if (self::isNotNull($message)) {
                throw new InvalidArgumentException(
                    self::printf(
                        $message,
                        [
                            'object' => $input,
                            'class' => $objectOrClass
                        ]
                    )
                );
            }

            return false;
        }

        return true;
    }

    /**
     * Checks if the object is of this class or has this class as one of its
     * parents.
     *
     * @param mixed $input Object or class to check.
     * @param mixed $objectOrClass Object or class name.
     * @param bool $allowString Allow class names as string.
     * @param bool $native Performs native PHP is_a function.
     * @param string|null $message Error message to throw.
     *
     * @return boolean
     */
    public static function isA(
        $input = null,
        $objectOrClass = null,
        ?bool $allowString = true,
        ?bool $native = true,
        ?string $message = '"{object}" is not of "{class}"'
    ): bool
    {
        $object = null;
        $class = null;

        if (self::isObject($input)) {
            $object = get_class($input);
        } elseif (self::isString($input) or class_exists($input)) {
            $object = $input;
        }

        if (self::isObject($objectOrClass)) {
            $class = get_class($objectOrClass);
        } elseif (
            self::isString($objectOrClass) or (class_exists($objectOrClass) or self::isInterface($objectOrClass))
        ) {
            $class = $objectOrClass;
        }

        if (!is_a($object, $class, $allowString)) {
            if ($native) {
                return false;
            } else {
                throw new InvalidArgumentException(
                    self::printf(
                        $message,
                        ['object' => $object, 'class' => $class]
                    )
                );
            }
        }

        return true;
    }

    /**
     * Checks if the object has this class as one of its parents or implements
     * it.
     *
     * @param mixed $object Object to check.
     * @param string|null $class_name Class name.
     * @param bool $allow_string Allow class names as string.
     * @param string $message Error message to throw.
     *
     * @return boolean
     */
    public static function isSubclassOf(
        $object = null,
        string $class_name = null,
        bool $allow_string = true,
        string $message = '%(object)s is not a subclass of %(class_name)s'
    ): bool
    {
        if (!is_subclass_of($object, $class_name, $allow_string)) {
            throw new InvalidArgumentException(
                self::printf(
                    $message,
                    ['object' => $object, 'class_name' => $class_name]
                )
            );
        }

        return true;
    }

    /**
     * Determine whether the given value is array like.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isArrayLike($value = null): bool
    {
        return ($value instanceof ArrayObject) or ($value instanceof ArrObject);
    }

    /**
     * Determine whether the given value is a Collection.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isCollection($value = null): bool
    {
        return ($value instanceof Collection) or ($value instanceof CollectionInterface);
    }

    /**
     * Determine whether the given `$path` is file.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isFile(?string $path = null): bool
    {
        return is_file($path);
    }

    /**
     * Determine whether the given `$path` is writable.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isWritable(?string $path = null): bool
    {
        return is_writable($path);
    }

    /**
     * Determine whether the given `$path` is readable.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isReadable(?string $path = null): bool
    {
        return is_readable($path);
    }

    /**
     * Determine whether the given `$path` is directory.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isDirectory(?string $path = null): bool
    {
        return is_dir($path);
    }

    /**
     * Determine whether the given `$path` is directory.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isDir(?string $path = null): bool
    {
        return is_dir($path);
    }

    /**
     * Determine whether the given value is \Error.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isError($value = null): bool
    {
        return ($value instanceof Error);
    }

    /**
     * Determine whether the given value is \Throwable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isThrowable($value = null): bool
    {
        return ($value instanceof Throwable);
    }

    /**
     * Determine whether the given value is \Exception.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isException($value = null): bool
    {
        return ($value instanceof Exception);
    }

    /**
     * Determine whether the given value is Jsonable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isJsonable($value = null): bool
    {
        return ($value instanceof Jsonable);
    }

    /**
     * Determine whether the given value is Yamlable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isYamlable($value = null): bool
    {
        return ($value instanceof Yamlable);
    }

    /**
     * Determine whether the given value is Xmlable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isXmlable($value = null): bool
    {
        return ($value instanceof Xmlable);
    }

    /**
     * Determine whether the given value is JsonSerializable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isJsonSerializable($value = null): bool
    {
        return ($value instanceof JsonSerializable);
    }

    /**
     * Determine whether the given value is YamlSerializable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isYamlSerializable($value = null): bool
    {
        return ($value instanceof YamlSerializable);
    }

    /**
     * Determine whether the given value is XmlSerializable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isXmlSerializable($value = null): bool
    {
        return ($value instanceof XmlSerializable);
    }

    /**
     * Determine whether the given value is Serializable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isSerializable($value = null): bool
    {
        return ($value instanceof Serializable);
    }

    /**
     * Determine whether the given value is Hashable.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isHashable($value = null): bool
    {
        return ($value instanceof Hashable);
    }

    /**
     * Is it an HTTP GET Request?
     *
     * @return boolean
     */
    public static function isGetRequest(): bool
    {
        return 'GET' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it an HTTP POST Request?
     *
     * @return boolean
     */
    public static function isPostRequest(): bool
    {
        return 'POST' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it an HTTP PUT Request?
     *
     * @return boolean
     */
    public static function isPutRequest(): bool
    {
        return 'PUT' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it an HTTP DELETE Request?
     *
     * @return boolean
     */
    public static function isDeleteRequest(): bool
    {
        return 'DELETE' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it an HTTP HEAD Request?
     *
     * @return boolean
     */
    public static function isHeadRequest(): bool
    {
        return 'HEAD' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it an HTTP PATCH Request?
     *
     * @return boolean
     */
    public static function isPatchRequest(): bool
    {
        return 'PATCH' === $_SERVER['REQUEST_METHOD'];
    }

    /**
     * Is it a secure HTTP?
     *
     * @return boolean
     */
    public static function isHttps(): bool
    {
        return isset($_SERVER['HTTPS']) &&
            !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off';
    }

    /**
     * Checks that an object is of the desired
     * type, if not throws an exception.
     *
     * @param object|null $object $object
     * @param string|null $type
     * @param string|null $message
     *
     * @return bool
     */
    public static function objectIsOfType(
        object $object = null,
        string $type = null,
        string $message = null
    ): bool
    {
        if (!is_a($object, $type)) {
            throw new InvalidArgumentException($message);
        }

        return true;
    }

    /**
     * Checks that a Dictionary key or value
     * is of the desire type, if not throws
     * an exception.
     *
     * @param mixed $value
     * @param mixed $valueType
     * @param string|null $message
     *
     * @return bool
     */
    public static function valueIsOfType(
        $value = null,
        $valueType = null,
        string $message = null
    ): bool
    {
        $newValueType = is_object($value) ? get_class($value) : gettype($value);

        if ($newValueType != $valueType) {
            throw new InvalidArgumentException($message);
        }

        return true;
    }

    /**
     * Recursively converts the given value into an object.
     *
     * @param mixed $value Value to convert.
     *
     * @return object
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toObject($value = null): object
    {
        $result = null;
        $retVal = null;

        if (self::isArray($value)) {
            $result = new stdClass();

            foreach ($value as $key => &$val) {
                if (self::isArray($val)) {
                    if (self::isAssociativeArray($val)) {
                        $val = self::toObject($val);
                    } else {
                        $retVal = $val;
                    }
                } else {
                    $retVal = $val;
                }

                $result->$key = $retVal;
            }
        } else {
            // $result = $value;
            $result = self::toArray($value);
            $result = self::toObject($result);
        }

        return $result;
    }

    /**
     * Converts the given value into double.
     *
     * @param mixed $value Value to convert.
     *
     * @return double|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toDouble($value = null): ?float
    {
        return doubleval($value);
    }

    /**
     * Converts the given value into int.
     *
     * @param mixed $value Value to convert.
     *
     * @return integer|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toInt($value = null): int
    {
        return intval($value);
    }

    /**
     * Converts the given value into bool.
     *
     * @param mixed $value Value to convert.
     *
     * @return bool|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toBool($value = null): bool
    {
        return boolval($value);
    }

    /**
     * Converts the given value into binary.
     *
     * @param mixed $value Value to convert.
     *
     * @return string|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toBinary($value = null): ?string
    {
        return (string)$value;
    }

    /**
     * Converts the given float value into string.
     *
     * @param mixed $value Value to convert.
     *
     * @return string|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function floatToString($value = null): string
    {
        $string = (string)$value;

        if (preg_match('~\.(\d+)E([+-])?(\d+)~', $string, $matches)) {
            $decimals = $matches[2] === '-' ? strlen($matches[1]) + $matches[3] : 0;
            $string = number_format($value, $decimals, SINGLE_DOT, EMPTY_STRING);
        }

        return $string;
    }

    /**
     * Creates MD5 hash from given string.
     *
     * @param string $input String to hash.
     *
     * @return string
     */
    public static function md5($input = null): string
    {
        return @md5($input);
    }

    /**
     * @param null $objectOrClass
     * @param null $globals
     *
     * @return array
     * @throws ErrorException
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public static function getPublicMethods(
        $objectOrClass = null,
        &$globals = null
    ): array
    {
        $class = null;

        if (self::isObject($objectOrClass)) {
            $class = get_class($objectOrClass);
        } elseif (self::isString($objectOrClass) and class_exists($objectOrClass)) {
            $class = $objectOrClass;
        }

        $result = null;

        $refClass = null;

        try {
            $refClass = new ReflectionClass($class);
        } catch (ReflectionException $ex) {
            //
        }

        $methods = $refClass->getMethods(ReflectionMethod::IS_PUBLIC);

        $methods = array_filter(
            $methods,
            function ($method) {
                $name = $method->getName();

                if (!Str::startsWith($name, '__')) {
                    if (!in_array($name, self::$ignoredMethodNames)) {
                        if (!function_exists($name)) {
                            if ($name === 'dump') {
                                self::d($method);
                            }

                            return $name;
                        }
                    }
                }

                return $method;
            }
        );

        $methods = array_map(
            function ($method) {
                return $method->getName();
            },
            $methods
        );

        $result = [
            'class' => $class,
            'methods' => $methods,
        ];

        if (isset($globals) && !empty($globals)) {
            global $globals;
            $globals = &$GLOBALS;

            foreach ($methods as $method) {
                $globals[$method] = function (...$args) use ($method, $class) {
                    return self::callMethod($class, $method->getName(), $args);
                };
            }

            // return;
        }

        return $result;
    }

    /**
     * Dumps the passed variables/arguments to the standard output.
     *
     * @param mixed $variable Variable to dump.
     * @param mixed ...$variables Variables to dump.
     *
     * @return mixed
     * @throws ErrorException
     */
    public static function d($variable, ...$variables)
    {
        return self::dump($variable, ...$variables);
    }

    /**
     * Dumps the passed variables/arguments to the standard output.
     *
     * @param mixed $variable Variable to dump.
     * @param mixed ...$variables Variables to dump.
     *
     * @return mixed
     * @throws ErrorException
     */
    public static function dump($variable, ...$variables)
    {
        if ($variable && self::isEmpty($variables)) {
            (new Dumper())->dump($variable);
        } else if ($variable && self::isNotEmpty($variables)) {
            $vars = [$variable] + $variables;
            foreach ($vars as $var) {
                (new Dumper())->dump($var);
            }
        }

        foreach ($variables as $var) {
            (new Dumper())->dump($var);
        }

        if (1 < func_num_args()) {
            return func_get_args();
        }

        return $variable;
    }

    /**
     * Simpler version of call_user_func_array (for performances).
     *
     * @param mixed $objectOrClass The class name.
     * @param string|null $methodName The method name.
     * @param array|null $parameters The arguments.
     *
     * @return mixed
     */
    public static function callMethod(
        $objectOrClass = null,
        ?string $methodName = null,
        ?array $parameters = []
    )
    {
        $class = null;
        $retVal = null;

        if (self::isObject($objectOrClass)) {
            $class = get_class($objectOrClass);
        } elseif (self::isString($objectOrClass) and class_exists($objectOrClass)) {
            $class = $objectOrClass;
        }

        switch (count($parameters)) {
            case 0:
                $retVal = $class::$methodName();
                break;
            case 1:
                $retVal = $class::$methodName($parameters[0]);
                break;
            case 2:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1]
                );
                break;
            case 3:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2]
                );
                break;
            case 4:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3]
                );
                break;
            case 5:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4]
                );
                break;
            case 6:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5]
                );
                break;
            case 7:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5],
                    $parameters[6]
                );
                break;
            case 8:
                $retVal = $class::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5],
                    $parameters[6],
                    $parameters[7]
                );
                break;
        }

        return $retVal;
    }

    public static function getMethodParameters(
        string $className = null,
        string $methodName = null
    ): array
    {
        $refMethod = null;

        if (class_exists($className)) {
            if (self::hasMethod($className, $methodName)) {
                try {
                    $refMethod = new ReflectionMethod($className, $methodName);
                } catch (ReflectionException $ex) {
                    //
                }
            }
        }

        $result = [];

        foreach ($refMethod->getParameters() as $method) {
            $type = (string)$method->getType();

            try {
                $result[$method->getName()] = [
                    // 'name'         => $method->getName(),
                    'position' => $method->getPosition(),
                    'type' => ($type !== "") ? $type : null,
                    'defaultValue' => $method->getDefaultValue(),
                ];
            } catch (ReflectionException $ex) {
                //
            }
        }

        return [
            'class' => $className,
            // 'closure'    => $refMethod->getClosure(),
            'method' => $methodName,
            'parameters' => $result,
        ];
    }

    /**
     * Determine whether the given `object` has the given method `methodName`.
     *
     * @param mixed $object
     * @param string|null $methodName
     *
     * @return bool
     */
    public static function hasMethod($object = null, ?string $methodName = null): bool
    {
        $classExists = false;

        if (self::isObject($object)) {
            $classExists = @class_exists(@get_class($object));
        } else if (self::isString($object)) {
            $classExists = @class_exists($object);
        }

        if ((self::isObject($object) || $classExists) and self::isNotNull($methodName)) {
            return method_exists($object, $methodName);
        } else {
            try {
                return (new ReflectionClass($object))->hasMethod($methodName);
            } catch (ReflectionException $ex) {
                return false;
            }
        }
    }

    /**
     * Determine whether the given `object` has the given property `propertyName`.
     *
     * @param mixed $object
     * @param string|null $propertyName
     *
     * @return bool
     * @throws ReflectionException
     */
    public static function hasProperty($object = null, ?string $propertyName = null): bool
    {
        $classExists = false;

        if (self::isObject($object)) {
            $classExists = @class_exists(@get_class($object));
        }

        try {
            if ((self::isObject($object) || $classExists) and self::isNotNull($propertyName)) {
                return property_exists($object, $propertyName);
            }
        } catch (Exception $ex) {
            return (new ReflectionClass($object))->hasProperty($propertyName);
        } catch (TypeError $ex) {
            return false;
        }

        return false;
    }

    /**
     * Returns an array with the names of all modules compiled and loaded.
     *
     * @param boolean $zendExtensions Only return Zend extensions, if not
     *                                     then regular extensions, like mysqli are listed.
     *                                     Defaults to `FALSE` (return regular extensions).
     *
     * @return array Returns an indexed array of all the modules names.
     */
    public static function extensions(?bool $zendExtensions = false): array
    {
        return get_loaded_extensions($zendExtensions);
    }

    /**
     * Returns a list of registered hashing algorithms suitable for `hash_hmac`.
     *
     * @return array|null Returns a numerically indexed array containing the list of
     *                    supported hashing algorithms suitable for `hash_hmac()`.
     */
    public static function hashHmacAlgorithms(): ?array
    {
        return hash_hmac_algos();
    }

    /**
     * Returns a list of registered hashing algorithms.
     *
     * @return array|null Returns a numerically indexed array containing the list of
     *                    supported hashing algorithms.
     */
    public static function hashAlgorithms(): ?array
    {
        return hash_algos();
    }

    /**
     * include all files in given dir.
     * @param string|null $path
     */
    public static function includeAll(string $path = null): void
    {
        self::requireAll($path, false);
    }

    /**
     * Requires all files in given dir.
     * @param string|null $path
     * @param bool $requireMode
     * @noinspection PhpUndefinedVariableInspection
     * @noinspection PhpIncludeInspection
     */
    public static function requireAll(
        string $path = null,
        bool   $requireMode = true
    ): void
    {
        if (is_dir($path)) {
            $files = glob(dirname($path) . '/*.php');
        }

        foreach ($files as $file) {
            ($requireMode) ? require $file : include $file;
        }

        // foreach (glob(dirname($path) . '/*.php') as $file) {
        //     include $file;
        // }
    }

    public static function parseClassName(string $class = null): array
    {
        return [
            'namespace' => array_slice(explode('\\', $class), 0, -1),
            'classname' => join(EMPTY_STRING, array_slice(explode('\\', $class), -1)),
        ];
    }

    /**
     * @return DateTimeInterface
     * @throws Exception
     */
    public static function getUtcNow(): DateTimeInterface
    {
        return new \Datetime('now', new DateTimeZone('UTC'));
    }

    /** @noinspection PhpUnhandledExceptionInspection */
    public static function hashObject(
        object $object = null,
        string $hashType = 'crc32'
    ): string
    {
        $hashType = strtolower($hashType);

        $result = null;
        $serializedObject = null;

        try {
            if (self::hasMethod($object, 'serialize')) {
                $serializedObject = $object->serialize();
            } else {
                $serializedObject = serialize($object);
            }
        } catch (ReflectionException $ex) {
            //
        }

        switch ($hashType) {
            case 'md5':
                $result = (string)md5($serializedObject);
                break;
            case 'crc32':
                $result = (string)crc32($serializedObject);
                break;
            case 'strlen':
                $result = (string)strlen($serializedObject);
                break;
            case 'adler32':
                $result = (string)hash('adler32', $serializedObject);
                break;
            case 'spl':
                $result = (string)spl_object_hash($object);
                break;
            default:
                throw new Exception("Incorrect hash function: $hashType");
        }

        return $result;
    }

    /**
     * Gets the equivalent alpha of the given number (Like in SpreadSheet
     * applications).
     *
     * Example:
     *     1 => A
     *     2 => B
     *     27 => AA
     *     28 => AB
     *     14558 => UMX
     *
     * @param integer $number number to get the alpha for
     * @param integer|bool $index use index
     *
     * @return string the equivalent string or alpha
     */
    public static function numberToAlpha(
        int  $number = 0,
        bool $index = false
    ): string
    {
        if (0 === $number) {
            $number += 1;
        }

        $numberValue = ($index ? $number : $number - 1);
        $numeric = ($numberValue % 26);
        $letter = chr(65 + $numeric);
        $num = intval($numberValue / 26);

        if ($num > 0) {
            return self::numberToAlpha($num, $index) . $letter;
        }

        return $letter;
    }

    /**
     * Gets the equivalent number of the given alpha (Like in SpreadSheet
     * applications).
     *
     * Example:
     *   A => 1
     *   B => 2
     *   AA => 27
     *   AB => 28
     *   UMX => 14558
     *
     * @param null|string $alpha alpha/string to get number for
     *
     * @return integer the equivalent number
     */
    public static function alphaToNumber(string $alpha = null): int
    {
        $alphaArray = array_reverse(str_split($alpha));

        return array_reduce(
            array_keys($alphaArray),
            function (int $data, int $item) use (&$alphaArray) {
                $data += (ord(strtolower($alphaArray[$item])) - 96) * (pow(26, $item));
                return $data;
            },
            0
        );
    }

    /**
     * String from column index.
     *
     * @param integer $columnIndex Column index (A = 1)
     *
     * @return string
     */
    public static function stringFromColumnIndex(int $columnIndex = 0): string
    {
        static $indexCache = [];

        if (!isset($indexCache[$columnIndex])) {
            $indexValue = $columnIndex;
            $base26 = null;

            do {
                $characterValue = ($indexValue % 26) ?: 26;
                $indexValue = ($indexValue - $characterValue) / 26;
                $base26 = chr($characterValue + 64) . ($base26 ?: EMPTY_STRING);
            } while ($indexValue > 0);

            $indexCache[$columnIndex] = $base26;
        }

        return $indexCache[$columnIndex];
    }

    /**
     * Column index from string.
     *
     * @param string|null $alpha eg 'A'
     *
     * @return integer Column index (A = 1)
     * @throws Exception
     */
    public static function columnIndexFromString(string $alpha = null): int
    {
        // Using a lookup cache adds a slight memory overhead, but boosts speed
        // caching using a static within the method is faster than a class
        // static, though it's additional memory overhead
        static $indexCache = [];

        if (isset($indexCache[$alpha])) {
            return $indexCache[$alpha];
        }

        // It's surprising how costly the strtoupper() and ord() calls actually
        // are, so we use a lookup array rather than use ord() and make it case
        // insensitive to get rid of the strtoupper() as well. Because it's a
        // static, there's no significant memory overhead either
        static $columnLookup = [
            'A' => 1,
            'B' => 2,
            'C' => 3,
            'D' => 4,
            'E' => 5,
            'F' => 6,
            'G' => 7,
            'H' => 8,
            'I' => 9,
            'J' => 10,
            'K' => 11,
            'L' => 12,
            'M' => 13,
            'N' => 14,
            'O' => 15,
            'P' => 16,
            'Q' => 17,
            'R' => 18,
            'S' => 19,
            'T' => 20,
            'U' => 21,
            'V' => 22,
            'W' => 23,
            'X' => 24,
            'Y' => 25,
            'Z' => 26,

            'a' => 1,
            'b' => 2,
            'c' => 3,
            'd' => 4,
            'e' => 5,
            'f' => 6,
            'g' => 7,
            'h' => 8,
            'i' => 9,
            'j' => 10,
            'k' => 11,
            'l' => 12,
            'm' => 13,
            'n' => 14,
            'o' => 15,
            'p' => 16,
            'q' => 17,
            'r' => 18,
            's' => 19,
            't' => 20,
            'u' => 21,
            'v' => 22,
            'w' => 23,
            'x' => 24,
            'y' => 25,
            'z' => 26,
        ];

        // We also use the language construct isset() rather than the more
        // costly strlen() function to match the length of $alpha for improved
        // performance
        if (isset($alpha[0])) {
            if (!isset($alpha[1])) {
                $indexCache[$alpha] = $columnLookup[$alpha];

                return $indexCache[$alpha];
            } elseif (!isset($alpha[2])) {
                $indexCache[$alpha] = $columnLookup[$alpha[0]] *
                    26 + $columnLookup[$alpha[1]];

                return $indexCache[$alpha];
            } elseif (!isset($alpha[3])) {
                $indexCache[$alpha] = $columnLookup[$alpha[0]] * 676 +
                    $columnLookup[$alpha[1]] * 26 + $columnLookup[$alpha[2]];

                return $indexCache[$alpha];
            }
        }

        throw new Exception(
            'Column string index can not be ' . ((isset($alpha[0])) ? 'longer than 3 characters' : 'empty')
        );
    }

    /**
     * Returns a readable error type description given error number.
     * @param integer|null $errorNo
     * @return string|null
     */
    public static function readableErrorType(int $errorNo = null): ?string
    {
        $errorTypes = [
            E_ERROR => 'error',
            E_WARNING => 'warning',
            E_NOTICE => 'notice',
            E_STRICT => 'strict',
            E_USER_ERROR => 'user error',
            E_USER_WARNING => 'user warning',
            E_USER_NOTICE => 'user notice',
        ];

        if (isset($errorTypes[$errorNo])) {
            return $errorTypes[$errorNo];
        } else {
            return $errorNo;
        }
    }

    /**
     * Returns boolean value of path.
     *
     * @param string $path
     * @param DOMXPath $xpath
     * @param DomNode|null $context
     * @param mixed $default
     *
     * @return bool|null|string|mixed
     */
    public static function getBooleanValueFromXPath(
        string   $path,
        DOMXPath $xpath,
        DomNode  $context = null,
                 $default = null
    )
    {
        $value = self::getValueFromXPath($path, $xpath, $context, $default);

        if (null === $value) {
            return null;
        }

        return 'true' === $value || true === $value;
    }

    /**
     * Returns value of path.
     *
     * @param string $path
     * @param DOMXPath $xpath
     * @param DomNode|null $context
     * @param mixed $default
     *
     * @return bool|null|string|mixed
     */
    public static function getValueFromXPath(
        string   $path,
        DOMXPath $xpath,
        DomNode  $context = null,
                 $default = null
    )
    {
        $result = $xpath->query($path, $context);

        if (0 === $result->length) {
            return $default;
        }

        $item = $result->item(0);

        if (null === $item) {
            return $default;
        }

        return $item->nodeValue;
    }

    /**
     * Create a function that can only be called once.
     *
     * @param callable|null $function The function
     *
     * @return Closure
     */
    public static function once(callable $function = null): Closure
    {
        return self::only($function, 1);
    }

    /**
     * Create a function that can only be called $times times.
     *
     * @param callable|null $function Callback
     * @param integer|null $canBeCalledTimes The number of times
     *
     * @return Closure
     */
    public static function only(
        callable $function = null,
        int      $canBeCalledTimes = null
    ): Closure
    {
        $unique = mt_rand();

        // Create a closure that check if the function was already called
        return function () use ($function, $canBeCalledTimes, $unique) {
            // Generate unique hash of the function
            $arguments = func_get_args();
            $signature = self::getSignature($unique, $function, $arguments);

            // Get counter
            $numberOfTimesCalled = self::hasBeenCalledTimes($signature);

            // If the function has been called too many times, cancel
            // Else, increment the count
            if ($numberOfTimesCalled >= $canBeCalledTimes) {
                return false;
            } else {
                ++self::$canBeCalledTimes[$signature];
            }

            return call_user_func_array($function, $arguments);
        };
    }

    /**
     * Get a function's signature.
     *
     * @param null $unique Unique string.
     * @param callable|null $function The function to get signature from.
     * @param array $arguments The function arguments.
     *
     * @return string The unique id
     */
    public static function getSignature(
        $unique = null,
        callable $function = null,
        array $arguments = []
    ): string
    {
        $function = var_export($function, true);
        $arguments = var_export($arguments, true);

        if (self::isNumber($unique)) {
            $unique = (string)$unique;
        }

        return md5($unique . '_' . $function . '_' . $arguments);
    }

    /**
     * Get the number of times a function has been called.
     *
     * @param string|null $unique The function unique ID
     *
     * @return int
     */
    public static function hasBeenCalledTimes(string $unique = null): int
    {
        return Arr::setAndGet(self::$canBeCalledTimes, $unique, 0);
    }

    /**
     * Create a function that can only be called after $times times.
     *
     * @param callable|null $function The function to call.
     * @param integer|null $times Call times.
     *
     * @return Closure
     */
    public static function after(callable $function = null, int $times = null): Closure
    {
        $unique = mt_rand();

        // Create a closure that check if the function was already called
        return function () use ($function, $times, $unique) {
            // Generate unique hash of the function
            $arguments = func_get_args();
            $signature = self::getSignature($unique, $function, $arguments);

            // Get counter
            $called = self::hasBeenCalledTimes($signature);

            // Prevent calling before a certain number
            if ($called < $times) {
                self::$canBeCalledTimes[$signature] += 1;

                return false;
            }

            return call_user_func_array($function, $arguments);
        };
    }

    /**
     * Caches the result of a function and refer to it ever after.
     *
     * @param callable|null $function The function to cache.
     *
     * @return Closure
     */
    public static function cache(callable $function = null): Closure
    {
        $unique = mt_rand();

        return function () use ($function, $unique) {
            // Generate unique hash of the function
            $arguments = func_get_args();
            $signature = self::getSignature($unique, $function, $arguments);

            if (isset(self::$cached[$signature])) {
                return self::$cached[$signature];
            }

            $result = call_user_func_array($function, $arguments);
            self::$cached[$signature] = $result;

            return $result;
        };
    }

    /**
     * Only allow a function to be called every X ms.
     *
     * @param callable|null $function The function to throttle.
     * @param integer|null $ms Throttling time in millisecond.
     *
     * @return Closure
     */
    public static function throttle(callable $function = null, int $ms = null): Closure
    {
        $unique = mt_rand();

        return function () use ($function, $ms, $unique) {
            // Generate unique hash of the function
            $arguments = func_get_args();
            $signature = self::getSignature($unique, $function, $arguments);

            // Check last called time and update it if necessary
            $last = self::getLastCalledTime($signature);
            $difference = time() - $last;

            // Execute the function if the conditions are here
            if ($last === time() or $difference > $ms) {
                self::$throttle[$signature] = time();

                return call_user_func_array($function, $arguments);
            }

            return false;
        };
    }

    /**
     * Get the last time a function was called.
     *
     * @param string|null $unique The function unique ID
     *
     * @return int
     */
    public static function getLastCalledTime(string $unique = null): int
    {
        return Arr::setAndGet(self::$canBeCalledTimes, $unique, time());
    }

    /**
     * Prefill function arguments.
     *
     * @param callable|null $func The function to prefill.
     *
     * @return Closure
     */
    public static function partial(callable $func = null): Closure
    {
        $boundArgs = array_slice(func_get_args(), 1);

        return function () use ($boundArgs, $func) {
            $args = [];
            $calledArgs = func_get_args();
            $position = 0;

            for ($i = 0, $len = count($boundArgs); $i < $len; ++$i) {
                $args[] = null === $boundArgs[$i] ?
                    $calledArgs[$position++] : $boundArgs[$i];
            }

            return call_user_func_array(
                $func,
                array_merge(
                    $args,
                    array_slice($calledArgs, $position)
                )
            );
        };
    }

    /**
     * Get all methods from an object.
     * @param object|null $object
     * @return array
     */
    public static function methods(object $object = null): array
    {
        return get_class_methods(get_class($object));
    }

    /**
     * Unpack an object's properties.
     * @param object|null $object
     * @param null $attribute
     * @return object
     */
    public static function unpack(object $object = null, $attribute = null): object
    {
        $object = (array)$object;
        $object = $attribute
            ? Arr::get($object, $attribute)
            : Arr::getFirst($object);

        return (object)$object;
    }

    /**
     * Converts data from JSON.
     *
     * @param string|null $data The data to parse
     *
     * @return mixed
     */
    public static function fromJSON(string $data = null)
    {
        return Json::decode($data);
    }

    #
    #region JSON
    #

    /**
     * Converts data to JSON.
     *
     * @param array|object $data The data to convert
     *
     * @return string Converted data
     */
    public static function toJSON($data = null): string
    {
        return Json::encode($data);
    }

    /**
     * Converts data from YAML.
     *
     * @param string|null $data The data to parse
     *
     * @return mixed
     */
    public static function fromYAML(string $data = null)
    {
        return Yaml::decode($data);
    }

    #
    #endregion JSON
    #

    #
    #region YAML
    #

    /**
     * Converts data to YAML.
     *
     * @param array|object $data The data to convert
     *
     * @return string Converted data
     */
    public static function toYAML($data = null): string
    {
        return Yaml::encode($data);
    }

    /**
     * Converts data from XML.
     *
     * @param string|null $xml The data to parse
     *
     * @return array
     */
    public static function fromXML(string $xml = null): array
    {
        $xml = simplexml_load_string($xml);
        $xml = Json::encode($xml);
        $xml = Json::decode($xml);

        return $xml;
    }

    #
    #endregion YAML
    #

    #
    #region XML
    #

    /**
     * Converts data from CSV.
     *
     * @param string|null $data The data to parse.
     * @param bool $hasHeaders Whether the CSV has headers.
     *
     * @return mixed
     */
    public static function fromCSV(
        string $data = null,
        bool   $hasHeaders = false
    ): array
    {
        $data = trim($data);

        // Explodes rows
        $data = self::explodeWith($data, [PHP_EOL, "\r", "\n"]);
        $data = array_map(
            function ($row) {
                return self::explodeWith($row, [';', "\t", ',']);
            },
            $data
        );

        // Get headers
        $headers = $hasHeaders ? $data[0] : array_keys($data[0]);
        if ($hasHeaders) {
            Arr::shift($data);
        }

        // Parse the columns in each row
        $array = [];
        foreach ($data as $row => $columns) {
            foreach ($columns as $columnNumber => $column) {
                if (is_string($column)) {
                    $column = Str::replace($column, '"', EMPTY_STRING);
                    // $column = \str_replace('"', '', $column);
                }

                $array[$row][$headers[$columnNumber]] = $column;
            }
        }

        return $array;
    }

    #
    #endregion XML
    #

    #
    #region CSV
    #

    /**
     * Tries to explode a string with an array of delimiters.
     *
     * @param null $string $string The string
     * @param array|null $delimiters An array of delimiters
     *
     * @return array
     */
    public static function explodeWith($string = null, array $delimiters = null): ?array
    {
        $array = $string;

        foreach ($delimiters as $delimiter) {
            $array = explode($delimiter, $string);
            if (1 === count($array)) {
                continue;
            } else {
                return $array;
            }
        }

        return $array;
    }

    /**
     * Converts data to CSV.
     *
     * @param mixed $data The data to convert.
     * @param string $delimiter Delimiter.
     * @param bool $exportHeaders Exports data with headers.
     *
     * @return string CSV converted data.
     */
    public static function toCSV(
        $data = null,
        string $delimiter = ';',
        bool $exportHeaders = false
    ): string
    {
        $csv = [];

        // Convert objects to arrays
        if (is_object($data)) {
            $data = (array)$data;
        }

        // Don't convert if it's not an array
        if (!is_array($data)) {
            return $data;
        }

        // Fetch headers if requested
        if ($exportHeaders) {
            $headers = array_keys(Arr::getFirst($data));
            $csv[] = implode($delimiter, $headers);
        }

        // Quote values and create row
        foreach ($data as $header => $row) {
            // If single column
            if (!is_array($row)) {
                $csv[] = '"' . $header . '"' . $delimiter . '"' . $row . '"';
                continue;
            }

            // Else add values
            foreach ($row as $key => $value) {
                $row[$key] = '"' . stripslashes($value) . '"';
            }

            $csv[] = implode($delimiter, $row);
        }

        return implode(PHP_EOL, $csv);
    }

    #
    #endregion CSV
    #

    /**
     * Gets the type from the given value/variable as string.
     *
     * @param string|null $variable The value/variable to get.
     * @param string $type Type.
     *
     * @return mixed
     */
    public static function getTypedValueFromString(
        string $variable = null,
        string $type = 'STRING'
    )
    {
        if (self::isNull($variable) or empty($variable)) {
            throw new InvalidArgumentException('Variable cannot be empty.');
        }

        if (self::isNull($type) or empty($type)) {
            throw new InvalidArgumentException('Type cannot be empty.');
        }

        $type = strtoupper($type);

        $returnValue = null;

        switch ($type) {
            case 'FLOAT':
                $returnValue = self::toFloat($variable);
                break;
            case 'SERIALIZED':
                $returnValue = unserialize($variable);
                break;
            case 'JSON':
                $returnValue = Json::decode($variable);
                break;
            case 'ARRAY':
                if (self::isJson($variable)) {
                    $returnValue = Json::decode($variable);
                }
                break;
            case 'CHAR':
                if (self::isChar($variable)) {
                    $returnValue = $variable;
                }
                break;
            case 'YAML':
                $returnValue = Yaml::decode($variable);
                break;
            case 'BOOLEAN':
                $returnValue = self::toBoolean($variable);
                break;
            case 'INTEGER':
            case 'NUMBER':
                $returnValue = self::toInteger($variable);
                break;
            case 'EMAIL':
                if (self::isEmail($variable)) {
                    $returnValue = $variable;
                }
                break;
            case 'DATETIME':
                $returnValue = self::toDateTime($variable);
                break;
            case 'TIMESTAMP':
                $variable = intval($variable);
                $returnValue = self::toDateTime($variable);
                break;
            // TODO: Try to figure out a better solution for `URL` detection and storing !_!
            // case 'URL':
            //     $returnValue = new Uri($variable);
            //     break;
            case 'STRING':
            case 'URL':
            default:
                $returnValue = $variable;
                break;
        }

        return $returnValue;
    }

    /**
     * Converts the given value into float.
     *
     * @param mixed $value Value to convert.
     *
     * @return float|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toFloat($value = null): float
    {
        return floatval($value);
    }

    /**
     * Determine whether the given string is valid JSON string.
     *
     * @param mixed $input String to check.
     *
     * @return boolean
     */
    public static function isJson($input = null): bool
    {
        if (self::isString($input) and json_decode($input, true)) {
            return json_last_error() === JSON_ERROR_NONE;
        }

        return false;
    }

    /**
     * Determine whether the given value is character.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isChar($value = null): bool
    {
        $strLen = function () use (&$value): int {
            if (function_exists('mb_strlen')) {
                return mb_strlen($value);
            }

            return strlen($value);
        };

        return self::isString($value) and $strLen() === 1;
    }

    /**
     * Converts the given value into bool.
     *
     * @param mixed $value Value to convert.
     *
     * @return bool|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toBoolean($value = null): bool
    {
        if (self::isString($value)) {
            $words = [
                'yes' => [
                    '1',
                    'affirmative',
                    'all right',
                    'aye',
                    'indubitably',
                    'most assuredly',
                    'of course',
                    'ok',
                    'okay',
                    'on',
                    'oui',
                    'sure thing',
                    'sure',
                    't',
                    'true',
                    'vrai',
                    'y',
                    'yea',
                    'yeah',
                    'yep',
                    'yes+',
                    'yup',
                    'حسنا',
                    'حسناً',
                    'نعم',
                ],

                'no' => [
                    '0',
                    'absolutely not',
                    'by no means',
                    'f',
                    'false',
                    'faux',
                    'na',
                    'nah',
                    'negative',
                    'never ever',
                    'never',
                    'no way',
                    'no*',
                    'non',
                    'nope',
                    'off',
                    'لا',
                ],
            ];

            $yesWords = implode('|', $words['yes']);
            $noWords = implode('|', $words['no']);

            if (preg_match('/^(' . $yesWords . ')$/i', $value)) {
                return true;
            } elseif (preg_match('/^(' . $noWords . ')$/i', $value)) {
                return false;
            }
        }

        // return \boolval($value);

        return (bool)$value;
    }

    /**
     * Converts the given value into int.
     *
     * @param mixed $value Value to convert.
     *
     * @return integer|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toInteger($value = null): int
    {
        return intval($value);
    }

    /**
     * Determine whether the given value is a valid E-Mail string.
     *
     * @param string $string String E-Mail to check.
     *
     * @return boolean
     */
    public static function isEmail($string = null): bool
    {
        return preg_match(
                '/^([a-z0-9_.-])+@([a-z0-9_.-])+\.([a-z])+([a-z])+/i',
                $string
            ) > 0;
    }

    /**
     * Converts the given value to \DateTime.
     *
     * @param mixed $value Value to convert.
     *
     * @return DateTimeInterface
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function toDateTime($value = null): DateTimeInterface
    {
        $datetime = null;

        if (is_string($value)) {
            try {
                $datetime = new \DateTime($value);
            } catch (Exception $ex) {
                //
                // $datetime = new \DateTime((string) strtotime($value));
            }
        } elseif (Validator::isValidNumber($value)) {
            $value = '@' . (string)$value;

            try {
                $datetime = new \DateTime($value);
            } catch (Exception $ex) {
                //
            }
        } elseif ($value instanceof DateTimeInterface) {
            $datetime = $value;
        }

        return $datetime;
    }

    /**
     * Gets the type from the given value/variable as string.
     *
     * @param mixed $value The value/variable to get type form.
     *
     * @return null|string
     */
    public static function getTypeOf($value = null): ?string
    {
        // $nativeType = \gettype($value);
        $retVal = null;

        if (self::isNotNull($value) or self::isNotEmpty($value)) {
            if (self::isSerialized($value)) {
                $retVal = 'SERIALIZED';
            } elseif (self::isChar($value)) {
                $retVal = 'CHAR';
            } elseif (self::isFloat($value)) {
                $retVal = 'FLOAT';
            } elseif (self::isBool($value)) {
                $retVal = 'BOOLEAN';
            } elseif (self::isInteger($value)) {
                $retVal = 'NUMBER';
            } elseif (self::isDouble($value)) {
                $retVal = 'DOUBLE';
            } elseif (self::isDateTime($value)) {
                $retVal = 'DATETIME';
            } elseif (self::isBoolean($value)) {
                $retVal = 'BOOLEAN';
            } elseif (self::isInteger($value)) {
                $retVal = 'INTEGER';
            } elseif (self::isTimestamp($value)) {
                $retVal = 'TIMESTAMP';
            } elseif (self::isString($value) and self::isJson($value)) {
                $retVal = 'JSON';
            } elseif (self::isString($value) and self::isUrl($value)) {
                $retVal = 'URL';
            } elseif (self::isString($value) and self::isEmail($value)) {
                $retVal = 'EMAIL';
            } elseif (self::isString($value) and self::isXml($value)) {
                $retVal = 'XML';
            } elseif (self::isString($value)) {
                $retVal = 'STRING';
            } elseif (self::isArray($value) and self::isAssoc($value)) {
                // $retVal = 'ASSOCIATIVE_ARRAY';
                $retVal = 'ARRAY';
            } elseif (self::isArray($value) and !self::isAssoc($value)) {
                $retVal = 'ARRAY';
            } elseif (self::isArray($value)) {
                $retVal = 'ARRAY';
            } /* else if (self::isString($value) and self::isYaml($value)) {
                $retVal = 'YAML';
            } else if (self::isObject($value)) {
                $retVal = 'OBJECT';
            } */
        }

        // DEBUG('nativeType:', $nativeType);
        // DEBUG('retVal:', $retVal);

        return $retVal;
    }

    /**
     * Check value to find if it was serialized.
     *
     * If $data is not an string, then returned value will always be false.
     * Serialized data is always a string.
     *
     * @param mixed $data Value to check to see if was serialized
     *
     * @return boolean
     */
    public static function isSerialized($data = null): bool
    {
        // If it isn't a string, it isn't serialized
        if (!self::isString($data)) {
            return false;
        }

        $data = trim($data);

        // Is it the serialized NULL value?
        if ($data === 'N;') {
            return true;
        } elseif ($data === 'b:0;' || $data === 'b:1;') {
            // Is it a serialized boolean?
            return true;
        }

        $length = strlen($data);

        // Check some basic requirements of all serialized strings
        if ($length < 4 || $data[1] !== ':' || ($data[$length - 1] !== ';' && $data[$length - 1] !== '}')) {
            return false;
        }

        return @unserialize($data) !== false;
    }

    /**
     * Determine whether the given value is an instance of \DateTimeInterface.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     */
    public static function isDateTime($value = null): bool
    {
        return self::isDate($value);
    }

    /**
     * Determine whether the given value is an instance of \DateTimeInterface.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean
     * @noinspection PhpUnusedLocalVariableInspection
     */
    public static function isDate($value = null): bool
    {
        $result = false;

        if (self::isString($value)) {
            try {
                $datetime = new \DateTime($value);
                $result = true;
            } catch (Exception $ex) {
                $result = false;
            }
        } elseif (Validator::isValidNumber($value)) {
            try {
                $value = '@' . (string)$value;
                $datetime = new \DateTime($value);
                $result = true;
            } catch (Exception $ex) {
                $result = false;
            }
        } elseif ($value instanceof DateTimeInterface) {
            $result = true;
        }

        return $result;
    }

    /**
     * Determine whether the given value is valid timestamp.
     *
     * @param mixed $input Value to check.
     *
     * @return boolean
     */
    public static function isTimestamp($input = null): bool
    {
        // if (self::isInteger($input)) {
        //     // $input = self::toInteger($input);
        //     return \ctype_digit($input) && $input <= 2147483647;
        // }

        // try {
        //     new \DateTime('@' . $input);
        // } catch (\Exception $ex) {
        //     return false;
        // }

        // return true;

        $check = (is_int($input) or is_float($input))
            ? $input
            : (string)(int)$input;

        return ($check === $input) and ((int)$input <= PHP_INT_MAX) and ((int)$input >= ~PHP_INT_MAX);
    }

    /**
     * Determine whether the given value is a valid URL string.
     *
     * @param string $string String URL to check.
     *
     * @return boolean
     */
    public static function isUrl($string = null): bool
    {
        return preg_match(
                '%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|' .
                '(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{0' .
                '0a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6})' .
                ')(?::\d+)?(?:[^\s]*)?$%iu',
                $string
            ) > 0;
    }

    /**
     * Determine whether the given string is valid XML string.
     *
     * @param mixed $input String to check.
     *
     * @return boolean
     */
    public static function isXml($input = null): bool
    {
        if (self::isString($input)) {
            $input = trim($input);

            if (empty($input)) {
                return false;
            }

            // HTML go to hell!
            if (stripos($input, '<!DOCTYPE html>') !== false) {
                return false;
            }

            libxml_use_internal_errors(true);
            simplexml_load_string($input);
            $errors = libxml_get_errors();
            libxml_clear_errors();

            return empty($errors);
        }

        return false;
    }

    /**
     * Gets the type from the given value/variable as string.
     *
     * @param mixed $value The value/variable to get type form.
     *
     * @return null|string
     */
    public static function getType($value = null): ?string
    {
        // $nativeType = \gettype($value);
        $retVal = null;

        if (self::isNotNull($value) or self::isNotEmpty($value)) {
            if (self::isSerialized($value)) {
                $retVal = 'serialized';
            } elseif (self::isChar($value)) {
                $retVal = 'char';
            } elseif (self::isFloat($value)) {
                $retVal = 'float';
            } elseif (self::isDouble($value)) {
                $retVal = 'double';
            } elseif (self::isDateTime($value)) {
                $retVal = 'datetime';
            } elseif (self::isBoolean($value)) {
                $retVal = 'boolean';
            } elseif (self::isInteger($value)) {
                $retVal = 'integer';
            } elseif (self::isJson($value)) {
                $retVal = 'json';
            } elseif (self::isXml($value)) {
                $retVal = 'xml';
            } elseif (self::isString($value)) {
                $retVal = 'string';
            } elseif (self::isArray($value) and self::isAssoc($value)) {
                $retVal = 'associative_array';
            } elseif (self::isArray($value) and !self::isAssoc($value)) {
                $retVal = 'array';
            } elseif (self::isArray($value)) {
                $retVal = 'array';
            } elseif (self::isObject($value)) {
                $retVal = 'object';
            }
        }

        return $retVal;
    }

    /**
     * @param $expression
     *
     * @return Type\Type
     * @throws Exception
     */
    public static function typeOf($expression): Type\Type
    {
        return Type\TypeFactory::expression($expression);
    }

    /**
     * Get classes by the given `namespace`.
     *
     * @param string|null $namespace
     *
     * @return array|null
     */
    public static function getClassesByNamespace(?string $namespace = null): ?array
    {
        $classes = get_declared_classes();
        $neededClasses = array_filter($classes, function ($item) use ($namespace) {
            return strpos($item, $namespace) === 0;
        });

        if (self::isNotEmpty($neededClasses)) {
            $retVal = array_values($neededClasses);
            ksort($retVal);
            return $retVal;
        }

        return null;
    }

    public static function toUTCDateTimeImmutable($value): DateTimeImmutable
    {
        $tz = new DateTimeZone('UTC');
        $now = time();

        if (
            ($value instanceof DateTimeInterface)
            && $result = DateTimeImmutable::createFromFormat('U.u', $value->format('U.u'))
        ) {
            return $result->setTimezone($tz);
        }

        if ($value === null || $value === 0 || is_bool($value)) {
            $value = '0';
        }

        if (is_scalar($value) || (is_object($value) && method_exists($value, '__toString'))) {
            $value = (string)$value;
        } else {
            $type = is_object($value) ? get_class($value) : gettype($value);
            throw new InvalidArgumentException("This {$type} cannot be parsed to a DateTime value");
        }

        if (ctype_digit($value)) {
            // Seconds
            if ($value === '0' || mb_strlen($value) === mb_strlen((string)$now)) {
                if ($result = DateTimeImmutable::createFromFormat('U', $value)) {
                    return $result->setTimezone($tz);
                }
            }

            // Milliseconds
            if (
                (mb_strlen($value) === mb_strlen((string)($now * 1000)))
                && $result = DateTimeImmutable::createFromFormat('U.u', sprintf('%F', (float)($value / 1000)))
            ) {
                return $result->setTimezone($tz);
            }
        }

        // microtime
        if (preg_match('@(?P<msec>^0?\.\d+) (?P<sec>\d+)$@', $value, $matches)) {
            $value = (string)((float)$matches['sec'] + (float)$matches['msec']);

            if ($result = DateTimeImmutable::createFromFormat('U.u', sprintf('%F', $value))) {
                return $result->setTimezone($tz);
            }
        }

        try {
            return (new DateTimeImmutable($value))->setTimezone($tz);
        } catch (Throwable $e) {
            throw new InvalidArgumentException($e->getMessage());
        }
    }

    /**
     * Converts the given __`$time`__ to an `ISO-8601` duration.
     *
     * @param string|integer $time Time to convert.
     *
     * @return string
     */
    public static function timeToIso8601Duration($time): string
    {
        $units = [
            'Y' => 365 * 24 * 3600,
            'D' => 24 * 3600,
            'H' => 3600,
            'M' => 60,
            'S' => 1,
        ];

        $str = "P";
        $istime = false;

        foreach ($units as $unitName => &$unit) {
            $quot = intval($time / $unit);
            $time -= $quot * $unit;
            $unit = $quot;

            if ($unit > 0) {
                // There may be a better way to do this
                if (!$istime && in_array($unitName, ['H', 'M', 'S'])) {
                    $str .= 'T';
                    $istime = true;
                }

                $str .= strval($unit) . $unitName;
            }
        }

        return $str;
    }

    /**
     * Converts the given __`$seconds`__ into an `ISO-8601` duration.
     *
     * @param integer $seconds Seconds to convert.
     *
     * @return string
     */
    public static function toIso8601Duration(int $seconds = 0): string
    {
        $days = floor($seconds / 86400);
        $seconds = $seconds % 86400;

        $hours = floor($seconds / 3600);
        $seconds = $seconds % 3600;

        $minutes = floor($seconds / 60);
        $seconds = $seconds % 60;

        return sprintf('P%dDT%dH%dM%dS', $days, $hours, $minutes, $seconds);
    }

    /**
     * Converts the given __`$dateInterval`__ into an `ISO-8601` duration.
     *
     * @param DateInterval $dateInterval
     *
     * @return string
     */
    public static function dateIntervalToISO8601Duration(DateInterval $dateInterval): string
    {
        $duration = 'P';

        if (!empty($dateInterval->y)) {
            $duration .= "{$dateInterval->y}Y";
        }

        if (!empty($dateInterval->m)) {
            $duration .= "{$dateInterval->m}M";
        }

        if (!empty($dateInterval->d)) {
            $duration .= "{$dateInterval->d}D";
        }

        if (!empty($dateInterval->h) || !empty($dateInterval->i) || !empty($dateInterval->s)) {
            $duration .= 'T';

            if (!empty($dateInterval->h)) {
                $duration .= "{$dateInterval->h}H";
            }

            if (!empty($dateInterval->i)) {
                $duration .= "{$dateInterval->i}M";
            }

            if (!empty($dateInterval->s)) {
                $duration .= "{$dateInterval->s}S";
            }
        }

        if ($duration === 'P') {
            // Empty duration (zero seconds)
            $duration = 'PT0S';
        }

        return $duration;
    }

    /**
     * Parses audio/video length/duration into string.
     *
     * @param string|integer $duration
     * @param boolean|null $withHours
     * @param boolean|null $withMilliseconds
     *
     * @return string
     */
    public static function parseDuration(
        $duration = null,
        ?bool $withHours = true,
        ?bool $withMilliseconds = false
    ): string {
        // if (self::isString($duration)) {
        //     $duration = self::toInteger($duration);
        // }

        $cb = $duration > 0 ? 'floor' : 'ceil';

        $days = self::callFunction($cb, ($duration / 86400));
        $hours = self::callFunction($cb, ($duration - ($days * 86400)) / 3600);
        $minutes = self::callFunction($cb, ($duration - ($days * 86400) - ($hours * 3600)) / 60);
        $seconds = self::callFunction($cb, ($duration - ($days * 86400) - ($hours * 3600) - ($minutes * 60)));
        $ms = self::callFunction($cb, $duration) % 1000;

        // $value = explode('.', (string)$duration)[1];
        // D($value % 1000);

        if (!$withHours) {
            $minutes += $hours * 60;
        }

        if ($days < 10) {
            $hours = '0' . $days;
        }

        if ($hours < 10) {
            $hours = '0' . $hours;
        }

        if ($minutes < 10) {
            $minutes = '0' . $minutes;
        }

        if ($seconds < 10) {
            $seconds = '0' . $seconds;
        }

        $retVal = '';

        if ($withHours and !$withMilliseconds) {
            $retVal = "$hours:$minutes:$seconds";
        } else if ($withHours and $withMilliseconds) {
            $retVal = "$hours:$minutes:$seconds";
            $retVal .= '.' . Lang::addZero($ms, 3);
        } else if (!$withHours and !$withMilliseconds) {
            $retVal = "$minutes:$seconds";
        } else if (!$withHours and $withMilliseconds) {
            $retVal = "$minutes:$seconds";
            $retVal .= '.' . Lang::addZero($ms, 3);
        }

        return $retVal;
    }

    /**
     * @param null $callable
     * @param mixed ...$arguments
     * @return mixed
     */
    public static function callFunction($callable = null, ...$arguments)
    {
        return Callback::invokeSafe($callable, $arguments);
    }

    /**
     * @param integer $value
     * @param integer|null $digits
     *
     * @return string
     */
    public static function addZero(int $value, ?int $digits = null): string
    {
        $digits = $digits ?? 2;

        $isNegative = $value < 0;
        $buffer = self::stringify($value);

        // Strip minus sign if number is negative
        if ($isNegative) {
            $buffer = substr($buffer, 1);
        }

        // $size = ($digits - strlen($buffer)) + 1;
        $size = ($digits - strlen($buffer));
        $arr = array_fill(0, $size, '0');
        // $arr[$size - 1] = $buffer;
        $buffer = implode('', $arr) . $buffer;
        // $buffer = implode('', $arr);
        // D($buffer);
        // Adds back minus sign if needed
        return ($isNegative ? '-' : '') . $buffer;
    }

    /**
     * Converts the given value into string.
     *
     * @param mixed $value Value to convert.
     *
     * @return string|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function stringify($value = null): string
    {
        if (self::isFloat($value)) {
            return self::doubleToString($value);
        } elseif (self::isDouble($value)) {
            return self::doubleToString($value);
        } elseif (self::isNumber($value)) {
            return (string)$value;
        } elseif (self::isObject($value)) {
            // return self::dumpAsString($value);
            return self::printf('<%s>', get_class($value));
        } elseif ($value === false) {
            return 'false';
        } elseif ($value === true) {
            return 'true';
        } elseif (self::isNull($value)) {
            return 'null';
        } elseif (self::isString($value)) {
            return '\'' . trim($value) . '\'';
        } elseif (self::isArray($value)) {
            // return Json::encode($value);
            return str_replace(',', ', ', json_encode($value));
        }

        return $value;
    }

    /**
     * Converts the given double value into string.
     *
     * @param mixed $value Value to convert.
     *
     * @return string|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function doubleToString($value = null): string
    {
        // $string = (string) $value;

        // // if (\preg_match('~(\d+)\.(\d+)E([+-])?(\d+)~', $string, $matches)) {
        // if (\preg_match('/(\d+)\.(\d+)E([+-])?(\d+)/', $string, $matches)) {
        //     self::d($matches);
        //     // $decimals = $matches[2] === '-' ? strlen($matches[1]) + $matches[3] : 0;
        //     // $string = \number_format($value, $decimals, \SINGLE_DOT, \EMPTY_STRING);
        // }

        $string = (string)$value;

        if (preg_match('@\.(\d+)E([+-])?(\d+)@', $string, $matches)) {
            $decimals = $matches[2] === '-' ? strlen($matches[1]) + $matches[3] : 0;
            $string = number_format($value, $decimals, SINGLE_DOT, EMPTY_STRING);
        }

        // return \number_format((float) $value, 5, \SINGLE_DOT, \EMPTY_STRING);

        return $string;
    }

    public static function audioDuration($duration = null): string
    {
        $minutes = self::parseInt($duration / 60);
        $seconds = self::parseInt($duration % 60);

        return "$minutes:$seconds";
    }

    /**
     * Returns the integer value of __`$var`__, using the specified
     * __`$base`__ for the conversion (the default is base `10`).
     *
     * @param mixed $var The scalar value being converted to an integer.
     * @param integer|null $base The base for the conversion.
     *
     * @return integer The integer value of $var on success, or 0 on
     *                 failure. Empty arrays return 0, non-empty arrays
     *                 return 1.
     */
    public static function parseInt($var = null, ?int $base = 10): int
    {
        return intval($var, $base);
    }

    /**
     * Delays the program execution for the given number of `seconds`.
     *
     * @param integer|null $seconds Halt time in seconds.
     *
     * @return integer|boolean Returns zero on success, or `FALSE` on error.
     */
    public static function sleep(?int $seconds = null)
    {
        return sleep($seconds);
    }

    /**
     * Delays program execution for the given number of `microSeconds`.
     *
     * @param integer|null $microSeconds Halt time in microseconds. A microsecond is one millionth of a second.
     *
     * @return void No value is returned.
     */
    public static function usleep(?int $microSeconds = null): void
    {
        usleep($microSeconds);
    }

    /**
     * Delays program execution for the given number of `seconds` and `nanoSeconds`.
     *
     * @param integer|null $seconds Must be a non-negative integer.
     * @param integer|null $nanoSeconds Must be a non-negative integer less than 1 billion.
     *
     * @return boolean Returns `TRUE` on success or `FALSE` on failure.
     */
    public static function nanosleep(?int $seconds = null, ?int $nanoSeconds = null): bool
    {
        return time_nanosleep($seconds, $nanoSeconds);
    }

    #
    #region Misc. Methods
    #

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new Lang();
        }

        return self::$instance;
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
            ' is singleton and cannot be cloned, use instance() method'
        );
    }

    #
    #endregion Misc. Methods
    #

    #
    #region Magic Methods
    #

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    #
    #endregion Magic Methods
    #
}
