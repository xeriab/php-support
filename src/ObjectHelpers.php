<?php

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Traits\StaticClassTrait;
use Exen\Support\Exception\MemberAccessException;

/**
 * \Exen\Support\SmartObjectTrait helpers.
 */
final class ObjectHelpers
{
    use StaticClassTrait;

    /**
     *
     * @throws MemberAccessException
     */
    public static function strictGet(
        string $class = null,
        string $name = null
    ) {
        $refClass = new \ReflectionClass($class);

        $hint = self::getSuggestion(
            array_merge(
                array_filter(
                    $refClass->getProperties(
                        \ReflectionProperty::IS_PUBLIC |
                        \ReflectionProperty::IS_PROTECTED
                    ),
                    function ($property) {
                        return ! $property->isStatic();
                    }
                ),
                self::parseFullDoc(
                    $refClass,
                    '~^[ \t*]*@property(?:-read)?' .
                    '[ \t]+(?:\S+[ \t]+)??\$(\w+)~m'
                )
            ),
            $name
        );

        throw new MemberAccessException(
            "Cannot read an undeclared property $class::\$$name".
            ($hint ? ", did you mean \$$hint?" : '.')
        );
    }

    /**
     *
     * @throws MemberAccessException
     */
    public static function strictSet(
        string $class = null,
        string $name = null
    ): void {
        $refClass = new \ReflectionClass($class);

        $hint = self::getSuggestion(
            array_merge(
                array_filter(
                    $refClass->getProperties(
                        \ReflectionProperty::IS_PUBLIC |
                        \ReflectionProperty::IS_PROTECTED
                    ),
                    function ($property) {
                        return ! $property->isStatic();
                    }
                ),
                self::parseFullDoc(
                    $refClass,
                    '~^[ \t*]*@property(?:-write)?'.
                    '[ \t]+(?:\S+[ \t]+)??\$(\w+)~m'
                )
            ),
            $name
        );

        throw new MemberAccessException(
            "Cannot write to an undeclared property $class::\$$name" .
            ($hint ? ", did you mean \$$hint?" : '.')
        );
    }

    /**
     *
     * @throws MemberAccessException
     */
    public static function strictCall(
        string $class = null,
        string $method = null,
        array $additionalMethods = []
    ): void {
        $hint = self::getSuggestion(
            array_merge(
                get_class_methods($class),
                self::parseFullDoc(
                    new \ReflectionClass($class),
                    '~^[ \t*]*@method[ \t]+(?:\S+[ \t]+)??(\w+)\(~m'
                ),
                $additionalMethods
            ),
            $method
        );

        // Called parent::$method()
        if (method_exists($class, $method)) {
            $class = 'parent';
        }

        throw new MemberAccessException(
            "Call to undefined method $class::$method()" .
            ($hint ? ", did you mean $hint()?" : '.')
        );
    }

    /**
     *
     * @throws MemberAccessException
     */
    public static function strictStaticCall(
        string $class = null,
        string $method = null
    ): void {
        $hint = self::getSuggestion(
            array_filter(
                (new \ReflectionClass($class))->getMethods(
                    \ReflectionMethod::IS_PUBLIC
                ),
                function ($_method) {
                    return $_method->isStatic();
                }
            ),
            $method
        );

        throw new MemberAccessException(
            "Call to undefined static method $class::$method()" .
            ($hint ? ", did you mean $hint()?" : '.')
        );
    }

    /**
     * Returns array of magic properties defined by annotation @property.
     *
     * @return array of [name => bit mask]
     *
     * @internal
     */
    public static function getMagicProperties($class): ?array
    {
        static $cache;

        $properties = &$cache[$class];

        if (null !== $properties) {
            return $properties;
        }

        $refClass = new \ReflectionClass($class);

        preg_match_all(
            '~^  [ \t*]*  @property(|-read|-write)  [ \t]+  ' .
            '[^\s$]+  [ \t]+  \$  (\w+)  ()~mx',
            (string) $refClass->getDocComment(),
            $matches,
            PREG_SET_ORDER
        );

        $properties = [];

        foreach ($matches as list(, $type, $name)) {
            $uname = ucfirst($name);

            $write = '-read' !== $type &&
                $refClass->hasMethod($nm = 'set'.$uname) &&
                ($refMethod = $refClass->getMethod($nm)) &&
                $refMethod->getName() === $nm &&
                !$refMethod->isPrivate() &&
                !$refMethod->isStatic();

            $read = '-write' !== $type &&
                ($refClass->hasMethod($nm = 'get'.$uname) ||
                    $refClass->hasMethod($nm = 'is'.$uname)) &&
                ($refMethod = $refClass->getMethod($nm)) &&
                $refMethod->getName() === $nm &&
                !$refMethod->isPrivate() &&
                !$refMethod->isStatic();

            if ($read || $write) {
                $properties[$name] = $read << 0 |
                    ('g' === $nm[0]) << 1 |
                    $refMethod->returnsReference() << 2 |
                    $write << 3;
            }
        }

        foreach ($refClass->getTraits() as $trait) {
            $properties += self::getMagicProperties($trait->getName());
        }

        if ($parent = get_parent_class($class)) {
            $properties += self::getMagicProperties($parent);
        }

        return $properties;
    }

    /**
     * Finds the best suggestion (for 8-bit encoding).
     *
     * @param (\ReflectionFunctionAbstract|\ReflectionParameter|\ReflectionClass|\ReflectionProperty|string)[]
     *
     * @internal
     */
    public static function getSuggestion(
        array $possibilities = [],
        $value = null
    ): ?string {
        $best = null;
        $minimum = (\strlen($value) / 4 + 1) * 10 + .1;
        $normal = \preg_replace(
            $re = '#^(get|set|has|is|add)(?=[A-Z])#',
            '',
            $value
        );

        foreach (\array_unique($possibilities, \SORT_REGULAR) as $item) {
            $item = $item instanceof \Reflector ? $item->getName(): $item;

            if ($item !== $value && (($length = \levenshtein($item, $value, 10, 11, 10)) < $minimum
                || ($length = \levenshtein(
                    \preg_replace($re, '', $item),
                    $normal,
                    10,
                    11,
                    10
                ) + 20) < $minimum)
            ) {
                $minimum = $length;
                $best = $item;
            }
        }

        return $best;
    }

    private static function parseFullDoc(?\ReflectionClass $class = null, ?string $pattern = null)
    {
        do {
            $document[] = $class->getDocComment();
            $traits = $class->getTraits();

            while ($trait = array_pop($traits)) {
                $document[] = $trait->getDocComment();
                $traits += $trait->getTraits();
            }
        } while ($class = $class->getParentClass());

        return \preg_match_all(
            $pattern,
            \implode($document),
            $matches
        ) ? $matches[1] : [];
    }

    /**
     * Checks if the public non-static property exists.
     *
     * @return bool|string returns 'event' if the property exists and has event like name
     *
     * @internal
     */
    public static function hasProperty(
        string $class = null,
        string $name = null
    ) {
        static $cache;

        $property = &$cache[$class][$name];

        if (null === $property) {
            $property = false;

            try {
                $refProperty = new \ReflectionProperty($class, $name);

                if ($refProperty->isPublic() && !$refProperty->isStatic()) {
                    $property = $name >= 'onA' &&
                        $name < 'on_' ? 'event' : true;
                }
            } catch (\ReflectionException $ex) {
            }
        }

        return $property;
    }
}
