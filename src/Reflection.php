<?php

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Traits\StaticClassTrait;

/**
 * PHP reflection helpers.
 */
class Reflection
{
    use StaticClassTrait;

    private static $builtinTypes = [
        'array'    => 0x1,
        'bool'     => 0x1,
        'callable' => 0x1,
        'float'    => 0x1,
        'int'      => 0x1,
        'iterable' => 0x1,
        'object'   => 0x1,
        'string'   => 0x1,
        'void'     => 0x1,
    ];

    /**
     *
     * @param string
     *
     * @return bool
     */
    public static function isBuiltinType(string $type = null): bool
    {
        return isset(self::$builtinTypes[strtolower($type)]);
    }

    /**
     *
     * @return string|null
     */
    public static function getReturnType(\ReflectionFunctionAbstract $func)
    {
        return \PHP_VERSION_ID >= 70000 && $func->hasReturnType() ?
            static::normalizeType((string) $func->getReturnType(), $func) :
            null;
    }

    /**
     *
     * @return string|null
     */
    public static function getParameterType(\ReflectionParameter $param)
    {
        if (PHP_VERSION_ID >= 70000) {
            return $param->hasType()
            ? static::normalizeType((string) $param->getType(), $param)
            : null;
        } elseif ($param->isArray() || $param->isCallable()) {
            return $param->isArray() ? 'array' : 'callable';
        } else {
            try {
                return ($ref = $param->getClass()) ? $ref->getName(): null;
            } catch (\ReflectionException $e) {
                if (\preg_match('#Class (.+) does not exist#', $e->getMessage(), $m)) {
                    return $m[1];
                }

                throw $e;
            }
        }
    }

    private static function normalizeType(string $type = null, \Reflector $reflection = null): string
    {
        $lower = \strtolower($type);

        if ('self' === $lower) {
            return $reflection->getDeclaringClass()->getName();
        } elseif ('parent' === $lower && $reflection->getDeclaringClass()->getParentClass()) {
            return $reflection->getDeclaringClass()->getParentClass()->getName();
        } else {
            return $type;
        }
    }

    /**
     *
     * @return mixed
     *
     * @throws \ReflectionException when default value is not available or resolvable
     */
    public static function getParameterDefaultValue(\ReflectionParameter $param)
    {
        if ($param->isDefaultValueConstant()) {
            $const = $orig = $param->getDefaultValueConstantName();
            $pair = explode('::', $const);

            if (isset($pair[1]) && 'self' === strtolower($pair[0])) {
                $pair[0] = $param->getDeclaringClass()->getName();
            }

            if (isset($pair[1]) && PHP_VERSION_ID >= 70100) {
                try {
                    $rcc = new \ReflectionClassConstant($pair[0], $pair[1]);
                } catch (\ReflectionException $e) {
                    $name = static::toString($param);
                    throw new \ReflectionException("Unable to resolve constant $orig used as default value of $name.", 0, $e);
                }

                return $rcc->getValue();
            }

            $const = implode('::', $pair);

            if (!defined($const)) {
                $const = substr((string) strrchr($const, '\\'), 1);
                if (isset($pair[1]) || !defined($const)) {
                    $name = static::toString($param);
                    throw new \ReflectionException("Unable to resolve constant $orig used as default value of $name.");
                }
            }

            return constant($const);
        }

        return $param->getDefaultValue();
    }

    /**
     * Returns declaring class or trait.
     *
     * @return \ReflectionClass
     */
    public static function getPropertyDeclaringClass(\ReflectionProperty $prop)
    {
        foreach ($prop->getDeclaringClass()->getTraits() as $trait) {
            if ($trait->hasProperty($prop->getName())) {
                return static::getPropertyDeclaringClass($trait->getProperty($prop->getName()));
            }
        }

        return $prop->getDeclaringClass();
    }

    /**
     * Are documentation comments available?
     *
     * @return bool
     */
    public static function areCommentsAvailable()
    {
        static $res;

        return null === $res
        ? $res = (bool) (new \ReflectionMethod(__CLASS__, __METHOD__))->getDocComment()
        : $res;
    }

    /**
     *
     * @return string
     */
    public static function toString(\Reflector $ref)
    {
        if ($ref instanceof \ReflectionClass) {
            return $ref->getName();
        } elseif ($ref instanceof \ReflectionMethod) {
            return $ref->getDeclaringClass()->getName().'::'.$ref->getName();
        } elseif ($ref instanceof \ReflectionFunction) {
            return $ref->getName();
        } elseif ($ref instanceof \ReflectionProperty) {
            return static::getPropertyDeclaringClass($ref)->getName().'::$'.$ref->getName();
        } elseif ($ref instanceof \ReflectionParameter) {
            return '$'.$ref->getName().' in '.static::toString($ref->getDeclaringFunction()).'()';
        } else {
            throw new \Exen\Support\Exception\InvalidArgumentException();
        }
    }

    /**
     * Expands class name into full name.
     *
     * @param string
     *
     * @return string full name
     *
     * @throws \Exen\Support\Exception\InvalidArgumentException
     */
    public static function expandClassName($name, \ReflectionClass $rc)
    {
        $lower = strtolower($name);
        if (empty($name)) {
            throw new \Exen\Support\Exception\InvalidArgumentException('Class name must not be empty.');
        } elseif (isset(self::$builtinTypes[$lower])) {
            return $lower;
        } elseif ('self' === $lower) {
            return $rc->getName();
        } elseif ('\\' === $name[0]) { // fully qualified name
            return ltrim($name, '\\');
        }

        $uses = static::getUseStatements($rc);
        $parts = explode('\\', $name, 2);
        if (isset($uses[$parts[0]])) {
            $parts[0] = $uses[$parts[0]];

            return implode('\\', $parts);
        } elseif ($rc->inNamespace()) {
            return $rc->getNamespaceName().'\\'.$name;
        } else {
            return $name;
        }
    }

    /**
     *
     * @return array of [alias => class]
     */
    public static function getUseStatements(\ReflectionClass $class)
    {
        static $cache = [];
        if (!isset($cache[$name = $class->getName()])) {
            if ($class->isInternal()) {
                $cache[$name] = [];
            } else {
                $code = file_get_contents($class->getFileName());
                $cache = static::parseUseStatements($code, $name) + $cache;
            }
        }

        return $cache[$name];
    }

    /**
     * Parses PHP code.
     *
     * @param string
     *
     * @return array of [class => [alias => class, ...]]
     */
    private static function parseUseStatements($code, $forClass = null)
    {
        $tokens = token_get_all($code);
        $namespace = $class = $classLevel = $level = null;
        $res = $uses = [];

        while ($token = current($tokens)) {
            next($tokens);
            switch (is_array($token) ? $token[0] : $token) {
                case T_NAMESPACE:
                    $namespace = ltrim(static::fetch($tokens, [T_STRING, T_NS_SEPARATOR]).'\\', '\\');
                    $uses = [];
                    break;

                case T_CLASS:
                case T_INTERFACE:
                case T_TRAIT:
                    if ($name = static::fetch($tokens, T_STRING)) {
                        $class = $namespace.$name;
                        $classLevel = $level + 1;
                        $res[$class] = $uses;
                        if ($class === $forClass) {
                            return $res;
                        }
                    }
                    break;

                case T_USE:
                    while (!$class && ($name = static::fetch($tokens, [T_STRING, T_NS_SEPARATOR]))) {
                        $name = ltrim($name, '\\');
                        if (static::fetch($tokens, '{')) {
                            while ($suffix = static::fetch($tokens, [T_STRING, T_NS_SEPARATOR])) {
                                if (static::fetch($tokens, T_AS)) {
                                    $uses[static::fetch($tokens, T_STRING)] = $name.$suffix;
                                } else {
                                    $tmp = explode('\\', $suffix);
                                    $uses[end($tmp)] = $name.$suffix;
                                }
                                if (!static::fetch($tokens, ',')) {
                                    break;
                                }
                            }
                        } elseif (static::fetch($tokens, T_AS)) {
                            $uses[static::fetch($tokens, T_STRING)] = $name;
                        } else {
                            $tmp = explode('\\', $name);
                            $uses[end($tmp)] = $name;
                        }
                        if (!static::fetch($tokens, ',')) {
                            break;
                        }
                    }
                    break;

                case T_CURLY_OPEN:
                case T_DOLLAR_OPEN_CURLY_BRACES:
                case '{':
                    $level++;
                    break;

                case '}':
                    if ($level === $classLevel) {
                        $class = $classLevel = null;
                    }
                    --$level;
            }
        }

        return $res;
    }

    private static function fetch(&$tokens, $take)
    {
        $res = null;

        while ($token = current($tokens)) {
            list($token, $s) = is_array($token) ? $token : [$token, $token];
            if (in_array($token, (array) $take, true)) {
                $res .= $s;
            } elseif (!in_array($token, [T_DOC_COMMENT, T_WHITESPACE, T_COMMENT], true)) {
                break;
            }
            next($tokens);
        }

        return $res;
    }
}
