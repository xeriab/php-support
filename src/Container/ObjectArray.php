<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class ObjectArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'object';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
