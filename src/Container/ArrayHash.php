<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class ArrayHash extends \stdClass implements \ArrayAccess, \Countable, \IteratorAggregate
{
    /**
     *
     * @param array to wrap
     * @param bool
     *
     * @return static
     */
    public static function from(array $array = [], bool $recursive = true): ?object
    {
        $object = new static();

        foreach ($array as $key => $value) {
            if ($recursive && \is_array($value)) {
                $object->$key = static::from($value, true);
            } else {
                $object->$key = $value;
            }
        }

        return $object;
    }

    /**
     * Returns an iterator over all items.
     *
     * @return \RecursiveArrayIterator
     */
    public function getIterator()
    {
        return new \RecursiveArrayIterator((array) $this);
    }

    /**
     * Returns items count.
     *
     * @return int
     */
    public function count()
    {
        return \count((array) $this);
    }

    /**
     * Replaces or appends a item.
     */
    public function offsetSet($key, $value)
    {
        if (!\is_scalar($key)) { // prevents null
            throw new Exception\InvalidArgumentException(sprintf('Key must be either a string or an integer, %s given.', gettype($key)));
        }

        $this->$key = $value;
    }

    /**
     * Returns a item.
     *
     * @return mixed
     */
    public function offsetGet($key)
    {
        return $this->$key;
    }

    /**
     * Determines whether a item exists.
     *
     * @return bool
     */
    public function offsetExists($key)
    {
        return isset($this->$key);
    }

    /**
     * Removes the element from this list.
     */
    public function offsetUnset($key)
    {
        unset($this->$key);
    }
}
