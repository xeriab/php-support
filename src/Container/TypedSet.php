<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Container\Traits\SetTrait;

/**
 * Class TypedSet
 *
 * @since 0.1
 */
class TypedSet extends TypedCollection
{
    use SetTrait;
}
