<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class DoubleArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'double';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
