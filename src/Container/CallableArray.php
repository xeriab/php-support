<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class CallableArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'callable';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
