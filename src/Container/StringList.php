<?php

declare(strict_types=1);

namespace Exen\Support\Container;

final class StringList extends AbstractTypedArray
{
    /**
     * {@inheritDoc}
     */
    const TYPE = 'string';

    /**
     * {@inheritDoc}
     */
    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }

    /**
     * {@inheritDoc}
     */
    final public function toString(): string
    {
        return '[' . \implode(', ', $this->toArray()) . ']';
    }

    /**
     * {@inheritDoc}
     */
    final public function __toString(): string
    {
        return $this->toString();
    }
}
