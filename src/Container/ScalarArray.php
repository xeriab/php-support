<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class ScalarArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'scalar';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
