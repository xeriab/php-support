<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class BoolArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'bool';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
