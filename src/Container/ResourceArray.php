<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class ResourceArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'resource';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
