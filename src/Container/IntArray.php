<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class IntArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'int';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
