<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Exception\NotValidObjectTypeException;
use Exen\Support\Exception\UndefinedOffsetException;

use Traversable;

/**
 * Class TypedCollection
 *
 * @since 0.1
 */
class TypedCollection extends Collection
{
    /**
     *
     * @var string
     */
    private $objectType = null;

    /**
     *
     * @param string $objectType
     */
    public function __construct(string $objectType = null)
    {
        $this->setObjectType($objectType);
    }

    /**
     *
     * @return string
     */
    public function getObjectType(): string
    {
        return $this->objectType;
    }

    /**
     *
     * @param string $objectType
     *
     * @return $this
     */
    protected function setObjectType(string $objectType = null): self
    {
        $this->objectType = $objectType;

        return $this;
    }

    public function offsetSet($offset, $value)
    {
        $this->guardAgainstNotValidObjectType($value);

        parent::offsetSet($offset, $value);
    }

    public function offsetGet($offset)
    {
        if (!isset($this->collection[$offset])) {
            throw new UndefinedOffsetException();
        }

        return parent::offsetGet($offset);
    }

    /**
     *
     * @param $value
     *
     * @throws NotValidObjectTypeException
     */
    private function guardAgainstNotValidObjectType($value)
    {
        if (!$value instanceof $this->objectType) {
            throw new NotValidObjectTypeException();
        }
    }
}
