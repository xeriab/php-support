<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Traits\SmartObjectTrait;

class ArrayList implements \ArrayAccess, \Countable, \IteratorAggregate
{
    use SmartObjectTrait;

    private $list = [];

    /**
     * Returns an iterator over all items.
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->list);
    }

    /**
     * Returns items count.
     *
     * @return int
     */
    public function count()
    {
        return count($this->list);
    }

    /**
     * Replaces or appends a item.
     *
     * @param int|null
     * @param mixed
     *
     * @throws \Exen\Support\Exception\OutOfRangeException
     */
    public function offsetSet($index, $value)
    {
        if (null !== $index && !is_int($index)) {
            trigger_error('Index is not integer.', E_USER_NOTICE);
        }

        if (null === $index) {
            $this->list[] = $value;
        } elseif ($index < 0 || $index >= count($this->list)) {
            throw new \Exen\Support\Exception\OutOfRangeException('Offset invalid or out of range');
        } else {
            $this->list[(int) $index] = $value;
        }
    }

    /**
     * Returns a item.
     *
     * @param int
     *
     * @return mixed
     *
     * @throws \Exen\Support\Exception\OutOfRangeException
     */
    public function offsetGet($index)
    {
        if (!is_int($index)) {
            trigger_error('Index is not integer.', E_USER_NOTICE);
        }

        if ($index < 0 || $index >= count($this->list)) {
            throw new \Exen\Support\Exception\OutOfRangeException('Offset invalid or out of range');
        }

        return $this->list[(int) $index];
    }

    /**
     * Determines whether a item exists.
     *
     * @param int
     *
     * @return bool
     */
    public function offsetExists($index)
    {
        return $index >= 0 && $index < count($this->list);
    }

    /**
     * Removes the element at the specified position in this list.
     *
     * @param int
     *
     * @throws \Exen\Support\Exception\OutOfRangeException
     */
    public function offsetUnset($index)
    {
        if (!is_int($index)) {
            trigger_error('Index is not integer.', E_USER_NOTICE);
        }

        if ($index < 0 || $index >= count($this->list)) {
            throw new \Exen\Support\Exception\OutOfRangeException('Offset invalid or out of range');
        }

        array_splice($this->list, (int) $index, 1);
    }

    /**
     * Prepends a item.
     *
     * @param mixed
     */
    public function prepend($value)
    {
        $first = array_slice($this->list, 0, 1);
        $this->offsetSet(0, $value);
        array_splice($this->list, 1, 0, $first);
    }
}
