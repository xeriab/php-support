<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class StringArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'string';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
