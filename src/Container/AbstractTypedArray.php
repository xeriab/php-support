<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Container\Interfaces\TypedArrayInterface;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Utils;

use ArrayObject;
use ArrayAccess;
use InvalidArgumentException;

abstract class AbstractTypedArray extends ArrayObject implements TypedArrayInterface
{
    use MacroableTrait;

    /**
     * Types supported by this class.
     */
    const ALLOWED_TYPES = [
        'array'    => '\\Exen\\Support\\Lang::isArray',
        'bool'     => '\\Exen\\Support\\Lang::isBoolean',
        'callable' => '\\Exen\\Support\\Lang::isCallable',
        'char'     => '\\Exen\\Support\\Lang::isChar',
        'double'   => '\\Exen\\Support\\Lang::isDouble',
        'float'    => '\\Exen\\Support\\Lang::isFloat',
        'int'      => '\\Exen\\Support\\Lang::isInteger',
        'object'   => '\\Exen\\Support\\Lang::isObject',
        'resource' => '\\Exen\\Support\\Lang::isResource',
        'scalar'   => '\\Exen\\Support\\Lang::isScalar',
        'string'   => '\\Exen\\Support\\Lang::isString',
    ];

    /**
     * Current type.
     */
    const TYPE = null;

    /**
     * Types supported by this class.
     *
     * @var array
     */
    protected $allowedTypes = [
        'array'    => '\\Exen\\Support\\Lang::isArray',
        'bool'     => '\\Exen\\Support\\Lang::isBoolean',
        'callable' => '\\Exen\\Support\\Lang::isCallable',
        'char'     => '\\Exen\\Support\\Lang::isChar',
        'double'   => '\\Exen\\Support\\Lang::isDouble',
        'float'    => '\\Exen\\Support\\Lang::isFloat',
        'int'      => '\\Exen\\Support\\Lang::isInteger',
        'object'   => '\\Exen\\Support\\Lang::isObject',
        'resource' => '\\Exen\\Support\\Lang::isResource',
        'scalar'   => '\\Exen\\Support\\Lang::isScalar',
        'string'   => '\\Exen\\Support\\Lang::isString',
    ];

    /**
     * Current type.
     *
     * @var null|string
     */
    // protected $type = null;

    public function __construct(string $type = null, ...$paramerters)
    {
        if (Utils::sizeOf($paramerters) === 1) {
            if (Utils::isArray($paramerters[0])) {
                $paramerters = $paramerters[0];
            } else {
                $paramerters = [$paramerters[0]];
            }
        }

        $input = ($paramerters);

        // $this->type = (static::TYPE) ? static::TYPE : ($type) ?: $type;

        $this->validate($input, __METHOD__);

        if (!Utils::isAssociative($input)) {
            parent::__construct($input);
        }
    }

    /// PUBLIC METHODS

    public function offsetSet($index, $value)
    {
        $type     = $this->getType();
        $callback = $this->allowedTypes[$type];

        if (Utils::isArray($callback)) {
            $callback = $callback[0];
        }

        if ($value instanceof $type) {
            parent::offsetSet($index, $value);
            return;
        }

        if ($callback($value)) {
            parent::offsetSet($index, $value);
            return;
        }

        throw new InvalidArgumentException(
            static::class . ': Elements passed to ' . __METHOD__ .
            ' must be of the type ' . $type . '.'
        );
    }

    public function size(): int
    {
        return $this->count();
    }

    public function length(): int
    {
        return $this->size();
    }

    /**
     * Exports the object to an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->getArrayCopy();
    }

    /**
     * Exports the object to an array.
     *
     * @return array
     */
    public function asArray(): array
    {
        return (array) $this;
    }

    /**
     * Exports the object to an array.
     *
     * @return array
     */
    public function toPhpArray(): array
    {
        return $this->toArray();
    }

    /// PROTECTED METHODS

    protected function getType(): ?string
    {
        return (static::TYPE) ? static::TYPE : ($this->type) ?: $this->type;
    }

    /**
     * Validates type.
     *
     * @param array|null  $array
     * @param string|null $method
     *
     * @return void
     */
    protected function validate(?array &$array = [], ?string $method = null): void
    {
        $arrType = $this->getType();

        if (!Utils::isAssociative($array)) {
            if (\class_exists($arrType)) {
                $this->allowedTypes[$arrType] = function ($value) use ($arrType): bool {
                    if ($value instanceof $arrType) {
                        return true;
                    }

                    return false;
                };
            }

            if (Utils::isEmpty(static::ALLOWED_TYPES[$arrType])) {
                throw new InvalidArgumentException(
                    static::class . ': Elements passed to ' . $method ?: __METHOD__ .
                    ' are not supported.'
                );
            }

            // return call_user_func_array([$controller, $actionName], $args);

            $checkCallback = static::ALLOWED_TYPES[$arrType];

            if (Utils::isString($checkCallback)) {
                // $callback = \call_user_func($type, $value);
                $callback = $checkCallback;
            } elseif ($arrType === 'char' && Utils::isArray($checkCallback)) {
                // $callback = \call_user_func([$type[0], $type[1]], $value);
                $callback = static::class . '::' . $checkCallback[0];
            }

            if (Utils::sizeOf($array) > Utils::sizeOf(Utils::filter($array, $callback))) {
                throw new InvalidArgumentException(
                    static::class . ': Elements passed to ' . $method ?: __METHOD__ .
                    ' must be of the type ' . $arrType . '.'
                );
            }
        }
    }

    /// STATIC METHODS

    /**
     * {@inheritDoc}
     */
    public static function create(...$paramerters): self
    {
        return new static(...$paramerters);
    }

    /// MAGIC METHODS

    public function __get($name = null)
    {
        if ($name === 'length') {
            return $this->size();
        }

        return $this->{$name};
    }
}
