<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Countable;
use Exception;
use ArrayAccess;
use Traversable;
use ArrayIterator;
use CachingIterator;
use IteratorAggregate;
use Exen\Support\Lang;
use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Serializable;
use Exen\Support\Interfaces\Hashable;
use Exen\Support\Json\Json;
use Exen\Support\Yaml\Yaml;
use Exen\Support\Arr;
use Exen\Support\Traits\HashCodeTrait;
use Exen\Support\Traits\MacroableTrait;

/**
 * A generic LIFO Stack.
 */
class Stack implements
    Arrayable,
    Jsonable,
    JsonSerializable,
    // ArrayAccess,
    Countable,
    IteratorAggregate,
    Serializable,
    Hashable
{
    use MacroableTrait;
    use HashCodeTrait;

    /**
     * The data container.
     *
     * @var array
     */
    private $data = null;

    /**
     * The type of the values
     * for this Stack.
     *
     * @var mixed
     */
    private $type = null;

    /**
     * Creates a new Stack.
     *
     * @param string $type
     */
    public function __construct(string $type = null)
    {
        $this->data = [];
        $this->type = $type;
    }

    /**
     * Clears the data values.
     *
     * @return void
     */
    public function clear(): void
    {
        $this->data = [];
    }

    /**
     * Checks if the stack is empty.
     *
     * @return bool
     */
    public function isEmpty(): bool
    {
        return $this->count() === 0;
    }

    /**
     * Returns whether the pair is not empty.
     *
     * @return boolean
     */
    public function isNotEmpty(): bool
    {
        return (!$this->isEmpty());
    }

    /**
     * Gets the element at
     * the end of the Stack.
     *
     * @return mixed
     */
    public function peek()
    {
        return $this->data[$this->count() - 1];
    }

    /**
     * Pops the element at
     * the end of the stack.
     *
     * @return mixed
     */
    public function pop()
    {
        return \array_pop($this->data);
    }

    /**
     * Adds a new element at
     * the end of the Stack.
     *
     * @param mixed $value
     *
     * @throws \InvalidArgumentException
     *
     * @return mixed
     */
    public function push($value = null)
    {
        $message = \sprintf(
            'The type specified for this collection is %s, you cannot pass a value of type %s',
            $this->type,
            \gettype($value)
        );

        Lang::valueIsOfType($value, $this->type, $message);

        $this->data[] = $value;

        return $value;
    }

    /*
     * --------------------------------------------------------------
     * Countable interface
     * --------------------------------------------------------------
     */

    /**
     * Returns the length of the Stack.
     *
     * @return int
     */
    public function count(): int
    {
        return \count($this->data);
    }

    /*
     * --------------------------------------------------------------
     * ArrayAccess interface
     * --------------------------------------------------------------
     */

    // public function offsetSet($offset, $value)
    // {
    //     if (\is_null($offset)) {
    //         $this->data[] = $value;
    //     } else {
    //         $this->data[$offset] = $value;
    //     }
    // }

    // public function offsetExists($offset)
    // {
    //     return isset($this->data[$offset]);
    // }

    // public function offsetUnset($offset)
    // {
    //     unset($this->data[$offset]);
    // }

    // public function offsetGet($offset)
    // {
    //     return isset($this->data[$offset]) ? $this->data[$offset] : null;
    // }

    /*
     * --------------------------------------------------------------
     * IteratorAggregate interface
     * --------------------------------------------------------------
     */

    /**
     * Get an iterator for the stored data
     *
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->data);
    }

    public function toArray(array $options = null, int $depth = null)
    {
        return \array_map(
            function ($value) use ($options, $depth) {
                return $value instanceof Arrayable ?
                    $value->toArray($options, $depth) :
                    $value;
            },
            $this->data
        );
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return \array_map(
            function ($value) {
                if ($value instanceof JsonSerializable) {
                    return $value->jsonSerialize();
                } elseif ($value instanceof Jsonable) {
                    return \json_decode($value->toJson(), true);
                } elseif ($value instanceof Arrayable) {
                    return $value->toArray();
                }

                return $value;
            },
            $this->data
        );
    }

    /**
     * {inherit doc}
     */
    public function toJson(int $options = Json::PRETTY)
    {
        return Json::encode($this->toArray(), $options);
    }

    /**
     * {inherit doc}
     */
    public function toYaml(int $options = Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK)
    {
        return Yaml::encode($this->toArray(), $options);
    }

    /**
     * {inherit doc}
     */
    public function serialize()
    {
        return @\serialize($this->data);
    }

    /**
     * {inherit doc}
     */
    public function unserialize($serialized)
    {
        $unserialized = (array) @\unserialize($serialized);
        $this->data = $unserialized;
    }
}
