<?php

declare(strict_types=1);

namespace Exen\Support\Container\Interfaces;

use Countable;
use ArrayAccess;
use IteratorAggregate;

use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Hashable;

/**
 * Dictionary Interface.
 */
interface DictionaryInterface extends
    Arrayable,
    Jsonable,
    JsonSerializable,
    ArrayAccess,
    Countable,
    Hashable,
    IteratorAggregate
{
    /**
     * Create a new collection instance if the value isn't one already.
     *
     * @param mixed $items
     *
     * @return static
     */
    public static function make($items = []);

    public function add($value): void;

    public function get(int $offset);

    public function remove(int $offset): void;

    public function update(int $offset, $value): bool;
}
