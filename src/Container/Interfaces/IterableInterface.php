<?php

declare(strict_types=1);

namespace Exen\Support\Container\Interfaces;

interface IterableInterface
{
    public function forEach(callable $callback): void;
}
