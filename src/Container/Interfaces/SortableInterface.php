<?php

declare(strict_types=1);

namespace Exen\Support\Container\Interfaces;

use Exen\Support\Container\BaseCollection;

interface SortableInterface
{
    public function sort(callable $callback): ?BaseCollection;
}
