<?php

declare(strict_types=1);

namespace Exen\Support\Container\Interfaces;

use ArrayAccess;
use Countable;
use IteratorAggregate;

// use Serializable;

interface TypedArrayInterface extends Countable, IteratorAggregate, ArrayAccess
{
}
