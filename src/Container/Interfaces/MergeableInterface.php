<?php

declare(strict_types=1);

namespace Exen\Support\Container\Interfaces;

use Exen\Support\Container\BaseCollection;

interface MergeableInterface
{
    public function merge(BaseCollection $collection): BaseCollection;
}
