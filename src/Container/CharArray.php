<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class CharArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'char';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
