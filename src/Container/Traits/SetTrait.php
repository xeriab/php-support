<?php

declare(strict_types=1);

namespace Exen\Support\Container\Traits;

trait SetTrait
{
    public function offsetSet($offset, $value)
    {
        if (!\in_array($value, $this->collection, true)) {
            parent::offsetSet($offset, $value);
        }
    }
}
