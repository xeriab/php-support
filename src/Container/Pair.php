<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use LogicException;
use RuntimeException;
use stdClass;

use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Serializable;
use Exen\Support\Interfaces\Yamlable;
use Exen\Support\Interfaces\YamlSerializable;
use Exen\Support\Interfaces\Objectable;
use Exen\Support\Json\Json;
use Exen\Support\Yaml\Yaml;
use Exen\Support\Lang;

/**
 * Class Pair
 *
 * @since 0.1
 */
class Pair implements
    Jsonable,
    Arrayable,
    Yamlable,
    JsonSerializable,
    YamlSerializable,
    Serializable
{
    /**
     * Pair key.
     *
     * @var null|mixed
     */
    protected $key = null;

    /**
     * Pair value.
     *
     * @var null|mixed
     */
    protected $value = null;

    /**
     * Creates a new instance using a given key and value.
     *
     * @param mixed $key   The key.
     * @param mixed $value The value.
     */
    public function __construct($key = null, $value = null)
    {
        $this->key = $key;
        $this->value = $value;
    }

    /**
     * Returns the key of the pair.
     *
     * @return mixed
     */
    public function key()
    {
        return $this->key;
    }

    /**
     * Returns the key of the pair.
     *
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * Returns the value of the pair.
     *
     * @return mixed
     */
    public function value()
    {
        return $this->value;
    }

    /**
     * Returns the value of the pair.
     *
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value of the value property.
     *
     * @param mixed $value
     *
     * @return void
     */
    public function setValue($value = null): void
    {
        $this->value = $value;
    }

    /**
     * Returns whether the pair is empty.
     *
     * @return boolean
     */
    public function isEmpty(): bool
    {
        return ($this->key === null and $this->value === null);
    }

    /**
     * Returns whether the pair is not empty.
     *
     * @return boolean
     */
    public function isNotEmpty(): bool
    {
        return (!$this->isEmpty());
    }

    /**
     * Removes all values from the pair.
     *
     * @return void
     */
    public function clear(): void
    {
        // $this->key = null;
        // $this->value = null;

        unset($this->key);
        unset($this->value);
    }

    /**
     * Returns a shallow copy of the pair.
     *
     * @return \Exen\Support\Container\Pair
     */
    public function copy(): self
    {
        $copy = clone $this;
        return $copy;
    }

    /**
     * {inherit doc}
     */
    public function toArray(array $options = null, int $depth = null): array
    {
        if ($this->isNotEmpty()) {
            return \array_map(
                function ($value) use ($options, $depth) {
                    return $value instanceof Arrayable ?
                        $value->toArray($options, $depth) :
                        $value;
                },
                [$this->key => $this->value]
            );
        }

        return [];
    }

    /**
     * {inherit doc}
     */
    public function toObject(int $options = 0): ?Object
    {
        if ($this->isNotEmpty()) {
            $object = new stdClass;
            $object->key = $this->key;
            $object->value = $this->value;

            return $object;
        }

        return null;
    }

    /**
     * {inherit doc}
     */
    public function serialize()
    {
        return @\serialize((array) $this->toObject());
    }

    /**
     * {inherit doc}
     */
    public function unserialize($serialized)
    {
        $data = (array) @\unserialize($serialized);

        foreach ($data as $key => &$value) {
            $this->$key = $data[$key];
        }
    }

    /**
     * {inherit doc}
     */
    public function jsonSerialize(): array
    {
        if ($this->isNotEmpty()) {
            return \array_map(
                function ($value) {
                    if ($value instanceof JsonSerializable) {
                        return $value->jsonSerialize();
                    } elseif ($value instanceof Jsonable) {
                        return \json_decode($value->toJson(), true);
                    } elseif ($value instanceof Arrayable) {
                        return $value->toArray();
                    }

                    return $value;
                },
                [$this->key => $this->value]
            );
        }

        return [];
    }

    /**
     * {inherit doc}
     */
    public function yamlSerialize(): array
    {
        if ($this->isNotEmpty()) {
            return \array_map(
                function ($value) {
                    if ($value instanceof YamlSerializable) {
                        return $value->yamlSerialize();
                    } elseif ($value instanceof Yamlable) {
                        return Yaml::decode($value->toYaml());
                    } elseif ($value instanceof Arrayable) {
                        return $value->toArray();
                    }

                    return $value;
                },
                [$this->key => $this->value]
            );
        }

        return [];
    }

    /**
     * {inherit doc}
     */
    public function toJson(int $options = 0): string
    {
        return Json::encode($this, $options);
    }

    /**
     * {inherit doc}
     */
    public function toYaml(int $options = 0): string
    {
        return Yaml::encode($this, $options);
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function toString(): string
    {
        // return $this->toJson();

        // return $this->toJson(Json::PRETTY);

        return Lang::sprintf(
            '<%(className)s \'%(key)s\', \'%(value)s\'>',
            [
                'className' => Lang::parseClassName(static::class)['classname'],
                'key'       => (string) $this->key,
                'value'     => (string) $this->value
            ]
        );

//         $result = <<<REPR
// %(className)s
// (
//     [key] => %(key)s
//     [value] => %(value)s
// )
// REPR;

//         return Lang::sprintf(
//             $result, [
//                 'className' => Lang::parseClassName(static::class)['classname'],
//                 'key'       => (string) $this->key,
//                 'value'     => (string) $this->value
//             ]
//         );
    }

    // /**
    //  *
    //  * @param mixed $key
    //  * @param mixed $value
    //  *
    //  * @return mixed
    //  * @throws \RuntimeException
    //  */
    // public function __get($key)
    // {
    //     if (\property_exists($this, $key)) {
    //         return $this->$key;
    //     } elseif (isset($this->$key)) {
    //         return $this->$key;
    //     } else {
    //         throw new RuntimeException(
    //             'The property "'.$key.'" of class "'.
    //             static::class.'" does not exist.'
    //         );
    //     }
    // }

    /**
     * Allows access to the key property by its value.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name)
    {
        // if ($name === $this->key) {
        //     return $this->value;
        // }

        if (\property_exists($this, $name)) {
            return $this->value;
        } elseif ($name === $this->key) {
            return $this->value;
        } else {
            throw new RuntimeException(
                'The property "'.$name.'" of class "'.
                static::class.'" does not exist.'
            );
        }
    }

    /**
     *
     * @param mixed $key
     * @param mixed $value
     *
     * @return \Exen\Support\Container\Pair
     * @throws \RuntimeException
     */
    public function __set($key, $value): self
    {
        if (\property_exists($this, $key)) {
            $this->$key = $value;
        } elseif ($key === $this->key) {
            $this->value = $value;
        } else {
            throw new RuntimeException(
                'The property "'.$key.'" of class "'.
                static::class.'" does not exist.'
            );
        }

        return $this;
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
