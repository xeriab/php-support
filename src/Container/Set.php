<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Container\Traits\SetTrait;

/**
 * Class Set
 *
 * @since 0.1
 */
class Set extends Collection
{
    use SetTrait;
}
