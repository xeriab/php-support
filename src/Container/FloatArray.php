<?php

declare(strict_types=1);

namespace Exen\Support\Container;

class FloatArray extends AbstractTypedArray
{
    /**
     * Current type.
     */
    const TYPE = 'float';

    final public function __construct(...$paramerters)
    {
        parent::__construct(null, ...$paramerters);
    }
}
