<?php

declare(strict_types=1);

namespace Exen\Support\Container;

use Exen\Support\Arr;
use Exen\Support\Traits\SmartObjectTrait;
use Exen\Support\Interfaces\JsonSerializable;

class ArrObject implements \ArrayAccess, \Countable, \IteratorAggregate, JsonSerializable
{
    use SmartObjectTrait;

    /**
     * @var array
     */
    private $items = [];

    /**
     * Constructor.
     *
     * @param mixed $items
     * @param mixed $defaults
     */
    public function __construct($items = [], $defaults = [])
    {
        $this->items = Arr::merge((array) $defaults, (array) $items);
    }

    /**
     * Checks if the given key exists.
     *
     * @param  string $key
     * @return bool
     */
    public function has($key)
    {
        return Arr::has($this->items, $key);
    }

    /**
     * Gets a value by key.
     *
     * @param  string $key
     * @param  mixed  $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return Arr::get($this->items, $key, $default);
    }

    /**
     * Sets a value.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return self
     */
    public function set($key, $value)
    {
        Arr::set($this->items, $key, $value);

        return $this;
    }

    /**
     * Removes one or more items.
     *
     * @param  array|string $keys
     * @return self
     */
    public function remove($keys)
    {
        Arr::rem($this->items, $keys);

        return $this;
    }

    /**
     * Push value to the end of array.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return self
     */
    public function push($key, $value)
    {
        $items = $this->get($key);
        $items[] = $value;

        return $this->set($key, $items);
    }

    /**
     * Removes a value from array.
     *
     * @param  string $key
     * @param  mixed  $value
     * @return self
     */
    public function pull($key, $value)
    {
        $items = $this->get($key);

        Arr::pull($items, $value);

        return $this->set($key, $items);
    }

    /**
     * Merges a items from another array.
     *
     * @param  mixed $items
     * @param  bool  $replace
     * @return self
     */
    public function merge($items, $replace = false)
    {
        $this->items = Arr::merge($this->items, $items, $replace);

        return $this;
    }

    /**
     * Extracts config items.
     *
     * @param  array $keys
     * @param  bool  $include
     * @return array
     */
    public function extract($keys, $include = true)
    {
        return Arr::extract($this->items, $keys, $include);
    }

    /**
     * Gets the value count.
     *
     * @return int
     */
    public function count()
    {
        return \count($this->items);
    }

    /**
     * Gets the keys as array.
     *
     * @return array
     */
    public function keys()
    {
        return \array_keys($this->items);
    }

    /**
     * Gets the items as a numerically indexed array.
     *
     * @return array
     */
    public function items()
    {
        return \array_values($this->items);
    }

    /**
     * Gets the items as a plain array.
     *
     * @return array
     */
    public function toArray()
    {
        return $this->items;
    }

    /**
     * Implements JsonSerializable interface.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return $this->items;
    }

    /**
     * Implements ArrayAccess interface.
     *
     * @see has()
     */
    public function offsetExists($key)
    {
        return $this->has($key);
    }

    /**
     * Implements ArrayAccess interface.
     *
     * @see get()
     */
    public function offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * Implements ArrayAccess interface.
     *
     * @see set()
     */
    public function offsetSet($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Implements ArrayAccess interface.
     *
     * @see remove()
     */
    public function offsetUnset($key)
    {
        $this->remove($key);
    }

    /**
     *
     * @return \ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->items);
    }
}
