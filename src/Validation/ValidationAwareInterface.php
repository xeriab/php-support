<?php

declare(strict_types=1);

namespace Exen\Support\Validation;

/**
 * Defines a common interface for objects that can create validation rules for fields.
 *
 * @package Exen\Support\Validation
 * @author  Fuel Development Team
 * @since   2.0
 */
interface ValidationAwareInterface
{

    /**
     * Should populate the given validator with the needed rules.
     * If the validator is null one should be created.
     *
     * @param Validator $validator
     *
     * @return Validator
     *
     * @since 2.0
     */
    public function populateValidator(Validator $validator);
}
