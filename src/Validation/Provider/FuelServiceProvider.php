<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Provider;

use Fuel\Dependency\ServiceProvider;

/**
 * FuelServiceProvider implementation for Validation
 *
 * @package Exen\Support\Validation
 * @author  Fuel Development Team
 * @since   2.0
 */
class FuelServiceProvider extends ServiceProvider
{
    public $provides = array('validator', 'validation.ruleprovider.array');

    public function provide()
    {
        $this->register('validator', function ($dic) {
            return $dic->resolve('Exen\Support\Validation\Validator');
        });

        $this->register('validation.ruleprovider.array', function ($dic) {
            return $dic->resolve('Exen\Support\Validation\RuleProvider\FromArray');
        });
    }
}
