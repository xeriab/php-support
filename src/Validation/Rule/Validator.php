<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Rule;

use Exen\Support\Validation\AbstractRule;
use Exen\Support\Validation\ValidatableInterface;
use InvalidArgumentException;

/**
 * Allows validation of nested data structures.
 *
 * @package Exen\Support\Validation\Rule
 * @author  Fuel Development Team
 *
 * @since   2.0
 */
class Validator extends AbstractRule
{

    /**
     * {@inheritDoc}
     */
    protected $message = 'The child model is invalid.';

    /**
     * @param ValidatableInterface $params
     *
     * @return $this
     */
    public function setParameter($params)
    {
        if ($params !== null && ! $params instanceof ValidatableInterface) {
            throw new InvalidArgumentException('VAL-009: Provided parameter does not implement ValidatableInterface');
        }

        return parent::setParameter($params);
    }

    /**
     * {@inheritDoc}
     */
    public function validate($value, $field = null, $allFields = null)
    {
        /** @var ValidatableInterface $validator */
        $validator = $this->getParameter();

        return $validator->run($value);
    }
}
