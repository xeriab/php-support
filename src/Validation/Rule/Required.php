<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Rule;

use Exen\Support\Validation\AbstractRule;

/**
 * Checks that the given field exists
 *
 * @package Exen\Support\Validation\Rule
 * @author  Fuel Development Team
 *
 * @since   2.0
 */
class Required extends AbstractRule
{

    /**
     * Contains the rule failure message
     *
     * @var string
     */
    protected $message = 'The field is required and has not been specified.';

    protected $alwaysRun = true;

    /**
     * @param mixed $value
     * @param null  $field
     * @param null  $allFields
     *
     * @return bool
     *
     * @since 2.0
     */
    public function validate($value, $field = null, $allFields = null)
    {
        // Make sure the array key exists in the data
        // This check will only be performed if $field and $allFields are set. Else only the value passed will be tested
        if ($field === null or ($allFields !== null and ! isset($allFields[$field]))) {
            return false;
        }

        return ($value === 0 or $value === false or ! empty($value) or $value === '0');
    }
}
