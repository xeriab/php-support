<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Rule;

/**
 * Checks that the given field exists if the conditional field is passed and is not empty
 *
 * @package Exen\Support\Validation\Rule
 * @author  Fuel Development Team
 *
 * @since   2.0
 */
class RequiredIf extends Required
{

    /**
     * @param mixed $value
     * @param null  $field
     * @param array|null  $allFields
     *
     * @return bool
     *
     * @since 2.0
     */
    public function validate($value, $field = null, $allFields = null)
    {
        $requiredField = $this->getParameter();

        if ($allFields !== null and array_key_exists($requiredField, $allFields) and ! empty($allFields[$requiredField])) {
            return parent::validate($value, $field, $allFields);
        }

        return true;
    }
}
