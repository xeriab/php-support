<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Rule;

use Exen\Support\Validation\AbstractRule;

/**
 * Checks if a value is a valid number
 *
 * @package Exen\Support\Validation\Rule
 * @author  Fuel Development Team
 *
 * @since   2.0
 */
class Number extends AbstractRule
{

    /**
     * Contains the rule failure message
     *
     * @var string
     */
    protected $message = 'The field is not valid number.';

    /**
     * Performs validation on the given value
     *
     * @param mixed  $value Value to validate
     * @param string $field Unused by this rule
     * @param array  $allFields Unused by this rule
     *
     * @return bool
     *
     * @since 2.0
     */
    public function validate($value, $field = null, $allFields = null)
    {
        return is_numeric($value);
    }
}
