<?php

declare(strict_types=1);

namespace Exen\Support\Validation\Rule;

use Exen\Support\Validation\AbstractRule;

/**
 * Validates that a field is a valid IP address. Returns true for both IPv4 and v6.
 *
 * @package Exen\Support\Validation\Rule
 * @author  Fuel Development Team
 *
 * @since 2.0
 */
class Mac extends AbstractRule
{

    /**
     * Contains the rule failure message
     *
     * @var string
     */
    protected $message = 'The field is not a valid MAC address.';

    /**
     * Returns true if the given value is a valid MAC address
     *
     * @param mixed  $value Value to validate
     * @param string $field Unused by this rule
     * @param array  $allFields Unused by this rule
     *
     * @return bool
     *
     * @since 2.0
     */
    public function validate($value, $field = null, $allFields = null)
    {
        return (preg_match('/([a-fA-F0-9]{2}[:|\-]?){6}/', $value) == 1);
    }
}
