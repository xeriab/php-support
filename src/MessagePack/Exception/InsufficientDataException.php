<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\Exception;

class InsufficientDataException extends UnpackingFailedException
{
    public static function unexpectedLength(string $buffer, int $offset, int $expectedLength): self
    {
        $actualLength = \strlen($buffer) - $offset;
        $message = "Not enough data to unpack: expected $expectedLength, got $actualLength.";

        return new self($message);
    }
}
