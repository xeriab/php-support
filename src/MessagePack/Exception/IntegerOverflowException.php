<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\Exception;

class IntegerOverflowException extends UnpackingFailedException
{
    private $value;

    public function __construct(int $value)
    {
        parent::__construct(\sprintf('The value is too big: %u.', $value));

        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}
