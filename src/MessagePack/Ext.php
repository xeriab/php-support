<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack;

final class Ext
{
    public $type;
    public $data;

    public function __construct(int $type, string $data)
    {
        $this->type = $type;
        $this->data = $data;
    }
}
