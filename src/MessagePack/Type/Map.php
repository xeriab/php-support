<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\Type;

final class Map
{
    public array $map;

    public function __construct(array $map)
    {
        $this->map = $map;
    }
}
