<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\Type;

final class Binary
{
    public string $data;

    public function __construct(string $data)
    {
        $this->data = $data;
    }
}
