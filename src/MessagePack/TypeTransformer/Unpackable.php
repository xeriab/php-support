<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\BufferUnpacker;

interface Unpackable
{
    public function getType(): int;

    public function unpack(BufferUnpacker $unpacker, int $extLength);
}
