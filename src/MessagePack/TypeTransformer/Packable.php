<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\Packer;

interface Packable
{
    public function pack(Packer $packer, $value): ?string;
}
