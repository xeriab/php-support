<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\Packer;
use Exen\Support\MessagePack\Type\Map;

class MapTransformer implements Packable
{
    public function pack(Packer $packer, $value): ?string
    {
        return $value instanceof Map
            ? $packer->packMap($value->map)
            : null;
    }
}
