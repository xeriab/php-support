<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack\TypeTransformer;

use Exen\Support\MessagePack\Packer;
use Exen\Support\MessagePack\Type\Binary;

class BinaryTransformer implements Packable
{
    public function pack(Packer $packer, $value): ?string
    {
        return $value instanceof Binary
            ? $packer->packBin($value->data)
            : null;
    }
}
