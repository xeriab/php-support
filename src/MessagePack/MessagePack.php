<?php

declare(strict_types=1);

namespace Exen\Support\MessagePack;

use Exen\Support\MessagePack\Exception\InvalidOptionException;
use Exen\Support\MessagePack\Exception\PackingFailedException;
use Exen\Support\MessagePack\Exception\UnpackingFailedException;

final class MessagePack
{
    /**
     * @codeCoverageIgnore
     */
    private function __construct()
    {
    }

    /**
     * @param mixed $value
     * @param PackOptions|int|null $options
     *
     * @throws InvalidOptionException
     * @throws PackingFailedException
     *
     * @return string
     */
    public static function pack($value, $options = null) : string
    {
        return (new Packer($options))->pack($value);
    }

    /**
     * @param string $data
     * @param UnpackOptions|int|null $options
     *
     * @throws InvalidOptionException
     * @throws UnpackingFailedException
     *
     * @return mixed
     */
    public static function unpack(string $data, $options = null)
    {
        return (new BufferUnpacker($data, $options))->unpack();
    }
}
