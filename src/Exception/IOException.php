<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * IO exception class.
 *
 * @since 0.1
 */
class IOException extends Exception
{
}
