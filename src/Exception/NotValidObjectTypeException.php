<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class NotValidObjectTypeException.
 */
class NotValidObjectTypeException extends Exception
{
}
