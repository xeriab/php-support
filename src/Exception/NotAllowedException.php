<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Not allowed exception class.
 *
 * @since 0.1
 */
class NotAllowedException extends Exception
{
    protected $code = 403;
}
