<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class InvalidStyleException.
 */
class InvalidStyleException extends Exception
{
    public function __construct($styleName)
    {
        parent::__construct("Invalid style $styleName.");
    }
}
