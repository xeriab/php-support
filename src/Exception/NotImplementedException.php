<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class NotImplementedException.
 */
class NotImplementedException extends Exception
{
    /**
     *
     * @var string
     */
    protected $methodName;

    /**
     * NotImplementedException constructor.
     *
     * @param string $methodName
     * @param string $message
     */
    public function __construct(string $methodName = null, string $message = null)
    {
        $this->methodName = $methodName;

        if (\func_num_args() > 1) {
            $this->message = $message;
        } else {
            $this->message = "Method/Function \"{$methodName}\" is not implemented yet!";
        }

        // $message = $message ?: "Method/Function {$methodName} is not implemented yet!";
        // parent::__construct($methodName, $message);
    }

    /**
     * Get Method Name.
     *
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->methodName;
    }
}
