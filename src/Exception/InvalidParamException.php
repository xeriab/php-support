<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Invalid Param Exception.
 */
class InvalidParamException extends Exception
{
}
