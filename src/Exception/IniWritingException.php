<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Exception when writing a INI configuration.
 */
class IniWritingException extends Exception
{
}
