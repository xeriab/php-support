<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Not unique exception.
 */
class NotUniqueException extends Exception
{
}
