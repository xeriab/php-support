<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class PropertyNotDefined.
 *
 * @since 0.1
 */
class PropertyNotDefined extends Exception
{
    /**
     *
     * @var array $property
     */
    protected $property;

    /**
     * InvalidPathException constructor.
     *
     * @param array $property Property array.
     * @param string $message
     */
    public function __construct(array $property, string $message = '')
    {
        $this->property = $property;

        /** @noinspection PhpUnusedLocalVariableInspection */
        $errorMessage = "";

        if (func_num_args() > 1) {
            $errorMessage = $message;
        } else {
            $errorMessage = "Property '{$property[0]}' of '{$property[1]}' is not defined";
        }

        $this->message = $errorMessage;

        parent::__construct($errorMessage, 0);
    }

    /**
     * Get Property.
     *
     * @return array
     */
    public function getProperty(): array
    {
        return $this->property;
    }
}
