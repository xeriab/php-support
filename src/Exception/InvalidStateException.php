<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Invalid state Exception.
 */
class InvalidStateException extends Exception
{
}
