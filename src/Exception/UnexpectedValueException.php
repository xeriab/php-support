<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * UnexpectedValueException class.
 *
 * @since 0.1
 */
class UnexpectedValueException extends Exception
{
}
