<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Exception when reading a INI configuration.
 */
class IniReadingException extends Exception
{
}
