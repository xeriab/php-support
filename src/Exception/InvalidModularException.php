<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class InvalidModularException.
 */
class InvalidModularException extends Exception
{
}
