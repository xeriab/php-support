<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Out of range exception class.
 *
 * @since 0.1
 */
class OutOfRangeException extends Exception
{
}
