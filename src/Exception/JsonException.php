<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Json exception class.
 *
 * @since 0.1
 */
class JsonException extends Exception
{
}
