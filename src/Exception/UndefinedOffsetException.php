<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * UndefinedOffset Exception.
 */
class UndefinedOffsetException extends Exception
{
}
