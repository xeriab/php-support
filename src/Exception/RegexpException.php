<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Regexp Exception.
 */
class RegexpException extends Exception
{
}
