<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class EmptyFileException.
 */
class EmptyFileException extends InvalidPathException
{
}
