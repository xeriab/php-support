<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Unauthorized exception class.
 *
 * @since 0.1
 */
class UnauthorizedException extends Exception
{
    protected $code = 401;
}
