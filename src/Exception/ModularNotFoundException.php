<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class ModularNotFoundException.
 */
class ModularNotFoundException extends InvalidPathException
{
}
