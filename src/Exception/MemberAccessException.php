<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class MemberAccessException.
 */
class MemberAccessException extends Exception
{
}
