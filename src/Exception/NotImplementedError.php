<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Class NotImplementedError.
 */
class NotImplementedError extends Exception
{
    /**
     *
     * @var string
     */
    protected $name;

    /**
     * NotImplementedError constructor.
     *
     * @param string $name
     * @param string $message
     */
    public function __construct(?string $name, ?string $message = null)
    {
        $this->name = $name;

        if (\func_num_args() > 1) {
            $this->message = $message;
        } else {
            $this->message = "\"{$name}\" is not implemented yet!";
        }
    }

    /**
     * Get Method Name.
     *
     * @return string
     */
    public function getMethodName(): string
    {
        return $this->name;
    }
}
