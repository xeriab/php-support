<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * This throws when cyclic line detected.
 * Class CyclicException.
 *
 * @since 0.1
 */
class CyclicException extends Exception
{
    protected $message = 'Cyclic role three detected';
}
