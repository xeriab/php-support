<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Assertion exception class.
 *
 * @since 0.1
 */
class AssertionException extends Exception
{
}
