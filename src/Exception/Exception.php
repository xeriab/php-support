<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

use Exception as BaseException;

/**
 * Base Exception.
 */
class Exception extends BaseException
{
}
