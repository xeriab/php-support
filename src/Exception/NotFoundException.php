<?php

declare(strict_types=1);

namespace Exen\Support\Exception;

/**
 * Not found exception class.
 *
 * @since 0.1
 */
class NotFoundException extends Exception
{
    protected $code = 404;
}
