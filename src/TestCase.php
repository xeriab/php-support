<?php

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use PHPUnit\Framework\TestCase as PhpUnitTestCase;
use Exen\Support\Traits\MacroableTrait;
use function array_slice;
use function explode;
use function get_class;
use function join;
use function sprintf;

/**
 * TestCase class.
 *
 * @since 0.1
 */
class TestCase extends PhpUnitTestCase
{
    use MacroableTrait;

    /**
     * Instantiate this class.
     *
     * @param string|null $name
     * @param array|null $data
     * @param string|null $dataName
     * @return self
     */
    public static function instance(?string $name = null, ?array $data = [], ?string $dataName = ''): self
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static($name, $data, $dataName);
        }

        return $instance;
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
                ' is singleton and cannot be cloned, use instance() method'
        );
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        $parseClassName = function (string $class = null): array {
            return [
                'namespace' => array_slice(explode('\\', $class), 0, -1),
                'classname' => join(
                    '',
                    array_slice(explode('\\', $class), -1)
                )
            ];
        };

        return sprintf('<%s>', $parseClassName(static::class)['classname']);
    }
}
