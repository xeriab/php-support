<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support;

use function chr;
use function ord;
use function strlen;
use function substr;

/**
 * BinaryStream class.
 *
 * Methods for working with binary strings
 *
 * @since 0.1.1
 */
class BinaryStream
{
    /** @var int */
    public $offset = null;

    /** @var string */
    public $buffer = EMPTY_STRING;

    public function __construct(string $buffer = EMPTY_STRING, int $offset = 0)
    {
        $this->buffer = $buffer;
        $this->offset = $offset;
    }

    public function reset(): void
    {
        $this->buffer = null;
        $this->offset = null;
    }

    public function setBuffer(string $buffer = EMPTY_STRING, int $offset = 0)
    {
        $this->buffer = $buffer;
        $this->offset = $offset;
    }

    public function getOffset(): int
    {
        return $this->offset;
    }

    public function getBuffer(): string
    {
        return $this->buffer;
    }

    /**
     * @param int|bool $length
     *
     * @return string
     */
    public function get($length = null): string
    {
        if ($length === true) {
            $str = substr($this->buffer, $this->offset);
            $this->offset = strlen($this->buffer);
            return $str;
        } elseif ($length < 0) {
            $this->offset = strlen($this->buffer) - 1;
            return "";
        } elseif ($length === 0) {
            return "";
        }

        return $length === 1 ?
            $this->buffer{$this->offset++} :
            substr($this->buffer, ($this->offset += $length) - $length, $length);
    }

    public function getRemaining(): string
    {
        $str = substr($this->buffer, $this->offset);
        $this->offset = strlen($this->buffer);
        return $str;
    }

    public function put(string $str = null): self
    {
        $this->buffer .= $str;

        return $this;
    }

    public function getBool(): bool
    {
        return $this->get(1) !== "\x00";
    }

    public function putBool(bool $v = null): self
    {
        $this->buffer .= ($v ? "\x01" : "\x00");

        return $this;
    }

    public function getByte(): int
    {
        return ord($this->buffer{$this->offset++});
    }

    public function putByte(int $v = null): self
    {
        $this->buffer .= chr($v);

        return $this;
    }

    public function getShort(): int
    {
        return Binary::readShort($this->get(2));
    }

    public function getSignedShort(): int
    {
        return Binary::readSignedShort($this->get(2));
    }

    public function putShort(int $v = null): self
    {
        $this->buffer .= Binary::writeShort($v);

        return $this;
    }

    public function getLShort(): int
    {
        return Binary::readLShort($this->get(2));
    }

    public function getSignedLShort(): int
    {
        return Binary::readSignedLShort($this->get(2));
    }

    public function putLShort(int $v = null): self
    {
        $this->buffer .= Binary::writeLShort($v);

        return $this;
    }

    public function getTriad(): int
    {
        return Binary::readTriad($this->get(3));
    }

    public function putTriad(int $v = null): self
    {
        $this->buffer .= Binary::writeTriad($v);

        return $this;
    }

    public function getLTriad(): int
    {
        return Binary::readLTriad($this->get(3));
    }

    public function putLTriad(int $v = null): self
    {
        $this->buffer .= Binary::writeLTriad($v);

        return $this;
    }

    public function getInt(): int
    {
        return Binary::readInt($this->get(4));
    }

    public function putInt(int $v = null): self
    {
        $this->buffer .= Binary::writeInt($v);

        return $this;
    }

    public function getLInt(): int
    {
        return Binary::readLInt($this->get(4));
    }

    public function putLInt(int $v = null): self
    {
        $this->buffer .= Binary::writeLInt($v);

        return $this;
    }

    public function getFloat(): float
    {
        return Binary::readFloat($this->get(4));
    }

    public function getRoundedFloat(int $accuracy = null): float
    {
        return Binary::readRoundedFloat($this->get(4), $accuracy);
    }

    public function putFloat(float $v = null): self
    {
        $this->buffer .= Binary::writeFloat($v);

        return $this;
    }

    public function getLFloat(): float
    {
        return Binary::readLFloat($this->get(4));
    }

    public function getRoundedLFloat(int $accuracy = null): float
    {
        return Binary::readRoundedLFloat($this->get(4), $accuracy);
    }

    public function putLFloat(float $v = null): self
    {
        $this->buffer .= Binary::writeLFloat($v);

        return $this;
    }

    /**
     * @return int
     */
    public function getLong(): int
    {
        return Binary::readLong($this->get(8));
    }

    /**
     * @param int|null $v
     * @return BinaryStream
     */
    public function putLong(int $v = null): self
    {
        $this->buffer .= Binary::writeLong($v);

        return $this;
    }

    /**
     * @return int
     */
    public function getLLong(): int
    {
        return Binary::readLLong($this->get(8));
    }

    /**
     * @param int|null $v
     * @return BinaryStream
     */
    public function putLLong(int $v = null): self
    {
        $this->buffer .= Binary::writeLLong($v);

        return $this;
    }

    /**
     * Reads a 32-bit variable-length unsigned integer from the buffer and returns it.
     * @return int
     */
    public function getUnsignedVarInt(): int
    {
        return Binary::readUnsignedVarInt($this->buffer, $this->offset);
    }

    /**
     * Writes a 32-bit variable-length unsigned integer to the end of the buffer.
     * @param int|null $v
     * @return BinaryStream
     */
    public function putUnsignedVarInt(int $v = null): self
    {
        $this->put(Binary::writeUnsignedVarInt($v));

        return $this;
    }

    /**
     * Reads a 32-bit zigzag-encoded variable-length integer from the buffer and returns it.
     * @return int
     */
    public function getVarInt(): int
    {
        return Binary::readVarInt($this->buffer, $this->offset);
    }

    /**
     * Writes a 32-bit zigzag-encoded variable-length integer to the end of the buffer.
     * @param int|null $v
     * @return BinaryStream
     */
    public function putVarInt(int $v = null): self
    {
        $this->put(Binary::writeVarInt($v));

        return $this;
    }

    /**
     * Reads a 64-bit variable-length integer from the buffer and returns it.
     * @return int
     */
    public function getUnsignedVarLong(): int
    {
        return Binary::readUnsignedVarLong($this->buffer, $this->offset);
    }

    /**
     * Writes a 64-bit variable-length integer to the end of the buffer.
     * @param int|null $v
     * @return BinaryStream
     */
    public function putUnsignedVarLong(int $v = null): self
    {
        $this->buffer .= Binary::writeUnsignedVarLong($v);

        return $this;
    }

    /**
     * Reads a 64-bit zigzag-encoded variable-length integer from the buffer and returns it.
     * @return int
     */
    public function getVarLong(): int
    {
        return Binary::readVarLong($this->buffer, $this->offset);
    }

    /**
     * Writes a 64-bit zigzag-encoded variable-length integer to the end of the buffer.
     * @param int|null $v
     * @return BinaryStream
     */
    public function putVarLong(int $v = null): self
    {
        $this->buffer .= Binary::writeVarLong($v);

        return $this;
    }

    /**
     * Returns whether the offset has reached the end of the buffer.
     * @return bool
     */
    public function feof(): bool
    {
        return !isset($this->buffer{$this->offset});
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return (string) $this->getBuffer();
    }
}
