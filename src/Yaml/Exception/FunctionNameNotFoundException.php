<?php

declare(strict_types=1);

namespace Exen\Support\Yaml\Exception;

class FunctionNameNotFoundException extends \Exception
{
}
