<?php /** @noinspection PhpUnusedParameterInspection */
/** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support\Yaml;

use LogicException;
use Exen\Support\Arr;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Lang;
use Exen\Support\PhpObject;
use Exen\Support\Str;
use Exen\Support\Traits\MacroableTrait;
use Symfony\Component\Yaml\Yaml as SymfonyYaml;
use function array_filter;
use function get_class;
use const ARRAY_FILTER_USE_BOTH;
use const SPACE_CHAR;

/**
 * Class for various `YAML` helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 *
 * @since 0.1
 */
final class Yaml extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const DUMP_OBJECT                     = 1;
    const PARSE_EXCEPTION_ON_INVALID_TYPE = 2;
    const PARSE_OBJECT                    = 4;
    const PARSE_OBJECT_FOR_MAP            = 8;
    const DUMP_EXCEPTION_ON_INVALID_TYPE  = 16;
    const PARSE_DATETIME                  = 32;
    const DUMP_OBJECT_AS_MAP              = 64;
    const DUMP_MULTI_LINE_LITERAL_BLOCK   = 128;
    const PARSE_CONSTANT                  = 256;
    const PARSE_CUSTOM_TAGS               = 512;
    const DUMP_EMPTY_ARRAY_AS_SEQUENCE    = 1024;
    const PRETTY_PRINT                    = 2048;
    const FORCE_OBJECT                    = 1;
    const FORCE_ARRAY                     = 0b0001;
    const PRETTY                          = 0b0010;

    /**
     * Yaml instance.
     *
     * @var Yaml
     */
    private static $instance = null;

    /**
     * Yaml instances.
     *
     * @var Yaml[]
     */
    private static $instances = [];

    /**
     * Parses a YAML file into a PHP value.
     *
     * Usage:
     * ```php
     * $array = Yaml::parseFile('config.yml');
     * print_r($array);
     * ```
     *
     * @param string|null $filename The path to the YAML file to be parsed
     * @param int|null $options
     * @return mixed The YAML converted to a PHP value
     *
     */
    public static function parseFile(string $filename = null, ?int $options = 0)
    {
        $yaml = new YamlParser();
        return $yaml->parseFile($filename, $options);
    }

    /**
     * Parses YAML into a PHP value.
     *
     *  Usage:
     *  <code>
     *   $array = Yaml::parse(file_get_contents('config.yml'));
     *   print_r($array);
     *  </code>
     *
     * @param string|null $input A string containing YAML
     * @param int|null $options A bit field of PARSE_* constants to customize the YAML parser behavior
     *
     * @return mixed The YAML converted to a PHP value
     *
     */
    public static function parse(?string $input = null, ?int $options = 0)
    {
        $yaml = new YamlParser();
        return $yaml->parse($input, $options);
    }

    /**
     * Dumps a PHP value to a YAML string.
     *
     * The dump method, when supplied with an array, will do its best
     * to convert the array into friendly YAML.
     *
     * @param mixed $input The PHP value
     * @param int|null $options A bit field of DUMP_* constants to customize the dumped YAML string
     * @param int|null $inline The level where you switch to inline YAML
     * @param int|null $indent The amount of spaces to use for indentation of nested nodes
     *
     * @return string A YAML string representing the original PHP value
     */
    public static function dump(
        $input = null,
        ?int $options = 0,
        ?int $inline = 3,
        ?int $indent = 2
    ): ?string {
        $flags  = self::DUMP_EMPTY_ARRAY_AS_SEQUENCE;
        $flags |= self::DUMP_MULTI_LINE_LITERAL_BLOCK;
        $flags |= self::DUMP_OBJECT;
        $flags |= self::DUMP_OBJECT_AS_MAP;

        $pretty1 = ($options & self::PRETTY ? self::PRETTY_PRINT : 0);

        $USE_PRETTY_YAML = Lang::getConstant('USE_PRETTY_YAML', null, false);

        $pretty2 = $USE_PRETTY_YAML ?
            self::PRETTY_PRINT : 0;

        $flags |= $pretty1 | $pretty2;

        $options = ($options & $flags);

        $object = null;

        if (Lang::isArray($input)) {
            $object = $input;
        } else if (Lang::isObject($input)) {
            if (Lang::hasMethod($input, 'yamlSerialize')) {
                $object = $input->yamlSerialize();
            } elseif (Lang::hasMethod($input, 'toArray')) {
                $object = $input->toArray();
            } else {
                $object = $input;
            }
        } else {
            $object = $input;
        }

        return SymfonyYaml::dump($object, $inline, $indent, $options);
    }

    /**
     * Dumps a PHP value to a YAML string.
     *
     * The dump method, when supplied with an array, will do its best
     * to convert the array into friendly YAML.
     *
     * @param null $value
     * @param int|null $options A bit field of DUMP_* constants to customize the dumped YAML string
     *
     * @return string A YAML string representing the original PHP value
     */
    public static function encode($value = null, ?int $options = 0): ?string
    {
        return self::dump($value, $options);
    }

    /**
     * Parses YAML into a PHP value.
     *
     *  Usage:
     *  <code>
     *   $array = Yaml::parse(file_get_contents('config.yml'));
     *   print_r($array);
     *  </code>
     *
     * @param string|null $yaml A string containing YAML
     * @param int|null $options A bit field of PARSE_* constants to customize the YAML parser behavior
     * @param bool $sorted Sort values
     *
     * @return mixed The YAML converted to a PHP value
     *
     */
    public static function decode(?string $yaml = null, ?int $options = 0, ?bool $sorted = false)
    {
        return self::parse($yaml, $options);
    }

    /**
     * Converts a PHP value to a YAML string, optionally replacing values if
     * a replacer function is specified or optionally including only the specified
     * properties if a replacer array is specified.
     *
     * @param mixed      $value    The value to convert to a YAML string.
     * @param mixed      $replacer A function that alters the behavior of the
     *                             stringification process, or an array of String
     *                             and Number objects that serve as a whitelist
     *                             for selecting/filtering the properties of the
     *                             value object to be included in the YAML string.
     *                             If this value is null or not provided, all
     *                             properties of the object are included in the
     *                             resulting YAML string.
     * @param string|int $space    A String or Number that's used to insert
     *                             white space into the output YAML string for
     *                             readability purposes. If this is a Number,
     *                             it indicates the number of space characters
     *                             to use as white space; this number is capped
     *                             at 10 (if it is greater, the value is just 10).
     *                             Values less than 1 indicate that no space
     *                             should be used. If this is a String, the string
     *                             (or the first 10 characters of the string,
     *                             if it's longer than that) is used as white space.
     *                             If this parameter is not provided (or is null),
     *                             no white space is used.
     *
     * @return string|null         Returns YAML string if the process/parsing went
     *                             well, Null if something went wrong.
     *
     * @since 0.1.0
     */
    public static function stringify(
        $value = null,
        $replacer = null,
        $space = null
    ): ?string {
        $result = null;
        $rs = '    ';

        if (Lang::isNotNull($replacer) and Lang::isCallableOrClosure($replacer)) {
            $result = array_filter($value, $replacer, ARRAY_FILTER_USE_BOTH);
            $result = self::encode($result);
        } else {
            $result = self::encode($value);
        }

        if (Lang::isNotNull($replacer)) {
            if (Lang::isCallableOrClosure($replacer)) {
                // $result = \array_filter($value, $replacer, \ARRAY_FILTER_USE_BOTH);
                $result = Arr::filter($value, $replacer);
                $result = self::encode($result);
            } elseif (Lang::isArray($replacer)) {
                $callback = function ($key) use ($replacer) {
                    return (Lang::isArray($key, $replacer));
                };

                // $result = \array_filter($value, $callback, \ARRAY_FILTER_USE_KEY);
                $result = Arr::filter($value, $callback, Arr::FILTER_USE_KEY);
                $result = self::encode($result);
            } else {
                $result = self::encode($value);
            }
        }

        if (Lang::isNotNull($space)) {
            if (Lang::isString($space)) {
                $result = Str::replace($result, $rs, $space);
                // $result = Str::replaceIndents($result, 1, $space);
            } elseif (Lang::isNumeric($space)) {
                if ($space <= 10) {
                    // $ident  = str_repeat(\SPACE_CHAR, $space);
                    $ident  = Str::repeat(SPACE_CHAR, $space);
                    $result = Str::replace($result, $rs, $ident);
                // $result = Str::indent($result, $space);
                } else {
                    $result = Str::replace($result, $rs, '  ');
                    // $result = Str::indent($result, 2);
                }
            }
        }

        return $result;
    }

    /**
     * Yaml constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new Yaml();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
                ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
