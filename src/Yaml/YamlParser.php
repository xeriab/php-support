<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support\Yaml;

use DateTime;
use Exception;
use Exen\Support\Yaml\Exception\FunctionNameNotFoundException;
use Exen\Support\Yaml\Exception\FunctionNotFoundException;
use Symfony\Component\Yaml\Parser as NativeYamlParser;
use Symfony\Component\Yaml\Yaml as NYC;

class YamlParser extends NativeYamlParser
{
    /**
     *
     * @var array
     */
    protected static $functions = [];

    /**
     *
     * @var array
     */
    protected static $constants = [];

    /**
     *
     * @param string|null $name
     */
    public static function registerConstant(string $name = null): void
    {
        // if (defined($name)) {
        // self::$constants[$name] = constant($name);
        self::$constants[$name] = $name;
        // }
    }

    /**
     *
     * @param string|null $name
     * @param callable|null $callable $callable
     */
    public static function registerFunction(string $name = null, callable $callable = null): void
    {
        self::$functions[$name] = $callable;
    }

    /**
     *
     * @param string|null $path
     */
    public static function registerFileFunction(string $path = null): void
    {
        self::registerFunction(
            'file',
            function ($fileName) use ($path) {
                $path = null === $path ? __DIR__ : rtrim($path, DIRECTORY_SEPARATOR);
                $filePath = DIRECTORY_SEPARATOR === $fileName[0] ? $fileName : $path.DIRECTORY_SEPARATOR.$fileName;

                if (false === is_readable($filePath)) {
                    throw new Exception('File "'.$filePath.'" not found.');
                }

                return file_get_contents($filePath);
            }
        );
    }

    public static function registerDateFunction(): void
    {
        self::registerFunction(
            'date',
            function ($date = null) {
                return new DateTime($date);
            }
        );
    }

    /**
     *
     * @param string|null $value
     * @param int $flags
     *
     * @return array|null
     * @throws Exception
     */
    public function parse(string $value = null, int $flags = 0): ?array
    {
        $yamlFlags = $flags;
        $yamlFlags |= NYC::PARSE_OBJECT;
        $yamlFlags |= NYC::PARSE_CUSTOM_TAGS;
        $yamlFlags |= NYC::PARSE_DATETIME;
        $yamlFlags |= NYC::PARSE_CONSTANT;

        $rv = parent::parse($value, $yamlFlags);

        if (is_array($rv)) {
            $this->parseConstants($rv);
            // $this->parseValues($rv);
        }

        return $rv;
    }

    /**
     *
     * @param array $constants
     * @return YamlParser
     *
     * @throws FunctionNameNotFoundException
     * @throws FunctionNotFoundException
     */
    protected function parseConstants(array &$constants = []): self
    {
        // DEBUG($constants);

        foreach ($constants as &$constant) {
            $isArray = is_array($constant);
            $isString = is_string($constant);

            // DEBUG($isArray);
            // DEBUG($isString);
            // DEBUG($constant);

            if ($isArray) {
                $this->parseConstants($constant);
            } elseif ($isString && 'ex/const ' === substr($constant, 0, 9)) {
                $name = null;
                $parameters = [];

                DEBUG(substr($constant, 0, 9) === 'ex/const ');

                foreach (token_get_all('<?php ' . $constant) as $token) {
                    if (is_array($token)) {
                        if ($token[0] === T_STRING && $name === null) {
                            $name = $token[1];
                        } elseif ($token[0] === T_STRING) {
                            $parameters[] = $token[1];
                        } elseif ($token[0] === T_CONSTANT_ENCAPSED_STRING) {
                            $parameters[] = substr($token[1], 1, -1);
                        } elseif ($token[0] === T_LNUMBER || $token[0] === T_DNUMBER) {
                            $parameters[] = $token[1];
                        }
                    }

                    DEBUG($token[-1]);
                }

                $name = str_replace('ex/const ', '', $constant);

                // DEBUG(token_get_all('<?php ' . $constant));

                // DEBUG($name);

                // if (defined($name)) {
                // }

                if (null === $name) {
                    throw new FunctionNameNotFoundException('Constant name cannot be found in '.$constant.'.');
                } elseif (false === isset(self::$constants[$name])) {
                    throw new FunctionNotFoundException('Constant "'.$name.'" not found.');
                }

                $constant = constant(self::$constants[$name]);
                $constant = call_user_func_array(self::$constants[$name], $parameters);
            } /*elseif ($isString && substr($constant, 0, 1) === '<' && substr($constant, -1) === '>') {
            }*/
        }

        return $this;
    }

    /**
     *
     * @param array $values
     *
     * @return YamlParser
     *
     * @throws FunctionNameNotFoundException
     * @throws FunctionNotFoundException
     */
    protected function parseValues(array &$values = []): self
    {
        foreach ($values as &$value) {
            $isArray = is_array($value);
            $isString = is_string($value);

            if ($isArray) {
                $this->parseValues($value);
            } elseif ($isString && '<' === substr($value, 0, 1) && '>' === substr($value, -1)) {
                $name = null;
                $parameters = [];

                // DEBUG(token_get_all('<?php ' . $value));

                foreach (token_get_all('<?php '.$value) as $token) {
                    if (is_array($token)) {
                        if (T_STRING === $token[0] && null === $name) {
                            $name = $token[1];
                        } elseif (T_STRING === $token[0]) {
                            $parameters[] = $token[1];
                        } elseif (T_CONSTANT_ENCAPSED_STRING === $token[0]) {
                            $parameters[] = substr($token[1], 1, -1);
                        } elseif (T_LNUMBER === $token[0] || T_DNUMBER === $token[0]) {
                            $parameters[] = $token[1];
                        }
                    }
                }

                if (null === $name) {
                    throw new FunctionNameNotFoundException('Function name cannot be found in '.$value.'.');
                } elseif (false === isset(self::$functions[$name])) {
                    throw new FunctionNotFoundException('Function "'.$name.'" not found.');
                }

                $value = call_user_func_array(self::$functions[$name], $parameters);
            } /*elseif ($isString && substr($value, 0, 1) === '<' && substr($value, -1) === '>') {
            }*/
        }

        return $this;
    }
}
