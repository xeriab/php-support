<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Exen\Support\PhpObject;

/**
 * RequestParameters class.
 *
 * @since 0.1.0
 */
final class RequestParameters extends PhpObject
{
    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request = null;

    /**
     * @var \Symfony\Component\HttpFoundation\ParameterBag
     */
    private $parameterData = null;

    /**
     * RequestParameters constructor
     *
     * @param \Symfony\Component\HttpFoundation\Request|null $request
     */
    public function __construct(?Request $request = null)
    {
        $this->request = $request;

        $method = $this->request->getMethod();

        if ($method === 'GET') {
            $this->getParameterData = $this->request->query;
            $this->parameterData    = $this->request->query;
        }

        if ($method === 'POST') {
            $this->postParameterData = $this->request->request;
            $this->parameterData     = $this->request->request;
        }

        if ($method === 'PUT') {
            $this->putParameterData = $this->request->attributes;
            $this->parameterData    = $this->request->attributes;
        }
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function get(?string $name = null)
    {
        return $this->getParameterData->get($name);
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function post(?string $name = null)
    {
        return $this->postParameterData->get($name);
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function put(?string $name = null)
    {
        return $this->putParameterData->get($name);
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return \Symfony\Component\HttpFoundation\ParameterBag
     */
    public function all(?string $name = null): ParameterBag
    {
        if ($name) {
            return $this->parameterData->get($name);
        }

        return $this->parameterData;
    }

    /**
     * Get request.
     *
     * @return \Symfony\Component\HttpFoundation\Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name = null, $arguments = [])
    {
        return \call_user_func([$this->request, $name], $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
