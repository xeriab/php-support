<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\Traits\SmartObjectTrait;
use Exen\Support\Lang;

final class FileInfo extends \SplFileInfo
{
    use SmartObjectTrait;

    public function getLastAccessTime(): int
    {
        return $this->getATime();
    }

    public function getHumanReadableSuze(int $decimals = 0): string
    {
        return Lang::sizeFormat($this->getSize(), $decimals);
    }

    public function toString(): string
    {
        return $this->getRealPath();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
