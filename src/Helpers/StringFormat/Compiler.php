<?php /** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * FormatterPlaceholder.
 *
 * @category  Helpers
 * @package   Exen\Support\Helpers\StringFormat
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types = 1);

namespace Exen\Support\Helpers\StringFormat;

use function array_key_exists;
use function base_convert;
use function basename;
use function call_user_func;
use function count;
use function debug_backtrace;
use function dirname;
use function end;
use function explode;
use function get_class_methods;
use function in_array;
use function is_array;
use function is_null;
use function is_numeric;
use function method_exists;
use function preg_match;
use function preg_replace_callback;
use function property_exists;
use function str_pad;
use function strlen;
use function strtoupper;
use function trigger_error;
use function vsprintf;

/**
 * Class Compiler.
 *
 * @internal
 */
class Compiler
{
    /** @deprecated */
    const MODE_INDEX       = 'indexed';
    const MODE_INDEXED     = 'indexed';
    const MODE_NAMED       = 'named';
    const MODE_PLACEHOLDER = 'placeholder';
    const MODE_PYTHON      = 'python';

    const TRACE_LEVEL_MAX = 6;

    /**
     * Matrix for mapping string suffixes to values provided for base_convert function.
     *
     * @var array
     */
    protected static array $matrixBaseConvert = [
        'b' => 2,
        'o' => 8,
        'd' => 10,
        'x' => 16,
        'X' => 16,
    ];

    /**
     * Matrix for mapping string suffixes to values provided for str_pad function.
     *
     * @var array
     */
    protected static array $matrixStrPad = [
        '<' => STR_PAD_RIGHT,
        '>' => STR_PAD_LEFT,
        '^' => STR_PAD_BOTH,
    ];

    /**
     * Regular expressions for key used in template.
     *
     * Key must be one of Compiler::MODE_* constant, and value is regular expression used to find key in tokens
     *
     * @var array
     */
    protected static array $regexExprKeys = [
        self::MODE_INDEXED     => '\d*',
        self::MODE_NAMED       => '\w+',
        self::MODE_PLACEHOLDER => '\d+',
    ];

    /**
     * Regular expression for finding tokens in format.
     *
     * @var string
     */
    protected static string $rxp_token = <<<'REGEX'
@
        \{              # opening brace
            (
                [^}]*   # all but closing brace
            )
        \}              # closing brace
@mx
REGEX;
    // protected static $rxp_token = '
    //     /
    //     \{              # opening brace
    //         (
    //             [^}]*   # all but closing brace
    //         )
    //     \}              # closing brace
    // /x';

    /** @var array */
    protected array $traceClass;

    /** @var array */
    protected array $traceFile;

    /**
     * Mode we are run.
     *
     * @var integer one of: Compiler::MODE_NORMAL, Compiler::MODE_NAMED
     */
    protected $mode;

    /**
     * Pointer for accessing given elements when no placeholder in format is given.
     *
     * @var int
     */
    protected int $pointer = 0;

    /**
     * @var string
     */
    protected string $format;

    /**
     * @var array
     */
    protected array $params;

    /** @var array */
    protected array $evaluatedParams = [];

    /** @var string|null */
    protected ?string $compiledResult;

    /**
     * Compiler constructor.
     *
     * @param string $format
     * @param array $params
     * @param string $mode Compiler::MODE_
     */
    public function __construct(string $format, array $params, string $mode)
    {
        $this->format = $format;
        $this->params = $params;
        $this->mode = $mode;
        $this->traceFile = [];
        $this->compiledResult = null;

        $this->findTraces();
    }

    /** @noinspection SpellCheckingInspection */
    protected function findTraces()
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS, static::TRACE_LEVEL_MAX);
        $trace = array_reverse($trace);
        $tracePrevious = null;

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($trace as $itemIdx => $traceItem) {
            if ($this->traceFile) {
                $this->traceClass = $tracePrevious;
                break;
            }

            if (
                !isset($traceItem['class']) && in_array($traceItem['function'], [
                    'Exen\Support\Helpers\StringFormat\iformat',
                    'Exen\Support\Helpers\StringFormat\iformatl',
                    'Exen\Support\Helpers\StringFormat\nformat',
                    'Exen\Support\Helpers\StringFormat\phformat',
                ])
            ) {
                $this->traceFile = $traceItem;
            } elseif (
                isset($traceItem['function']) && $traceItem['function'] == 'compile' &&
                isset($traceItem['class']) && in_array($traceItem['class'], [
                    'Exen\Support\Helpers\StringFormat\FormatterIndexed',
                    'Exen\Support\Helpers\StringFormat\FormatterNamed',
                    'Exen\Support\Helpers\StringFormat\FormatterPlaceholder',
                ])
            ) {
                $this->traceFile = $traceItem;
            } else {
                $tracePrevious = $traceItem;
            }
        }
    }

    /**
     * @return string
     */
    public function getFormat(): string
    {
        return $this->format;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }

    protected function evaluateParam($param)
    {
        $this->evaluatedParams[] = $param;
    }
    /**
     * @return array
     */
    public function getEvaluatedParams(): array
    {
        return $this->evaluatedParams;
    }

    /**
     * Helper function - test for existence of key in given parameters.
     *
     * @param string|int $key
     *
     * @return boolean
     */
    protected function hasKey($key): bool
    {
        return ($this->mode == self::MODE_INDEXED && $key == '') || array_key_exists($key, $this->params);
    }

    /**
     * Helper function for find current param.
     *
     * @param integer $key parameter index (optional)
     *
     * @return mixed
     */
    protected function getParam($key = '')
    {
        if ($key === '') {
            $key = $this->pointer++;
        }

        return $this->params[$key];
    }

    /**
     * Callback for preg_replace_callback - here is doing all magic with replacing format token with
     * proper values from given params.
     *
     * @param array $data matched token data
     *
     * @return string
     */
    protected function formatCallback(array $data = []): string
    {
        if (count($data) < 2) {
            return $data[0];
        }

        // Simple auto or explicit placeholder
        if ($this->mode == self::MODE_INDEXED && $this->hasKey($data[1])) {
            $this->evaluateParam($data[1] || $this->pointer);
            return $this->getParam($data[1]);
        // Simple named, explicit placeholder
        } elseif ($this->mode == self::MODE_NAMED && strlen($data[1]) > 0 && $this->hasKey($data[1])) {
            $this->evaluateParam($data[1]);
            return $this->getParam($data[1]);
        // Simple placeholder,
        } elseif ($this->mode == self::MODE_PLACEHOLDER && strlen($data[1]) > 0 && $this->hasKey($data[1])) {
            $this->evaluateParam($data[1]);
            return $this->getParam($data[1]);
        // Keywords
        } elseif (
            preg_match(
                '/^@(class|classLong|method|methodLong|function|file|fileLong|dir|dirLong|line)$/mx',
                $data[1],
                $match
            )
        ) {
            switch ($match[1]) {
                case 'classLong':
                    if (!$this->traceClass || !isset($this->traceClass['class'])) {
                        $msg = "Bad usage of @classLong keyword in {$this->traceFile['file']} on " .
                            "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
                        trigger_error($msg, E_USER_WARNING);

                        return '';
                    }

                    return $this->traceClass['class'];
                case 'class':
                    if (!$this->traceClass || !isset($this->traceClass['class'])) {
                        $msg = "Bad usage of @class keyword in {$this->traceFile['file']} on " .
                            "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
                        trigger_error($msg, E_USER_WARNING);

                        return '';
                    }

                    $cls = explode('\\', $this->traceClass['class']);

                    return end($cls);
                case 'method':
                    if (!$this->traceClass || !isset($this->traceClass['class'])) {
                        $msg = "Bad usage of @method keyword in {$this->traceFile['file']} on " .
                            "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
                        trigger_error($msg, E_USER_WARNING);

                        return '';
                    }

                    $cls = explode('\\', $this->traceClass['class']);
                    $cls = end($cls);

                    return $cls . '::' . $this->traceClass['function'];
                case 'methodLong':
                    if (!$this->traceClass || !isset($this->traceClass['class'])) {
                        $msg = "Bad usage of @methodLong keyword in {$this->traceFile['file']} on " .
                            "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
                        trigger_error($msg, E_USER_WARNING);

                        return '';
                    }

                    return $this->traceClass['class'] . '::' . $this->traceClass['function'];
                case 'function':
                    if (!$this->traceClass || !isset($this->traceClass['function'])) {
                        $msg = "Bad usage of @function keyword in {$this->traceFile['file']} on " .
                            "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
                        trigger_error($msg, E_USER_WARNING);

                        return '';
                    }

                    return $this->traceClass['function'];
                case 'file':
                    return basename($this->traceFile['file']);
                case 'fileLong':
                    return $this->traceFile['file'];
                case 'dir':
                    return basename(dirname($this->traceFile['file']));
                case 'dirLong':
                    return dirname($this->traceFile['file']);
                case 'line':
                    return $this->traceFile['line'];
            }
        // Text alignment
        } elseif (preg_match('
            @
            ^
                (' . self::$regexExprKeys[$this->mode] . ')  # placeholder
                :                                            # explicit colon
                (.)?                                         # pad character
                ([<>^])                                      # alignment
                (\d+)                                        # pad length
            $
            @mx', $data[1], $match) && $this->hasKey($match[1])) {
            $this->evaluateParam($match[1]);
            return str_pad(
                $this->getParam($match[1]),
                $match[4],
                (strlen($match[2]) > 0 ? $match[2] : ' '),
                static::$matrixStrPad[$match[3]]
            );
        // Sprintf pattern
        } elseif (preg_match('
            @
            ^
                (' . self::$regexExprKeys[$this->mode] . ')  # placeholder
                %                                            # explicit percent
                (.*)                                         # sprintf pattern
            $
            @mx', $data[1], $match) && $this->hasKey($match[1])) {
            $this->evaluateParam($match[1]);
            return vsprintf($match[2], $this->getParam($match[1]));
        // Call object method or get object property
        } elseif (preg_match('
            @
            ^
                (' . self::$regexExprKeys[$this->mode] . ')  # placeholder
                ->                                           # explicit arrow
                (\w+)                                        # keyword (field or method name)
            $
            @mx', $data[1], $match) && $this->hasKey($match[1])) {
            $this->evaluateParam($match[1]);
            $param = $this->getParam($match[1]);
            if (method_exists($param, $match[2])) {
                return call_user_func(array($param, $match[2]));
            } elseif (property_exists($param, $match[2])) {
                return $param->{$match[2]};
            } elseif (in_array('__call', get_class_methods($param))) {
                return call_user_func(array($param, $match[2]));
            } elseif (in_array('__get', get_class_methods($param))) {
                return $param->{$match[2]};
            } else {
                return $data[0];
            }
        // Converting integer to other base
        } elseif (preg_match('
            @
            ^
            (' . self::$regexExprKeys[$this->mode] . ')  # placeholder
            [#]                                          # explicit hash
            (?:
                (\d+)                                    # source base
                [#]                                      # explicit hash
            )?
            ([dxXob]|\d\d?)                              # destination base
            $
            @mx', $data[1], $match) && $this->hasKey($match[1])) {
            $this->evaluateParam($match[1]);
            $ret = base_convert(
                $this->getParam($match[1]),                     // value to convert
                ($match[2] ?: 10),                   // source base (defaults to 10)
                (
                    is_numeric($match[3])                         // destination base is:
                    ? $match[3]                                 // - numeric
                    : self::$matrixBaseConvert[$match[3]]       // - or named
                )
            );
            if ($match[3] == 'X') {
                $ret = strtoupper($ret);
            }

            return $ret;
        // Array index
        } elseif (preg_match('
            @
            ^
                (' . self::$regexExprKeys[$this->mode] . ')  # placeholder
                \[                                           # opening square bracket
                    (\w+)                                    # key
                \]                                           # closing square bracket
            $
            @mx', $data[1], $match) &&
            $this->hasKey($match[1]) &&
            is_array($ret = $this->getParam($match[1])) &&
            isset($ret[$match[2]])
        ) {
            $this->evaluateParam($match[1]);
            return $ret[$match[2]];
        } else { // unknown token
            $msg = "Unknown token found in format: {$data[0]} in {$this->traceFile['file']} on " .
                "line {$this->traceFile['line']} (by Exen\\Support\\Helpers\\StringFormat)";
            trigger_error($msg, E_USER_WARNING);

            return $data[0];
        }

        return '';
    }

    /**
     * Compile $this->format and fill it's placeholders with data from $this->params.
     *
     * @return string
     */
    public function run(): string
    {
        if (is_null($this->compiledResult)) {
            $this->compiledResult = preg_replace_callback(
                static::$rxp_token,
                [$this, 'formatCallback'],
                $this->format
            );
        }

        return $this->compiledResult;
    }
}
