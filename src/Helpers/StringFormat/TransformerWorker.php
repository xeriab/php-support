<?php /** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * TransformerWorker.
 *
 * @category  Helpers
 * @package   Exen\Support\Helpers\StringFormat
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types = 1);

namespace Exen\Support\Helpers\StringFormat;

use function array_shift;
use function array_unshift;
use function call_user_func_array;
use function extension_loaded;
use function func_get_args;
use function is_callable;
use function is_null;
use function mb_internal_encoding;
use function mb_strlen;
use function mb_strtolower;
use function mb_strtoupper;
use function mb_substr;
use function str_ireplace;
use function str_replace;
use function strlen;
use function substr;

class TransformerWorker
{
    /**
     * @var string
     */
    protected $string;

    /**
     * Transformer constructor.
     *
     * @param $string string to transform
     */
    public function __construct(string $string)
    {
        $this->string = $string;
    }

    /**
     * Wrapper for str_replace.
     *
     * If $to is callable, it's executed with two args:
     *  $from - string to replace from
     *  $trfm - current Transformer instance ($this)
     *
     * @param string $from
     * @param string|callable $to
     *
     * @return TransformerWorker
     */
    public function replace(string $from, $to): self
    {
        if (is_callable($to)) {
            $to = $to($from, $this->string);
        }

        $this->string = str_replace($from, $to, $this->string);
        return $this;
    }

    /**
     * Wrapper for str_ireplace.
     *
     * If $to is callable, it's executed with two args:
     *  $from - string to replace from
     *  $trfm - current Transformer instance ($this)
     *
     * @param string $from
     * @param string|callable $to
     *
     * @return TransformerWorker
     */
    public function ireplace(string $from, $to): self
    {
        if (is_callable($to)) {
            $to = $to($from, $this->string);
        }

        $this->string = str_ireplace($from, $to, $this->string);
        return $this;
    }

    /**
     * Wrapper for ltrim.
     *
     * @param string $charmask
     *
     * @return TransformerWorker
     */
    public function lstrip($charmask = " \t\n\r\0\x0B"): self
    {
        return $this->transform('ltrim', $charmask);
    }

    /**
     * Calls given callable from $fn. As a first argument is passed transformed string
     * from Transformer, then pass there other args from Transformer::transform method.
     *
     * @return TransformerWorker
     */
    public function transform(): self
    {
        $args = func_get_args();
        $fn = array_shift($args);
        array_unshift($args, $this->string);

        $this->string = call_user_func_array($fn, $args);
        return $this;
    }

    /**
     * Wrapper for rtrim.
     *
     * @param string $charmask
     *
     * @return TransformerWorker
     */
    public function rstrip($charmask = " \t\n\r\0\x0B"): self
    {
        return $this->transform('rtrim', $charmask);
    }

    /**
     * Wrapper for strtoupper.
     *
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function upper($encoding = null): self
    {
        if (!$this->hasMbstring()) {
            return $this->transform('strtoupper');
        }

        $encoding = $this->encoding($encoding);

        return $this->transform('mb_convert_case', MB_CASE_UPPER, $encoding);
    }

    protected function hasMbstring(): bool
    {
        return extension_loaded('mbstring');
    }

    protected function encoding($encoding)
    {
        if (is_null($encoding) && $this->hasMbstring()) {
            return mb_internal_encoding();
        }

        return $encoding;
    }

    /**
     * Wrapper for strtolower.
     *
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function lower($encoding = null): self
    {
        if (!$this->hasMbstring()) {
            return $this->transform('strtolower');
        }

        $encoding = $this->encoding($encoding);

        return $this->transform('mb_convert_case', MB_CASE_LOWER, $encoding);
    }

    /**
     * Wrapper for ucfirst.
     *
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function upperFirst($encoding = null): self
    {
        if (!$this->hasMbstring()) {
            return $this->transform('ucfirst');
        }

        $encoding = $this->encoding($encoding);

        $string = mb_strtoupper(mb_substr($this->string, 0, 1, $encoding), $encoding);

        $this->string = $string . mb_substr($this->string, 1, mb_strlen($this->string, $encoding), $encoding);
        return $this;
    }

    /**
     * Wrapper for lcfirst.
     *
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function lowerFirst($encoding = null): self
    {
        if (!$this->hasMbstring()) {
            return $this->transform('lcfirst');
        }

        $encoding = $this->encoding($encoding);

        $string = mb_strtolower(mb_substr($this->string, 0, 1, $encoding), $encoding);

        $this->string = $string . mb_substr($this->string, 1, mb_strlen($this->string, $encoding), $encoding);
        return $this;
    }

    /**
     * Wrapper for ucwords.
     *
     * @param string $delimiters (ignored if php do not handle this parameter)
     *
     * @return TransformerWorker
     */
    public function upperWords($delimiters = null): self
    {
        if (is_null($delimiters)) {
            $delimiters = " \t\r\n\f\v";
        }

        if (
            (version_compare(PHP_VERSION, '5.4.32', '>=') && version_compare(PHP_VERSION, '5.5.0', '<')) ||
            version_compare(PHP_VERSION, '5.5.16', '>=')
        ) {
            return $this->transform('ucwords', $delimiters);
        } else {
            return $this->transform('ucwords');
        }
    }

    /**
     * Wrapper for wordwrap.
     *
     * @param int $width
     * @param string $break
     * @param bool $cut
     *
     * @return TransformerWorker
     */
    public function wordWrap($width = 75, $break = "\n", $cut = false): self
    {
        return $this->transform('wordwrap', $width, $break, $cut);
    }

    /**
     * Wrapper for mb_substr / substr.
     *
     * @param $start
     * @param int|null $length
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function substr($start, $length = null, $encoding = null): self
    {
        if (!$this->hasMbstring()) {
            return $this->transform('substr', $start, $length);
        }

        $encoding = $this->encoding($encoding);

        // workaround for php < 5.4.8
        if (is_null($length)) {
            $length = mb_strlen($this->string, $encoding);
        }

        return $this->transform('mb_substr', $start, $length, $encoding);
    }

    /**
     * Wrapper for str_repeat.
     *
     * @param integer $count
     *
     * @return TransformerWorker
     */
    public function repeat(int $count): self
    {
        return $this->transform('str_repeat', $count);
    }

    /**
     * Reverse string.
     *
     * @param null $encoding
     *
     * @return TransformerWorker
     */
    public function reverse($encoding = null): self
    {
        $reversed = '';

        if (!$this->hasMbstring()) {
            $len = strlen($this->string);
            for ($i = $len - 1; $i >= 0; --$i) {
                $reversed .= substr($this->string, $i, 1);
            }
        } else {
            $encoding = $this->encoding($encoding);

            $len = mb_strlen($this->string, $encoding);
            for ($i = $len - 1; $i >= 0; --$i) {
                $reversed .= mb_substr($this->string, $i, 1, $encoding);
            }
        }

        $this->string = $reversed;
        return $this;
    }

    /**
     * Squash and unify white characters into single space.
     *
     * @return TransformerWorker
     */
    public function squashWhitechars(): self
    {
        return $this->regexReplace('/[[:space:]]+/u', ' ')->strip();
    }

    /**
     * Wrapper for trim.
     *
     * @param string $charmask
     *
     * @return TransformerWorker
     */
    public function strip($charmask = " \t\n\r\0\x0B"): self
    {
        return $this->transform('trim', $charmask);
    }

    /**
     * Wrapper for preg_replace or preg_replace_callback (depends on $replacement
     * being callback or not).
     *
     * @param string $pattern
     * @param string $replacement
     * @param int $limit
     *
     * @return TransformerWorker
     */
    public function regexReplace(string $pattern, string $replacement, $limit = -1): self
    {
        if (is_callable($replacement)) {
            $rxp_function = 'preg_replace_callback';
        } else {
            $rxp_function = 'preg_replace';
        }

        $this->string = $rxp_function($pattern, $replacement, $this->string, $limit);

        return $this;
    }

    /**
     * Insert given string at $idx.
     *
     * @param string $substring
     * @param integer $idx
     * @param string|null $encoding
     *
     * @return TransformerWorker
     */
    public function insert(string $substring, int $idx, $encoding = null): self
    {
        if ($idx <= 0) {
            return $this->prefix($substring);
        }

        if (!$this->hasMbstring()) {
            $len = strlen($this->string);
            if ($idx >= $len) {
                return $this->suffix($substring);
            }

            $start = substr($this->string, 0, $idx);
            $end = substr($this->string, $idx, $len);
        } else {
            $encoding = $this->encoding($encoding);
            $len = mb_strlen($this->string, $encoding);
            if ($idx >= $len) {
                return $this->suffix($substring);
            }

            $start = mb_substr($this->string, 0, $idx, $encoding);
            $end = mb_substr($this->string, $idx, $len, $encoding);
        }

        $this->string = $start . $substring . $end;
        return $this;
    }

    /**
     * Prepend some string on the beginning.
     *
     * @param string $string
     *
     * @return TransformerWorker
     */
    public function prefix(string $string): self
    {
        $this->string = $string . $this->string;
        return $this;
    }

    /**
     * Append some string to the end.
     *
     * @param string $string
     *
     * @return TransformerWorker
     */
    public function suffix(string $string): self
    {
        $this->string = $this->string . $string;
        return $this;
    }

    /**
     * Prepend $substring if string doesn't begin with it.
     *
     * @param $substring
     * @param null $encoding
     *
     * @return TransformerWorker
     */
    public function ensurePrefix($substring, $encoding = null): self
    {
        if (!$this->hasMbstring()) {
            $len = strlen($substring);
            if (substr($this->string, 0, $len) == $substring) {
                return $this;
            }
        } else {
            $encoding = $this->encoding($encoding);
            $len = mb_strlen($substring, $encoding);
            if (mb_substr($this->string, 0, $len, $encoding) == $substring) {
                return $this;
            }
        }

        return $this->prefix($substring);
    }

    /**
     * Append $substring if string doesn't end with it.
     *
     * @param $substring
     * @param null $encoding
     *
     * @return TransformerWorker
     */
    public function ensureSuffix($substring, $encoding = null): self
    {
        if (!$this->hasMbstring()) {
            $len = strlen($substring);
            if (substr($this->string, -$len) == $substring) {
                return $this;
            }
        } else {
            $encoding = $this->encoding($encoding);

            $len = mb_strlen($substring, $encoding);
            if (mb_substr($this->string, -$len, mb_strlen($this->string, $encoding), $encoding) == $substring) {
                return $this;
            }
        }

        return $this->suffix($substring);
    }

    /**
     * Append some string to the beginning and to the end.
     *
     * @param string $string
     *
     * @return $this
     */
    public function surround(string $string): self
    {
        $this->string = $string . $this->string . $string;
        return $this;
    }

    /**
     * Adds PHP_EOL to the end of string.
     *
     * @return TransformerWorker
     */
    public function eol(): self
    {
        $this->string .= PHP_EOL;
        return $this;
    }

    /**
     * Adds \r\n to the end of string.
     *
     * @return TransformerWorker
     */
    public function eolrn(): self
    {
        $this->string .= "\r\n";
        return $this;
    }

    /**
     * Adds \n to the end of string.
     *
     * @return TransformerWorker
     */
    public function eoln(): self
    {
        $this->string .= "\n";
        return $this;
    }

    public function __toString(): string
    {
        return (string)$this->string;
    }
}
