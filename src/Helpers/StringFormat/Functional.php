<?php /** @noinspection SpellCheckingInspection */
/** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * Functional.
 *
 * @category  Helpers
 * @package   Exen\Support\Helpers\StringFormat
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types = 1);

namespace Exen\Support\Helpers\StringFormat;

use function array_shift;
use function call_user_func_array;
use function func_get_args;

/**
 * Functional API for FormatterIndex. Instead of:
 *
 *   new FormatterIndex('some {} format')->compile('glorious');
 *
 * you can call:
 *
 *   iformat('some {} format', ['glorious']);
 *
 * Few characters less :)
 *
 * @param string $format
 * @param array $params parameters used to fill placeholders
 *
 * @return Transformer
 */
function iformat(string $format, $params = []): Transformer
{
    $fmt = new FormatterIndexed($format);
    return call_user_func_array([$fmt, 'compile'], $params);
}

/**
 * Functional API for FormatterIndex. Instead of:
 *
 *   new FormatterIndex('some {} format')->compile('glorious');
 *
 * you can call:
 *
 *   iformatl('some {} format', 'glorious');
 *
 * Few characters less :)
 *
 * @param string $format
 * @return Transformer
 */
function iformatl(string $format): Transformer
{
    $params = func_get_args();
    array_shift($params);
    $fmt = new FormatterIndexed($format);
    return call_user_func_array([$fmt, 'compile'], $params);
}

/**
 * Functional API for FormatterNamed. Instead of:
 *
 *   new FormatterNamed('some {adjective} format')->compile(['adjective' => 'glorious']);
 *
 * you can call:
 *
 *   nformat('some {adjective} format', ['adjective' => 'glorious']);
 *
 * Few characters less :)
 *
 * @param string $format
 * @param array $params parameters used to fill placeholders
 *
 * @return Transformer
 */
function nformat(string $format, $params = []): Transformer
{
    $fmt = new FormatterNamed($format);
    return $fmt->compile($params);
}

/**
 * Functional API for FormatterPlaceholder. Instead of:
 *
 *   new FormatterNamed('some %1 format')->compile(['glorious']);
 *
 * you can call:
 *
 *   phformat('some %1 format', ['glorious']);
 *
 * Few characters less :)
 *
 * @param string $format
 * @param array $params parameters used to fill placeholders
 *
 * @return Transformer
 */
function phformat(string $format, $params = []): Transformer
{
    $fmt = new FormatterPlaceholder($format);
    return $fmt->compile($params);
}
