<?php /** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * Transformer.
 *
 * @category  Helpers
 * @package   Exen\Support\Helpers\StringFormat
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types = 1);

namespace Exen\Support\Helpers\StringFormat;

use function func_get_args;

class Transformer
{
    /** @var array */
    protected $modifiers = [];
    /** @var Compiler */
    protected $input;
    /** @var string */
    protected $unfolded;

    /**
     * TransformerBuilder constructor.
     *
     * @param Compiler $input
     */
    public function __construct(Compiler $input)
    {
        $this->input = $input;
    }

    /**
     * @return Compiler
     */
    public function getInput(): Compiler
    {
        return $this->input;
    }

    /**
     * @param string $name modifier name
     * @param array $args modifier arguments
     * @return Transformer
     */
    protected function addModifier(string $name, array $args): Transformer
    {
        $modifiers = $this->modifiers;
        $modifiers[] = array('name' => $name, 'args' => $args);

        $ret = new static($this->getInput());
        $ret->modifiers = $modifiers;

        return $ret;
    }

    /**
     * Calls given callable from $fn. As a first argument is passed transformed string
     * from Transformer, then pass there other args from Transformer::transform method.
     *
     * @return Transformer
     */
    public function transform(): Transformer
    {
        return $this->addModifier('transform', func_get_args());
    }

    /**
     * Wrapper for str_replace.
     *
     * If $to is callable, it's executed with two args:
     *  $from - string to replace from
     *  $trfm - current Transformer instance ($this)
     *
     * @param string $from
     * @param string|callable $to
     *
     * @return Transformer
     */
    public function replace(string $from, $to): Transformer
    {
        return $this->addModifier('replace', array($from, $to));
    }

    /**
     * Wrapper for str_ireplace.
     *
     * If $to is callable, it's executed with two args:
     *  $from - string to replace from
     *  $trfm - current Transformer instance ($this)
     *
     * @param string $from
     * @param string|callable $to
     *
     * @return Transformer
     */
    public function ireplace(string $from, $to): Transformer
    {
        return $this->addModifier('ireplace', array($from, $to));
    }

    /**
     * Wrapper for preg_replace or preg_replace_callback (depends on $replacement
     * being callback or not).
     *
     * @param string $pattern
     * @param string $replacement
     * @param int $limit
     *
     * @return Transformer
     */
    public function regexReplace(string $pattern, string $replacement, $limit = -1): Transformer
    {
        return $this->addModifier('regexReplace', array($pattern, $replacement, $limit));
    }

    /**
     * Wrapper for trim.
     *
     * @param string $charmask
     *
     * @return Transformer
     */
    public function strip($charmask = " \t\n\r\0\x0B"): Transformer
    {
        return $this->addModifier('strip', array($charmask));
    }

    /**
     * Wrapper for ltrim.
     *
     * @param string $charmask
     *
     * @return Transformer
     */
    public function lstrip($charmask = " \t\n\r\0\x0B"): Transformer
    {
        return $this->addModifier('lstrip', array($charmask));
    }

    /**
     * Wrapper for rtrim.
     *
     * @param string $charmask
     *
     * @return Transformer
     */
    public function rstrip($charmask = " \t\n\r\0\x0B"): Transformer
    {
        return $this->addModifier('rstrip', array($charmask));
    }

    /**
     * Wrapper for strtoupper.
     *
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function upper($encoding = null): Transformer
    {
        return $this->addModifier('upper', array($encoding));
    }

    /**
     * Wrapper for strtolower.
     *
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function lower($encoding = null): Transformer
    {
        return $this->addModifier('lower', array($encoding));
    }

    /**
     * Wrapper for ucfirst.
     *
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function upperFirst($encoding = null): Transformer
    {
        return $this->addModifier('upperFirst', array($encoding));
    }

    /**
     * Wrapper for lcfirst.
     *
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function lowerFirst($encoding = null): Transformer
    {
        return $this->addModifier('lowerFirst', array($encoding));
    }

    /**
     * Wrapper for ucwords.
     *
     * @param string $delimiters (ignored if php do not handle this parameter)
     *
     * @return Transformer
     */
    public function upperWords($delimiters = null): Transformer
    {
        return $this->addModifier('upperWords', array($delimiters));
    }

    /**
     * Wrapper for wordwrap.
     *
     * @param int    $width
     * @param string $break
     * @param bool   $cut
     *
     * @return Transformer
     */
    public function wordWrap($width = 75, $break = "\n", $cut = false): Transformer
    {
        return $this->addModifier('wordWrap', array($width, $break, $cut));
    }

    /**
     * Wrapper for mb_substr / substr.
     *
     * @param $start
     * @param int|null $length
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function substr($start, $length = null, $encoding = null): Transformer
    {
        return $this->addModifier('substr', array($start, $length, $encoding));
    }

    /**
     * Wrapper for str_repeat.
     *
     * @param integer $count
     *
     * @return Transformer
     */
    public function repeat(int $count): Transformer
    {
        return $this->addModifier('repeat', array($count));
    }

    /**
     * Reverse string.
     *
     * @param null $encoding
     *
     * @return Transformer
     */
    public function reverse($encoding = null): Transformer
    {
        return $this->addModifier('reverse', array($encoding));
    }

    /**
     * Squash and unify white characters into single space.
     *
     * @return Transformer
     */
    public function squashWhitechars(): Transformer
    {
        return $this->addModifier('squashWhitechars', []);
    }

    /**
     * Insert given string at $idx.
     *
     * @param string $substring
     * @param integer $idx
     * @param string|null $encoding
     *
     * @return Transformer
     */
    public function insert(string $substring, int $idx, $encoding = null): Transformer
    {
        return $this->addModifier('insert', array($substring, $idx, $encoding));
    }

    /**
     * Prepend $substring if string doesn't begin with it.
     *
     * @param $substring
     * @param null $encoding
     *
     * @return Transformer
     */
    public function ensurePrefix($substring, $encoding = null): Transformer
    {
        return $this->addModifier('ensurePrefix', array($substring, $encoding));
    }

    /**
     * Append $substring if string doesn't end with it.
     *
     * @param $substring
     * @param null $encoding
     *
     * @return Transformer
     */
    public function ensureSuffix($substring, $encoding = null): Transformer
    {
        return $this->addModifier('ensureSuffix', array($substring, $encoding));
    }

    /**
     * Prepend some string on the beginning.
     *
     * @param string $string
     *
     * @return Transformer
     */
    public function prefix(string $string): Transformer
    {
        return $this->addModifier('prefix', array($string));
    }

    /**
     * Append some string to the end.
     *
     * @param string $string
     *
     * @return Transformer
     */
    public function suffix(string $string): Transformer
    {
        return $this->addModifier('suffix', array($string));
    }

    /**
     * Append some string to the beginning and to the end.
     *
     * @param string $string
     *
     * @return Transformer
     */
    public function surround(string $string): Transformer
    {
        return $this->addModifier('surround', array($string));
    }

    /**
     * Adds PHP_EOL to the end of string.
     *
     * @return Transformer
     */
    public function eol(): Transformer
    {
        return $this->addModifier('eol', []);
    }

    /**
     * Adds \r\n to the end of string.
     *
     * @return Transformer
     */
    public function eolrn(): Transformer
    {
        return $this->addModifier('eolrn', []);
    }

    /**
     * Adds \n to the end of string.
     *
     * @return Transformer
     */
    public function eoln(): Transformer
    {
        return $this->addModifier('eoln', []);
    }

    /**
     * Apply all modifiers for given input and return formatted string.
     *
     * @return string
     */
    public function unfold(): string
    {
        if (is_null($this->unfolded)) {
            $string = $this->input->run();
            $transformer = new TransformerWorker($string);
            foreach ($this->modifiers as $modifier) {
                call_user_func_array(array($transformer, $modifier['name']), $modifier['args']);
            }
            $this->unfolded = (string) $transformer;
        }

        return $this->unfolded;
    }

    public function __toString(): string
    {
        return $this->unfold();
    }
}
