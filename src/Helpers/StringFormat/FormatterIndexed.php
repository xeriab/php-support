<?php /** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * FormatterPlaceholder.
 *
 * @category  Helpers
 * @package   Exen\Support\Helpers\StringFormat
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types = 1);

namespace Exen\Support\Helpers\StringFormat;

use function array_shift;
use function func_get_args;
use function func_num_args;

class FormatterIndexed implements FormatterInterface
{
    /**
     * Store provided by user format string.
     *
     * @var string
     */
    protected $format = null;

    /**
     * Params for placeholders from format.
     *
     * @var array
     */
    protected $params = [];

    /**
     * @param string $format format to compile
     */
    public function __construct(string $format)
    {
        $this->format = $format;

        if (func_num_args() > 1) {
            $args = func_get_args();
            array_shift($args);
            $this->params = $args;
        }
    }

    /**
     * Parse given format and fill it's placeholders with params.
     *
     * @param ...mixed $params parameters used to format given string
     *
     * @return Transformer
     */
    public function compile(): Transformer
    {
        $params = (func_num_args() > 0 ? func_get_args() : $this->params);

        $compiler = new Compiler($this->format, $params, Compiler::MODE_INDEXED);

        return new Transformer($compiler);
    }

    public function __toString(): ?string
    {
        return $this->format;
    }
}
