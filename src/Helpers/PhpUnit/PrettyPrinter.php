<?php

declare(strict_types=1);

namespace Exen\Support\Helpers\PhpUnit;

use PHPUnit\Framework\Test;
use PHPUnit\Framework\TestFailure;
use PHPUnit\Framework\TestListener;
use PHPUnit\Framework\TestSuite;
use PHPUnit\TextUI\ResultPrinter;
use PHPUnit\Util\Filter;

class PrettyPrinter extends ResultPrinter implements TestListener
{
    protected $className;
    protected $previousClassName;
    protected $timeColors;

    protected $defaultTimeColors = [
        '1'    => 'fg-red',
        '.400' => 'fg-yellow',
        '0'    => 'fg-green',
    ];

    public function startTestSuite(TestSuite $suite): void
    {
        if ($this->debug && \is_null($this->timeColors)) {
            if (defined('EXEN_PRINTER_TIME_COLORS') && is_array(EXEN_PRINTER_TIME_COLORS)) {
                $this->timeColors = EXEN_PRINTER_TIME_COLORS;
                \krsort($this->timeColors, \SORT_NUMERIC);
            } else {
                $this->timeColors = $this->defaultTimeColors;
            }
        }

        parent::startTestSuite($suite);
    }

    public function startTest(Test $test): void
    {
        $this->className = \get_class($test);

        if (!$this->debug) {
            parent::startTest($test);
        }
    }

    public function endTest(Test $test, float $time): void
    {
        parent::endTest($test, $time);

        $testMethodName = \PHPUnit\Util\Test::describe($test);

        // Convert snakeCase method name to camelCase
        $testMethodName[1] = \str_replace('_', '', \ucwords($testMethodName[1], '_'));

        \preg_match_all('/((?:^|[A-Z])[a-z]+)/', $testMethodName[1], $matches);
        $testNameArray = \array_map('strtolower', $matches[0]);

        // Check if prefix is test remove it
        if ($testNameArray[0] === 'test') {
            \array_shift($testNameArray);
        }

        $name = implode(' ', $testNameArray);

        // Get the data set name
        $name = $this->handleDataSetName($name, $testMethodName[1]);

        $color = 'fg-green';
        if ($test->getStatus() !== 0) {
            $color = 'fg-red';
        }

        $this->write(' ');
        $this->writeWithColor($color, $name, false);
        $this->write(' ');

        $timeColor = $time > 0.5 ? 'fg-yellow' : 'fg-white';
        $this->writeWithColor($timeColor, '[' . number_format($time, 3) . 's]', true);
    }

    protected function writeProgress(string $progress): void
    {
        if ($this->previousClassName !== $this->className) {
            $this->write("\n");
            $this->writeWithColor('bold', $this->className, false);
            $this->writeNewLine();
        }

        $this->previousClassName = $this->className;

        if ($progress == '.') {
            // TODO: Fix me
            // $this->writeWithColor('fg-green', '  ✓', false);
            $this->writeWithColor('fg-green', '  [OK]', false);
        } else {
            // TODO: Fix me
            // $this->writeWithColor('fg-red', '  x', false);
            $this->writeWithColor('fg-red', '  [KO]', false);
        }
    }

    protected function printDefectTrace(TestFailure $defect): void
    {
        $this->write($this->formatExceptionMsg($defect->getExceptionAsString()));
        $trace = Filter::getFilteredStacktrace(
            $defect->thrownException()
        );
        if (!empty($trace)) {
            $this->write("\n" . $trace);
        }
        $exception = $defect->thrownException()->getPrevious();
        while ($exception) {
            $this->write(
                "\nCaused by\n" .
                    TestFailure::exceptionToString($exception) . "\n" .
                    Filter::getFilteredStacktrace($exception)
            );
            $exception = $exception->getPrevious();
        }
    }

    protected function formatExceptionMsg($exceptionMessage): string
    {
        $exceptionMessage = \str_replace("+++ Actual\n", '', $exceptionMessage);
        $exceptionMessage = \str_replace("--- Expected\n", '', $exceptionMessage);
        $exceptionMessage = \str_replace('@@ @@', '', $exceptionMessage);

        if ($this->colors) {
            $exceptionMessage = \preg_replace('/^(Exception.*)$/m', "\033[01;31m$1\033[0m", $exceptionMessage);
            $exceptionMessage = \preg_replace('/(Failed.*)$/m', "\033[01;31m$1\033[0m", $exceptionMessage);
            $exceptionMessage = \preg_replace("/(\-+.*)$/m", "\033[01;32m$1\033[0m", $exceptionMessage);
            $exceptionMessage = \preg_replace("/(\++.*)$/m", "\033[01;31m$1\033[0m", $exceptionMessage);
        }

        return $exceptionMessage;
    }

    private function handleDataSetName($name, $testMethodName): string
    {
        \preg_match('/\bwith data set "([^"]+)"/', $testMethodName, $dataSetMatch);

        if (empty($dataSetMatch)) {
            return $name;
        }

        return $name . ' [' . $dataSetMatch[1] . ']';
    }
}
