<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Lang;
use Exen\Support\PhpObject;
use Exen\Support\Sanitizer;
use Exen\Support\Traits\MacroableTrait;

class Hook extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const KEY_FUNCTION      = 0x1;
    const KEY_ACCEPTED_ARGS = 0x2;

    /**
     * @var array[]|array[][]
     */
    protected $filters = [];

    /**
     * @var array
     */
    protected $merged = [];

    /**
     * @var array
     */
    protected $current = [];

    /**
     * @var array
     */
    protected $actions = [];

    /**
     * Hook instance.
     *
     * @var \Exen\Support\Helpers\Hook
     */
    private static $instance = null;

    /**
     * Hook instances.
     *
     * @var \Exen\Support\Helpers\Hook[]
     */
    private static $instances = [];

    /**
     * Hook constructor.
     */
    final public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * Returns the Hook instance.
     *
     * @return \Exen\Support\Helpers\Hook
     */
    public static function instance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Returns the Hook instance.
     *
     * @return \Exen\Support\Helpers\Hook
     */
    public static function getInstance(): self
    {
        return static::instance();
    }

    /**
     * Add Hooks Function it just like a WordPress add_action() / add_filter() hooks.
     *
     * @param string          $hookName          Hook name
     * @param callable|string $callable          Callable
     * @param int             $priority          Priority
     * @param int             $acceptedArguments Count of accepted arguments/parameters.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function add(
        ?string $hookName = null,
        $callable = null,
        ?int $priority = 10,
        ?int $acceptedArguments = 1
    ): bool {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName) {
            throw new \InvalidArgumentException('Invalid Hook Name Specified', \E_USER_ERROR);
        }

        $id = Lang::uniqueID($callable);

        if (null === $id) {
            throw new \RuntimeException(
                \sprintf(
                    'Invalid callable specified on hook name %s',
                    $hookName
                ),
                \E_USER_ERROR
            );
        }

        $priority = !\is_numeric($priority)
            ? 10
           : \abs(\intval($priority));

        if (!isset($this->filters[$hookName])) {
            $this->filters[$hookName] = [];
        }

        if (!isset($this->filters[$hookName][$priority])) {
            $this->filters[$hookName][$priority] = [];
        }

        $this->filters[$hookName][$priority][$id] = [
            self::KEY_FUNCTION      => $callable,
            self::KEY_ACCEPTED_ARGS => $acceptedArguments,
        ];

        // Remove merged hook
        unset($this->merged[$hookName]);

        return true;
    }

    /**
     * Add Hooks Function it just like a WordPress add_action() / add_filter() hooks.
     *
     * @param string          $hookName          Hook name
     * @param callable|string $callable          Callable
     * @param int             $priority          Priority
     * @param int             $acceptedArguments Count of accepted arguments/parameters.
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function addAction(
        ?string $hookName = null,
        $callable = null,
        ?int $priority = 10,
        ?int $acceptedArguments = 1
    ): bool {
        if (Lang::isCallableOrClosure($callable)) {
            return $this->add($hookName, $callable, $priority, $acceptedArguments);
        }

        return false;
    }

    /**
     * Check if hook name exists.
     *
     * @param string       $hookName        Hook name
     * @param string|mixed $functionToCheck Specially Functions on Hook
     *
     * @return bool|int
     */
    public function exists(?string $hookName = null, $functionToCheck = false)
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName || !isset($this->filters[$hookName])) {
            return false;
        }

        // Don't reset the internal array pointer
        $has = empty($this->filters[$hookName]);

        // Make sure at least one priority has a filter callback
        if ($has) {
            $exists = false;
            foreach ($this->filters[$hookName] as $callbacks) {
                if (!empty($callbacks)) {
                    $exists = true;
                    break;
                }
            }

            if (!$exists) {
                $has = false;
            }
        }

        // Recheck
        if (false === $functionToCheck || false === $has) {
            return $has;
        }

        if (!$id = Lang::uniqueID($functionToCheck)) {
            return false;
        }

        foreach ($this->filters[$hookName] as $priority) {
            if (isset($priority[$id])) {
                return $priority;
            }
        }

        return false;
    }

    /**
     * Applying Hooks for replaceable and returning as $value param.
     *
     * @param string $hookName Hook Name replaceable
     * @param mixed  $value    returning value
     *
     * @return mixed
     */
    public function apply(?string $hookName = null, $value = null)
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName) {
            throw new \InvalidArgumentException(
                'Invalid Hook Name Specified',
                \E_USER_ERROR
            );
        }

        if (!isset($this->filters[$hookName])) {
            return $value;
        }

        // add increment data
        $this->current[] = $hookName;

        /*
         * Sorting
         */
        if (!isset($this->merged[$hookName])) {
            \ksort($this->filters[$hookName]);
            $this->merged[$hookName] = true;
        }

        // Reset sorting position
        \reset($this->filters[$hookName]);
        $args = \func_get_args();

        do {
            foreach (\current($this->filters[$hookName]) as $collection) {
                if (!\is_null($collection[self::KEY_FUNCTION])) {
                    $args[1] = $value;
                    $value = \call_user_func_array(
                        $collection[self::KEY_FUNCTION],
                        \array_slice(
                            $args,
                            1,
                            (int) $collection[self::KEY_ACCEPTED_ARGS]
                        )
                    );
                }
            }
        } while (false !== \next($this->filters[$hookName]));

        \array_pop($this->current);

        return $value;
    }

    /**
     * Call hook from existing declared hook record.
     *
     * @param string $hookName Hook Name
     * @param string $arg      the arguments for next parameter
     *
     * @return bool
     */
    public function call(?string $hookName = null, $arg = ''): bool
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName) {
            return false;
        }

        if (!isset($this->actions[$hookName])) {
            $this->actions[$hookName] = 1;
        } else {
            ++$this->actions[$hookName];
        }

        if (!isset($this->filters[$hookName])) {
            return false;
        }

        $this->current[] = $hookName;

        $args = [];

        if (\is_array($arg) && 1 == \count($arg) && isset($arg[0]) && \is_object($arg[0])) {
            $args[] = &$arg[0];
        } else {
            $args[] = $arg;
        }

        for ($a = 2, $num = \func_num_args(); $a < $num; ++$a) {
            $args[] = \func_get_arg($a);
        }

        // Sort
        if (!isset($this->merged[$hookName])) {
            \ksort($this->filters[$hookName]);
            $this->merged[$hookName] = true;
        }

        \reset($this->filters[$hookName]);

        do {
            foreach (\current($this->filters[$hookName]) as $collection) {
                if (!\is_null($collection[self::KEY_FUNCTION])) {
                    @\call_user_func_array(
                        $collection[self::KEY_FUNCTION],
                        \array_slice(
                            $args,
                            0,
                            (int) $collection[self::KEY_ACCEPTED_ARGS]
                        )
                    );
                }
            }
        } while (false !== next($this->filters[$hookName]));

        \array_pop($this->current);

        return true;
    }

    /**
     * Replace Hooks Function, this will replace all existing hooks.
     *
     * @param string          $hookName          Hook Name
     * @param string          $functionToReplace Function to replace
     * @param callable|string $callable          Callable
     * @param int             $priority          priority
     * @param int             $acceptedArguments num count of accepted args / parameter
     * @param bool            $create            true if want to create new if not exists
     *
     * @return bool
     *
     * @throws \InvalidArgumentException
     * @throws \RuntimeException
     */
    public function replace(
        string $hookName = null,
        string $functionToReplace,
        $callable = null,
        int $priority = 10,
        int $acceptedArguments = 1,
        bool $create = true
    ): bool {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName) {
            throw new \InvalidArgumentException(
                'Invalid Hook Name Specified',
                \E_USER_ERROR
            );
        }

        if (!Lang::isCallableOrClosure($callable)) {
            throw new \RuntimeException(
                'Invalid Hook Callable Specified',
                \E_USER_ERROR
            );
        }

        if (($has = $this->exists($hookName, $functionToReplace)) || $create) {
            $has && $this->remove($hookName, $functionToReplace);
            // Add hooks first
            return $this->add($hookName, $callable, $priority, $acceptedArguments);
        }

        return false;
    }

    /**
     * Removing Hook (remove single hook).
     *
     * @param string $hookName         Hook Name
     * @param string $functionToRemove functions that to remove from determine $hookName
     * @param int    $priority         priority
     *
     * @return bool
     */
    public function remove(string $hookName = null, string $functionToRemove, int $priority = 10): bool
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName) {
            return false;
        }

        $functionToRemove = Lang::uniqueID($functionToRemove);

        $r = $this->filters[$hookName][$priority];
        $r = $r ? $r->has($functionToRemove): alse;

        if (true === $r) {
            $this->filters[$hookName][$priority]->remove($functionToRemove);

            if ($this->filters[$hookName][$priority]->isEmpty()) {
                unset($this->filters[$hookName][$priority]);
            }

            if (empty($this->filters[$hookName])) {
                $this->filters[$hookName] = [];
            }

            unset($this->merged[$hookName]);
        }

        return $r;
    }

    /**
     * Remove all of the hooks from a filter.
     *
     * @param string   $hookName the filter to remove hooks from
     * @param int|bool $priority Optional. The priority number to remove. Default false.
     *
     * @return bool
     */
    public function removeAll(string $hookName = null, $priority = false): bool
    {
        if (isset($this->filters[$hookName])) {
            if (false === $priority || null === $priority) {
                $this->filters[$hookName] = [];
            } elseif (isset($this->filters[$hookName][$priority])) {
                $this->filters[$hookName][$priority] = [];
            }
        }

        unset($this->merged[$hookName]);

        return true;
    }

    /**
     * Current position.
     *
     * @return string functions
     */
    public function current()
    {
        return \end($this->current);
    }

    /**
     * Count all existences Hook.
     *
     * @param string $hookName Hook name
     *
     * @return int Hooks Count
     */
    public function count(string $hookName = null): int
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName || !isset($this->filters[$hookName])) {
            return 0;
        }

        return \count($this->filters[$hookName]);
    }

    /**
     * Check if hook has doing.
     *
     * @param string $hookName Hook name
     *
     * @return bool true if has doing
     */
    public function hasDoing(string $hookName = null): bool
    {
        if (null === $hookName) {
            return !empty($this->current);
        }

        $hookName = Sanitizer::sanitizeKeyName($hookName);

        return $hookName && isset($this->current[$hookName]);
    }

    /**
     * Check if action hook as execute.
     *
     * @param string $hookName Hook Name
     *
     * @return int Count of hook action if has did action
     */
    public function hasCalled(string $hookName = null): nt
    {
        $hookName = Sanitizer::sanitizeKeyName($hookName);

        if (!$hookName || !isset($this->actions[$hookName])) {
            return 0;
        }

        return $this->actions[$hookName];
    }

    /**
     * Call all hook.
     *
     * string $args     the arguments for next parameter
     *
     * @return void
     */
    public function callAllHook($args = null)
    {
        \reset($this->filters['all']);

        do {
            foreach ((array) \current($this->filters['all']) as $the) {
                if (!\is_null($the['function'])) {
                    \call_user_func_array($the['function'], $args);
                }
            }
        } while (\next($this->filters['all']) !== false);
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
