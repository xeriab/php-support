<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\PhpObject;
use Exen\Support\Lang;

final class Gravatar extends PhpObject implements Singletonable
{
    use MacroableTrait;

    /**
     * Gravatar instance.
     *
     * @var \Exen\Support\Helpers\Gravatar
     */
    private static $instance = null;

    /**
     * Gravatar instances.
     *
     * @var \Exen\Support\Helpers\Gravatar[]
     */
    private static $instances = [];

    /**
     * @var string
     */
    private $baseUrl = 'http://www.gravatar.com';

    /**
     * @var array
     */
    private $options = [];

    /**
     * @var \Symfony\Component\OptionsResolver\OptionsResolver
     */
    private $resolver = null;

    public function __construct(array $options = [])
    {
        $this->resolver = new OptionsResolver();
        $this->resolver->setDefaults([
            'size'    => null,
            'rating'  => null,
            'default' => null,
        ]);
        $this->resolveOptions($options);

        self::$instance = &$this;
    }

    /**
     * Returns the Gravatar instance.
     *
     * @return \Exen\Support\Helpers\Gravatar
     */
    public static function instance(): self
    {
        return self::$instance;
    }

    /**
     * Returns the Gravatar instance.
     *
     * @return \Exen\Support\Helpers\Gravatar
     */
    public static function getInstance(): self
    {
        return self::$instance;
    }

    /**
     * Gets gravatar URL.
     *
     * @param string|null $email
     * @param array|null $options
     *
     * @return string
     */
    public function getUrl(?string $email = null, ?array $options = []): string
    {
        $this->resolveOptions($options);

        $url = \sprintf(
            '%s/avatar/%s',
            $this->baseUrl,
            \md5(\strtolower(trim($email)))
        );

        if ($query = \http_build_query($this->options)) {
            $url .= '?'.$query;
        }

        return $url;
    }

    /**
     * Sets base URL.
     *
     * @param string|null $baseUrl
     *
     * @return self
     */
    public function setBaseUrl(?string $baseUrl = null): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    /**
     * Resolves options.
     *
     * @param array $options
     *
     * @return void
     */
    private function resolveOptions(array $options = []): void
    {
        if (Lang::isNotEmpty($this->options)) {
            $this->resolver->setDefaults($this->options);
        }

        $this->options = $this->resolver->resolve($options);

        if (Lang::isSet($this->options, 'default')) {
            $this->options['default'] = \rawurlencode($this->options['default']);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
