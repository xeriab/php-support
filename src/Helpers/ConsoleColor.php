<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\Exception\InvalidStyleException;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\PhpObject;

/**
 * Class ConsoleColor.
 *
 * @since 1.0
 */
class ConsoleColor extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const FOREGROUND      = 38;
    const BACKGROUND      = 48;
    const COLOR256_REGEXP = '~^(bg_)?color_([0-9]{1,3})$~';
    const RESET_STYLE     = 0;

    /**
     * ConsoleColor instance.
     *
     * @var \Exen\Support\Helpers\ConsoleColor
     */
    private static $instance = null;

    /**
     * ConsoleColor instances.
     *
     * @var \Exen\Support\Helpers\ConsoleColor[]
     */
    private static $instances = [];

    /**
     * Console coloe support status.
     *
     * @var boolean
     */
    private $isSupported = false;

    /**
     * Force style.
     *
     * @var boolean
     */
    private $forceStyle = false;

    /**
     * Console ANSI Styles.
     *
     * @var array
     */
    private $styles = [
        'none'             => null,
        'bold'             => '1',
        'dark'             => '2',
        'dim'              => '2',
        'italic'           => '3',
        'underline'        => '4',
        'blink'            => '5',
        'reverse'          => '7',
        'concealed'        => '8',

        'nodim'            => '22',

        'default'          => '39',
        'black'            => '30',
        'red'              => '31',
        'green'            => '32',
        'yellow'           => '33',
        'blue'             => '34',
        'magenta'          => '35',
        'cyan'             => '36',
        'light_gray'       => '37',

        'dark_gray'        => '90',
        'light_red'        => '91',
        'light_green'      => '92',
        'light_yellow'     => '93',
        'light_blue'       => '94',
        'light_magenta'    => '95',
        'light_cyan'       => '96',
        'white'            => '97',

        'bg_default'       => '49',
        'bg_black'         => '40',
        'bg_red'           => '41',
        'bg_green'         => '42',
        'bg_yellow'        => '43',
        'bg_blue'          => '44',
        'bg_magenta'       => '45',
        'bg_cyan'          => '46',
        'bg_light_gray'    => '47',

        'bg_dark_gray'     => '100',
        'bg_light_red'     => '101',
        'bg_light_green'   => '102',
        'bg_light_yellow'  => '103',
        'bg_light_blue'    => '104',
        'bg_light_magenta' => '105',
        'bg_light_cyan'    => '106',
        'bg_white'         => '107',
    ];

    /**
     * Console themes.
     *
     * @var array
     */
    private $themes = [];

    /**
     * ConsoleColor constructor.
     */
    public function __construct()
    {
        $this->isSupported = $this->isSupported();
        self::$instance = &$this;
    }

    /**
     * Returns the ConsoleColor instance.
     *
     * @return \Exen\Support\Helpers\ConsoleColor
     */
    public static function instance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Returns the ConsoleColor instance.
     *
     * @return \Exen\Support\Helpers\ConsoleColor
     */
    public static function getInstance(): self
    {
        return static::instance();
    }

    public function wrap(?string $first = null, ?string $last = null, ?string $message = null): string
    {
        return "\033[" . $first . 'm' . $message . "\033[" . $last . 'm';
    }

    /**
     * @param string|array $style
     * @param string $text
     *
     * @return string
     * @throws InvalidStyleException
     * @throws \InvalidArgumentException
     */
    public function apply($style = null, ?string $text = null): string
    {
        if (!$this->isStyleForced() and !$this->isSupported()) {
            return $text;
        }

        if (\is_string($style)) {
            $style = [$style];
        }

        if (!\is_array($style)) {
            throw new \InvalidArgumentException("Style must be string or array.");
        }

        $sequences = [];

        foreach ($style as $s) {
            if (isset($this->themes[$s])) {
                $sequences = \array_merge($sequences, $this->themeSequence($s));
            } elseif ($this->isValidStyle($s)) {
                $sequences[] = $this->styleSequence($s);
            } else {
                throw new InvalidStyleException($s);
            }
        }

        $sequences = \array_filter(
            $sequences,
            function ($val) {
                return $val !== null;
            }
        );

        if (empty($sequences)) {
            return $text;
        }

        return $this->escSequence(
            \implode(';', $sequences)
        ) . $text . $this->escSequence(self::RESET_STYLE);
    }

    /**
     * @param bool $boolean
     */
    public function setForceStyle(?bool $boolean = false): void
    {
        $this->forceStyle = (bool) $boolean;
    }

    /**
     * @return bool
     */
    public function isStyleForced(): bool
    {
        return $this->forceStyle;
    }

    /**
     * @param array $themes
     *
     * @throws InvalidStyleException
     * @throws \InvalidArgumentException
     */
    public function setThemes(array $themes): void
    {
        $this->themes = [];

        foreach ($themes as $name => $styles) {
            $this->addTheme($name, $styles);
        }
    }

    /**
     * @param string $name
     * @param array|string $styles
     *
     * @throws \InvalidArgumentException
     * @throws InvalidStyleException
     */
    public function addTheme($name, $styles): void
    {
        if (\is_string($styles)) {
            $styles = [$styles];
        }
        if (!\is_array($styles)) {
            throw new \InvalidArgumentException("Style must be string or array.");
        }

        foreach ($styles as $style) {
            if (!$this->isValidStyle($style)) {
                throw new InvalidStyleException($style);
            }
        }

        $this->themes[$name] = $styles;
    }

    /**
     * @return array
     */
    public function getThemes(): array
    {
        return $this->themes;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasTheme($name): bool
    {
        return isset($this->themes[$name]);
    }

    /**
     * @param string $name
     */
    public function removeTheme($name): void
    {
        unset($this->themes[$name]);
    }

    /**
     * @return bool
     */
    public function isSupported(): bool
    {
        if (DS === '\\') {
            if (\function_exists('sapi_windows_vt100_support')
                and @\sapi_windows_vt100_support(\STDOUT)
            ) {
                return true;
            } elseif (\getenv('ANSICON') !== false
                or \getenv('ConEmuANSI') === 'ON'
            ) {
                return true;
            }

            return false;
        } else {
            return \function_exists('posix_isatty') and @\posix_isatty(\STDOUT);
        }
    }

    /**
     * @return bool
     */
    public function are256ColorsSupported(): bool
    {
        if (DS === '\\') {
            return \function_exists('sapi_windows_vt100_support') and
                @\sapi_windows_vt100_support(\STDOUT);
        } else {
            return \strpos(\getenv('TERM'), '256color') !== false;
        }
    }

    /**
     * @return array
     */
    public function getPossibleStyles(): array
    {
        return \array_keys($this->styles);
    }

    /**
     * @param string $name
     *
     * @return string[]
     */
    private function themeSequence($name): array
    {
        $sequences = [];

        foreach ($this->themes[$name] as $style) {
            $sequences[] = $this->styleSequence($style);
        }

        return $sequences;
    }

    /**
     * @param string $style
     *
     * @return string
     */
    private function styleSequence(?string $style = null)
    {
        if (\array_key_exists($style, $this->styles)) {
            return $this->styles[$style];
        }

        if (!$this->are256ColorsSupported()) {
            return null;
        }

        \preg_match(self::COLOR256_REGEXP, $style, $matches);

        $type = $matches[1] === 'bg_' ? self::BACKGROUND: self::FOREGROUND;
        $value = $matches[2];

        return "$type;5;$value";
    }

    /**
     * @param string $style
     *
     * @return bool
     */
    private function isValidStyle(?string $style = null): bool
    {
        return \array_key_exists($style, $this->styles)
            or \preg_match(self::COLOR256_REGEXP, $style);
    }

    /**
     * @param string|int $value
     *
     * @return string
     */
    private function escSequence($value = null): string
    {
        return "\033[{$value}m";
        // return "\u001b[{$value}m";
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
