<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\PhpObject;

/**
 * RequireBinding class.
 *
 * @since 0.1
 */
final class RequireBinding extends PhpObject implements Singletonable
{
    /**
     * RequireBinding instance.
     *
     * @var \Exen\Support\Helpers\RequireBinding
     */
    private static $instance = null;

    /**
     * RequireBinding instances.
     *
     * @var \Exen\Support\Helpers\RequireBinding[]
     */
    private static $instances = [];

    /**
     * RequireBinding constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * Returns the RequireBinding instance.
     *
     * @return \Exen\Support\Helpers\RequireBinding
     */
    public static function instance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Returns the RequireBinding instance.
     *
     * @return \Exen\Support\Helpers\RequireBinding
     */
    public static function getInstance(): self
    {
        return static::instance();
    }

    /**
     * @param string|null $file
     * @param mixed       $bind
     *
     * @return mixed
     */
    public static function with(?string $file = null, $bind = null)
    {
        return (function ($file) {
            /**
             * @noinspection PhpIncludeInspection
             */
            return include $file;
        })->call($bind, $file);
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
