<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\Exception\InvalidStyleException;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\PhpObject;

/**
 * Class Color.
 *
 * @since 1.0
 */
class Color extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const BG_BLACK      = "\033[40m";
    const BG_BLUE       = "\033[44m";
    const BG_CYAN       = "\033[46m";
    const BG_GREEN      = "\033[42m";
    const BG_LIGHT_GRAY = "\033[47m";
    const BG_MAGENTA    = "\033[45m";
    const BG_RED        = "\033[41m";
    const BG_WHITE      = "\e[107m";
    const BG_YELLOW     = "\033[43m";
    const BLACK         = "\033[0;30m";
    const BLUE          = "\033[0;34m";
    const BOLD_WHITE    = "\033[1;38m";
    const BROWN         = "\033[0;33m";
    const CYAN          = "\033[0;36m";
    const DARK_GRAY     = "\033[1;30m";
    const GRAY          = "\033[0;90m";
    const GREEN         = "\033[0;32m";
    const LIGHT_BLUE    = "\033[1;34m";
    const LIGHT_CYAN    = "\033[1;36m";
    const LIGHT_GRAY    = "\033[0;37m";
    const LIGHT_GREEN   = "\033[1;32m";
    const LIGHT_MAGENTA = "\033[1;35m";
    const LIGHT_PURPLE  = "\033[1;35m";
    const LIGHT_RED     = "\033[1;31m";
    const LIGHT_WHITE   = "\033[1;38m";
    const LIGHT_YELLOW  = "\033[1;93m";
    const MAGENTA       = "\033[0;35m";
    const NORMAL        = "\e[0m";
    const PURPLE        = "\033[0;35m";
    const RED           = "\033[0;31m";
    const RESET         = "\e[0m";
    const WHITE         = "\e[0m";
    const YELLOW        = "\033[0;33m";

    const FOREGROUND       = 38;
    const BACKGROUND       = 48;
    const COLOR_256_REGEXP = '~^(bg_)?color_([0-9]{1,3})$~';
    const RESET_STYLE      = 0;

    /**
     * Console ANSI Styles.
     *
     * @var array
     */
    const ANSI_STYLES = [
        'none'                => null,
        'reset'               => '0',

        'bold'                => '1',
        'dark'                => '2',
        'dim'                 => '2',
        'italic'              => '3',
        'underline'           => '4',
        'blink'               => '5',
        'reverse'             => '7',
        'concealed'           => '8',

        'nodim'               => '22',

        'default'             => '39',
        'black'               => '30',
        'red'                 => '31',
        'green'               => '32',
        'yellow'              => '33',
        'blue'                => '34',
        'magenta'             => '35',
        'cyan'                => '36',
        'gray'                => '37',
        'light_gray'          => '37',

        'dark_gray'           => '90',
        'dark_red'            => '91',
        'dark_green'          => '92',
        'dark_yellow'         => '93',
        'dark_blue'           => '94',
        'dark_magenta'        => '95',
        'dark_cyan'           => '96',

        'light_black'         => '90',
        'light_red'           => '91',
        'light_green'         => '92',
        'light_yellow'        => '93',
        'light_blue'          => '94',
        'light_magenta'       => '95',
        'light_cyan'          => '96',
        'white'               => '97',

        'bg_default'          => '49',
        'bg_black'            => '40',
        'bg_red'              => '41',
        'bg_green'            => '42',
        'bg_yellow'           => '43',
        'bg_blue'             => '44',
        'bg_magenta'          => '45',
        'bg_cyan'             => '46',
        'bg_light_gray'       => '47',

        'bg_dark_gray'        => '100',
        'bg_light_red'        => '101',
        'bg_light_green'      => '102',
        'bg_light_yellow'     => '103',
        'bg_light_blue'       => '104',
        'bg_light_magenta'    => '105',
        'bg_light_cyan'       => '106',
        'bg_white'            => '107',
    ];

    /**
     * Console themes.
     *
     * @var array
     */
    private static $themes = [];

    /**
     * Color instance.
     *
     * @var \Exen\Support\Helpers\Color
     */
    private static $instance = null;

    /**
     * Color instances.
     *
     * @var \Exen\Support\Helpers\Color[]
     */
    private static $instances = [];

    /**
     * Color constructor.
     */
    public function __construct()
    {
        $this->isSupported = $this->isSupported();
        self::$instance = &$this;
    }

    /**
     * Returns the Color instance.
     *
     * @return \Exen\Support\Helpers\Color
     */
    public static function instance(): self
    {
        if (static::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    /**
     * Returns the Color instance.
     *
     * @return \Exen\Support\Helpers\Color
     */
    public static function getInstance(): self
    {
        return static::instance();
    }

    public static function normal()
    {
        return self::NORMAL;
    }

    public static function reset()
    {
        return self::RESET;
    }

    // WHITE
    public static function white()
    {
        return self::WHITE;
    }

    public static function lwhite()
    {
        return self::LIGHT_WHITE;
    }

    public static function lightWhite()
    {
        return self::LIGHT_WHITE;
    }

    public static function boldWhite()
    {
        return self::LIGHT_WHITE;
    }

    public static function bgWhite($bold = 0)
    {
        return "\e[{$bold};30;107m";
    }

    // RED
    public static function red()
    {
        return self::RED;
    }

    public static function lred()
    {
        return self::LIGHT_RED;
    }

    public static function lightRed()
    {
        return self::LIGHT_RED;
    }

    public static function boldRed()
    {
        return self::LIGHT_RED;
    }

    public static function bgRed($bold = 0)
    {
        return "\e[{$bold};97;41m";
    }

    // BLUE
    public static function blue()
    {
        return self::BLUE;
    }

    public static function lblue()
    {
        return self::LIGHT_BLUE;
    }

    public static function lightBlue()
    {
        return self::LIGHT_BLUE;
    }

    public static function boldBlue()
    {
        return self::LIGHT_BLUE;
    }

    public static function bgBlue($bold = 0)
    {
        return "\e[{$bold};30;44m";
    }

    // GREEN
    public static function green()
    {
        return self::GREEN;
    }

    public static function lgreen()
    {
        return self::LIGHT_RED;
    }

    public static function lightGreen()
    {
        return self::LIGHT_GREEN;
    }

    public static function boldGreen()
    {
        return self::LIGHT_GREEN;
    }

    public static function bgGreen($bold = 0)
    {
        return "\e[{$bold};30;42m";
    }

    // CYAN
    public static function cyan()
    {
        return self::CYAN;
    }

    public static function lcyan()
    {
        return self::LIGHT_CYAN;
    }

    public static function lightCyan()
    {
        return self::LIGHT_CYAN;
    }

    public static function boldCyan()
    {
        return self::LIGHT_CYAN;
    }

    public static function bgCyan($bold = 0)
    {
        return "\e[{$bold};30;46m";
    }

    // YELLOW
    public static function yellow()
    {
        return self::YELLOW;
    }

    public static function lyellow()
    {
        return self::LIGHT_YELLOW;
    }

    public static function lightYellow()
    {
        return self::LIGHT_YELLOW;
    }

    public static function boldYellow()
    {
        return self::LIGHT_YELLOW;
    }

    public static function bgYellow($bold = 0)
    {
        return "\e[{$bold};30;43m";
    }

    // MAGENTA
    public static function magenta()
    {
        return self::MAGENTA;
    }

    public static function lmagenta()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function lightMagenta()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function boldMagenta()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function bgMagenta($bold = 0)
    {
        return "\e[{$bold};30;45m";
    }

    // PURPLE
    public static function purple()
    {
        return self::MAGENTA;
    }

    public static function lpurple()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function lightPurple()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function boldPurple()
    {
        return self::LIGHT_MAGENTA;
    }

    public static function bgPurple($bold = 0)
    {
        return "\e[{$bold};30;45m";
    }

    // GRAY
    public static function gray()
    {
        return self::GRAY;
    }

    public static function lgray()
    {
        return self::LIGHT_GRAY;
    }

    public static function lightGray()
    {
        return self::LIGHT_GRAY;
    }

    public static function boldGray()
    {
        return self::LIGHT_GRAY;
    }

    public static function bgGray($bold = 0)
    {
        return "\e[{$bold};30;47m";
    }

    /**
     * @return array
     */
    public static function getPossibleStyles(): array
    {
        return \array_keys(self::ANSI_STYLES);
    }

    /**
     * @param string $name
     *
     * @return string[]
     */
    private static function themeSequence($name): array
    {
        $sequences = [];

        foreach (self::$themes[$name] as $style) {
            $sequences[] = self::styleSequence($style);
        }

        return $sequences;
    }

    /**
     * @param string $style
     *
     * @return string
     */
    private static function styleSequence(?string $style = null): string
    {
        if (\array_key_exists($style, self::ANSI_STYLES)) {
            return self::ANSI_STYLES[$style];
        }

        if (!$this->are256ColorsSupported()) {
            return null;
        }

        \preg_match(self::COLOR_256_REGEXP, $style, $matches);

        $type = $matches[1] === 'bg_' ? self::BACKGROUND : self::FOREGROUND;
        $value = $matches[2];

        return "$type;5;$value";
    }

    /**
     * @param string $style
     *
     * @return bool
     */
    private static function isValidStyle(?string $style = null): bool
    {
        return \array_key_exists($style, self::ANSI_STYLES)
            or \preg_match(self::COLOR_256_REGEXP, $style);
    }

    /**
     * @param string|int $value
     *
     * @return string
     */
    private static function escSequence($value = null): string
    {
        return "\033[{$value}m";
        // return "\u001b[{$value}m";
    }

    public static function wrapText(?string $first = null, ?string $last = null, ?string $message = null): string
    {
        $firstStyle = null;
        $lastStyle = null;

        if (\array_key_exists($first, self::ANSI_STYLES)) {
            $firstStyle = self::ANSI_STYLES[$first];
        } else {
            $firstStyle = $first;
        }

        if (\array_key_exists($last, self::ANSI_STYLES)) {
            $lastStyle = self::ANSI_STYLES[$last];
        } else {
            $lastStyle = $last;
        }

        return "\033[" . $firstStyle . 'm' . $message . "\033[" . $lastStyle . 'm';
    }

    /**
     * @param string|array $style
     * @param string $text
     *
     * @return string
     *
     * @throws InvalidStyleException
     * @throws \InvalidArgumentException
     */
    public static function wrap($style = null, ?string $text = null): string
    {
        if (\is_string($style)) {
            $style = [$style];
        }

        if (!\is_array($style)) {
            throw new \InvalidArgumentException("Style must be string or array.");
        }

        $sequences = [];

        foreach ($style as $s) {
            if (isset(self::$themes[$s])) {
                $sequences = \array_merge($sequences, self::themeSequence($s));
            } elseif (self::isValidStyle($s)) {
                $sequences[] = self::styleSequence($s);
            } else {
                throw new InvalidStyleException($s);
            }
        }

        $sequences = \array_filter(
            $sequences,
            function ($val) {
                return $val !== null;
            }
        );

        if (empty($sequences)) {
            return $text;
        }

        return self::escSequence(
            \implode(';', $sequences)
        ) . $text . self::escSequence(self::RESET_STYLE);
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \getClass($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return sSprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
