<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Exen\Support\PhpObject;

/**
 * PropertyHookAble class.
 *
 * @since 0.1.0
 */
final class PropertyHookAble extends PhpObject
{
    /**
     * @var string
     */
    const PREFIX = __CLASS__;

    /**
     * @var Hook
     */
    protected $hook = null;

    /**
     * @var array
     */
    protected $called = [];

    /**
     * PropertyHookAble constructor.
     *
     * @param \Exen\Support\Helpers\Hook $hook
     */
    public function __construct(Hook $hook = null)
    {
        $this->hook = $hook;
    }

    /**
     * @param null|string $name
     * @param mixed       $value
     * @param array[]     ...$param
     *
     * @return void
     */
    public function set(?string $name = null, $value = null, ...$param): void
    {
        $args = \func_get_args();
        $args[0] = self::PREFIX.$name;

        $this->called[$name] = [
            'called' => false,
            'value' => $args,
        ];
    }

    /**
     * @param null|string $name
     *
     * @return void
     */
    public function remove(?string $name = null): void
    {
        unset($this->called[$name]);
    }

    /**
     * @param null|string $name
     *
     * @return bool
     */
    public function has(?string $name = null): bool
    {
        return \array_key_exists($name, $this->called);
    }

    /**
     * @param null|string $name
     * @param mixed       $default
     *
     * @return mixed
     */
    public function get(?string $name = null, $default = null)
    {
        if (!$this->has($name)) {
            return $default;
        }

        if (isset($this->called[$name]['called'])) {
            $this->called[$name] = [
                'value' => \call_user_func_array([$this->hook, 'apply'], $this->called[$name]['value']),
            ];
        }

        return $this->called[$name]['value'];
    }

    /**
     * @param null|string $name
     * @param mixed       $default
     * @param array[]     ...$param
     *
     * @return mixed
     */
    public function getOrApply(?string $name = null, $default = null, ...$param)
    {
        if (!$this->has($name)) {
            \call_user_func_array([$this, 'set'], \func_get_args());
        }

        if (isset($this->called[$name]['called'])) {
            $this->called[$name] = [
                'value' => \call_user_func_array([$this->hook, 'apply'], $this->called[$name]['value']),
            ];
        }

        return $this->called[$name]['value'];
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
