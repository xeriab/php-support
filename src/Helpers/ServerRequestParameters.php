<?php

declare(strict_types=1);

namespace Exen\Support\Helpers;

use Psr\Http\Message\ServerRequestInterface;

/**
 * ServerRequestParameters class.
 *
 * @since 0.1.0
 */
final class ServerRequestParameters
{
    /**
     * @var \Psr\Http\Message\ServerRequestInterface
     */
    private $request = null;

    /**
     * @var array
     */
    private $parameterData = [];

    /**
     * RequestParameters constructor
     *
     * @param \Psr\Http\Message\ServerRequestInterface|null $request
     */
    public function __construct(?ServerRequestInterface $request = null)
    {
        $this->request = $request;

        if ($this->request->isGet()) {
            $this->getParameterData = $this->request->getQueryParams();
            $this->parameterData = $this->request->getQueryParams();
        }

        if ($this->request->isPost()) {
            $this->postParamData = $this->request->getParsedBody();
            $this->parameterData = $this->request->getParsedBody();
        }

        if ($this->request->isPut()) {
            $this->putParamData = $this->request->getParsedBody();
            $this->parameterData = $this->request->getParsedBody();
        }
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function get(?string $name = null)
    {
        return $this->getParameterData[$name];
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function post(?string $name = null)
    {
        return $this->postParamData[$name];
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return mixed
     */
    public function put(?string $name = null)
    {
        return $this->putParamData[$name];
    }

    /**
     *
     *
     * @param string|null $name
     *
     * @return array
     */
    public function all(?string $name = null): array
    {
        if ($name) {
            return $this->parameterData[$name];
        }

        return $this->parameterData;
    }

    /**
     * Get request.
     *
     * @return \Psr\Http\Message\ServerRequestInterface
     */
    public function getRequest(): ServerRequestInterface
    {
        return $this->request;
    }

    /**
     * {@inheritDoc}
     */
    public function __call($name = null, $arguments = [])
    {
        return \call_user_func([$this->request, $name], $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return (string) \get_called_class();
    }
}
