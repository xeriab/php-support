<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter converts the value to float.
 */
class FloatFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return \floatval((string) $value);
    }
}
