<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter keeps only alphabetic characters and digits of the value.
 */
class AlnumFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return \preg_replace('/[^[:alnum:]]/u', '', (string) $value);
    }
}
