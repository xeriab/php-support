<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter keeps only alphabetic characters of the value.
 */
class AlphaFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return \preg_replace('/[^[:alpha:]]/u', '', (string) $value);
    }
}
