<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter strips the newline control characters of the value.
 */
class StripNewlinesFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return \str_replace(["\n", "\r"], '', (string) $value);
    }
}
