<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter converts the value to string.
 */
class StringFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return (string) $value;
    }
}
