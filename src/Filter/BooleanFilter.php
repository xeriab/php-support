<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter converts the value to boolean.
 */
class BooleanFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return (bool) @\strval($value);
    }
}
