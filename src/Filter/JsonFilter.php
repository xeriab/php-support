<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

use Exen\Support\Json\Json;

/**
 * This filter decodes a JSON string to a array.
 */
class JsonFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        if (\is_string($value)) {
            return Json::decode($value, Json::FORCE_ARRAY);
        }
    }
}
