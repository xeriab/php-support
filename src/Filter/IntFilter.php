<?php

declare(strict_types=1);

namespace Exen\Support\Filter;

/**
 * This filter converts the value to integer.
 */
class IntFilter extends AbstractFilter
{
    /**
     * {@inheritDoc}
     */
    public function filter($value)
    {
        return (int) ((string) $value);
    }
}
