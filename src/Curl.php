<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Exception\Exception;
use Exen\Support\Traits\MacroableTrait;
use function curl_close;
use function curl_exec;
use function curl_getinfo;
use function curl_init;
use function curl_setopt;
use function error_log;
use function filter_var;
use const CURLINFO_HEADER_OUT;
use const CURLINFO_HTTP_CODE;
use const CURLOPT_FOLLOWLOCATION;
use const CURLOPT_HTTPHEADER;
use const CURLOPT_MAXREDIRS;
use const CURLOPT_PORT;
use const CURLOPT_POST;
use const CURLOPT_POSTFIELDS;
use const CURLOPT_RETURNTRANSFER;
use const CURLOPT_SSL_VERIFYPEER;
use const CURLOPT_TIMEOUT;
use const CURLOPT_URL;
use const CURLOPT_USERPWD;

/**
 * Class for Curl.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
final class Curl
{
    use MacroableTrait;

    private $curl;
    private $result;
    protected $http_response_code;

    /**
     * Curl constructor.
     */
    public function __construct()
    {
        $this->curl = curl_init();
    }

    /**
     * Set the url for the curl
     *
     * @param string $url
     *
     * @throws Exception
     */
    public function setUrl(string $url)
    {
        $url = filter_var($url, FILTER_VALIDATE_URL);

        if ($url == false) {
            throw new Exception("Invalid url");
        } else {
            curl_setopt($this->curl, CURLOPT_URL, $url);
        }
    }

    /**
     * When set to true it follows any "Location: " header that the server sends
     * as part of the HTTP header.
     *
     * @param bool $bool
     */
    public function setFollowLocation(bool $bool)
    {
        curl_setopt($this->curl, CURLOPT_FOLLOWLOCATION, $bool);
    }

    /**
     * When set to true it determines whether curl verifies the authenticity of
     * the peer's certificate.
     *
     * @param bool $bool
     */
    public function setVerifyPeer(bool $bool)
    {
        curl_setopt($this->curl, CURLOPT_SSL_VERIFYPEER, $bool);
    }

    /**
     * Set headers for the curl request
     *
     * @param array $headers
     */
    public function setHeaders(array $headers)
    {
        curl_setopt($this->curl, CURLOPT_HTTPHEADER, $headers);
    }

    /**
     * When set to true it return the result of the curl_exec() as a string
     * instead of outputting it directly.
     *
     * @param bool $bool
     */
    public function setReturn(bool $bool)
    {
        if ($bool) {
            curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, $bool);
        }
    }

    /**
     * When set to true POST is used
     *
     * @param bool $bool
     */
    public function usePost(bool $bool)
    {
        if ($bool) {
            curl_setopt($this->curl, CURLOPT_POST, 1);
        }
    }

    /**
     * Set data to be sent via POST
     *
     * @param array $data
     */
    public function setArrayPostData(array $data)
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }

    /**
     * Set data to be sent via POST
     *
     * @param string $data
     */
    public function setStringPostData(string $data)
    {
        curl_setopt($this->curl, CURLOPT_POSTFIELDS, $data);
    }

    /**
     * Set port to be used for request
     *
     * @param int $port
     */
    public function setPort(int $port)
    {
        curl_setopt($this->curl, CURLOPT_PORT, $port);
    }

    /**
     * Set the max number of redirects that will be followed
     *
     * @param int $max
     */
    public function setMaxRedirects(int $max)
    {
        curl_setopt($this->curl, CURLOPT_MAXREDIRS, $max);
    }

    /**
     * Set timeout for request
     *
     * @param int $timeout time in seconds
     */
    public function setTimeout(int $timeout)
    {
        curl_setopt($this->curl, CURLOPT_TIMEOUT, $timeout);
    }

    /**
     * Set the credentials used for HTTP authentication
     *
     * @param string $username
     * @param string $password
     */
    public function setCredentials(string $username, string $password)
    {
        $userPassword = $username . ':' . $password;
        curl_setopt($this->curl, CURLOPT_USERPWD, $userPassword);
    }

    /**
     * Set any \CURLOPT_XXX option
     *
     * @param int $option
     * @param mixed $value
     */
    public function setOption(int $option, $value)
    {
        curl_setopt($this->curl, $option, $value);
    }

    /**
     * Execute the curl request and store the results
     */
    public function execute()
    {
        curl_setopt($this->curl, CURLINFO_HEADER_OUT, true);
        $this->result = curl_exec($this->curl);
        $this->http_response_code = curl_getinfo($this->curl, CURLINFO_HTTP_CODE);
        error_log("Curl Response: $this->result");
    }

    /**
     * Return the headers sent
     *
     * @return mixed
     */
    public function getSentHeader()
    {
        return curl_getinfo($this->curl, CURLINFO_HEADER_OUT);
    }

    /**
     * Returns the result of the curl_exec()
     *
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Curl destructor.
     */
    public function __destruct()
    {
        curl_close($this->curl);
    }
}
