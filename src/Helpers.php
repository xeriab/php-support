<?php

declare(strict_types=1);

use Exen\Support\Arr;
use Exen\Support\Async\Pool;
use Exen\Support\Async\Process\ParallelProcess;
use Exen\Support\Async\Process\RunnableInterface;
use Exen\Support\Async\Runtime\ParentRuntime;
use Exen\Support\Async\Task;
use Exen\Support\Closure\SerializableClosure;
use Exen\Support\ExenCollection;
use Exen\Support\Dot;
use Exen\Support\Helpers\Color;
use Exen\Support\Helpers\Hook;
use Exen\Support\HigherOrderTapProxy;
use Exen\Support\Inflector;
use Exen\Support\Interfaces\Htmlable;
use Exen\Support\Lang;
use Exen\Support\Optional;
use Exen\Support\Str;
use Exen\Support\Type;

// use Exen\Support\Closure;

if (!defined('SYSLOG_FACILITY')) {
    // define('SYSLOG_FACILITY', LOG_LOCAL2);
    // define('SYSLOG_FACILITY', LOG_WARNING);
    // define('SYSLOG_FACILITY', LOG_LOCAL0);
    define('SYSLOG_FACILITY', LOG_LOCAL2 | LOG_PID | LOG_PERROR | LOG_DEBUG);
}

if (!isset($_SERVER['REQUEST_TIME_FLOAT'])) {
    $_SERVER['REQUEST_TIME_FLOAT'] = microtime(true);
}

if (!isset($_SERVER['REQUEST_TIME'])) {
    $_SERVER['REQUEST_TIME'] = $_SERVER['REQUEST_TIME_FLOAT'];
}

if (!function_exists('isAssociativeArray')) {
    /**
     * Determines if the given `object` is an associative array.
     *
     * An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param mixed $object
     *
     * @return boolean
     */
    function isAssociativeArray($object = null): bool
    {
        return Lang::isAssociativeArray($object);
    }
}

if (!function_exists('isNotAssociativeArray')) {
    /**
     * Determines if the given `object` isn't an associative array.
     *
     * An array is "associative" if it doesn't have sequential numerical keys
     * beginning with zero.
     *
     * @param mixed $object
     *
     * @return boolean
     */
    function isNotAssociativeArray($object = null): bool
    {
        return Lang::isNotAssociativeArray($object);
    }
}

if (!function_exists('parseClassName')) {
    /**
     * Parse the given class name.
     *
     * @param string|null $class Class to parse.
     *
     * @return array
     */
    function parseClassName(string $class = null): array
    {
        return [
            'namespace' => array_slice(explode('\\', $class), 0, -1),
            'classname' => join(EMPTY_STRING, array_slice(explode('\\', $class), -1)),
        ];
    }
}

if (!function_exists('xprintf')) {
    /**
     * Enhanced printf function
     *
     * @param string|null $inputString String to print.
     * @param mixed ...$arguments Arguments to pass to the given string.
     *
     * @return null|string
     */
    function xprintf(?string $inputString = null, ...$arguments): ?string
    {
        return Lang::printf($inputString, ...$arguments);
    }
}

if (!function_exists('hashObject')) {
    function hashObject(object $object = null, string $hashType = 'crc32'): ?string
    {
        try {
            return Lang::hashObject($object, $hashType);
        } catch (Exception $ex) {
            return null;
        }
    }
}

if (!function_exists('utcNow')) {
    /** @noinspection PhpUnhandledExceptionInspection */
    function utcNow(): DateTimeInterface
    {
        return Lang::utcNow();
    }
}

if (!function_exists('timestamp')) {
    /** @noinspection PhpUnhandledExceptionInspection */
    function timestamp(): int
    {
        return Lang::utcNow()->getTimestamp();
    }
}

if (!function_exists('currentYear')) {
    /** @noinspection PhpUnhandledExceptionInspection */
    function currentYear(): string
    {
        return Lang::utcNow()->format('Y');
    }
}

if (!function_exists('append_config')) {
    /**
     * Assign high numeric IDs to a config item to force appending.
     *
     * @param array|null $array $array
     *
     * @return array
     */
    function append_config(array $array = null): ?array
    {
        $start = 9999;

        foreach ($array as $key => $value) {
            if (is_numeric($key)) {
                ++$start;

                $array[$start] = Arr::pull($array, $key);
            }
        }

        return $array;
    }
}

if (!function_exists('array_add')) {
    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     *
     * @param array $array
     * @param string $key
     * @param mixed $value
     *
     * @return array
     * @Test
     */
    function array_add(array $array, string $key, $value): array
    {
        return Arr::add($array, $key, $value);
    }
}

if (!function_exists('array_collapse')) {
    /**
     * Collapse an array of arrays into a single array.
     *
     * @param array $array
     *
     * @return array
     */
    function array_collapse(array $array): array
    {
        return Arr::collapse($array);
    }
}

if (!function_exists('array_divide')) {
    /**
     * Divide an array into two arrays. One with keys and the other with values.
     *
     * @param array $array
     *
     * @return array
     */
    function array_divide(array $array): array
    {
        return Arr::divide($array);
    }
}

if (!function_exists('array_dot')) {
    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param array $array
     * @param string $prepend
     *
     * @return array
     */
    function array_dot(array $array, string $prepend = EMPTY_STRING): array
    {
        return Arr::dot($array, $prepend);
    }
}

if (!function_exists('array_except')) {
    /**
     * Get all the given array except for a specified array of keys.
     *
     * @param array $array
     * @param array|string $keys
     *
     * @return array
     */
    function array_except(array $array, $keys): array
    {
        return Arr::except($array, $keys);
    }
}

if (!function_exists('array_first')) {
    /**
     * Return the first element in an array passing a given truth test.
     *
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     *
     * @return mixed
     */
    function array_first(array $array, callable $callback = null, $default = null)
    {
        return Arr::first($array, $callback, $default);
    }
}

if (!function_exists('array_flatten')) {
    /**
     * Flatten a multi-dimensional array into a single level.
     *
     * @param array $array
     * @param int $depth
     *
     * @return array
     * @noinspection PhpUnusedParameterInspection
     */
    function array_flatten(array $array, int $depth = INF): array
    {
        return Arr::flatten($array);
    }
}

if (!function_exists('array_forget')) {
    /**
     * Remove one or many array items from a given array using "dot" notation.
     *
     * @param array $array
     * @param array|string $keys
     */
    function array_forget(array &$array, $keys)
    {
        Arr::forget($array, $keys);
    }
}

if (!function_exists('array_get')) {
    /**
     * Get an item from an array using "dot" notation.
     *
     * @param ArrayAccess|array $array
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    function array_get($array, string $key, $default = null)
    {
        return Arr::get($array, $key, $default);
    }
}

if (!function_exists('array_has')) {
    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param ArrayAccess|array $array
     * @param string|array $keys
     *
     * @return bool
     */
    function array_has($array, $keys): bool
    {
        return Arr::has($array, $keys);
    }
}

if (!function_exists('array_last')) {
    /**
     * Return the last element in an array passing a given truth test.
     *
     * @param array $array
     * @param callable|null $callback
     * @param mixed $default
     *
     * @return mixed
     */
    function array_last(array $array, callable $callback = null, $default = null)
    {
        return Arr::last($array, $callback, $default);
    }
}

if (!function_exists('array_only')) {
    /**
     * Get a subset of the items from the given array.
     *
     * @param array $array
     * @param array|string $keys
     *
     * @return array
     */
    function array_only(array $array, $keys): array
    {
        return Arr::only($array, $keys);
    }
}

if (!function_exists('array_pluck')) {
    /**
     * Pluck an array of values from an array.
     *
     * @param array $array
     * @param string|array $value
     * @param string|array|null $key
     *
     * @return array
     */
    function array_pluck(array $array, $value, $key = null): array
    {
        return Arr::pluck($array, $value, $key);
    }
}

if (!function_exists('array_prepend')) {
    /**
     * Push an item onto the beginning of an array.
     *
     * @param array $array
     * @param mixed $value
     * @param mixed $key
     *
     * @return array
     */
    function array_prepend(array $array, $value, $key = null): array
    {
        return Arr::prepend($array, $value, $key);
    }
}

if (!function_exists('array_pull')) {
    /**
     * Get a value from the array, and remove it.
     *
     * @param array $array
     * @param string $key
     * @param mixed $default
     *
     * @return mixed
     */
    function array_pull(array &$array, string $key, $default = null)
    {
        return Arr::pull($array, $key, $default);
    }
}

if (!function_exists('array_random')) {
    /**
     * Get a random value from an array.
     *
     * @param array $array
     * @param int|null $num
     *
     * @return mixed
     */
    function array_random(array $array, ?int $num = null)
    {
        return Arr::random($array, $num);
    }
}

if (!function_exists('array_set')) {
    /**
     * Set an array item to a given value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     *
     * @param array $array
     * @param string $key
     * @param mixed $value
     *
     * @return array
     */
    function array_set(array &$array, string $key, $value): array
    {
        return Arr::set($array, $key, $value);
    }
}

if (!function_exists('array_sort')) {
    /**
     * Sort the array by the given callback or attribute name.
     *
     * @param array $array
     * @param callable|string|null $callback
     *
     * @return array
     */
    function array_sort(array $array, $callback = null): array
    {
        return Arr::sort($array, $callback);
    }
}

if (!function_exists('array_sort_recursive')) {
    /**
     * Recursively sort an array by keys and values.
     *
     * @param array $array
     *
     * @return array
     */
    function array_sort_recursive(array $array): array
    {
        return Arr::sortRecursive($array);
    }
}

if (!function_exists('array_where')) {
    /**
     * Filter the array using the given callback.
     *
     * @param array $array
     * @param callable $callback
     *
     * @return array
     */
    function array_where(array $array, callable $callback): array
    {
        return Arr::where($array, $callback);
    }
}

if (!function_exists('array_wrap')) {
    /**
     * If the given value is not an array, wrap it in one.
     *
     * @param mixed $value
     *
     * @return array
     */
    function array_wrap($value): array
    {
        return Arr::wrap($value);
    }
}

if (!function_exists('blank')) {
    /**
     * Determine if the given value is "blank".
     *
     * @param mixed $value
     *
     * @return bool
     */
    function blank($value): bool
    {
        if (is_null($value)) {
            return true;
        }

        if (is_string($value)) {
            return EMPTY_STRING === trim($value);
        }

        if (is_numeric($value) || is_bool($value)) {
            return false;
        }

        if ($value instanceof Countable) {
            return 0 === count($value);
        }

        return empty($value);
    }
}

if (!function_exists('camel_case')) {
    /**
     * Convert a value to camel case.
     *
     * @param string $value
     *
     * @return string
     */
    function camel_case(string $value): string
    {
        return Str::camel($value);
    }
}

if (!function_exists('class_basename')) {
    /**
     * Get the class "basename" of the given object / class.
     *
     * @param string|object $class
     *
     * @return string
     */
    function class_basename($class): string
    {
        $class = is_object($class) ? get_class($class) : $class;

        return basename(str_replace('\\', '/', $class));
    }
}

if (!function_exists('class_uses_recursive')) {
    /**
     * Returns all traits used by a class, its parent classes and trait of their traits.
     *
     * @param object|string $class
     *
     * @return array
     */
    function class_uses_recursive($class): array
    {
        if (is_object($class)) {
            $class = get_class($class);
        }

        $results = [];

        foreach (array_reverse(class_parents($class)) + [$class => $class] as $class) {
            $results += trait_uses_recursive($class);
        }

        return array_unique($results);
    }
}

if (!function_exists('collect')) {
    /**
     * Create a collection from the given value.
     *
     * @param mixed $value
     *
     * @return ExenCollection
     */
    function collect($value = null): ExenCollection
    {
        return new ExenCollection($value);
    }
}

if (!function_exists('data_fill')) {
    /**
     * Fill in data where it's missing.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     *
     * @return mixed
     */
    function data_fill(&$target, $key, $value): array
    {
        return data_set($target, $key, $value, false);
    }
}

if (!function_exists('data_get')) {
    /**
     * Get an item from an array or object using "dot" notation.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $default
     *
     * @return mixed
     */
    function data_get($target, $key, $default = null)
    {
        if (is_null($key)) {
            return $target;
        }

        $key = is_array($key) ? $key : explode('.', $key);

        while (!is_null($segment = array_shift($key))) {
            if ('*' === $segment) {
                if ($target instanceof ExenCollection) {
                    $target = $target->all();
                } elseif (!is_array($target)) {
                    return value($default);
                }

                $result = Arr::pluck($target, $key);

                return in_array('*', $key) ? Arr::collapse($result) : $result;
            }

            if (Arr::accessible($target) && Arr::exists($target, $segment)) {
                $target = $target[$segment];
            } elseif (is_object($target) && isset($target->{$segment})) {
                $target = $target->{$segment};
            } else {
                return value($default);
            }
        }

        return $target;
    }
}

if (!function_exists('data_set')) {
    /**
     * Set an item on an array or object using dot notation.
     *
     * @param mixed $target
     * @param string|array $key
     * @param mixed $value
     * @param bool $overwrite
     *
     * @return mixed
     */
    function data_set(&$target, $key, $value, bool $overwrite = true): array
    {
        $segments = is_array($key) ? $key : explode('.', $key);

        if ('*' === ($segment = array_shift($segments))) {
            if (!Arr::accessible($target)) {
                $target = [];
            }

            if ($segments) {
                foreach ($target as &$inner) {
                    data_set($inner, $segments, $value, $overwrite);
                }
            } elseif ($overwrite) {
                foreach ($target as &$inner) {
                    $inner = $value;
                }
            }
        } elseif (Arr::accessible($target)) {
            if ($segments) {
                if (!Arr::exists($target, $segment)) {
                    $target[$segment] = [];
                }

                data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite || !Arr::exists($target, $segment)) {
                $target[$segment] = $value;
            }
        } elseif (is_object($target)) {
            if ($segments) {
                if (!isset($target->{$segment})) {
                    $target->{$segment} = [];
                }

                data_set($target->{$segment}, $segments, $value, $overwrite);
            } elseif ($overwrite || !isset($target->{$segment})) {
                $target->{$segment} = $value;
            }
        } else {
            $target = [];

            if ($segments) {
                data_set($target[$segment], $segments, $value, $overwrite);
            } elseif ($overwrite) {
                $target[$segment] = $value;
            }
        }

        return $target;
    }
}

if (!function_exists('e')) {
    /**
     * Escape HTML special characters in a string.
     *
     * @param Htmlable|string $value
     * @param bool $doubleEncode
     *
     * @return string
     */
    function e($value, bool $doubleEncode = true): string
    {
        if ($value instanceof Htmlable) {
            return $value->toHtml();
        }

        return htmlspecialchars($value, ENT_QUOTES, 'UTF-8', $doubleEncode);
    }
}

if (!function_exists('ends_with')) {
    /**
     * Determine if a given string ends with a given substring.
     *
     * @param string $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    function ends_with(string $haystack, $needles): bool
    {
        return Str::endsWith($haystack, $needles);
    }
}

if (!function_exists('env')) {
    /**
     * Gets the value of an environment variable.
     *
     * @param string $key
     * @param mixed $default
     * @return array|bool|mixed|string|null
     */
    function env(string $key, $default = null)
    {
        $value = getenv($key);

        if (false === $value) {
            return value($default);
        }

        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return EMPTY_STRING;
            case 'null':
            case '(null)':
                return null;
        }

        if (($valLen = strlen($value)) > 1 && '"' === $value[0] && '"' === $value[$valLen - 1]) {
            return substr($value, 1, -1);
        }

        return $value;
    }
}

if (!function_exists('filled')) {
    /**
     * Determine if a value is "filled".
     *
     * @param mixed $value
     *
     * @return bool
     */
    function filled($value): bool
    {
        return !blank($value);
    }
}

if (!function_exists('head')) {
    /**
     * Get the first element of an array. Useful for method chaining.
     *
     * @param array $array
     *
     * @return mixed
     */
    function head(array $array = [])
    {
        return reset($array);
    }
}

if (!function_exists('kebab_case')) {
    /**
     * Convert a string to kebab case.
     *
     * @param string $value
     *
     * @return string
     */
    function kebab_case(string $value): string
    {
        return Str::kebab($value);
    }
}

if (!function_exists('last')) {
    /**
     * Get the last element from an array.
     *
     * @param array $array
     *
     * @return mixed
     */
    function last(array $array = [])
    {
        return end($array);
    }
}

if (!function_exists('object_get')) {
    /**
     * Get an item from an object using "dot" notation.
     *
     * @param object $object
     * @param string|null $key
     * @param mixed $default
     *
     * @return mixed
     */
    function object_get(object $object, ?string $key, $default = null): object
    {
        if (is_null($key) || EMPTY_STRING == trim($key)) {
            return $object;
        }

        foreach (explode('.', $key) as $segment) {
            if (!is_object($object) || !isset($object->{$segment})) {
                return value($default);
            }

            $object = $object->{$segment};
        }

        return $object;
    }
}

if (!function_exists('optional')) {
    /**
     * Provide access to optional objects.
     *
     * @param mixed $value
     * @param callable|null $callback
     *
     * @return mixed
     */
    function optional($value = null, callable $callback = null): ?Optional
    {
        if (is_null($callback)) {
            return new Optional($value);
        } elseif (!is_null($value)) {
            return $callback($value);
        }

        return null;
    }
}

if (!function_exists('preg_replace_array')) {
    /**
     * Replace a given pattern with each value in the array in sequentially.
     *
     * @param string $pattern
     * @param array $replacements
     * @param string $subject
     *
     * @return string
     */
    function preg_replace_array(string $pattern, array $replacements, string $subject): string
    {
        $callback = function () use (&$replacements) {
            $retVal = [];

            /** @noinspection PhpUnusedLocalVariableInspection */
            foreach ($replacements as $key => $value) {
                $retVal = array_shift($replacements);
            }

            /** @noinspection PhpUnnecessaryLocalVariableInspection */
            $replacements = $retVal;

            return $replacements;
        };

        return preg_replace_callback($pattern, $callback, $subject);
    }
}

if (!function_exists('retry')) {
    /**
     * Retry an operation a given number of times.
     *
     * @param int $times
     * @param callable $callback
     * @param int $sleep
     *
     * @return mixed
     *
     * @throws Exception
     */
    function retry(int $times, callable $callback, int $sleep = 0)
    {
        --$times;

        beginning:
        try {
            return $callback();
        } catch (Exception $e) {
            if (!$times) {
                throw $e;
            }

            --$times;

            if ($sleep) {
                usleep($sleep * 1000);
            }

            goto beginning;
        }
    }
}

if (!function_exists('snake_case')) {
    /**
     * Convert a string to snake case.
     *
     * @param string $value
     * @param string $delimiter
     *
     * @return string
     */
    function snake_case(string $value, string $delimiter = '_'): string
    {
        return Str::snake($value, $delimiter);
    }
}

if (!function_exists('starts_with')) {
    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    function starts_with(string $haystack, $needles): bool
    {
        return Str::startsWith($haystack, $needles);
    }
}

if (!function_exists('str_after')) {
    /**
     * Return the remainder of a string after a given value.
     *
     * @param string $subject
     * @param string $search
     *
     * @return string
     */
    function str_after(string $subject, string $search): string
    {
        return Str::after($subject, $search);
    }
}

if (!function_exists('str_before')) {
    /**
     * Get the portion of a string before a given value.
     *
     * @param string $subject
     * @param string $search
     *
     * @return string
     */
    function str_before(string $subject, string $search): string
    {
        return Str::before($subject, $search);
    }
}

if (!function_exists('str_contains')) {
    /**
     * Determine if a given string contains a given substring.
     *
     * @param string $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    function str_contains(string $haystack, $needles): bool
    {
        return Str::contains($haystack, $needles);
    }
}

if (!function_exists('str_finish')) {
    /**
     * Cap a string with a single instance of a given value.
     *
     * @param string $value
     * @param string $cap
     *
     * @return string
     */
    function str_finish(string $value, string $cap): string
    {
        return Str::finish($value, $cap);
    }
}

if (!function_exists('str_is')) {
    /**
     * Determine if a given string matches a given pattern.
     *
     * @param string|array $pattern
     * @param string $value
     *
     * @return bool
     */
    function str_is($pattern, string $value): bool
    {
        return Str::is($pattern, $value);
    }
}

if (!function_exists('str_limit')) {
    /**
     * Limit the number of characters in a string.
     *
     * @param string $value
     * @param int $limit
     * @param string $end
     *
     * @return string
     */
    function str_limit(string $value, int $limit = 100, string $end = '...'): string
    {
        return Str::limit($value, $limit, $end);
    }
}

if (!function_exists('str_plural')) {
    /**
     * Get the plural form of an English word.
     *
     * @param string $value
     * @param int $count
     *
     * @return string
     */
    function str_plural(string $value, int $count = 2): string
    {
        return Str::plural($value, $count);
    }
}

if (!function_exists('str_random')) {
    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param int $length
     *
     * @return string
     *
     * @throws RuntimeException
     */
    function str_random(int $length = 16): string
    {
        return Str::random($length);
    }
}

if (!function_exists('str_replace_array')) {
    /**
     * Replace a given value in the string sequentially with an array.
     *
     * @param string $search
     * @param array $replace
     * @param string $subject
     *
     * @return string
     */
    function str_replace_array(string $search, array $replace, string $subject): string
    {
        return Str::replaceArray($search, $replace, $subject);
    }
}

if (!function_exists('str_replace_first')) {
    /**
     * Replace the first occurrence of a given value in the string.
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     *
     * @return string
     */
    function str_replace_first(string $search, string $replace, string $subject): string
    {
        return Str::replaceFirst($search, $replace, $subject);
    }
}

if (!function_exists('str_replace_last')) {
    /**
     * Replace the last occurrence of a given value in the string.
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     *
     * @return string
     */
    function str_replace_last(string $search, string $replace, string $subject): string
    {
        return Str::replaceLast($search, $replace, $subject);
    }
}

if (!function_exists('str_singular')) {
    /**
     * Get the singular form of an English word.
     *
     * @param string $value
     *
     * @return string
     */
    function str_singular(string $value): string
    {
        return Str::singular($value);
    }
}

if (!function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param string $title
     * @param string $separator
     * @param string $language
     *
     * @return string
     */
    function str_slug(string $title, string $separator = '-', string $language = 'en'): string
    {
        return Str::slug($title, $separator, $language);
    }
}

if (!function_exists('str_start')) {
    /**
     * Begin a string with a single instance of a given value.
     *
     * @param string $value
     * @param string $prefix
     *
     * @return string
     */
    function str_start(string $value, string $prefix): string
    {
        return Str::start($value, $prefix);
    }
}

if (!function_exists('studly_case')) {
    /**
     * Convert a value to studly caps case.
     *
     * @param string $value
     *
     * @return string
     */
    function studly_case(string $value): string
    {
        return Str::studly($value);
    }
}

if (!function_exists('tap')) {
    /**
     * Call the given Closure with the given value then return the value.
     *
     * @param mixed $value
     * @param callable|null $callback
     *
     * @return mixed
     */
    function tap($value, callable $callback = null): HigherOrderTapProxy
    {
        if (is_null($callback)) {
            return new HigherOrderTapProxy($value);
        }

        $callback($value);

        return $value;
    }
}

if (!function_exists('throw_if')) {
    /**
     * Throw the given exception if the given condition is true.
     *
     * @param mixed $condition
     * @param Throwable|string $exception
     * @param array ...$parameters
     *
     * @return mixed
     *
     * @throws Throwable
     */
    function throw_if($condition, $exception, ...$parameters)
    {
        if ($condition) {
            throw (is_string($exception) ? new $exception(...$parameters) : $exception);
        }

        return $condition;
    }
}

if (!function_exists('throw_unless')) {
    /**
     * Throw the given exception unless the given condition is true.
     *
     * @param mixed $condition
     * @param Throwable|string $exception
     * @param array ...$parameters
     *
     * @return mixed
     *
     * @throws Throwable
     */
    function throw_unless($condition, $exception, ...$parameters)
    {
        if (!$condition) {
            throw (is_string($exception) ? new $exception(...$parameters) : $exception);
        }

        return $condition;
    }
}

if (!function_exists('title_case')) {
    /**
     * Convert a value to title case.
     *
     * @param string $value
     *
     * @return string
     */
    function title_case(string $value): string
    {
        return Str::title($value);
    }
}

if (!function_exists('trait_uses_recursive')) {
    /**
     * Returns all traits used by a trait and its traits.
     *
     * @param string $trait
     *
     * @return array
     */
    function trait_uses_recursive(string $trait): array
    {
        $traits = class_uses($trait);

        foreach ($traits as $trait) {
            $traits += trait_uses_recursive($trait);
        }

        return $traits;
    }
}

if (!function_exists('transform')) {
    /**
     * Transform the given value if it is present.
     *
     * @param mixed $value
     * @param callable $callback
     * @param mixed $default
     *
     * @return mixed|null
     */
    function transform($value, callable $callback, $default = null)
    {
        if (filled($value)) {
            return $callback($value);
        }

        if (is_callable($default)) {
            return $default($value);
        }

        return $default;
    }
}

if (!function_exists('value')) {
    /**
     * Return the default value of the given value.
     *
     * @param mixed $value
     *
     * @return mixed
     */
    function value($value)
    {
        return $value instanceof Closure ? $value() : $value;
    }
}

if (!function_exists('windows_os')) {
    /**
     * Determine whether the current environment is Windows based.
     *
     * @return bool
     */
    function windows_os(): bool
    {
        return 'win' === strtolower(substr(PHP_OS, 0, 3));
    }
}

if (!function_exists('with')) {
    /**
     * Return the given value, optionally passed through the given callback.
     *
     * @param mixed $value
     * @param callable|null $callback
     *
     * @return mixed
     */
    function with($value, callable $callback = null)
    {
        return is_null($callback) ? $value : $callback($value);
    }
}

if (!function_exists('each')) {
    /**
     * Return the current key and value pair from an array and advance the
     * array cursor.
     *
     * @param array|null $array $array The input array.
     *
     * @return null|array
     */
    function each(array &$array = null): ?array
    {
        $key = key($array);
        $result = (null === $key) ? false : [$key, current($array), 'key' => $key, 'value' => current($array)];
        next($array);

        return $result;
    }
}

if (!function_exists('isArray')) {
    /**
     * Is the given value an array.
     *
     * @param mixed $value Value to check.
     *
     * @return boolean Returns true if $value is an Array, false instead.
     */
    function isArray($value = null): bool
    {
        return Lang::isArray($value);
    }
}

if (!function_exists('D')) {
    /**
     * Dumps the passed variables/arguments to the standard output.
     *
     * @param mixed $variable Variable to dump.
     * @param mixed ...$variables Variables to dump.
     *
     * @return mixed
     * @throws ErrorException
     */
    function D($variable = null, ...$variables)
    {
        return Lang::d($variable, ...$variables);
    }
}

if (!function_exists('DD')) {
    /**
     * Dumps the passed variables/arguments to the standard output then end the
     * script.
     *
     * @param mixed ...$variables Variables to dump.
     *
     * @return void
     * @throws ErrorException
     */
    function DD(...$variables): void
    {
        Lang::dd(...$variables);
    }
}

if (!function_exists('DEBUG')) {
    /**
     * Prints debug messages to console.
     *
     * @param mixed ...$arguments Arguments.
     *
     * @return void
     * @noinspection PhpUnhandledExceptionInspection
     * @noinspection PhpDocMissingThrowsInspection
     */
    function DEBUG(...$arguments): void
    {
        /** @var boolean */
        $DEBUG_MODE_ENABLED = Lang::getConstant(
            'DEBUG_MODE_ENABLED', null, false);

        /** @var string */
        $DEBUG_MODE_TITLE = Lang::getConstant(
            'DEBUG_MODE_TITLE', null, 'DEBUG');

        /** @var boolean */
        $DEBUG_MODE_TIMESTAMP = Lang::getConstant(
            'DEBUG_MODE_TIMESTAMP', null, false);

        /** @var string */
        $DEBUG_MODE_TIME_FORMAT = Lang::getConstant(
            'DEBUG_MODE_TIME_FORMAT', null, 'Y-m-d H:i:s A');

        /** @var boolean */
        $DEBUG_MODE_FILE_INFO = Lang::getConstant(
            'DEBUG_MODE_FILE_INFO', null, false);

        /** @var boolean */
        $DEBUG_MODE_FULL_PATH = Lang::getConstant(
            'DEBUG_MODE_FULL_PATH', null, false);

        /** @var boolean */
        $DEBUG_MODE_SHOW_LINE_NUMBER = Lang::getConstant(
            'DEBUG_MODE_SHOW_LINE_NUMBER', null, false);

        /** @var string */
        $DEBUG_MODE_LINE_NUMBER_SEPARATOR = Lang::getConstant(
            'DEBUG_MODE_LINE_NUMBER_SEPARATOR', null, ':');

        /** @var string */
        $DEBUG_MODE_PROTOCOL = Lang::getConstant(
            'DEBUG_MODE_PROTOCOL', null, 'file://');

        if (Lang::isSet($_SERVER, 'APP_DEBUG') and Lang::isSet($_SERVER, 'APP_ENV')) {
            if (
                !Lang::toBoolean($_SERVER['APP_DEBUG']) and $_SERVER['APP_ENV'] !== 'dev' and
                !$DEBUG_MODE_ENABLED
            ) {
                return;
            }
        }

        $debug = EMPTY_STRING;

        $trace = debug_backtrace();
        $node = reset($trace);
        $file = $DEBUG_MODE_FULL_PATH ? $node['file'] : basename($node['file']);
        $line = $node['line'];

        // $debug .= \str_replace('\\', '/', $file);
        // $debug .= ' @ ' . $line;
        // $debug .= ':' . $line;
        // $debug .= 'file:///' . $file . ':' . $line;
        // $debug .= 'file://' . $file;

        if ($DEBUG_MODE_PROTOCOL !== '' and !is_null($DEBUG_MODE_PROTOCOL)) {
            $debug .= $DEBUG_MODE_PROTOCOL . $file;
        }

        if ($DEBUG_MODE_SHOW_LINE_NUMBER) {
            // $debug .= '@' . $line;
            // $debug .= '#' . $line;
            if (!is_null($DEBUG_MODE_LINE_NUMBER_SEPARATOR)) {
                $debug .= $DEBUG_MODE_LINE_NUMBER_SEPARATOR . $line;
            }
        }

        $title = Color::wrap('reset', EMPTY_STRING) . Color::wrap('light_green', '[') .
            // Color::wrap('bold', $DEBUG_MODE_TITLE) .
            Color::wrap('green', $DEBUG_MODE_TITLE) .
            // Color::wrap('green', Color::wrap('bold', $DEBUG_MODE_TITLE)) .
            Color::wrap('light_green', ']');

        /** @noinspection PhpUnhandledExceptionInspection */
        $timeText = Color::wrap('reset', EMPTY_STRING) . Color::wrap('light_gray', '[') .
            // Color::wrap('dark_gray', utcNow()->format('d-m-Y H:i:s A')) .
            Color::wrap('dark_gray', utcNow()->format($DEBUG_MODE_TIME_FORMAT)) .
            Color::wrap('light_gray', ']');

        /** @noinspection PhpUnhandledExceptionInspection */
        $debugText = Color::wrap('reset', EMPTY_STRING) . Color::wrap('light_gray', '[') .
            Color::wrap('dark_gray', $debug) .
            Color::wrap('light_gray', ']');

        $timeText = ($DEBUG_MODE_TIMESTAMP) ? $timeText . SPACE_CHAR : EMPTY_STRING;
        $debugText = ($DEBUG_MODE_FILE_INFO) ? $debugText . SPACE_CHAR : EMPTY_STRING;

        $msg = $timeText . $title . SPACE_CHAR . $debugText;

        $argument = EMPTY_STRING;

        $argc = count($arguments) ?? 0;

        if (count($arguments) === 1) {
            $argument = $arguments[0];
        } elseif (count($arguments) >= 1) {
            $argument = $arguments[0];
            $arguments = array_slice($arguments, 1);
        }

        if (Lang::isString($argument) and Lang::isNotEmpty($argument) and $argc >= 2) {
            if (Lang::isFormatString($argument)) {
                $argument = $msg . Lang::printf($argument, ...$arguments);
            } else {
                $output = EMPTY_STRING;

                if (count($arguments) >= 2) {
                    foreach ($arguments as $arg) {
                        $output .= Lang::stringify($arg) . SPACE_CHAR;
                    }

                    $argument = $msg . $argument . SPACE_CHAR . $output;
                } else {
                    $output .= Lang::stringify($arguments[0]) . SPACE_CHAR;
                    $argument = $msg . $output;
                }
            }
        } elseif (Lang::isString($argument) and Lang::isNotEmpty($argument) and $argc === 1) {
            $argument = $msg . $argument;
        } elseif (!Lang::isString($argument) and Lang::isNotEmpty($argument) and $argc > 1) {
            $output = EMPTY_STRING;

            foreach ($arguments as $arg) {
                $output .= Lang::stringify($arg) . SPACE_CHAR;
            }

            $argument = $msg . $argument . SPACE_CHAR . $output;
        } else {
            $output = EMPTY_STRING;

            foreach ($arguments as $arg) {
                $output .= Lang::stringify($arg) . SPACE_CHAR;
            }

            $argument = $msg . $output;
        }

        @syslog(SYSLOG_FACILITY | LOG_DEBUG, $argument);

        @error_log(trim($argument), 4);
    }
}

if (!function_exists('isCli')) {
    function isCli(): bool
    {
        return (PHP_SAPI === 'cli' or defined('STDIN'));
    }
}

if (!function_exists('isCgi')) {
    function isCgi(): bool
    {
        return 'cgi' === substr(php_sapi_name(), 0, 3);
    }
}

if (!function_exists('terminal_info')) {
    function terminal_info(): object
    {
        $object = new stdClass;

        if (isCli()) {
            $object->width = intval(exec('tput cols'));
            $object->height = intval(exec('tput lines'));
            $object->colors = intval(exec('tput colors'));
        }

        return $object;
    }
}

if (!function_exists('getTypeOf')) {
    /**
     * Gets the type from the given value/variable as string.
     *
     * @param mixed $value The value/variable to get type form.
     *
     * @return null|string
     */
    function getTypeOf($value = null): ?string
    {
        // $nativeType = \gettype($value);
        $newType = null;

        if (Lang::isNotNull($value) or Lang::isNotEmpty($value)) {
            if (Lang::isSerialized($value)) {
                $newType = 'serialized';
            } elseif (Lang::isChar($value)) {
                $newType = 'char';
            } elseif (Lang::isFloat($value)) {
                $newType = 'float';
            } elseif (Lang::isDouble($value)) {
                $newType = 'double';
            } elseif (Lang::isDateTime($value)) {
                $newType = 'datetime';
            } elseif (Lang::isBoolean($value)) {
                $newType = 'boolean';
            } elseif (Lang::isInteger($value)) {
                $newType = 'integer';
            } elseif (Lang::isJson($value)) {
                $newType = 'json';
            } elseif (Lang::isXml($value)) {
                $newType = 'xml';
            } elseif (Lang::isString($value)) {
                $newType = 'string';
            } elseif (Lang::isArray($value) and Lang::isAssoc($value)) {
                $newType = 'associative_array';
            } elseif (Lang::isArray($value) and !Lang::isAssoc($value)) {
                $newType = 'array';
            } elseif (Lang::isArray($value)) {
                $newType = 'array';
            } elseif (Lang::isObject($value)) {
                $newType = 'object';
            }
        }

        // DEBUG('nativeType:', $nativeType);
        // DEBUG('newType:', $newType);

        return $newType;
    }
}

if (!function_exists('typeOf')) {
    /**
     * @param $expression
     *
     * @return Type\Type
     */
    function typeOf($expression): ?Type\Type
    {
        try {
            return Type\TypeFactory::expression($expression);
        } catch (Exception $ex) {
            return null;
        }
    }
}


if (!function_exists(__NAMESPACE__ . '\downcase')) {
    /**
     * Returns a lowercase string.
     *
     * @param string $str
     *
     * @return string
     */
    function downcase(string $str): string
    {
        return mb_strtolower($str);
    }
}

if (!function_exists(__NAMESPACE__ . '\upcase')) {
    /**
     * Returns an uppercase string.
     *
     * @param string $str
     *
     * @return string
     */
    function upcase(string $str): string
    {
        return mb_strtoupper($str);
    }
}

if (!function_exists(__NAMESPACE__ . '\capitalize')) {
    /**
     * Returns a copy of str with the first character converted to uppercase and the
     * remainder to lowercase.
     *
     * @param string $str
     * @param bool $preserve_str_end Whether the string end should be preserved or down-cased.
     *
     * @return string
     */
    function capitalize(string $str, bool $preserve_str_end = false): string
    {
        $end = mb_substr($str, 1);

        if (!$preserve_str_end) {
            $end = downcase($end);
        }

        return upcase(mb_substr($str, 0, 1)) . $end;
    }
}

/**
 * Forwards calls to `Inflector::get()->pluralize()`.
 *
 * @param string $word
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function pluralize(string $word, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->pluralize($word);
}

/**
 * Forwards calls to `Inflector::get()->singularize()`.
 *
 * @param string $word
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function singularize(string $word, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->singularize($word);
}

/**
 * Forwards calls to `Inflector::get()->camelize()`.
 *
 * @param string $str
 * @param bool $uppercase_first_letter
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function camelize(string $str, bool $uppercase_first_letter = false, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->camelize($str, $uppercase_first_letter);
}

/**
 * Forwards calls to `Inflector::get()->underscore()`.
 *
 * @param string $str
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function underscore(string $str, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->underscore($str);
}

/**
 * Forwards calls to `Inflector::get()->hyphenate()`.
 *
 * @param string $str
 * @param string $locale Locale identifier.
 *
 * @return string
 * @noinspection PhpUnused
 */
function hyphenate(string $str, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->hyphenate($str);
}

/**
 * Forwards calls to `Inflector::get()->humanize()`.
 *
 * @param string $str
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function humanize(string $str, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->humanize($str);
}

/**
 * Forwards calls to `Inflector::get()->titleize()`.
 *
 * @param string $str
 * @param string $locale Locale identifier.
 *
 * @return string
 */
function titleize(string $str, string $locale = INFLECTOR_DEFAULT_LOCALE): string
{
    return Inflector::get($locale)->titleize($str);
}

//

if (!function_exists('closureSerialize')) {
    /**
     * Serialize
     *
     * @param $data
     *
     * @return string
     */
    function closureSerialize($data): string
    {
        SerializableClosure::enterContext();
        SerializableClosure::wrapClosures($data);
        $data = serialize($data);
        SerializableClosure::exitContext();
        return $data;
    }
}

if (!function_exists('closureUnserialize')) {
    /**
     * Unserialize
     *
     * @param $data
     *
     * @return mixed
     */
    function closureUnserialize($data)
    {
        SerializableClosure::enterContext();
        $data = unserialize($data);
        SerializableClosure::unwrapClosures($data);
        SerializableClosure::exitContext();
        return $data;
    }
}

//

if (!function_exists('async')) {
    /**
     * @param Task|callable $task
     *
     * @return ParallelProcess
     */
    function async($task = null): RunnableInterface
    {
        return ParentRuntime::createProcess($task);
    }
}

if (!function_exists('await')) {
    function await(?Pool $pool = null): array
    {
        return $pool->wait();
    }
}

//

if (!function_exists('dot')) {
    /**
     * Create a new `Dot` object with the given items.
     *
     * @param mixed $items
     *
     * @return Dot
     */
    function dot($items): Dot
    {
        return new Dot($items);
    }
}

//

global $Hooks;

$Hooks = Hook::getInstance();
$Hooks->call('After_Hooks_Setup', $Hooks);

// Hook::getInstance()->call('After_Hooks_Setup', Hook::getInstance());
