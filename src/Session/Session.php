<?php

declare(strict_types=1);

namespace Exen\Support\Session;

use ArrayAccess;
use Countable;
use ArrayIterator;
use CachingIterator;
use IteratorAggregate;

use Exen\Support\Session\Adapter\SessionAdapterInterface;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Serializable;
use Exen\Support\Arr;
use Exen\Support\Json\Json;
use Exen\Support\Yaml\Yaml;

/**
 * Session handler
 */
final class Session implements
    Arrayable,
    Jsonable,
    JsonSerializable,
    ArrayAccess,
    Countable,
    IteratorAggregate,
    Serializable
{
    use MacroableTrait;

    /**
     * @var SessionAdapterInterface
     */
    private $adapter = null;

    /**
     * @var Session array
     */
    private $items = null;

    private static $instance = null;

    /**
     * Constructor
     *
     * @param SessionAdapterInterface $adapter
     */
    public function __construct(SessionAdapterInterface $adapter = null)
    {
        self::$instance = &$this;

        $this->items = &$_SESSION;
        $this->adapter = $adapter;
    }

    public static function instance(SessionAdapterInterface $adapter = null): ?self
    {
        if (null === self::$instance) {
            self::$instance = new static($adapter);
        }

        return self::$instance;
    }

    public static function getInstance(): ?self
    {
        return self::$instance;
    }

    /**
     * Starts the session - do not use session_start().
     *
     * @return bool True if session started
     */
    public function start(): bool
    {
        if ($this->isStarted()) {
            return false;
        }

        return $this->adapter->start();
    }

    /**
     * Checks if the session was started.
     *
     * @return bool
     */
    public function isStarted(): bool
    {
        return $this->adapter->isStarted();
    }

    /**
     * Migrates the current session to a new session id while maintaining all session attributes.
     *
     * Regenerates the session ID - do not use session_regenerate_id(). This method can optionally
     * change the lifetime of the new cookie that will be emitted by calling this method.
     *
     * @return bool True if session migrated, false if error
     */
    public function regenerateId()
    {
        return $this->adapter->regenerateId();
    }

    /**
     * Clears all session data and regenerates session ID.
     *
     * Do not use session_destroy().
     *
     * Invalidates the current session.
     *
     * Clears all session attributes and flashes and regenerates the session and deletes the old session from persistence.
     *
     * @return bool True if session invalidated, false if error
     */
    public function destroy()
    {
        return $this->adapter->destroy();
    }

    /**
     * Returns the session ID.
     *
     * @return string|null The session ID
     */
    public function getId()
    {
        return $this->adapter->getId();
    }

    /**
     * Sets the session ID.
     *
     * @param  string $id
     * @return void
     */
    public function setId(string $id = null): void
    {
        $this->adapter->setId($id);
    }

    /**
     * Returns the session name.
     *
     * @return string The session name
     */
    public function getName(): string
    {
        return $this->adapter->getName();
    }

    /**
     * Sets the session name.
     *
     * @return void
     */
    public function setName(string $name = null): void
    {
        $this->adapter->setName($name);
    }

    /**
     * Returns true if the attribute exists.
     *
     * @param  string $name
     * @return bool true if the attribute is defined, false otherwise
     */
    public function has(string $name = null): bool
    {
        return $this->adapter->has($name);
    }

    /**
     * Checks if the given key exists in the provided array.
     *
     * @param array      $array Array to validate
     * @param int|string $key   The key to look for
     *
     * @return bool
     */
    protected function exists($array = [], $key = null)
    {
        return @\array_key_exists($key, $array);
    }

    /**
     * Gets an attribute by key.
     *
     * @param  string     $name    The attribute name
     * @param  mixed|null $default The default value if not found
     * @return mixed|null
     */
    public function &get(string $name = null, $default = null)
    {
        $result = $this->adapter->get($name, $default);

        return $result;
    }

    /**
     * Sets an attribute by key.
     *
     * @param  array|int|string $keys
     * @param  mixed            $value
     * @return void
     */
    public function set($keys = null, $value = null): void
    {
        $this->adapter->set($keys, $value);
    }

    /**
     * Sets multiple attributes at once: takes a keyed array and sets each key => value pair.
     *
     * @param  array $values
     * @return void
     */
    public function replace(array $values = []): void
    {
        $this->adapter->replace($values);
    }

    /**
     * Deletes an attribute by key.
     *
     * @param  string $name
     * @return void
     */
    public function remove(string $name = null): void
    {
        $this->adapter->remove($name);
    }

    /**
     * Deletes an attribute by key.
     *
     * @param  string $name
     * @return void
     */
    public function delete(string $name = null): void
    {
        $this->adapter->remove($name);
    }

    /**
     * Clear all attributes.
     */
    public function clear(): void
    {
        $this->adapter->clear();
    }

    /**
     * Returns the number of attributes.
     *
     * @return int
     */
    public function count(): int
    {
        return $this->adapter->count();
    }

    /**
     * Force the session to be saved and closed.
     *
     * This method is generally not required for real sessions as the session
     * will be automatically saved at the end of code execution.
     *
     * @return void
     */
    public function save(): void
    {
        $this->adapter->save();
    }

    /**
     * Set session runtime options
     *
     * @param  array $config
     * @return void
     * @link   http://php.net/manual/en/session.configuration.php
     */
    public function setOptions(array $config = [])
    {
        $this->adapter->setOptions($config);
    }

    /**
     * Get session runtime options
     *
     * @return array
     */
    public function getOptions(): array
    {
        return $this->adapter->getOptions();
    }

    /**
     * Set cookie parameters.
     *
     * @link http://php.net/manual/en/function.session-set-cookie-params.php
     *
     * @param  int    $lifetime The lifetime of the cookie in seconds.
     * @param  string $path     The path where information is stored.
     * @param  string $domain   The domain of the cookie.
     * @param  bool   $secure   The cookie should only be sent over secure connections.
     * @param  bool   $httpOnly The cookie can only be accessed through the HTTP protocol.
     * @return void
     */
    public function setCookieParams(
        int $lifetime = null,
        string $path = null,
        string $domain = null,
        bool $secure = false,
        bool $httpOnly = false
    ): void {
        $this->adapter->setCookieParams($lifetime, $path, $domain, $secure, $httpOnly);
    }

    /**
     * Get cookie parameters.
     *
     * @see http://php.net/manual/en/function.session-get-cookie-params.php
     *
     * @return array
     */
    public function getCookieParams(): array
    {
        return $this->adapter->getCookieParams();
    }

    //

    /**
     * {@inheritDoc}
     */
    public function isActive(): bool
    {
        return $this->adapter->isActive();
    }

    /**
     * Set a given key / value pair or pairs
     * if the key doesn't exist already
     *
     * @param array|int|string $keys
     * @param mixed            $value
     */
    public function add($keys, $value = null)
    {
        if (\is_array($keys)) {
            foreach ($keys as $key => $value) {
                $this->add($key, $value);
            }
        } elseif (\is_null($this->get($keys))) {
            $this->set($keys, $value);
        }
    }

    /**
     * Set session value or array of values to specific namespace.
     *
     * @param string     $name  Session namespace
     * @param mixed      $key   Session key or an array of keys and values
     * @param mixed|null $value Session value to set if key is not an array
     */
    public function setTo(
        string $name = null,
        string $key = null,
        $value = null
    ): void {
        $result = null;

        if (\is_string($name) and !\is_null($name)) {
            $result = $this->get($name, []);
        }

        if (!empty($result)) {
            $result[$key] = $value;
        // $this->adapter->set($name, $result);
        } else {
            $result[$key] = $value;
        }

        $this->adapter->set($name, $result);
    }

    /**
     * Add session value or array of values to specific namespace.
     *
     * @param string     $name  Session namespace
     * @param mixed      $key   Session key or an array of keys and values
     * @param mixed|null $value Session value to add if key is not an array
     */
    public function addTo(
        string $name = null,
        $key = null,
        $value = null
    ): void {
        $result = null;

        if (\is_string($name) and !\is_null($name)) {
            $result = $this->get($name, []);
        }

        $result[$key] = $value;

        $this->adapter->add($key, $result);
    }

    /**
     * Get session value from specific namespace.
     *
     * @param string     $name    Session namespace
     * @param mixed|null $key     Session key
     * @param mixed|null $default Default value
     *
     * @return mixed
     */
    public function getFrom(
        string $name = null,
        $key = null,
        $default = null
    ) {
        $result = null;

        if (\is_string($name) and !\is_null($name)) {
            $result = $this->get($name, []);
        }

        if (!empty($result)) {
            $result = $result[$key];
        }

        return $default;
        // return $result;
    }

    //

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    /*
     * --------------------------------------------------------------
     * ArrayAccess interface
     * --------------------------------------------------------------
     */

    /**
     * Check if a given key exists
     *
     * @param  int|string $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return $this->has($key);
    }

    /**
     * Return the value of a given key
     *
     * @param  int|string $key
     * @return mixed
     */
    public function &offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * Set a given value to the given key
     *
     * @param int|string|null $key
     * @param mixed           $value
     */
    public function offsetSet($key, $value)
    {
        if (\is_null($key)) {
            $this->items[] = $value;

            return;
        }

        $this->set($key, $value);
    }

    /**
     * Delete the given key
     *
     * @param int|string $key
     */
    public function offsetUnset($key)
    {
        $this->delete($key);
    }

    public function toArray(array $options = null, int $depth = null)
    {
        return \array_map(
            function ($value) use ($options) {
                return $value instanceof Arrayable ?
                    $value->toArray($options, $depth) :
                    $value;
            },
            $this->items
        );
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    public function jsonSerialize()
    {
        return \array_map(
            function ($value) {
                if ($value instanceof JsonSerializable) {
                    return $value->jsonSerialize();
                } elseif ($value instanceof Jsonable) {
                    return \json_decode($value->toJson(), true);
                } elseif ($value instanceof Arrayable) {
                    return $value->toArray();
                }

                return $value;
            },
            $this->items
        );
    }

    /**
     *
     * @return string
     */
    public function serialize(): string
    {
        return \serialize($this->toArray());
    }

    /**
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $serialized = (array) \unserialize($serialized);
        $this->replace($serialized);
    }

    /**
     * {inherit doc}
     */
    public function toJson(int $options = Json::PRETTY)
    {
        return Json::encode($this->toArray(), $options);
    }

    /**
     * {inherit doc}
     */
    public function toYaml(int $options = Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK)
    {
        return Yaml::encode($this->toArray(), $options);
    }

    /**
     * Magic method for get.
     *
     * @param string $key
     *
     * @return mixed
     */
    public function __get($key)
    {
        return $this->get($key);
    }

    /**
     * Magic method for set.
     *
     * @param string $key
     * @param mixed  $value
     */
    public function __set($key, $value)
    {
        $this->set($key, $value);
    }

    /**
     * Magic method for delete.
     *
     * @param string $key
     */
    public function __unset($key)
    {
        $this->delete($key);
    }

    /**
     * Magic method for exists.
     *
     * @param string $key
     *
     * @return bool
     */
    public function __isset($key)
    {
        return $this->exists($key);
    }

    public function toString(): string
    {
        return $this->toJson();
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
