<?php

declare(strict_types=1);

namespace Exen\Support\Session\Adapter;

use RuntimeException;

/**
 * A memory (array) session handler adapter
 */
class MemorySessionAdapter implements SessionAdapterInterface
{
    private $data = [];

    private $id = '';

    private $started = false;

    private $name = '';

    private $config = [];

    private $cookie = [];

    public function __construct()
    {
        $this->setCookieParams(0, '/', '', false, true);

        $config = [];

        foreach (\ini_get_all('session') as $key => $value) {
            $config[substr($key, 8)] = $value['local_value'];
        }

        $this->setOptions($config);
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     */
    public function start(): bool
    {
        if (!$this->id) {
            $this->regenerateId();
        }

        $this->started = true;

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function isStarted(): bool
    {
        return $this->started;
    }

    /**
     * {@inheritDoc}
     */
    public function isActive(): bool
    {
        return $this->started;
    }

    /**
     * {@inheritDoc}
     */
    public function regenerateId(): bool
    {
        $this->id = \str_replace('.', '', \uniqid('sess_', true));
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function destroy(): bool
    {
        $this->data = [];
        $this->regenerateId();

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritDoc}
     */
    public function setId(string $id = null): void
    {
        $this->id = $id;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritDoc}
     */
    public function setName(string $name = null): void
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session name when session is active');
        }
        $this->name = $name;
    }

    /**
     * {@inheritDoc}
     */
    public function has(string $name = null): bool
    {
        if (empty($this->data)) {
            return false;
        }

        return \array_key_exists($name, $this->data);
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $name = null, $default = null)
    {
        if (!$this->has($name)) {
            return $default;
        }

        return $this->data[$name];
    }

    /**
     * {@inheritDoc}
     */
    public function set($keys = null, $value = null): void
    {
        $this->data[$keys] = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function replace(array $values = []): void
    {
        $this->data = \array_replace_recursive($this->data, $values);
    }

    /**
     * {@inheritDoc}
     */
    public function remove(string $name = null): void
    {
        unset($this->data[$name]);
    }

    /**
     * {@inheritDoc}
     */
    public function clear(): void
    {
        $this->data = [];
    }

    /**
     * {@inheritDoc}
     */
    public function count(): int
    {
        return \count($this->data);
    }

    /**
     * {@inheritDoc}
     */
    public function save(): void
    {
    }

    /**
     * {@inheritDoc}
     */
    public function setOptions(array $config = []): void
    {
        foreach ($config as $key => $value) {
            $this->config[$key] = $value;
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOptions(): array
    {
        return $this->config;
    }

    /**
     * {@inheritDoc}
     */
    public function setCookieParams(
        int $lifetime = null,
        string $path = null,
        string $domain = null,
        bool $secure = false,
        bool $httpOnly = false
    ): void {
        $this->cookie = [
            'lifetime' => $lifetime,
            'path' => $path,
            'domain' => $domain,
            'secure' => $secure,
            'httponly' => $httpOnly,
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getCookieParams(): array
    {
        return $this->cookie;
    }
}
