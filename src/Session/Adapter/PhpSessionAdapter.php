<?php

declare(strict_types=1);

namespace Exen\Support\Session\Adapter;

use RuntimeException;

/**
 * A PHP Session handler adapter
 */
class PhpSessionAdapter implements SessionAdapterInterface
{
    /**
     * @var Session array
     */
    protected $data = [];

    public function __construct()
    {
        $this->data = &$_SESSION;
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * {@inheritDoc}
     */
    public function start(): bool
    {
        return @\session_start();
    }

    /**
     * {@inheritDoc}
     */
    public function isStarted(): bool
    {
        return \session_status() === \PHP_SESSION_ACTIVE;
    }

    /**
     * {@inheritDoc}
     */
    public function isActive(): bool
    {
        return \session_status() === \PHP_SESSION_ACTIVE;
    }

    /**
     * {@inheritDoc}
     */
    public function regenerateId(): bool
    {
        return \session_regenerate_id(true);
    }

    /**
     * {@inheritDoc}
     */
    public function destroy(): bool
    {
        $this->clear();

        if (\ini_get('session.use_cookies')) {
            $params = \session_get_cookie_params();
            \setcookie(
                $this->getName(),
                '',
                \time() - 42000,
                $params['path'],
                $params['domain'],
                $params['secure'],
                $params['httponly']
            );
        }

        if ($this->isStarted()) {
            \session_destroy();
            \session_unset();
        }

        \session_write_close();

        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        return \session_id() ?: '';
    }

    /**
     * {@inheritDoc}
     */
    public function setId(string $id = null): void
    {
        \session_id($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return \session_name();
    }

    /**
     * {@inheritDoc}
     */
    public function setName(string $name = null): void
    {
        if ($this->isStarted()) {
            throw new RuntimeException('Cannot change session name when session is active');
        }

        \session_name($name);
    }

    /**
     * {@inheritDoc}
     */
    public function has(string $name = null): bool
    {
        if (empty($_SESSION)) {
            return false;
        }

        return \array_key_exists($name, $_SESSION);
    }

    /**
     * Checks if the given key exists in the provided array.
     *
     * @param array      $array Array to validate
     * @param int|string $key   The key to look for
     *
     * @return bool
     */
    protected function exists($array = [], $key = null)
    {
        return @\array_key_exists($key, $array);
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $name = null, $default = null)
    {
        //     return $this->has($name)
        //         ? $_SESSION[$name]
        //         : $default;

        if (\is_null($name)) {
            return $this->data;
        }

        if ($this->exists($this->data, $name)) {
            return $this->data[$name];
        }

        if (\strpos($name, '.') === false) {
            return $default;
        }

        $data = $this->data;

        foreach (\explode('.', $name) as $segment) {
            if (!\is_array($data) || !$this->exists($data, $segment)) {
                return $default;
            }

            $data = &$data[$segment];
        }

        // if (is_array($items)) {
        //     $items = new static($items);
        // }

        return $data;
    }

    /**
     * {@inheritDoc}
     */
    public function set($keys = null, $value = null): void
    {
        // $_SESSION[$keys] = $value;

        if (\is_array($keys)) {
            foreach ($keys as $key => $value) {
                $this->set($key, $value);
            }

            return;
        }

        $data = &$this->data;

        foreach (explode('.', $keys) as $key) {
            if (!isset($data[$key]) || !\is_array($data[$key])) {
                $data[$key] = [];
            }

            $data = &$data[$key];
        }

        $data = $value;
    }

    /**
     * {@inheritDoc}
     */
    public function replace(array $values = []): void
    {
        $_SESSION = \array_replace_recursive($_SESSION, $values);
    }

    /**
     * {@inheritDoc}
     */
    public function remove(string $name = null): void
    {
        unset($_SESSION[$name]);
    }

    /**
     * {@inheritDoc}
     */
    public function clear(): void
    {
        $_SESSION = [];
    }

    /**
     * {@inheritDoc}
     */
    public function count(): int
    {
        return \count($_SESSION);
    }

    /**
     * {@inheritDoc}
     */
    public function save(): void
    {
        \session_commit();
    }

    /**
     * {@inheritDoc}
     */
    public function setOptions(array $config = []): void
    {
        foreach ($config as $key => $value) {
            @\ini_set('session.' . $key, $value);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getOptions(): array
    {
        $config = [];

        foreach (@\ini_get_all('session') as $key => $value) {
            $config[\substr($key, 8)] = $value['local_value'];
        }

        return $config;
    }

    /**
     * {@inheritDoc}
     */
    public function setCookieParams(
        int $lifetime = null,
        string $path = null,
        string $domain = null,
        bool $secure = false,
        bool $httpOnly = false
    ): void {
        \session_set_cookie_params($lifetime, $path, $domain, $secure, $httpOnly);
    }

    /**
     * {@inheritDoc}
     */
    public function getCookieParams(): array
    {
        return \session_get_cookie_params();
    }
}
