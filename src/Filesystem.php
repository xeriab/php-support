<?php

declare(strict_types=1);

namespace Exen\Support;

use DirectoryIterator;
use FilesystemIterator;
use LogicException;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Exen\Support\Exception\IOException;
use Exen\Support\Exception\InvalidStateException;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Interfaces\Singletonable;
use function array_diff;
use function array_key_exists;
use function call_user_func_array;
use function chmod;
use function dirname;
use function error_get_last;
use function file_exists;
use function file_get_contents;
use function file_put_contents;
use function fopen;
use function get_class;
use function is_dir;
use function is_file;
use function is_link;
use function is_readable;
use function is_writable;
use function preg_match;
use function realpath;
use function rename;
use function rmdir;
use function scandir;
use function stream_copy_to_stream;
use function stream_is_local;
use function substr;

/**
 * Class for various `Filesystem` helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
class Filesystem extends PhpObject implements Singletonable
{
    use MacroableTrait;

    /**
     * Filesystem instance.
     *
     * @var Filesystem
     */
    private static $instance = null;

    /**
     * Filesystem instances.
     *
     * @var Filesystem[]
     */
    private static $instances = [];

    /**
     * Gets file path info.
     *
     * @param string|null $file Path or file name.
     * @param string|null $option Option.
     *
     * @return string|array
     */
    public static function getPathInfo(
        string $file = null,
        string $option = null
    ) {
        $info = Path::parse($file);

        if ($info['protocol'] != 'file') {
            $info['url'] = $info['pathname'];
        }

        // if ($adapter = self::getAdapter($info['protocol'])) {
        //     $info = $adapter->getPathInfo($info);
        // }

        if ($option === null) {
            return $info;
        }

        return array_key_exists($option, $info) ? $info[$option] : '';
    }

    /**
     * Gets canonicalized file path or localpath.
     *
     * @param string|null $file Path or file name.
     * @param bool $local Is it local path.
     *
     * @return string|false
     */
    public static function getPath(string $file = null, bool $local = false)
    {
        return self::getPathInfo(
            $file,
            $local ? 'localpath' : 'pathname'
        ) ?: false;
    }

    /**
     * Checks whether a file or directory exists.
     *
     * @param string|array $files Paths string or array.
     *
     * @return bool
     */
    public static function exists($files = null): bool
    {
        $files = (array) $files;

        foreach ($files as $file) {
            $file = self::getPathInfo($file, 'pathname');

            if (!file_exists($file)) {
                return false;
            }
        }

        return true;
    }

    /**
     * Makes a directory.
     *
     * @param string|null $directory
     * @param int $mode
     * @param bool $recursive
     * @return bool
     */
    public static function makeDir(
        string $directory = null,
        int $mode = 0777,
        bool $recursive = true
    ): bool {
        $directory = self::getPathInfo($directory, 'pathname');

        return is_dir($directory) ?
            true :
            @mkdir($directory, $mode, $recursive);
    }

    /**
     * List files and directories inside the specified path.
     *
     * @param string|null $directory
     *
     * @return array
     */
    public function listDir(string $directory = null): array
    {
        $directory = self::getPathInfo($directory, 'pathname');

        return array_diff(scandir($directory) ?: [], ['..', '.']);
    }

    /**
     * Copies a directory.
     *
     * @param string|null $source
     * @param string|null $target
     *
     * @return boolean
     * @throws IOException
     * @throws InvalidStateException
     * @noinspection PhpUnused
     */
    public function copyDir(string $source = null, string $target = null): bool
    {
        $source = self::getPathInfo($source, 'pathname');
        $target = self::getPathInfo($target, 'pathname');

        if (!is_dir($source) || !self::makeDir($target)) {
            return false;
        }

        if (substr($source, -1) != '/') {
            $source .= '/';
        }

        if (substr($target, -1) != '/') {
            $target .= '/';
        }

        foreach (self::listDir($source) as $file) {
            if (is_dir($source.$file)) {
                if (!self::copyDir($source.$file, $target.$file)) {
                    return false;
                }
            } elseif (is_file($source.$file)) {
                if (!self::copy($source.$file, $target.$file)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Reads a directory.
     *
     * @param string|null $directory
     * @param bool $recursive
     *
     * @return array
     * @throws IOException
     * @noinspection PhpUnused
     */
    public static function readDir(
        string $directory = null,
        bool $recursive = false
    ): array {
        if (!is_dir($directory)) { // @ - dir may already exist
            // throw new IOException("Directory '$directory' not found.");
            throw new IOException(
                "Unable to read directory '$directory'. " .
                error_get_last()['message']
            );
        }

        $directory = realpath($directory);

        $result = [];

        $contents = new DirectoryIterator($directory);

        foreach ($contents as $content) {
            if (!$content->isDot()) {
                if ($recursive) {
                    if (is_dir($content->getFilename())) {
                        $args = [
                            realpath($content->getFilename()),
                            $recursive
                        ];

                        $result[
                            $content->getFilename()
                        ] = call_user_func_array(
                            [
                            __CLASS__,
                            __FUNCTION__
                            ],
                            $args
                        );
                    } else {
                        $result[] = $content->getFilename();
                    }
                } else {
                    // $result[] = \realpath($content->getFilename());
                    $result[] = $content->getFilename();
                }
            }
        }

        return $result;
    }

    /**
     * Copies a file or directory.
     *
     * @param string|null $source
     * @param string|null $destination
     * @param bool $overwrite
     * @return boolean
     * @throws IOException
     * @throws InvalidStateException
     */
    public static function copy(
        string $source = null,
        string $destination = null,
        bool $overwrite = true
    ): bool {
        if (stream_is_local($source) && !file_exists($source)) {
            throw new IOException("File or directory '$source' not found.");
        } elseif (!$overwrite && file_exists($destination)) {
            throw new InvalidStateException(
                "File or directory '$destination' already exists."
            );
        } elseif (is_dir($source)) {
            self::makeDir($destination);

            foreach (new FilesystemIterator($destination) as $item) {
                self::delete($item->getPathname());
            }

            $iterator = new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator(
                    $source,
                    RecursiveDirectoryIterator::SKIP_DOTS
                ),
                RecursiveIteratorIterator::SELF_FIRST
            );

            foreach ($iterator as $item) {
                if ($item->isDir()) {
                    /** @noinspection PhpUndefinedMethodInspection */
                    self::makeDir(
                        $destination.'/'.$iterator->getSubPathName()
                    );
                } else {
                    /** @noinspection PhpUndefinedMethodInspection */
                    self::copy(
                        $item->getPathname(),
                        $destination.'/'.$iterator->getSubPathName()
                    );

                    return true;
                }
            }
        } else {
            self::makeDir(dirname($destination));

            if (false === @stream_copy_to_stream(
                fopen($source, 'r'),
                fopen($destination, 'w')
            )
            ) { // @ is escalated to exception
                throw new IOException(
                    "Unable to copy file '$source' to '$destination'."
                );
            }
        }

        return false;
    }

    /**
     * Deletes a file or directory.
     *
     * @param string|null $path
     * @throws IOException
     */
    public static function delete(string $path = null): void
    {
        if (is_file($path) || is_link($path)) {
            $func = DS === '\\' && is_dir($path) ? 'rmdir' : 'unlink';

            if (!@$func($path)) { // @ is escalated to exception
                throw new IOException("Unable to delete '$path'.");
            }
        } elseif (is_dir($path)) {
            foreach (new FilesystemIterator($path) as $item) {
                self::delete($item->getPathname());
            }

            if (!@rmdir($path)) { // @ is escalated to exception
                throw new IOException("Unable to delete directory '$path'.");
            }
        }
    }

    /**
     * Renames a file or directory.
     *
     * @param string|null $name
     * @param string|null $newName
     * @param bool $overwrite
     * @return void
     * @throws IOException
     * @throws InvalidStateException
     */
    public static function rename(
        string $name = null,
        string $newName = null,
        bool $overwrite = true
    ): void {
        if (!$overwrite && file_exists($newName)) {
            throw new InvalidStateException(
                "File or directory '$newName' already exists."
            );
        } elseif (!file_exists($name)) {
            throw new IOException("File or directory '$name' not found.");
        } else {
            self::makeDir(dirname($newName));

            if (realpath($name) !== realpath($newName)) {
                self::delete($newName);
            }

            if (!@rename($name, $newName)) { // @ is escalated to exception
                throw new IOException(
                    "Unable to rename file or directory '$name' to '$newName'."
                );
            }
        }
    }

    /**
     * Reads file content.
     *
     * @param string|null $file
     * @return string
     * @throws IOException
     */
    public static function read(string $file = null): string
    {
        $content = @file_get_contents($file); // @ is escalated to exception

        if (false === $content) {
            throw new IOException("Unable to read file '$file'.");
        }

        return $content;
    }

    /**
     * Writes a string to a file.
     *
     * @param string|null $file
     * @param null $content
     * @param int $mode
     * @return void
     * @throws IOException
     */
    public static function write(
        string $file = null,
        $content = null,
        int $mode = 0666
    ): void {
        self::makeDir(dirname($file));

        // @ is escalated to exception
        if (false === @file_put_contents($file, $content)) {
            throw new IOException("Unable to write file '$file'.");
        }

        // @ is escalated to exception
        if (null !== $mode && !@chmod($file, $mode)) {
            throw new IOException("Unable to chmod file '$file'.");
        }
    }

    /**
     * Determine whether the given `$path` is file.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isFile(?string $path = null): bool
    {
        return is_file($path);
    }

    /**
     * Determine whether the given `$path` is writable.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isWritable(?string $path = null): bool
    {
        return is_writable($path);
    }

    /**
     * Determine whether the given `$path` is readable.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     */
    public static function isReadable(?string $path = null): bool
    {
        return is_readable($path);
    }

    /**
     * Determine whether the given `$path` is directory.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     * @noinspection PhpUnused
     */
    public static function isDirectory(?string $path = null): bool
    {
        return is_dir($path);
    }

    /**
     * Determine whether the given `$path` is directory.
     *
     * @param mixed $path Path to check.
     *
     * @return boolean
     * @noinspection PhpUnused
     */
    public static function isDir(?string $path = null): bool
    {
        return is_dir($path);
    }

    /**
     * Is path absolute?
     *
     * @param string|null $path
     * @return bool
     * @noinspection PhpUnused
     */
    public static function isAbsolute(string $path = null): bool
    {
        return (bool) preg_match(
            '#([a-z]:)?[/\\]|[a-z][a-z0-9+.-]*://#Ai',
            $path
        );
    }

    /**
     * Get the content of given file and returns the results.
     *
     * @param string|null $path The path to the file
     *
     * @return string The results of the include
     *
     * @throws IOException
     * @since              0.2.4
     * @codeCoverageIgnore
     */
    public static function getContent(string $path = null): string
    {
        // return file_get_contents(realpath($path));

        // @ is escalated to exception
        $content = @file_get_contents(realpath($path));

        if (false === $content) {
            throw new IOException("Unable to read file '$path'.");
        }

        return $content;
    }

    /**
     * Filesystem constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
                ' is singleton and cannot be cloned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}

///
/// Class aliases
///

/**
 * Class for various PHP Utilities.
 *
 * Just an alias for \Exen\Support\Filesystem class.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
// final class Fs extends Filesystem
// {
//     //...
// }

// \class_alias('Exen\Support\Filesystem', 'Exen\Support\Fs');
