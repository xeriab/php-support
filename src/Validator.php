<?php

declare(strict_types=1);

namespace Exen\Support;

use LogicException;

/**
 * Class Validator.
 */
class Validator
{
    use Traits\StaticClassTrait;

    /**
     * Test if a give filesystem path is absolute.
     *
     * For example, '/foo/bar', or 'c:\windows'.
     *
     * @since 2.5.0
     *
     * @param string $path file path
     *
     * @return bool true if path is absolute, false is not absolute
     */
    public static function isAbsolutePath(string $path = null): bool
    {
        /**
         * This is definitive if true but fails if $path does not exist or contains
         * a symbolic link.
         */
        if (realpath($path) == $path) {
            return true;
        }

        if (0 == strlen($path) || '.' == $path[0]) {
            return false;
        }

        // Windows allows absolute paths like this.
        if (preg_match('#^[a-zA-Z]:\\/#', $path)) {
            return true;
        }

        // A path starting with / or \ is absolute; anything else is relative.
        return '/' == $path[0] || '\\' == $path[0];
    }

    /**
     * Whether a specific value is a valid IP address.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidIP($value = null): bool
    {
        return false !== \filter_var($value, \FILTER_VALIDATE_IP);
    }

    /**
     * Whether a specific value is a valid MAC address.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidMAC($value = null): bool
    {
        return false !== \filter_var($value, \FILTER_VALIDATE_MAC);
    }

    /**
     * Whether a specific value is a valid URL address.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidURL($value = null): bool
    {
        return false !== \filter_var($value, \FILTER_VALIDATE_URL);
    }

    /**
     * Whether a specific value is a valid E-Mail address.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidEmail($value = null)
    {
        return false !== \filter_var($value, \FILTER_VALIDATE_EMAIL);
    }

    /**
     * Whether a specific value is a valid integer number.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidInteger($value = null, int $min = null, int $max = null): bool
    {
        // $value = is_object($value) ? $value : false;

        $isNumber = (false !== \filter_var($value, \FILTER_VALIDATE_INT));

        $rv = (null !== $value) and
            // (false !== $value) and
            (\is_numeric($value)) and
            // it must validate to itself, allowing for 3.0, "6" etc.
            // ((int) $value) == $value and
            // Correct interval
            (null === $min || $min <= $value) and (null === $max || $value <= $max);

        return $rv or $isNumber;
    }

    /**
     * Validates a number, optionally checks for upper/lower boundaries.
     */
    public static function isValidNumber($value = null, int $min = null, int $max = null): bool
    {
        return null !== $value and
            false !== $value and
            // Numbers are numeric
            \is_numeric($value) and
            // Correct interval
            (null === $min || $min <= $value) and
            (null === $max || $value <= $max);
    }

    /**
     * Whether a specific value is a valid string.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidString($value = null, int $min = null, int $max = null, string $charset = 'UTF-8'): bool
    {
        try {
            $value = \is_string($value) ? $value : '';
            $length = (null == $charset) ? \strlen($value): \mb_strlen($value, $charset);
        } catch (\Exception $ex) {
            $length = 0;
        }

        // D(($length > 0) && (null === $min || $min <= $length) && (null === $max || $length <= $max));

        return ($length > 0) && (null === $min || $min <= $length) && (null === $max || $length <= $max);
    }

    /**
     * Whether a specific value is a valid float number.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidFloat($value = null): bool
    {
        return (false !== \filter_var($value, \FILTER_VALIDATE_FLOAT));
    }

    /**
     * Whether a specific value is a valid integer number.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidInt($value): bool
    {
        return (false !== \filter_var($value, \FILTER_VALIDATE_INT));
    }

    /**
     * Whether a specific value is a valid boolean.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidBoolean($value): bool
    {
        return (false !== \filter_var($value, \FILTER_VALIDATE_BOOLEAN));
    }

    /**
     * Whether a specific value is a valid numeric identifier.
     *
     * In other words, It checks whether the value
     * is a valid positive integer.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidID($value = null): bool
    {
        // $value = (int) $value;
        return static::isValidInt($value) && $value > 0;
    }

    /**
     * Whether a specific value is a valid MongoDB identifier.
     *
     * In other words, It checks whether the value
     * is a valid positive integer.
     *
     * @return bool
     *
     * @since 0.1
     */
    public static function isValidMongoID($value = null): bool
    {
        // return \preg_match('/^[a-f0-9]{24}$/i', $value) > 0;

        if ($value instanceof \ObjectID || $value instanceof \MongoId) {
            return true;
        } elseif (!\is_string($value)) {
            return false;
        }

        return (bool) preg_match('#^[a-f0-9]{24}$#i', $value);
    }

    /**
     * Check if a string is a valid date(time)
     *
     * DateTime::createFromFormat requires PHP >= 5.3
     *
     * @param string $datetimeStr
     * @param string $datetimeFormat
     * @param string $timezone (If timezone is invalid, php will throw an exception)
     *
     * @return bool
     */
    public static function isValidDateTime($datetimeStr, $datetimeFormat, $timezone): bool
    {
        $date = \DateTime::createFromFormat(
            $datetimeFormat,
            $datetimeStr,
            new \DateTimeZone($timezone)
        );

        return $date &&
            \DateTime::getLastErrors()["warning_count"] == 0 &&
            \DateTime::getLastErrors()["error_count"] == 0;
    }

    /**
     * Instantiate this class.
     *
     * @return self
     */
    public static function instance(): self
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * Constructor.
     *
     * @throws \LogicException
     */
    protected function __construct()
    {
    }

    /**
     * Clone.
     *
     * @return self
     * @throws \LogicException
     */
    public function __clone()
    {
        throw new LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }
}
