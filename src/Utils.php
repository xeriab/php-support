<?php

declare(strict_types=1);

namespace Exen\Support;

use UnexpectedValueException;
use BadMethodCallException;
use ReflectionClass;

use Exen\Support\Traits\MacroableTrait;

/**
 * Class for various PHP Utilities.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
class Utils
{
    use MacroableTrait;

    /**
     * The modules you want to use.
     *
     * @var array
     */
    protected $modules = [
        'Exen\Support\Lang',
        'Exen\Support\Arr',
        'Exen\Support\Str',
        // 'Exen\Support\Filesystem',
        // 'Exen\Support\Fs',
        // 'Exen\Support\Process',
    ];

    /**
     * The loaded module instances.
     *
     * @var array
     */
    protected $instances = [];

    /**
     * The instance of Utils.
     *
     * @var \Exen\Support\Utils
     */
    protected static $instance = null;

    /**
     * Load a module.
     *
     * @param string     $module   Module name.
     * @param mixed|null $instance Module instance.
     *
     * @return mixed
     * @throws UnexpectedValueException
     */
    public function load(string $module = null, $instance = null)
    {
        if (!\is_null($instance)) {
            if (!$this->hasModule($module)) {
                $this->addModule($module);
            }

            return ($this->instances[$module] = $instance);
        }

        if ($this->hasModule($module)) {
            // return ($this->instances[$module] = new $module);
            return ($this->instances[$module] = $module::instance());
        }

        throw new UnexpectedValueException("Module {$module} does not exist");
    }

    /**
     * Determine whether the module exists.
     *
     * @param string $module Module name.
     *
     * @return boolean
     */
    public function hasModule(string $module = null): bool
    {
        return \in_array($module, $this->modules);
    }

    /**
     * Add a new module.
     *
     * @param string $module Module name.
     *
     * @return void
     */
    public function addModule(string $module = null): void
    {
        $this->modules[] = $module;
    }

    /**
     * Get all modules.
     *
     * @return array
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * Determine whether a module was loaded.
     *
     * @param string $module Module name.
     *
     * @return boolean
     */
    public function isLoaded(?string $module = null): bool
    {
        return \array_key_exists($module, $this->instances);
    }

    /**
     * Load a module (if not yet) and return its instance.
     *
     * @param string $module Module name.
     *
     * @return mixed
     */
    public function getInstance(?string $module = null)
    {
        if (!$this->isLoaded($module)) {
            $this->load($module);
        }

        return $this->instances[$module];
    }

    /**
     * Determine whether the given object has a method.
     *
     * @param mixed  $object Object to check for method in.
     * @param string $method Method name.
     *
     * @return boolean
     */
    public function hasMethod($object = null, ?string $method = null): bool
    {
        return (new ReflectionClass($object))->hasMethod($method);
    }

    /**
     * Run a method and return its output.
     *
     * @param string|null $name      Method name to run.
     * @param array       $arguments Method arguments.
     *
     * @return mixed
     * @throws BadMethodCallException
     */
    public function run(?string $name = null, ?array $arguments = [])
    {
        foreach ($this->getModules() as $module) {
            $instance = $this->getInstance($module);

            if ($this->hasMethod($instance, $name)) {
                return \call_user_func_array([$instance, $name], $arguments);
            }
        }

        throw new BadMethodCallException("Method {$name} does not exist");
    }

    /**
     * Simpler version of call_user_func_array (for performances).
     *
     * @param string $className  The class name.
     * @param string $methodName The method name.
     * @param array  $parameters The arguments.
     *
     * @return mixed
     */
    protected static function callMethod(
        ?string $className = null,
        ?string $methodName = null,
        ?array $parameters = []
    ) {
        switch (\count($parameters)) {
            case 0:
                return $className::$methodName();
            case 1:
                return $className::$methodName($parameters[0]);
            case 2:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1]
                );
            case 3:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2]
                );
            case 4:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3]
                );
            case 5:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4]
                );
            case 6:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5]
                );
            case 7:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5],
                    $parameters[6]
                );
            case 8:
                return $className::$methodName(
                    $parameters[0],
                    $parameters[1],
                    $parameters[2],
                    $parameters[3],
                    $parameters[4],
                    $parameters[5],
                    $parameters[6],
                    $parameters[7]
                );
        }
    }

    /**
     * Handle calls to non-existent methods.
     *
     * @param string $method Method name.
     * @param array $parameters Method arguments.
     *
     * @return mixed
     */
    public function __call(string $method, array $parameters)
    {
        return \call_user_func_array([$this, 'run'], [$method, $parameters]);
    }

    /**
     * Handle calls to non-existent static methods.
     *
     * @param string $method Method name.
     * @param array $parameters Method arguments.
     *
     * @return mixed
     */
    public static function __callStatic(string $method, array $parameters)
    {
        if (!(self::$instance instanceof self)) {
            self::$instance = new self;
        }

        return \call_user_func_array([self::$instance, $method], $parameters);

        throw new BadMethodCallException('The method '.self::$instance.'::'.$method.' does not exist');
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }
}

///
/// Class aliases
///

/**
 * Class for various PHP Utilities.
 *
 * Just an alias for \Exen\Support\Utils class.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
// final class Util extends Utils
// {
//     //...
// }

// \class_alias('Exen\Support\Utils', 'Exen\Support\Util');
