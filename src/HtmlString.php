<?php

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Interfaces\Htmlable;

/**
 * HtmlString class.
 *
 * @since 0.1.1
 */
class HtmlString implements Htmlable
{
    /**
     * The HTML string.
     *
     * @var string
     */
    protected $html = null;

    /**
     * Create a new HTML string instance.
     *
     * @param string|null $html HTML string.
     */
    public function __construct(?string $html = null)
    {
        $this->html = $html;
    }

    /**
     * Get the HTML string.
     *
     * @return string
     */
    public function toHtml(): ?string
    {
        return $this->html;
    }

    /**
     * Get the HTML string.
     *
     * @return string
     */
    public function __toString(): ?string
    {
        return $this->toHtml();
    }
}
