<?php /** @noinspection SpellCheckingInspection */
/** @noinspection NonAsciiCharacters */

declare(strict_types=1);

namespace Exen\Support;

final class §NotAClass§
{
    private function __construct()
    {
        //...
    }
}
