<?php

declare(strict_types=1);

namespace Exen\Support\Traits;

use Exen\Support\Lang;

trait HashCodeTrait
{
    /**
     * {inherit doc}
     */
    public function equals($object = null): bool
    {
        return ($this === $object);
    }

    /**
     * {inherit doc}
     */
    public function hashCode(): string
    {
        return Lang::hashObject($this);
    }

    /**
     * {inherit doc}
     */
    public function hash(): string
    {
        return Lang::hashObject($this);
    }
}
