<?php /** @noinspection PhpComposerExtensionStubsInspection */
/** @noinspection PhpUndefinedNamespaceInspection */
/** @noinspection SpellCheckingInspection */
/** @noinspection DuplicatedCode */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnused */

//
// MIT License
//
// Copyright (c) 2019 - 2020 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//
//

/**
 * MacroableTrait.
 *
 * @category  Helpers
 * @package   Exen\Support\Traits
 * @author    Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @copyright Copyright (c) 2019 - 2021 Xeriab Nabil (aka KodeBurner) <xeriab@tuta.io>
 * @license   https://projects.xeriab.net/license.html
 */

declare(strict_types=1);

namespace Exen\Support\Traits;

use BadMethodCallException;
use Closure;
use ReflectionClass;
use ReflectionException;
use ReflectionMethod;
use function call_user_func_array;
use function sprintf;

trait MacroableTrait
{
    /**
     * The registered string macros.
     *
     * @var array
     */
    protected static $registeredObjectMacros = [];

    // /**
    //  * Register a custom macro.
    //  *
    //  * @param string          $name
    //  * @param object|callable $macro
    //  */
    // public static function macro(string $name = null, $macro = null): void
    // {
    //     static::$registeredObjectMacros[$name] = $macro;
    // }

    /**
     * Mix another object into the class.
     *
     * @param object|null $mixin
     *
     * @throws ReflectionException
     */
    public static function mixin(object $mixin = null): void
    {
        $methods = (new ReflectionClass($mixin))->getMethods(
            ReflectionMethod::IS_PUBLIC | ReflectionMethod::IS_PROTECTED
        );

        foreach ($methods as $method) {
            $method->setAccessible(true);
            static::registerMacro($method->name, $method->invoke($mixin));
        }
    }

    /**
     * Register a custom macro.
     *
     * @param string|null $name
     * @param null $macro
     */
    public static function registerMacro(string $name = null, $macro = null): void
    {
        static::$registeredObjectMacros[$name] = $macro;
    }

    /**
     * Dynamically handle calls to the class.
     *
     * @param string $method
     * @param array $parameters
     *
     * @return mixed
     *
     * @throws BadMethodCallException
     */
    public static function __callStatic(string $method, array $parameters)
    {
        if (!static::hasMacro($method)) {
            throw new BadMethodCallException(
                sprintf(
                    'Method %s::%s does not exist.',
                    static::class,
                    $method
                )
            );
        }

        if (static::$registeredObjectMacros[$method] instanceof Closure) {
            return call_user_func_array(
                Closure::bind(
                    static::$registeredObjectMacros[$method],
                    null,
                    static::class
                ),
                $parameters
            );
        }

        return call_user_func_array(
            static::$registeredObjectMacros[$method],
            $parameters
        );
    }

    /**
     * Checks if macro is registered.
     *
     * @param string|null $name
     *
     * @return bool
     */
    public static function hasMacro(string $name = null): bool
    {
        return isset(static::$registeredObjectMacros[$name]);
    }

    /**
     * Dynamically handle calls to the class.
     *
     * @param string $method
     * @param array $parameters
     *
     * @return mixed
     *
     * @throws BadMethodCallException
     */
    public function __call(string $method, array $parameters)
    {
        if (!static::hasMacro($method)) {
            throw new BadMethodCallException(
                sprintf(
                    'Method %s::%s does not exist.',
                    static::class,
                    $method
                )
            );
        }

        $macro = static::$registeredObjectMacros[$method];

        if ($macro instanceof Closure) {
            return call_user_func_array(
                $macro->bindTo($this, static::class),
                $parameters
            );
        }

        return call_user_func_array($macro, $parameters);
    }
}
