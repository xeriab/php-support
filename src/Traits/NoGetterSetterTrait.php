<?php

declare(strict_types=1);

namespace Exen\Support\Traits;

trait NoGetterSetterTrait
{
    /**
     * Error handler for unknown property accessor in class/object.
     *
     * @param string $name Unknown property name.
     *
     * @throws \BadMethodCallException
     */
    public function __get($name)
    {
        throw new \BadMethodCallException(
            \sprintf(
                "Unknown property '%s' on '%s'.",
                $name,
                \get_class($this)
            )
        );
    }

    /**
     * Error handler for unknown property mutator in class/object.
     *
     * @param string $name  Unknown property name.
     * @param mixed  $value Property value.
     *
     * @throws \BadMethodCallException
     */
    public function __set($name, $value): void
    {
        throw new \BadMethodCallException(
            \sprintf(
                "Unknown property '%s' on '%s'.",
                $name,
                \get_class($this)
            )
        );
    }
}
