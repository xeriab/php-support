<?php

declare(strict_types=1);

namespace Exen\Support\Traits;

trait LocalizableTrait
{
    /**
     * Run the callback with the given locale.
     *
     * @param string                                    $locale
     * @param \Symfony\Component\Translation\Translator $translator
     * @param \Closure                                  $callback
     *
     * @return bool
     */
    public function withLocale(
        string $locale = null,
        $translator = null,
        callable $callback = null
    ): bool {
        if (!$locale || !$translator) {
            return $callback();
        }

        $original = $translator->getLocale();

        try {
            $translator->setLocale($locale);

            return $callback();
        } finally {
            $translator->setLocale($original);
        }
    }
}
