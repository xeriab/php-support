<?php

declare(strict_types=1);

namespace Exen\Support\Traits;

trait GetterSetterTrait
{
    /**
     *
     * @return mixed
     *
     * @since 0.1
     */
    public function get(?string $key = null, ?bool $strict = false)
    {
        $key = \trim($key);

        if (\property_exists($this, $key)) {
            if ('data' == $key) {
                return $this->$key;
            }
        } else {
            if ($strict) {
                throw new \RuntimeException(
                    'The property "'.$key.'" of class "'.
                    \get_class($this).'" does not exist.'
                );
            } else {
                return $key;
            }
        }
    }

    /**
     *
     * @return mixed
     *
     * @since 0.1
     */
    public function set(?string $key = null, $value = null)
    {
        $key = \trim($key);

        if (\property_exists($this, $key)) {
            $this->$key = $value;
        }

        return $this;
    }
}
