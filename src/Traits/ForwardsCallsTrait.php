<?php
/**
 * Forwards calles trait.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category   Traits
 * @package    Support
 * @subpackage Traits
 * @author     Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license    https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link       https://gitlab.com/xeriab/php-support
 */
declare(strict_types=1);

namespace Exen\Support\Traits;

use Error;
use BadMethodCallException;

/**
 * Trait for call forwarding.
 *
 * @category   Traits
 * @package    Support
 * @subpackage Traits
 * @author     Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license    https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link       https://gitlab.com/xeriab/php-support
 */
trait ForwardsCallsTrait
{
    /**
     * Forward a method call to the given object.
     *
     * @param mixed  $object     Object.
     * @param string $method     Method name.
     * @param array  $parameters Parameters.
     *
     * @return mixed
     * @throws \BadMethodCallException
     */
    protected function forwardCallTo(
        $object = null,
        string $method = null,
        array $parameters = null
    ) {
        try {
            return $object->{$method}(...$parameters);
        } catch (Error | BadMethodCallException $ex) {
            $pattern = '~^Call to undefined method ' .
                '(?P<class>[^:]+)::(?P<method>[^\(]+)\(\)$~';

            if (! \preg_match($pattern, $ex->getMessage(), $matches)) {
                throw $ex;
            }

            if ($matches['class'] != \get_class($object)
                || $matches['method'] != $method
            ) {
                throw $ex;
            }

            static::throwBadMethodCallException($method);
        }
    }

    /**
     * Throw a bad method call exception for the given method.
     *
     * @param string $method Method name.
     *
     * @return void
     * @throws \BadMethodCallException
     */
    protected static function throwBadMethodCallException(
        string $method = null
    ): void {
        throw new BadMethodCallException(
            sprintf(
                'Call to undefined method %s::%s()',
                static::class,
                $method
            )
        );
    }
}
