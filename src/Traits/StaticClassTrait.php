<?php

declare(strict_types=1);

namespace Exen\Support\Traits;

use LogicException;
use Exen\Support\Exception\MemberAccessException;
use Exen\Support\ObjectHelpers;
use function get_called_class;
use function get_class;

/**
 * Static class.
 */
trait StaticClassTrait
{
    /**
     *
     * @throws LogicException
     */
    final public function __construct()
    {
        throw new LogicException('Class '. get_class($this).' is static and cannot be instantiated.');
    }

    /**
     *
     * @throws LogicException
     */
    final public function __destruct()
    {
        throw new LogicException('Class '. get_class($this).' is static and cannot be destructed.');
    }

    /**
     * Call to undefined static method.
     *
     * @param null $name
     * @param array $arguments
     * @throws MemberAccessException
     */
    public static function __callStatic($name = null, $arguments = [])
    {
        ObjectHelpers::strictStaticCall(get_called_class(), $name);
    }

    /**
     * __toString.
     *
     * @return string
     *
     * @since              0.1
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return (string) get_called_class();
    }
}
