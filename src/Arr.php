<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support;

use ArrayAccess;
use InvalidArgumentException;
use LogicException;
use SimpleXMLElement;
use stdClass;
use Exen\Support\Traits\MacroableTrait;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Exen\Support\Interfaces\Singletonable;
use Closure;
use function abs;
use function array_combine;
use function array_diff;
use function array_fill;
use function array_filter;
use function array_flip;
use function array_intersect;
use function array_intersect_key;
use function array_key_exists;
use function array_keys;
use function array_map;
use function array_merge;
use function array_pop;
use function array_push;
use function array_replace_recursive;
use function array_reverse;
use function array_search;
use function array_shift;
use function array_slice;
use function array_splice;
use function array_sum;
use function array_unique;
use function array_unshift;
use function array_values;
use function array_walk_recursive;
use function boolval;
use function call_user_func;
use function call_user_func_array;
use function count;
use function doubleval;
use function end;
use function explode;
use function floatval;
use function get_class;
use function get_object_vars;
use function implode;
use function in_array;
use function intval;
use function is_array;
use function is_bool;
use function is_int;
use function is_null;
use function is_numeric;
use function is_object;
use function is_scalar;
use function is_string;
use function key;
use function krsort;
use function ksort;
use function method_exists;
use function preg_match;
use function rand;
use function range;
use function reset;
use function round;
use function rtrim;
use function shuffle;
use function simplexml_load_string;
use function sort;
use function srand;
use function str_replace;
use function strpos;
use function strtolower;
use function strtoupper;
use function trim;
use function usort;
use function version_compare;
use const ARRAY_FILTER_USE_BOTH;
use const ARRAY_FILTER_USE_KEY;
use const EMPTY_STRING;
use const PHP_EOL;
use const PHP_VERSION;
use const SINGLE_DOT;
use const SORT_ASC;
use const SORT_DESC;

/**
 * Class for Array helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
class Arr extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const FILTER_USE_BOTH = ARRAY_FILTER_USE_BOTH;
    const FILTER_USE_KEY  = ARRAY_FILTER_USE_KEY;

    /**
     * Arr instance.
     *
     * @var Arr
     */
    private static $instance = null;

    /**
     * Arr instances.
     *
     * @var Arr[]
     */
    private static $instances = [];

    /**
     * Determine whether the given value is an accessible array.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function accessible($value = null): bool
    {
        return is_array($value) or ($value instanceof ArrayAccess);
    }

    /**
     * Determine whether the given value is an accessible array.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isAccessible($value = null): bool
    {
        return is_array($value) or ($value instanceof ArrayAccess);
    }

    /**
     * Determine whether the given value isn't an accessible array.
     *
     * @param mixed $value
     *
     * @return bool
     */
    public static function isNotAccessible($value = null): bool
    {
        return !self::isAccessible($value);
    }

    /**
     * Remove the duplicates from an array.
     *
     * @param array        $array
     * @param bool         $keepKeys
     * @param Closure|null $iterator
     *
     * @return array
     */
    public static function unique(
        array $array = [],
        bool $keepKeys = false,
        Closure $iterator = null
    ): array {
        if ($keepKeys) {
            if (!is_null($iterator)) {
                $array = array_filter($array, $iterator);
            } else {
                $array = array_unique($array);
            }

            // $array = \array_unique($array);
        } else {
            // This is faster version than the builtin array_unique().
            // http://stackoverflow.com/questions/8321620/array-unique-vs-array-flip
            // http://php.net/manual/en/function.array-unique.php
            $array = array_keys(array_flip($array));
        }

        return $array;
    }

    /**
     * Remove all instances of $ignore found in $elements (=== is used).
     *
     * @param array $elements
     * @param array $ignore
     *
     * @return array
     */
    public static function without(array $elements = [], array $ignore = []): array
    {
        foreach ($elements as $key => $node) {
            if (in_array($node, $ignore, true)) {
                unset($elements[$key]);
            }
        }

        return array_values($elements);
    }

    /**
     * Merge two arrays.
     *
     * @param array $one
     * @param array $another
     *
     * @return array
     */
    public static function zip(array $one = [], array $another = []): array
    {
        return array_merge($one, $another);
    }

    /**
     * Get the index of the first match.
     *
     * @param array $elements
     * @param mixed $element
     *
     * @return integer
     */
    public static function indexOf(array $elements = [], $element = null): int
    {
        return array_search($element, $elements, true);
    }

    /**
     * Return the intersection of two arrays.
     *
     * @param array $one
     * @param array $another
     *
     * @return array
     */
    public static function intersection(array $one = [], array $another = []): array
    {
        return array_values(array_intersect($one, $another));
    }

    /**
     * Returns an array containing the unique items in both arrays.
     *
     * @param array $one
     * @param array $another
     *
     * @return array
     */
    public static function union(array $one = [], array $another = []): array
    {
        return self::unique(self::zip($one, $another));
    }

    /**
     * Check is key exists
     *
     * @param string|null $key
     * @param mixed $array
     * @param bool $returnValue
     *
     * @return mixed
     */
    public static function key(
        string $key = null,
        array $array = [],
        bool $returnValue = false
    ) {
        $isExists = array_key_exists((string) $key, (array) $array);

        if ($returnValue) {
            if ($isExists) {
                return $array[$key];
            }

            return null;
        }

        return $isExists;
    }

    /**
     * Get the keys.
     *
     * @param array $array
     *
     * @return array
     */
    public static function keys(array $array = []): array
    {
        return array_keys($array);
    }

    /**
     * Get the values.
     *
     * @param array $array
     *
     * @return array
     */
    public static function values(array $array = []): array
    {
        return array_values($array);
    }

    /**
     * Check is value exists in the array
     *
     * @param string $value
     * @param mixed  $array
     * @param bool   $returnKey
     *
     * @return mixed
     *
     * @SuppressWarnings(PHPMD.ShortMethodName)
     */
    public static function in($value = null, array $array = [], bool $returnKey = false): bool
    {
        $inArray = in_array($value, $array, true);

        if ($returnKey) {
            if ($inArray) {
                return array_search($value, $array, true);
            }

            return false;
        }

        return $inArray;
    }

    /**
     * Add an element to an array using "dot" notation if it doesn't exist.
     *
     * @param array $array
     * @param string|null $key
     * @param mixed $value
     *
     * @return array
     */
    public static function add(array $array = [], string $key = null, $value = null): ?array
    {
        if (is_null(self::get($array, $key))) {
            self::set($array, $key, $value);
        }

        return $array;
    }

    /**
     * Collapse an array of arrays into a single array.
     *
     * @param array $array
     *
     * @return array
     */
    public static function collapse(array $array = []): ?array
    {
        $results = [];

        foreach ($array as $values) {
            if ($values instanceof ExenCollection) {
                $values = $values->all();
            } elseif (!is_array($values)) {
                continue;
            }

            $results = array_merge($results, $values);
        }

        return $results;
    }

    /**
     * Cross join the given arrays, returning all possible permutations.
     *
     * @param array ...$arrays
     *
     * @return array
     */
    public static function crossJoin(...$arrays): array
    {
        $results = [[]];

        foreach ($arrays as $index => $array) {
            $append = [];

            foreach ($results as $product) {
                foreach ($array as $item) {
                    $product[$index] = $item;

                    $append[] = $product;
                }
            }

            $results = $append;
        }

        return $results;
    }

    /**
     * Divide an array into two arrays. One with keys and the other with values.
     *
     * @param array $array
     *
     * @return array
     */
    public static function divide(array $array = []): array
    {
        return [self::keys($array), self::values($array)];
    }

    /**
     * Extracts a slice of the array.
     *
     * Returns the sequence of elements from the array $array as specified by the offset and length parameters.
     *
     * @param array $array
     * @param int|null $offset
     * @param int|null $length
     * @param bool $preserveKeys
     * @return mixed
     */
    public static function slice(
        array $array = [],
        int $offset = null,
        int $length = null,
        bool $preserveKeys = false
    ) {
        return array_slice($array, $offset, $length, $preserveKeys);
    }

    /**
     * Shifts an element off the beginning of array.
     *
     * Shifts the first value of the array off and returns it, shortening the array by one element and moving everything
     * down. All numerical array keys will be modified to start counting from zero while literal keys won't be touched.
     *
     * @param array $array
     *
     * @return mixed
     */
    public static function shift(array &$array = [])
    {
        return array_shift($array);
    }

    /**
     * Prepend one or more elements to the beginning of an array.
     *
     * prepends passed elements to the front of the array. Note that the list of elements is prepended as a whole,
     * so that the prepended elements stay in the same order. All numerical array keys will be modified to start
     * counting from zero while literal keys won't be changed.
     *
     * @param array $array
     * @param mixed ...$arguments
     * @return array|int
     */
    public static function unshift(array &$array = [], ...$arguments)
    {
        return array_unshift($array, ...$arguments);
    }

    /**
     * Flatten a multi-dimensional associative array with dots.
     *
     * @param array  $array
     * @param string $prepend
     *
     * @return array
     */
    public static function dot(array $array = [], string $prepend = EMPTY_STRING): ?array
    {
        $results = [];

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $results = array_merge($results, self::dot($value, $prepend . $key . SINGLE_DOT));
            } else {
                if (is_int($key)) {
                    $results[rtrim($prepend, SINGLE_DOT)][] = $value;
                } else {
                    $results[$prepend . $key] = $value;
                }
            }
        }

        return $results;
    }

    /**
     * Copy all properties from $source to $destination.
     *
     * @param array|null $source
     * @param array|null $destination
     *
     * @return array
     */
    public static function extend(array $source = null, array $destination = null): array
    {
        foreach ($source as $key => $value) {
            $destination[$key] = $value;
        }

        return $destination;
    }

    /**
     * Get all of the given array except for a specified array of keys.
     *
     * @param array        $array
     * @param array|string $keys
     *
     * @return array
     */
    public static function except(array $array = [], $keys = null): ?array
    {
        self::forget($array, $keys);

        return $array;
    }

    /**
     * Determine if the given key exists in the provided array.
     *
     * @param ArrayAccess|array $array
     * @param string|int         $key
     *
     * @return bool
     */
    public static function exists($array = null, $key = null): bool
    {
        if ($array instanceof ArrayAccess) {
            return $array->offsetExists($key);
        }

        return array_key_exists($key, $array);
    }

    /**
     * Return the first element in an array passing a given truth test.
     *
     * @param array         $array
     * @param Closure|null $callback
     * @param mixed         $default
     *
     * @return mixed
     */
    public static function first(array $array = [], Closure $callback = null, $default = null)
    {
        if (is_null($callback)) {
            if (empty($array)) {
                return Lang::checkValue($default);
            }

            foreach ($array as $item) {
                return $item;
            }
        }

        foreach ($array as $key => $value) {
            if (call_user_func($callback, $value, $key)) {
                return $value;
            }
        }

        return Lang::checkValue($default);
    }

    /**
     * Return the last element in an array passing a given truth test.
     *
     * @param array         $array
     * @param Closure|null $callback
     * @param mixed         $default
     *
     * @return mixed
     */
    public static function last(array $array = [], Closure $callback = null, $default = null)
    {
        if (is_null($callback)) {
            return empty($array) ? Lang::checkValue($default) : end($array);
        }

        return self::first(array_reverse($array, true), $callback, $default);
    }

    /**
     * Exclude the last n elements.
     *
     * @param array   $elements
     * @param integer $amount
     *
     * @return array
     */
    public static function initial(array $elements = [], int $amount = 1): array
    {
        return array_slice($elements, 0, count($elements) - $amount);
    }

    /**
     * Get the last elements from index $from.
     *
     * @param array   $elements
     * @param integer $from
     *
     * @return array
     */
    public static function rest(array $elements = [], int $from = 1): array
    {
        return array_slice($elements, $from);
    }

    /**
     * Remove falsy values.
     *
     * @param array $elements
     *
     * @return array
     */
    public static function pack(array $elements = []): array
    {
        return array_values(array_filter($elements));
    }

    /**
     * Returns the first key in an array.
     *
     * @param  array $array
     * @return int|string
     */
    public static function firstKey(array $array = [])
    {
        reset($array);
        return key($array);
    }

    /**
     * Returns the last key in an array.
     *
     * @param  array $array
     * @return int|string
     */
    public static function lastKey(array $array = [])
    {
        end($array);
        return key($array);
    }

    /**
     * Flatten a multi-dimensional array into a one dimensional array.
     *
     * @param  array   $array        The array to flatten
     * @param  boolean $preserveKeys Whether or not to preserve array keys. Keys from deeply nested arrays will
     *                               overwrite keys from shallow nested arrays
     * @return array
     */
    public static function flatten(array $array = [], bool $preserveKeys = false): array
    {
        $retVal = [];

        $callback = $preserveKeys ?
            function ($value, $key) use (&$retVal) {
                $retVal[$key] = $value;
            }
        : function ($value) use (&$retVal) {
            $retVal[] = $value;
        };

        array_walk_recursive($array, $callback);

        return $retVal;
    }

    /**
     * Flattens an array.
     *
     * @param array|null $array $array
     * @param string|null $path
     * @return array
     */
    public static function flat(?array $array = null, ?string $path = EMPTY_STRING): array
    {
        $retVal = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $retVal = array_merge($retVal, self::flat($value, $path . $key . SINGLE_DOT));
            } else {
                $retVal[$path . $key] = $value;
            }
        }

        return $retVal;
    }

    /**
     * Flattens an array.
     *
     * @param array|null $array $array
     * @param string|null $path
     * @return array
     */
    public static function flattenAsDotted(?array $array = null, ?string $path = EMPTY_STRING): array
    {
        $retVal = [];

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $retVal = array_merge($retVal, self::flattenAsDotted($value, $path . $key . SINGLE_DOT));
            } else {
                $retVal[$path . $key] = $value;
            }
        }

        return $retVal;
    }

    /**
     * Flatten an array with the given character as a key delimiter
     *
     * @param array|null $array
     * @param string|null $delimiter
     * @param string|null $prepend
     *
     * @return array
     */
    public static function flattenWith(
        ?array $array = null,
        ?string $delimiter = SINGLE_DOT,
        ?string $prepend = EMPTY_STRING
    ): array {
        $retVal = [];

        // if (\is_null($array)) {
        //     $array = [];
        // }

        foreach ($array as $key => $value) {
            if (is_array($value) && !empty($value)) {
                $retVal = array_merge(
                    $retVal,
                    self::flattenWith($array, $delimiter, $prepend . $key . $delimiter)
                );
            } else {
                $retVal[$prepend . $key] = $value;
            }
        }

        return $retVal;
    }

    /**
     * Compute the difference between the two.
     *
     * @param array $one
     * @param array $another
     *
     * @return array
     */
    public static function difference(array $one = [], array $another = []): array
    {
        return array_values(array_diff($one, $another));
    }

    /**
     * Create an array containing a range of elements.
     *
     * @param int|null $base
     * @param int|null $stop
     * @param integer $step
     *
     * @return array
     */
    public static function range(int $base = null, int $stop = null, int $step = 1): array
    {
        // Dynamic arguments
        if (!is_null($stop)) {
            $start = $base;
        } else {
            $start = 1;
            $stop = $base;
        }

        return range($start, $stop, $step);
    }

    /**
     * Fill an array with $times times some $data.
     *
     * @param mixed $data
     * @param int   $times
     *
     * @return array
     */
    public static function repeat($data = null, int $times = 1): array
    {
        $times = abs($times);

        if (0 === $times) {
            return [];
        }

        return array_fill(0, $times, $data);
    }

    /**
     * Check if all items in an array match a truth test.
     *
     * @param null $array $array
     * @param Closure|null $closure
     *
     * @return bool
     */
    public static function matches($array = null, Closure $closure = null): bool
    {
        // Reduce the array to only booleans
        $array = (array) static::forEach($array, $closure);

        // Check the results
        if (0 === count($array)) {
            return true;
        }

        $array = array_search(false, $array, false);

        return is_bool($array);
    }

    /**
     * Check if any item in an array matches a truth test.
     *
     * @param null $array $array
     * @param Closure|null $closure
     *
     * @return bool
     */
    public static function matchesAny($array = null, Closure $closure = null): bool
    {
        // Reduce the array to only booleans
        $array = (array) static::forEach($array, $closure);

        // Check the results
        if (0 === count($array)) {
            return true;
        }

        $array = array_search(true, $array, false);

        return is_int($array);
    }

    /**
     * Check if an item is in an array.
     * @param null $array
     * @param null $value
     * @return bool
     */
    public static function contains($array = null, $value = null): bool
    {
        return in_array($value, $array, true);
    }

    /**
     * Returns the average value of an array.
     *
     * @param null $array $array    The source array
     * @param int $decimals The number of decimals to return
     *
     * @return float|null The average value
     */
    public static function average($array = null, int $decimals = 0): ?float
    {
        return round((array_sum($array) / count($array)), $decimals);
    }

    /**
     * Remove one or many array items from a given array using "dot" notation.
     *
     * @param array|null $array $array
     * @param null $keys
     */
    public static function forget(?array &$array = [], $keys = null): void
    {
        $original = &$array;

        $keys = (array) $keys;

        if (0 === count($keys)) {
            return;
        }

        foreach ($keys as $key) {
            // if the exact key exists in the top-level, remove it
            if (self::exists($array, $key)) {
                unset($array[$key]);
                continue;
            }

            $parts = explode(SINGLE_DOT, $key);

            // clean up before each pass
            $array = &$original;

            while (count($parts) > 1) {
                $part = array_shift($parts);

                if (isset($array[$part]) && is_array($array[$part])) {
                    $array = &$array[$part];
                } else {
                    continue 2;
                }
            }

            unset($array[array_shift($parts)]);
        }
    }

    /**
     * Get an item from an array using "dot" notation.
     *
     * @param array|null $array $array
     * @param null $key
     * @param mixed $default
     *
     * @return mixed
     */
    public static function get(?array $array = null, $key = null, $default = null): ?array
    {
        if (self::isNotAccessible($array)) {
            return Lang::checkValue($default);
        }

        if (Lang::isNull($key)) {
            return $array;
        }

        if (self::exists($array, $key)) {
            return $array[$key];
        }

        if (false === Str::position($key, SINGLE_DOT)) {
            return $array[$key] ?? Lang::checkValue($default);
        }

        if (Lang::isArray($key)) {
            $return = [];

            foreach ($key as $k) {
                $return[$k] = self::get($array, $k, $default);
            }

            return $return;
        }

        $segments = explode(SINGLE_DOT, $key);

        foreach ($segments as $segment) {
            if (self::isAccessible($array) && self::exists($array, $segment)) {
                $array = $array[$segment];
            } else {
                return Lang::checkValue($default);
            }
        }

        return $array;
    }

    /**
     * Check if an item or items exist in an array using "dot" notation.
     *
     * @param ArrayAccess|array $array
     * @param string|array       $keys
     *
     * @return bool
     */
    public static function has($array = null, $keys = null): bool
    {
        if (is_null($keys)) {
            return false;
        }

        $keys = (array) $keys;

        if (!$array) {
            return false;
        }

        if ($keys === []) {
            return false;
        }

        foreach ($keys as $key) {
            $subKeyArray = $array;

            if (self::exists($array, $key)) {
                continue;
            }

            foreach (explode(SINGLE_DOT, $key) as $segment) {
                if (self::isAccessible($subKeyArray) && self::exists($subKeyArray, $segment)) {
                    $subKeyArray = $subKeyArray[$segment];
                } else {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Check if an item or items exist in an array.
     *
     * @param ArrayAccess|array $array
     * @param string|array       $items
     *
     * @return bool
     */
    public static function hasItem($array = null, $items = null): bool
    {
        if (is_null($items)) {
            return false;
        }

        $items = (array) $items;

        if (!$array) {
            return false;
        }

        if ($items === []) {
            return false;
        }

        foreach ($items as $item) {
            // $subKeyArray = $array;
            if (self::exists($array, $item)) {
                continue;
            } else {
                return false;
            }
        }

        return true;
    }

    /**
     * Determines if the given array is an associative array.
     *
     * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
     *
     * @param array $array
     *
     * @return bool
     */
    public static function isAssoc(array $array): bool
    {
        // $keys = array_keys($array);

        // return array_keys($keys) !== $keys;

        return array_keys($array) !== range(0, count($array) - 1);

        // $keys = array_keys($array);

        // foreach ($keys as $key) {
        //     if (!\is_int($key)) {
        //         return true;
        //     }
        // }

        // return false;
    }

    public static function isAssociative(array $array): bool
    {
        return self::isAssoc($array);
    }

    /**
     * Determines if the given array is not an associative array.
     *
     * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
     *
     * @param array $array
     *
     * @return bool
     */
    public static function isNotAssoc(array $array): bool
    {
        return !static::isAssoc($array);
    }

    /**
     * Determines if the given array is not an associative array.
     *
     * An array is "associative" if it doesn't have sequential numerical keys beginning with zero.
     *
     * @param array $array
     *
     * @return bool
     */
    public static function isNotAssociative(array $array): bool
    {
        return !static::isAssociative($array);
    }

    /**
     * Add cell to the start of assoc array
     *
     * @param array  $array
     * @param string $key
     * @param mixed  $value
     *
     * @return array
     */
    public static function unshiftAssoc(array &$array = [], $key = null, $value = null): array
    {
        $array = array_reverse($array, true);
        $array[$key] = $value;
        $array = array_reverse($array, true);

        return $array;
    }

    /**
     * Get a subset of the items from the given array.
     *
     * @param array        $array
     * @param array|string $keys
     *
     * @return array
     */
    public static function only(array $array = [], $keys = null): ?array
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }

    /**
     * Pluck an array of values from an array.
     *
     * @param array             $array
     * @param string|array      $value
     * @param string|array|null $key
     *
     * @return array
     */
    public static function pluck(array $array = [], $value = null, $key = null): ?array
    {
        $results = [];

        list($value, $key) = self::explodePluckParameters($value, $key);

        foreach ($array as $item) {
            $itemValue = Lang::arrayOrObjectGet($item, $value);

            // If the key is "null", we will just append the value to the array and keep
            // looping. Otherwise we will key the array using the value of the key we
            // received from the developer. Then we'll return the final array form.
            if (is_null($key)) {
                $results[] = $itemValue;
            } else {
                $itemKey = Lang::arrayOrObjectGet($item, $key);

                if (is_object($itemKey) && method_exists($itemKey, '__toString')) {
                    $itemKey = (string) $itemKey;
                }

                $results[$itemKey] = $itemValue;
            }
        }

        return $results;
    }

    /**
     * Explode the "value" and "key" arguments passed to "pluck".
     *
     * @param string|array      $value
     * @param string|array|null $key
     *
     * @return array
     */
    protected static function explodePluckParameters($value = null, $key = null): ?array
    {
        $value = is_string($value) ? explode(SINGLE_DOT, $value) : $value;

        $key = is_null($key) || is_array($key) ? $key : explode(SINGLE_DOT, $key);

        return [$value, $key];
    }

    /**
     * Get one field from array of arrays (array of objects)
     *
     * @param  array  $arrayList
     * @param  string $fieldName
     * @return array
     */
    public static function getField(array $arrayList = [], string $fieldName = 'id'): array
    {
        $retVal = [];

        if (!empty($arrayList) && is_array($arrayList)) {
            foreach ($arrayList as $option) {
                if (is_array($option)) {
                    $retVal[] = $option[$fieldName];
                } elseif (is_object($option)) {
                    if (isset($option->{$fieldName})) {
                        $retVal[] = $option->{$fieldName};
                    }
                }
            }
        }

        return $retVal;
    }

    /**
     * Group array by key
     *
     * @param  array  $arrayList
     * @param  string $key
     * @return array
     */
    public static function groupByKey(array $arrayList = [], string $key = 'id'): array
    {
        $retVal = [];

        foreach ($arrayList as $item) {
            if (is_object($item)) {
                if (isset($item->{$key})) {
                    $retVal[$item->{$key}][] = $item;
                }
            } elseif (is_array($item)) {
                if (self::key($key, $item)) {
                    $retVal[$item[$key]][] = $item;
                }
            }
        }

        return $retVal;
    }

    /**
     * Get the index of the given key inside the given array.
     *
     * @param array $array
     * @param null $key
     *
     * @return mixed
     */
    public static function index(array $array = [], $key = null)
    {
        return array_search($key, $array);
    }

    /**
     * Map the given associated array with the given `$callback`.
     *
     * @param array $array
     * @param Closure|callable|null $callback
     *
     * @return array
     */
    public static function mapAssoc(
        array $array = [],
        callable $callback = null
    ): array {
        return array_merge(...
            array_map(
                $callback,
                array_keys($array),
                $array
            )
        );
    }

    /**
     * Recursive array mapping
     *
     * @param array             $array
     * @param Closure|callable $function
     *
     * @return array
     */
    public static function map(array $array = [], $function = null): array
    {
        $retVal = [];

        if (static::isAssociative($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $retVal[$key] = self::map($value, $function);
                } else {
                    $retVal[$key] = $function($key, $value);
                }
            }
        } else {
            foreach ($array as $k => $item) {
                if (is_array($item)) {
                    $retVal[] = self::map($item, $function);
                } else {
                    $retVal[] = $function($k, $item);
                }
            }
        }

        // foreach ($array as $key => $value) {
        //     if (\is_array($value)) {
        //         $retVal[$key] = self::map($value, $function);
        //     } else {
        //         $retVal[$key] = $function($key, $value);
        //     }
        // }

        // $array = $retVal;

        return $retVal;
    }

    /**
     * Recursive array filtering
     *
     * @param array|null $array $array
     * @param mixed $callback
     * @param int|null $flag
     *
     * @return array
     */
    public static function filter(
        ?array $array = [],
        $callback = null,
        ?int $flag = Arr::FILTER_USE_BOTH
    ): array {
        // $retVal = [];

        // foreach ($array as $key => $value) {
        //     if (\is_array($value)) {
        //         $retVal[$key] = self::filter($value, $callback);
        //     } else {
        //         $retVal[$key] = $callback($key, $value);
        //     }
        // }

        // return $retVal;

        if (version_compare(PHP_VERSION, '5.6.0') >= 0) {
            return array_filter($array, $callback, $flag);
        }

        $filtered = [];

        foreach ($array as $key => $value) {
            $args = [$value];

            if ($flag === static::FILTER_USE_BOTH) {
                $args = [$value, $key];
            }

            if ($flag === static::FILTER_USE_KEY) {
                $args = [$key];
            }

            if (call_user_func_array($callback, $args)) {
                $filtered[$key] = $value;
            }
        }

        return $filtered;
    }

    /**
     * Push an item onto the beginning of an array.
     *
     * @param array $array
     * @param mixed $value
     * @param mixed $key
     *
     * @return array
     */
    public static function prepend(array $array = [], $value = null, $key = null): ?array
    {
        if (is_null($key)) {
            array_unshift($array, $value);
        } else {
            $array = [$key => $value] + $array;
        }

        return $array;
    }

    /**
     * Get a value from the array, and remove it.
     *
     * @param array|null $array $array
     * @param string|null $key
     * @param mixed $default
     *
     * @return mixed
     */
    public static function pull(?array &$array = [], string $key = null, $default = null): ?array
    {
        $value = self::get($array, $key, $default);
        self::forget($array, $key);
        return $value;
    }

    /**
     * Get one or a specified number of random values from an array.
     *
     * @param array    $array
     * @param int|null $number
     *
     * @return mixed
     *
     * @throws InvalidArgumentException
     */
    public static function random(array $array = [], int $number = null): array
    {
        $requested = is_null($number) ? 1 : $number;

        $count = count($array);

        if ($requested > $count) {
            throw new InvalidArgumentException(
                "You requested {$requested} items, but there are only {$count} items available."
            );
        }

        if (is_null($number)) {
            return $array[array_rand($array)];
        }

        if (0 === (int) $number) {
            return [];
        }

        $keys = array_rand($array, $number);

        $results = [];

        foreach ((array) $keys as $key) {
            $results[] = $array[$key];
        }

        return $results;
    }

    /**
     * Set an array item to a given value using "dot" notation.
     *
     * If no key is given to the method, the entire array will be replaced.
     *
     * @param array $array
     * @param string|null $key
     * @param mixed $value
     *
     * @return array
     */
    public static function set(array &$array = [], string $key = null, $value = null): array
    {
        if (Lang::isNull($key)) {
            return $array = $value;
        }

        $parts = explode(SINGLE_DOT, $key);

        while (count($parts) > 1) {
            $key = self::shift($parts);

            // If the key doesn't exist at this depth, we will just create an empty array
            // to hold the next value, allowing us to create the arrays to hold final
            // values at the correct depth. Then we'll keep digging into the array.
            if (!Lang::isSet($array, $key) or !Lang::isArray($array[$key])) {
                $array[$key] = [];
            }

            $array = &$array[$key];
        }

        $array[self::shift($parts)] = $value;

        return $array;
    }

    /**
     * Shuffle the given array and return the result.
     *
     * @param array    $array
     * @param int|null $seed
     *
     * @return array
     */
    public static function shuffle(array $array = [], int $seed = null): ?array
    {
        if (is_null($seed)) {
            shuffle($array);
        } else {
            srand($seed);

            usort(
                $array,
                function () {
                    return rand(-1, 1);
                }
            );
        }

        return $array;
    }

    /**
     * Sort the array using the given callback or "dot" notation.
     *
     * @param array                $array
     * @param Closure|string|null $callback
     *
     * @return array
     */
    public static function sort(array $array = [], $callback = null): ?array
    {
        return ExenCollection::make($array)->sortBy($callback)->all();
    }

    /**
     * Sort the given array of arrays/objects using paths.
     *
     * e.g.
     *
     *      $data = array(
     *          array('foobar' => 'b'),
     *          array('foobar' => 'a'),
     *      );
     *
     *      Arr::multiSort($data, '[foobar]', 'asc');
     *
     *      echo $data[0]; // "a"
     *
     * You can also use method names:
     *
     *      Arr::multiSort($data, 'getFoobar', 'asc');
     *
     * Or sort on multidimensional arrays:
     *
     *      Arr::multiSort($data, 'foobar.bar.getFoobar', 'asc');
     *
     * And you can sort on multiple paths:
     *
     *      Arr::multiSort($data, array('foo', 'bar'), 'asc');
     *
     * The path is any path accepted by the property access component:
     *
     * @see http://symfony.com/doc/current/components/property_access/introduction.html
     *
     * @param array $values
     * @param null $paths
     * @param string $direction Direction to sort in (either ASC or DESC)
     *
     * @return array
     */
    public static function multiSort(array $values = [], $paths = null, string $direction = 'ASC'): array
    {
        $accessor = PropertyAccess::createPropertyAccessor();

        $values = (array) $values;
        $paths = (array) $paths;

        usort(
            $values,
            function ($a, $b) use ($accessor, $paths) {
                $retVal = null;

                foreach ($paths as $i => $path) {
                    $aOrder = $accessor->getValue($a, $path);
                    $bOrder = $accessor->getValue($b, $path);

                    if (is_string($aOrder)) {
                        $aOrder = strtolower($aOrder);
                        $bOrder = strtolower($bOrder);
                    }

                    if ($aOrder == $bOrder) {
                        if (count($paths) == ($i + 1)) {
                            /** @noinspection PhpUnusedLocalVariableInspection */
                            $retVal = 0;
                        } else {
                            continue;
                        }
                    }

                    $retVal = ($aOrder < $bOrder) ? -1 : 1;
                }

                return $retVal;
            }
        );

        if ('DESC' == strtoupper($direction)) {
            $values = array_reverse($values);
        }

        return $values;
    }

    /**
     * Recursively sort an array by keys and values.
     *
     * @param array $array
     *
     * @return array
     */
    public static function sortRecursive(array $array = []): ?array
    {
        foreach ($array as &$value) {
            if (is_array($value)) {
                $value = self::sortRecursive($value);
            }
        }

        if (self::isAssociative($array)) {
            @ksort($array);
        } else {
            @sort($array);
        }

        return $array;
    }

    /**
     * Recursively sort an array by keys.
     *
     * @param array|null $array $array
     * @param array|null $keys
     *
     * @return array
     */
    public static function sortByKeys(?array $array = [], ?array $keys = []): array
    {
        // if (!self::isAssociative($keys)) {
        //     foreach ($array as &$value) {
        //         if (\is_array($value)) {
        //             $value = self::sortByKeys($value, $keys);
        //         }
        //     }

        //     if (self::has($array, $keys)) {
        //         $array = \array_merge(\array_flip($keys), $array);
        //         // $array = \array_replace(\array_flip($keys), $array);
        //     }
        // }

        // return $array;

        $commonKeysInOrder = array_intersect_key(array_flip($keys), $array);
        $commonKeysWithValue = array_intersect_key($array, $commonKeysInOrder);

        return array_merge($commonKeysInOrder, $commonKeysWithValue);
    }

    /**
     * Sort an array by keys based on another array
     *
     * @param array|null $array $array
     * @param array|null $orderArray
     * @return array
     */
    public static function sortByArray(?array $array = [], ?array $orderArray = []): array
    {
        // return \array_merge(\array_flip($orderArray), $array);

        // if (self::hasItem($array, $orderArray)) {
        //     // return \array_merge(\array_flip($orderArray), $array);
        //     return \array_replace(\array_flip($orderArray), $array);
        // }

        // return \array_replace(\array_flip($orderArray), $array);

        $ordered = [];

        foreach ($orderArray as $key) {
            if (array_key_exists($key, $array)) {
                $ordered[$key] = $array[$key];
                unset($array[$key]);
            }
        }

        return $ordered + $array;
    }

    /**
     * Sort array by the given key list.
     *
     * @param array|null $array $array
     * @param array|null $keyList
     * @return bool|array
     */
    public static function sortByKeyList(?array $array = [], ?array $keyList = [])
    {
        $ordered = [];

        if (empty($array) || empty($keyList)) {
            return false;
        }

        foreach ($keyList as $key) {
            $ordered[$key] = $array[$key];
        }

        return $ordered;
    }

    public static function moveDown(
        array $array = [],
        int $position = 0
    ): ?array {
        if (count($array) - 1 > $position) {
            $retVal = array_slice($array, 0, $position, true);
            $retVal[] = $array[$position + 1];
            $retVal[] = $array[$position];
            $retVal += array_slice($array, $position + 2, count($array), true);

            return ($retVal);
        } else {
            return $array;
        }
    }

    public static function moveUp(
        array $array = [],
        int $position = 0
    ): ?array {
        if ($position > 0 and $position < count($array)) {
            $retVal = array_slice($array, 0, ($position - 1), true);
            $retVal[] = $array[$position];
            $retVal[] = $array[$position - 1];
            $retVal += array_slice($array, ($position + 1), count($array), true);

            return ($retVal);
        } else {
            return $array;
        }
    }

    /**
     * Filter the array using the given callback.
     *
     * @param array $array
     * @param callable|null $callback
     *
     * @return array
     */
    public static function where(array $array = [], callable $callback = null): ?array
    {
        return array_filter($array, $callback, static::FILTER_USE_BOTH);
    }

    /**
     * If the given value is not an array and not null, wrap it in one.
     *
     * @param mixed $value
     *
     * @return array
     */
    public static function wrap($value = null): ?array
    {
        if (is_null($value)) {
            return [];
        }

        if (is_array($value) && !self::isAssociative($value)) {
            return $value;
        }

        return [$value];
    }

    /**
     *
     * @param string|null $glue
     * @param array $array
     * @return string
     */
    public static function implode(string $glue = null, array $array = []): string
    {
        $retVal = "";

        foreach ($array as $item) {
            if (is_array($item)) {
                $retVal .= self::implode($glue, $item) . $glue;
            } else {
                $retVal .= $item . $glue;
            }
        }

        if ($glue) {
            $retVal = Str::sub($retVal, 0, 0 - Str::length($glue));
        }

        return $retVal;
    }

    /**
     *
     * @param string|null $glue
     * @param array $array
     * @return string
     */
    public static function join(string $glue = null, array $array = []): string
    {
        $retVal = EMPTY_STRING;

        foreach ($array as $item) {
            if (is_array($item)) {
                $retVal .= self::implode($glue, $item) . $glue;
            } else {
                $retVal .= $item . $glue;
            }
        }

        if ($glue) {
            $retVal = Str::sub($retVal, 0, 0 - Str::length($glue));
        }

        return $retVal;
    }

    /**
     * Fix value types of the given array.
     *
     * @param array $array The Array to fix
     *
     * @return array
     *
     * @since              0.2.4
     * @codeCoverageIgnore
     */
    public static function fixValues(array &$array = []): ?array
    {
        foreach ($array as $key => $value) {
            // Numerical fix
            // Double fix
            // Float fix
            // Empty fix
            // String fix
            // Boolean and semi boolean fix

            if (preg_match('/^[1-9][0-9]*$/iD', $value)) {
                $array[$key] = intval($value);
            } elseif (preg_match('/^[0-9]*[.][0-9]*$/iD', $value)) {
                $array[$key] = doubleval($value);
            } elseif (preg_match('/^[0-9]*[.][0-9-]*$/iD', $value)) {
                $array[$key] = floatval($value);
            } elseif (empty($value)) {
                $array[$key] = null;
            } elseif (is_string($value)) {
                $array[$key] = $value;
            } elseif (preg_match('/^true|false|TRUE|FALSE|on|off|ON|OFF*$/iD', $value)) {
                $array[$key] = boolval($value);
            } else {
                $array[$key] = $value;
            }
        }

        return $array;
    }

    /**
     * Trim array elements.
     *
     * @param array $array Configuration items
     *
     * @return mixed
     *
     * @since              0.2.4
     * @codeCoverageIgnore
     */
    public static function trimElements(array &$array = []): ?array
    {
        $callback = function ($element) {
            return trim($element);
        };

        $array = array_map($callback, $array);

        return $array;
    }

    /**
     * Recursively converts the given object into an array.
     *
     * @param Object $object Object to convert
     *
     * @return array
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function objectToArray($object = null)
    {
        $retVal = null;

        if (is_object($object)) {
            $data = @get_object_vars($object);
            $retVal = array_map([__CLASS__, 'objectToArray'], $data);
        } elseif (is_array($object)) {
            $retVal = array_map([__CLASS__, 'objectToArray'], $object);
        } else {
            $retVal = $object;
        }

        return $retVal;
    }

    /**
     * Recursively converts the given array into an object.
     *
     * @param array|Object $array Array to convert
     *
     * @return Object|null
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function arrayToObject($array = null)
    {
        $retVal = null;

        if (is_array($array)) {
            $retVal = new stdClass();

            foreach ($array as $key => &$value) {
                if (is_array($value)) {
                    if (self::isAssociative($value)) {
                        $value = self::arrayToObject($value);
                        // $value = \array_map([__CLASS__, 'arrayToObject'], $value);
                    }
                }

                $retVal->$key = $value;
            }
        } else {
            $retVal = $array;
        }

        // $retVal = new \stdClass();

        // foreach ($array as $key => &$value) {
        //     if (\is_array($value)) {
        //         if (self::isAssociative($value)) {
        //             $value = self::arrayToObject($value);
        //         } else {
        //             $value = $value;
        //         }
        //     } else {
        //         $value = $value;
        //     }

        //     $retVal->$key = $value;
        // }

        return $retVal;
    }

    public static function toArray($value = null)
    {
        return self::objectToArray($value);
    }

    public static function toObject($value = null): ?object
    {
        return self::arrayToObject($value);
    }

    public static function find(array $array = [], $element = null, bool $strict = true): ?array
    {
        $retVal = [];

        if (array_key_exists($element, $array)) {
            $retVal[] = [
                $element => $array[$element],
            ];
        } elseif (!array_key_exists($element, $array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    $retVal = self::search($value, $element, $strict);
                }
            }
        }

        return $retVal;
    }

    /**
     * Searches for a given value in an array of arrays, objects and scalar values. You can optionally specify
     * a field of the nested arrays and objects to search in.
     *
     * @param array $array  The array to search
     * @param mixed $search The value to search for
     * @param bool  $field  The field to search in, if not specified all fields will be searched
     *
     * @return boolean|mixed  False on failure or the array key on success
     */
    public static function search(array $array = [], $search = null, bool $field = false)
    {
        // *grumbles* stupid PHP type system
        $search = (string) $search;

        foreach ($array as $key => $elem) {
            // *grumbles* stupid PHP type system
            $key = (string) $key;

            if ($field) {
                if (is_object($elem) && $elem->{$field} === $search) {
                    return $key;
                }

                if (is_array($elem) && $elem[$field] === $search) {
                    return $key;
                }

                if (is_scalar($elem) && $elem === $search) {
                    return $key;
                }
            } else {
                if (is_object($elem)) {
                    $elem = (array) $elem;
                    /**
                     * @noinspection NotOptimalIfConditionsInspection
                     */
                    if (in_array($search, $elem, false)) {
                        return $key;
                    }
                } elseif (is_array($elem) && in_array($search, $elem, false)) {
                    return $key;
                } elseif (is_scalar($elem) && $elem === $search) {
                    return $key;
                }
            }
        }

        return false;
    }

    public static function searchRecursively(array $array = null, $element = null, bool $strict = true): bool
    {
        foreach ($array as $value) {
            if (
                ($strict ? $value === $element : $value == $element) ||
                (is_array($value) && self::searchRecursively($element, $value, $strict))
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns an array containing all the elements of array after applying
     * the callback function to each one.
     *
     * @param array $array An array to run through the callback function
     * @param Closure|null $callback Callback function to run for each element in each array
     * @param boolean $onNoScalar Whether or not to call the callback function on non scalar values
     *                             (Objects, resources, etc)
     * @return array
     */
    public static function mapDeep(array $array = [], Closure $callback = null, bool $onNoScalar = false): array
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $args = [$value, $callback, $onNoScalar];
                $array[$key] = call_user_func_array([__CLASS__, __FUNCTION__], $args);
            } elseif (is_scalar($value) || $onNoScalar) {
                $array[$key] = $callback($value);
            }
        }

        return $array;
    }

    /**
     * Clean array by custom rule
     *
     * @param  array $haystack
     * @return array
     */
    public static function clean(array $haystack = []): array
    {
        return array_filter($haystack);
    }

    /**
     * Clean array before serialize to JSON
     *
     * @param  array $array
     * @return array
     */
    public static function cleanBeforeJson(array $array = []): array
    {
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $array[$key] = self::cleanBeforeJson($array[$key]);
            }

            if ($array[$key] === EMPTY_STRING || null === $array[$key]) {
                unset($array[$key]);
            }
        }

        return $array;
    }

    /**
     * Deletes element from array by the element's name.
     *
     * @param array|null  $array   array to delete element from
     * @param string|null $element element name to check for
     *
     * @return array returns the array after element deletion
     */
    public static function remove(array &$array = null, $element = null): array
    {
        $key = array_search($element, $array);

        if (false !== $key) {
            unset($array[$key]);
        } else {
            $key = self::search($array, $element);

            if (count($key) >= 1) {
                foreach ($key as $k => $v) {
                    unset($array[$k]);
                }
            }
        }

        return $array;
    }

    /**
     * Removes a value from array by key.
     *
     * @param array|null $array $array    Array to delete element from
     * @param null $elements Element to check for
     * @return array                  Returns the array after element deletion
     */
    public static function rem(array &$array = null, $elements = null): ?array
    {
        $original = &$array;

        foreach ((array) $elements as $element) {
            $parts = explode(SINGLE_DOT, $element);

            while (count($parts) > 1) {
                $part = self::shift($parts);

                if (Lang::isSet($array, $part) && Lang::isArray($array[$part])) {
                    $array = &$array[$part];
                }
            }

            unset($array[self::shift($parts)]);

            $array = &$original;
        }

        return $array;
    }

    /**
     * Expands an array.
     *
     * @param  array $array
     * @return array
     */
    public static function expand(array $array): array
    {
        $retVal = [];

        foreach ($array as $key => $value) {
            $values = &$retVal;
            $keys = explode(SINGLE_DOT, $key);

            while (count($keys) > 1) {
                $key = array_shift($keys);

                if (!isset($values[$key]) || !is_array($values[$key])) {
                    $values[$key] = [];
                }

                $values = &$values[$key];
            }

            $values[array_shift($keys)] = $value;
        }

        return $retVal;
    }

    /**
     * Extracts values by keys.
     *
     * @param array $data
     * @param array|null $keys
     * @param bool $include
     * @return array
     */
    public static function extract(array $data, array $keys = null, $include = true): array
    {
        if (!$keys) {
            return $data;
        }

        $data = self::flat($data);

        $retVal = [];

        foreach ($data as $keyPath => $value) {
            $add = !$include;

            foreach ($keys as $key) {
                if (0 === strpos($keyPath, $key)) {
                    $add = $include;
                    break;
                }
            }

            if ($add) {
                $retVal[$keyPath] = $value;
            }
        }

        return self::expand($retVal);
    }

    /**
     * Returns reference to array item.
     *
     * @param array
     * @param string|int|array one or more keys
     *
     * @return mixed
     *
     * @throws InvalidArgumentException if traversed item is not an array
     */
    public static function &getReference(array &$array = [], $key = null): array
    {
        foreach (is_array($key) ? $key : [$key] as $k) {
            if (is_array($array) || null === $array) {
                $array = &$array[$k];
            } else {
                throw new InvalidArgumentException(
                    'Traversed item is not an array.'
                );
            }
        }

        return $array;
    }

    public static function &getRef(array &$array = [], $key = null): array
    {
        return self::getReference($array, $key);
    }

    /**
     * Searches the array for a given key and returns the offset if successful.
     *
     * @param array $array
     * @param null $key
     * @return int|false offset if it is found, false otherwise
     */
    public static function searchKey(array $array = [], $key = null)
    {
        $foo = [$key => null];

        return array_search(key($foo), array_keys($array), true);
    }

    /**
     * Recursively appends elements of remaining keys from the second array to the first.
     *
     * @param array $arrayOne
     * @param array $arrayTwo
     * @return array
     */
    public static function mergeTree(array $arrayOne = [], array $arrayTwo = []): ?array
    {
        $mergedArray = $arrayOne + $arrayTwo;

        foreach (array_intersect_key($arrayOne, $arrayTwo) as $key => $value) {
            if (is_array($value) && is_array($arrayTwo[$key])) {
                $mergedArray[$key] = self::mergeTree($value, $arrayTwo[$key]);
            }
        }

        return $mergedArray;
    }

    /**
     * Reformat table to associative tree. Path looks like 'field|field[]field->field=field'.
     *
     * @param array $array
     * @param null $path
     * @return array|stdClass
     * @noinspection SpellCheckingInspection
     */
    public static function associate(array $array = [], $path = null)
    {
        $parts = is_array($path)
            ? $path
            // : preg_split('#(\[\]|->|=|\|)#', $path, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
            : preg_split('#(\[]|->|=|\|)#', $path, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        if (!$parts || '=' === $parts[0] || '|' === $parts[0] || $parts === ['->']) {
            throw new InvalidArgumentException("Invalid path '$path'.");
        }

        $retVal = '->' === $parts[0] ? new stdClass() : [];

        foreach ($array as $rowOrig) {
            $row = (array) $rowOrig;
            $x = &$retVal;

            for ($i = 0; $i < count($parts); ++$i) {
                $part = $parts[$i];

                if ('[]' === $part) {
                    $x = &$x[];
                } elseif ('=' === $part) {
                    if (isset($parts[++$i])) {
                        $x = $row[$parts[$i]];
                        $row = null;
                    }
                } elseif ('->' === $part) {
                    if (isset($parts[++$i])) {
                        $x = &$x->{$row[$parts[$i]]};
                    } else {
                        $row = is_object($rowOrig) ? $rowOrig : (object) $row;
                    }
                } elseif ('|' !== $part) {
                    $x = &$x[(string) $row[$part]];
                }
            }

            if (null === $x) {
                $x = $row;
            }
        }

        return $retVal;
    }

    /**
     * Inserts new array before item specified by key.
     * @param array $array
     * @param null $key
     * @param array $inserted
     */
    public static function insertBefore(array &$array = [], $key = null, array $inserted = []): void
    {
        $offset = (int) self::searchKey($array, $key);
        $array = array_slice($array, 0, $offset, true) + $inserted + array_slice($array, $offset, count($array), true);
    }

    /**
     * Inserts new array after item specified by key.
     * @param array $array
     * @param null $key
     * @param array $inserted
     */
    public static function insertAfter(array &$array = [], $key = null, array $inserted = []): void
    {
        $offset = self::searchKey($array, $key);
        $offset = false === $offset ? count($array) : $offset + 1;
        $array = array_slice($array, 0, $offset, true) + $inserted + array_slice($array, $offset, count($array), true);
    }

    /**
     * Renames key in array.
     * @param array $array
     * @param string|null $oldKey
     * @param string|null $newKey
     */
    public static function renameKey(array &$array = [], string $oldKey = null, string $newKey = null): void
    {
        $offset = self::searchKey($array, $oldKey);

        if (false !== $offset) {
            $keys = array_keys($array);
            $keys[$offset] = $newKey;
            $array = array_combine($keys, $array);
        }
    }

    /**
     * Returns array entries that match the pattern.
     *
     * @param array $array
     * @param string|null $pattern
     * @param int $flags
     * @return array
     * @throws Exception\RegexpException
     */
    public static function grep(array $array = [], string $pattern = null, int $flags = 0): ?array
    {
        return Str::pcre('preg_grep', [$pattern, $array, $flags]);
    }

    /**
     * Finds whether a variable is a zero-based integer indexed array.
     *
     * @param null $value
     * @return bool
     */
    public static function isList($value = null): bool
    {
        return is_array($value) && (!$value || array_keys($value) === range(0, count($value) - 1));
    }

    /**
     * Normalizes to associative array.
     *
     * @param array|null $array
     * @param null $filling
     * @return array
     */
    public static function normalize(array $array = null, $filling = null): ?array
    {
        $retVal = [];

        foreach ($array as $k => $v) {
            $retVal[is_int($k) ? $v : $k] = is_int($k) ? $filling : $v;
        }

        return $retVal;
    }

    /**
     * Invoke a function on all of an array's values.
     * @param null $array
     * @param null $callable
     * @param array $arguments
     * @return mixed
     */
    public static function invoke($array = null, $callable = null, $arguments = [])
    {
        // If one argument given for each iteration, create an array for it
        if (!is_array($arguments)) {
            $arguments = static::repeat($arguments, count($array));
        }

        // If the callable has arguments, pass them
        if ($arguments) {
            return array_map($callable, $array, $arguments);
        }

        return array_map($callable, $array);
    }

    /**
     * Get everything but the last $to items.
     * @param null $array
     * @param int $to
     * @return array|mixed
     */
    public static function init($array = null, int $to = 1): array
    {
        $slice = count($array) - $to;

        return static::getFirst($array, $slice);
    }

    /**
     * Get the first value from an array.
     * @param null $array
     * @param null $take
     * @return array|mixed
     */
    public static function getFirst($array = null, $take = null): array
    {
        if (!$take) {
            return array_shift($array);
        }

        return array_splice($array, 0, $take, true);
    }

    /**
     * Get the last value from an array.
     * @param null $array
     * @param null $take
     * @return array|mixed
     */
    public static function getLast($array = null, $take = null): array
    {
        if (!$take) {
            return array_pop($array);
        }

        return static::rest($array, -$take);
    }

    /**
     * Iterate over an array and modify the array's value.
     * @param null $array
     * @param Closure|null $closure
     * @return mixed|null
     */
    public static function forEach($array = null, Closure $closure = null)
    {
        foreach ($array as $key => $value) {
            $array[$key] = $closure($value, $key);
        }

        return $array;
    }

    /**
     * Replace the keys in an array with another set.
     *
     * @param array $array The array
     * @param array $keys  An array of keys matching the array's size
     *
     * @return array
     */
    public static function replaceKeys($array = null, $keys = null): array
    {
        $values = array_values($array);

        return array_combine($keys, $values);
    }

    /**
     * Iterate over an array and execute a callback for each loop.
     * @param null $array
     * @param Closure|null $closure
     * @return mixed|null
     */
    public static function at($array = null, Closure $closure = null)
    {
        foreach ($array as $key => $value) {
            $closure($value, $key);
        }

        return $array;
    }

    /**
     * Replace a value in an array.
     *
     * @param array  $array   The array
     * @param string $replace The string to replace
     * @param string $with    What to replace it with
     *
     * @return array
     */
    public static function replaceValue($array = null, $replace = null, $with = null): ?array
    {
        return static::forEach(
            $array,
            function ($value) use ($replace, $with) {
                return str_replace($replace, $with, $value);
            }
        );
    }

    /**
     * Sort an array by key.
     * @param null $array
     * @param string $direction
     * @return mixed|null
     */
    public static function sortKeys($array = null, string $direction = 'ASC')
    {
        $direction = ('desc' === strtolower($direction)) ? SORT_DESC : SORT_ASC;

        if (SORT_ASC === $direction) {
            ksort($array);
        } else {
            krsort($array);
        }

        return $array;
    }

    /**
     * Return all items that fail the truth test.
     * @param null $array
     * @param Closure|null $closure
     * @return array
     */
    public static function reject($array = null, Closure $closure = null): array
    {
        $filtered = [];

        foreach ($array as $key => $value) {
            if (!$closure($value, $key)) {
                $filtered[$key] = $value;
            }
        }

        return $filtered;
    }

    /**
     * Picks element from the array by key and return its value.
     *
     * @param array
     * @param string|int array key
     * @param mixed
     *
     * @return mixed
     *
     * @throws InvalidArgumentException if item does not exist and default value is not provided
     */
    public static function pick(array &$array = [], $key = null, $default = null)
    {
        if (array_key_exists($key, $array)) {
            $value = $array[$key];
            unset($array[$key]);

            return $value;
        } elseif (func_num_args() < 3) {
            throw new InvalidArgumentException("Missing item '$key'.");
        } else {
            return $default;
        }
    }

    /**
     *
     * @param array $array
     * @return array
     */
    public static function each(array &$array = []): ?array
    {
        $key = key($array);

        $retVal = (null === $key) ? false : [$key, current($array), 'key' => $key, 'value' => current($array)];

        next($array);

        return $retVal;
    }

    /**
     * Tests whether some element in the array passes the callback test.
     *
     * @param array $array
     * @param Closure|null $callback
     * @return bool
     */
    public static function some(array $array = [], Closure $callback = null): bool
    {
        foreach ($array as $k => $v) {
            if ($callback($v, $k, $array)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Tests whether all elements in the array pass the callback test.
     *
     * @param array $array
     * @param Closure|null $callback
     * @return bool
     */
    public static function every(array $array = [], Closure $callback = null): bool
    {
        foreach ($array as $k => $v) {
            if (!$callback($v, $k, $array)) {
                return false;
            }
        }

        return true;
    }

    // /**
    //  * Applies the callback to the elements of the array.
    //  *
    //  * @return array
    //  */
    // public static function map(array $array = [], Closure $callback = null): ?array
    // {
    //     $retVal = [];

    //     foreach ($array as $k => $v) {
    //         $retVal[$k] = $callback($v, $k, $array);
    //     }

    //     return $retVal;
    // }

    /**
     * Recursively merges two arrays.
     *
     * @param  array $array1
     * @param  array $array2
     * @param  bool  $replace
     * @return array
     */
    public static function merger(array $array1, array $array2, bool $replace = false): array
    {
        if ($replace) {
            return array_replace_recursive($array1, $array2);
        }

        foreach ($array2 as $key => $value) {
            if (isset($array1[$key])) {
                if (is_int($key)) {
                    $array1[] = $value;
                } elseif (is_array($value) && is_array($array1[$key])) {
                    $array1[$key] = static::merger($array1[$key], $value);
                } else {
                    $array1[$key] = $value;
                }
            } else {
                $array1[$key] = $value;
            }
        }

        return $array1;
    }

    /**
     * Merge two arrays recursively.
     *
     * @param mixed ...$arguments
     * @return null|array
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function merge(...$arguments): ?array
    {
        $array = $arguments[0];
        $arrays = array_slice($arguments, 1);

        if (!is_array($array)) {
            throw new InvalidArgumentException(
                'Arr::merge() - All arguments must be arrays.'
            );
        }

        foreach ($arrays as $arr) {
            if (!is_array($arr)) {
                throw new InvalidArgumentException(
                    'Arr::merge() - All arguments must be arrays.'
                );
            }

            foreach ($arr as $key => $value) {
                // Numeric keys are appended
                if (is_numeric($key) || is_int($key)) {
                    array_key_exists($key, $array) ?
                        array_push($array, $value) : $array[$key] = $value;
                } elseif (
                    is_array($value)
                    && array_key_exists($key, $array)
                    && is_array($array[$key])
                ) {
                    $array[$key] = self::merge($array[$key], $value);
                } else {
                    $array[$key] = $value;
                }
            }
        }

        return $array;
    }

    /**
     * Merge two arrays recursively | assoc.
     *
     * @param mixed ...$arguments
     * @return null|array
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function mergeAssoc(...$arguments): ?array
    {
        $array = $arguments[0];
        $arrays = array_slice($arguments, 1);

        if (!is_array($array)) {
            throw new InvalidArgumentException(
                'Arr::mergeAssoc() - All arguments must be arrays.'
            );
        }

        foreach ($arrays as $arr) {
            if (!is_array($arr)) {
                throw new InvalidArgumentException(
                    'Arr::mergeAssoc() - All arguments must be arrays.'
                );
            }

            foreach ($arr as $key => $value) {
                if (
                    is_array($value)
                    && array_key_exists($key, $array)
                    && is_array($array[$key])
                ) {
                    $array[$key] = self::mergeAssoc($array[$key], $value);
                } else {
                    $array[$key] = $value;
                }
            }
        }

        return $array;
    }

    /**
     * Un-sets dot-notated key from an array.
     *
     * @param array $array The search array
     * @param mixed $key   The dot-notated key or array of keys
     *
     * @return mixed
     *
     * @since              0.1.0
     * @codeCoverageIgnore
     */
    public static function delete(array &$array = [], $key = null)
    {
        if (is_null($key)) {
            return false;
        }

        if (is_array($key)) {
            $return = [];

            foreach ($key as $k) {
                $return[$k] = self::delete($array, $k);
            }

            return $return;
        }

        $key_parts = explode(SINGLE_DOT, $key);

        if (
            !is_array($array)
            || !array_key_exists($key_parts[0], $array)
        ) {
            return false;
        }

        $this_key = array_shift($key_parts);

        if (!empty($key_parts)) {
            $key = implode(SINGLE_DOT, $key_parts);
            return self::delete($array[$this_key], $key);
        } else {
            unset($array[$this_key]);
        }

        return true;
    }

    public static function isSet(array $array = [], $element = null): bool
    {
        return isset($array[$element]);
    }

    /**
     * Add some prefix to each key
     *
     * @param array $array
     * @param string|null $prefix
     * @return array
     */
    public static function addEachKey(array $array = [], string $prefix = null): array
    {
        $retVal = [];

        foreach ($array as $key => $item) {
            $retVal[$prefix . $key] = $item;
        }

        return $retVal;
    }

    /**
     * Convert assoc array to comment style
     *
     * @param  array $data
     * @return string
     */
    public static function toComment(array $data = []): string
    {
        $retVal = [];

        foreach ($data as $key => $value) {
            $retVal[] = $key . ': ' . $value . ';';
        }

        return implode(PHP_EOL, $retVal);
    }

    /**
     * Calculate the size of the given array.
     *
     * @param  array $array
     * @return integer
     */
    public static function size($array = null): int
    {
        return count($array);
    }

    /**
     *
     * @param array $arrayToParseAsXml
     *
     * @return SimpleXMLElement
     */
    public static function toXml(array $arrayToParseAsXml): SimpleXMLElement
    {
        $xmlStr   = self::toXmlString($arrayToParseAsXml);

        return simplexml_load_string($xmlStr);
    }

    /**
     *
     * @param array $data
     *
     * @return string
     */
    public static function toXmlString(array $data): string
    {
        $xml = '<?xml version="1.0" encoding="utf-8"?>';

        $xml .= '<document>';
        $xml .= static::parseAsXml($data);
        $xml .= '</document>';

        return $xml;
    }

    private static function parseAsXml(
        array $data,
        string $inheritedKey = null
    ): string {
        $xml = EMPTY_STRING;

        // if (\is_array($data)) {
        //     foreach ($data as $key => $value) {
        //         if (!\is_scalar($value)) {
        //             if (\is_array($value)) {
        //                 if (static::isAssociative($value) || !$value) {
        //                     $xml .= self::parseXmlKeyValue(
        //                         $key,
        //                         // \array_map([__CLASS__, 'parseAsXml'], $value)
        //                         self::parseAsXml($value)
        //                     );
        //                 }
        //             } else {
        //                 $xml .= self::parseAsXml($value, $key);
        //             }
        //         } elseif ($inheritedKey !== null) {
        //             $xml .= self::parseXmlKeyValue($inheritedKey, $value);
        //         } else {
        //             $xml .= self::parseXmlKeyValue($key, $value);
        //         }

        //         // $xml .= self::parseXmlKeyValue($key, $value);
        //     }
        // }

        foreach ($data as $key => $value) {
            if (!is_scalar($value)) {
                if (is_array($value)) {
                    if (static::isAssociative($value) || !$value) {
                        $xml .= self::parseXmlKeyValue(
                            $key,
                            // \array_map([__CLASS__, 'parseAsXml'], $value)
                            self::parseAsXml($value)
                        );
                    }
                } else {
                    $xml .= self::parseXmlKeyValue($key, $value);
                }
            } elseif ($inheritedKey !== null) {
                $xml .= self::parseXmlKeyValue($inheritedKey, $value);
            } else {
                $xml .= self::parseXmlKeyValue($key, $value);
            }

            // $xml .= self::parseXmlKeyValue($key, $value);
        }

        return $xml;
    }

    private static function parseXmlKeyValue(
        string $key = null,
        $value = null
    ): string {
        if (Lang::isBoolean($value)) {
            $value = Str::toBoolString($value);
        } elseif (Lang::isNull($value)) {
            $value = Str::toNullString($value);
        }

        if (empty($value)) {
            // return "<property name=\"${key}\">" .
            //     "<value>" . \EMPTY_STRING . "</value>" .
            //     "</property>";

            return "</${key}>";
        }

        // return "<property name=\"${key}\">" .
        //     "<value>" . $value  . "</value>" .
        //     "</property>";

        return "<${key}>" . $value  . "</${key}>";
    }

    /**
     * Get a value from a collection and set it if it wasn't.
     *
     * @param mixed  $collection The collection
     * @param string $key        The key
     * @param mixed  $default    The default value to set if it isn't
     *
     * @return mixed
     */
    public static function setAndGet(&$collection = null, $key = null, $default = null): ?array
    {
        // If the key doesn't exist, set it
        if (!static::has($collection, $key)) {
            $collection = static::set($collection, $key, $default);
        }

        return static::get($collection, $key);
    }

    /**
     * Arr constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
                ' is singleton and cannot be cloned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
