<?php
/**
 * Async RunnableInterface Interface.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category Async
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
declare(strict_types=1);

namespace Exen\Support\Async\Process;

interface RunnableInterface
{
    public function getId();

    public function getPid(): ?int;

    public function start();

    /**
     * @param callable $callback
     *
     * @return static
     */
    public function then(callable $callback);

    /**
     * @param callable $callback
     *
     * @return static
     */
    public function catch(callable $callback);

    /**
     * @param callable $callback
     *
     * @return static
     */
    public function timeout(callable $callback);

    public function stop();

    public function getOutput();

    public function getErrorOutput();

    public function triggerSuccess();

    public function triggerError();

    public function triggerTimeout();

    public function getCurrentExecutionTime(): float;
}
