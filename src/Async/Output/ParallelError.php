<?php
/**
 * Async Parallel Error.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category Async
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
declare(strict_types=1);

namespace Exen\Support\Async\Output;

use Exception;

class ParallelError extends Exception
{
    public static function fromException($exception): self
    {
        return new self($exception);
    }

    public static function outputTooLarge(int $bytes): self
    {
        return new self("The output returned by this child process is too large. The serialized output may only be $bytes bytes long.");
    }
}
