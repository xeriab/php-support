<?php
/**
 * Async Pool.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category Async
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
declare(strict_types=1);

namespace Exen\Support\Async;

use ArrayAccess;
use InvalidArgumentException;
use Exen\Support\Async\Process\RunnableInterface;
use Exen\Support\Async\Runtime\ParentRuntime;
use Exen\Support\Async\Process\ParallelProcess;
use Exen\Support\Async\Process\SynchronousProcess;

class Pool implements ArrayAccess
{
    public static $forceSynchronous = false;

    protected $concurrency = 20;
    protected $tasksPerProcess = 1;
    protected $timeout = 300;
    protected $sleepTime = 50000;

    /** @var \Exen\Support\Async\Process\RunnableInterface[] */
    protected $queue = [];

    /** @var \Exen\Support\Async\Process\RunnableInterface[] */
    protected $inProgress = [];

    /** @var \Exen\Support\Async\Process\RunnableInterface[] */
    protected $finished = [];

    /** @var \Exen\Support\Async\Process\RunnableInterface[] */
    protected $failed = [];

    /** @var \Exen\Support\Async\Process\RunnableInterface[] */
    protected $timeouts = [];

    protected $results = [];

    protected $status;

    public function __construct()
    {
        if (static::isSupported()) {
            $this->registerListener();
        }

        $this->status = new PoolStatus($this);
    }

    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    public static function isSupported(): bool
    {
        return
            function_exists('pcntl_async_signals')
            && function_exists('posix_kill')
            && ! self::$forceSynchronous;
    }

    public function concurrency(int $concurrency): self
    {
        $this->concurrency = $concurrency;

        return $this;
    }

    public function timeout(int $timeout): self
    {
        $this->timeout = $timeout;

        return $this;
    }

    public function autoload(string $autoloader): self
    {
        ParentRuntime::init($autoloader);

        return $this;
    }

    public function sleepTime(int $sleepTime): self
    {
        $this->sleepTime = $sleepTime;

        return $this;
    }

    public function notify()
    {
        if (count($this->inProgress) >= $this->concurrency) {
            return;
        }

        $process = array_shift($this->queue);

        if (! $process) {
            return;
        }

        $this->putInProgress($process);
    }

    /**
     * @param \Exen\Support\Async\Process\RunnableInterface|callable $process
     *
     * @return \Exen\Support\Async\Process\RunnableInterface
     */
    public function add($process): RunnableInterface
    {
        if (! is_callable($process) && ! $process instanceof RunnableInterface) {
            throw new InvalidArgumentException('The process passed to Pool::add should be callable.');
        }

        if (! $process instanceof RunnableInterface) {
            $process = ParentRuntime::createProcess($process);
        }

        $this->putInQueue($process);

        return $process;
    }

    public function wait(?callable $intermediateCallback = null): array
    {
        while ($this->inProgress) {
            foreach ($this->inProgress as $process) {
                if ($process->getCurrentExecutionTime() > $this->timeout) {
                    $this->markAsTimedOut($process);
                }

                if ($process instanceof SynchronousProcess) {
                    $this->markAsFinished($process);
                }
            }

            if (! $this->inProgress) {
                break;
            }

            if ($intermediateCallback) {
                call_user_func_array($intermediateCallback, [$this]);
            }

            usleep($this->sleepTime);
        }

        return $this->results;
    }

    public function putInQueue(RunnableInterface $process)
    {
        $this->queue[$process->getId()] = $process;

        $this->notify();
    }

    public function putInProgress(RunnableInterface $process)
    {
        if ($process instanceof ParallelProcess) {
            $process->getProcess()->setTimeout($this->timeout);
        }

        $process->start();

        unset($this->queue[$process->getId()]);

        $this->inProgress[$process->getPid()] = $process;
    }

    public function markAsFinished(RunnableInterface $process)
    {
        unset($this->inProgress[$process->getPid()]);

        $this->notify();

        $this->results[] = $process->triggerSuccess();

        $this->finished[$process->getPid()] = $process;
    }

    public function markAsTimedOut(RunnableInterface $process)
    {
        unset($this->inProgress[$process->getPid()]);

        $this->notify();

        $process->triggerTimeout();

        $this->timeouts[$process->getPid()] = $process;
    }

    public function markAsFailed(RunnableInterface $process)
    {
        unset($this->inProgress[$process->getPid()]);

        $this->notify();

        $process->triggerError();

        $this->failed[$process->getPid()] = $process;
    }

    public function offsetExists($offset)
    {
        // TODO

        return false;
    }

    public function offsetGet($offset)
    {
        // TODO
    }

    public function offsetSet($offset, $value)
    {
        $this->add($value);
    }

    public function offsetUnset($offset)
    {
        // TODO
    }

    /**
     * @return \Exen\Support\Async\Process\RunnableInterface[]
     */
    public function getQueue(): array
    {
        return $this->queue;
    }

    /**
     * @return \Exen\Support\Async\Process\RunnableInterface[]
     */
    public function getInProgress(): array
    {
        return $this->inProgress;
    }

    /**
     * @return \Exen\Support\Async\Process\RunnableInterface[]
     */
    public function getFinished(): array
    {
        return $this->finished;
    }

    /**
     * @return \Exen\Support\Async\Process\RunnableInterface[]
     */
    public function getFailed(): array
    {
        return $this->failed;
    }

    /**
     * @return \Exen\Support\Async\Process\RunnableInterface[]
     */
    public function getTimeouts(): array
    {
        return $this->timeouts;
    }

    public function status(): PoolStatus
    {
        return $this->status;
    }

    protected function registerListener()
    {
        pcntl_async_signals(true);

        pcntl_signal(SIGCHLD, function ($signo, $status) {
            while (true) {
                $pid = pcntl_waitpid(-1, $processState, WNOHANG | WUNTRACED);

                if ($pid <= 0) {
                    break;
                }

                $process = $this->inProgress[$pid] ?? null;

                if (! $process) {
                    continue;
                }

                if ($status['status'] === 0) {
                    $this->markAsFinished($process);

                    continue;
                }

                $this->markAsFailed($process);
            }
        });
    }
}
