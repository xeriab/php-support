<?php

/**
 * Async Child Runtime.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category Async
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */

declare(strict_types=1);

namespace Exen\Support\Async\Runtime;

use Exen\Support\Async\Runtime\ParentRuntime;
use Exen\Support\Async\Output\ParallelError;

try {
    $autoloader = $argv[1] ?? null;
    $serializedClosure = $argv[2] ?? null;

    if (!$autoloader) {
        throw new \InvalidArgumentException('No autoloader provided in child process.');
    }

    if (!\file_exists($autoloader)) {
        throw new \InvalidArgumentException("Could not find autoloader in child process: {$autoloader}");
    }

    if (!$serializedClosure) {
        throw new \InvalidArgumentException('No valid closure was passed to the child process.');
    }

    require_once $autoloader;

    $task = ParentRuntime::decodeTask($serializedClosure);

    $output = \call_user_func($task);

    $serializedOutput = \base64_encode(\serialize($output));

    $outputLength = 1024 * 10;

    if (\strlen($serializedOutput) > $outputLength) {
        throw ParallelError::outputTooLarge($outputLength);
    }

    \fwrite(STDOUT, $serializedOutput);

    exit(0);
} catch (\Throwable $exception) {
    require_once __DIR__ . '/../Output/SerializableException.php';
    $output = new \Exen\Support\Async\Output\SerializableException($exception);
    \fwrite(\STDERR, \base64_encode(\serialize($output)));
    exit(1);
}
