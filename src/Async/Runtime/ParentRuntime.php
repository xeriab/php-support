<?php

/**
 * Async Parent Runtime.
 *
 * PHP version 7
 *
 * LICENSE: This source file is subject to MIT license
 * that is available through the world-wide-web at the following URI:
 * https://opensource.org/licenses/MIT.  If you did not receive a copy of
 * the MIT License and are unable to obtain it through the web, please
 * send a note to license@xeriab.net so we can mail you a copy immediately.
 *
 * @category Async
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */

declare(strict_types=1);

namespace Exen\Support\Async\Runtime;

use Closure;
use Exen\Support\Async\Pool;
use Exen\Support\Async\Process\RunnableInterface;
use Exen\Support\Async\Process\ParallelProcess;
use Exen\Support\Async\Process\SynchronousProcess;
use Exen\Support\Closure\SerializableClosure;
use Symfony\Component\Process\Process;
// use function Exen\Support\Closure\serialize;
// use function Exen\Support\Closure\unserialize;

class ParentRuntime
{
    /** @var bool */
    protected static $isInitialised = false;

    /** @var string */
    protected static $autoloader;

    /** @var string */
    protected static $childProcessScript;

    protected static $currentId = 0;

    protected static $myPid = null;

    public static function init(?string $autoloader = null)
    {
        if (!$autoloader) {
            $existingAutoloaderFiles = \array_filter([
                __DIR__ . '/../../../../autoload.php',
                __DIR__ . '/../../../autoload.php',
                __DIR__ . '/../../../vendor/autoload.php',
                __DIR__ . '/../../vendor/autoload.php',
            ], function (?string $path) {
                return \file_exists($path);
            });

            $autoloader = reset($existingAutoloaderFiles);
        }

        self::$autoloader = $autoloader;
        self::$childProcessScript = __DIR__ . '/ChildRuntime.php';
        self::$isInitialised = true;
    }

    /**
     * @param \Exen\Support\Async\Task|callable $task
     *
     * @return \Exen\Support\Async\Process\RunnableInterface
     */
    public static function createProcess($task): RunnableInterface
    {
        if (!self::$isInitialised) {
            self::init();
        }

        if (!Pool::isSupported()) {
            return SynchronousProcess::create($task, self::getId());
        }

        $arr = [
            // 'php',
            'exec php',
            self::$childProcessScript,
            self::$autoloader,
            self::encodeTask($task),
        ];

        $command = implode(' ', $arr);
        // $process = new Process($arr);
        // D($command);

        // $process = new Process([$command]);
        $process = Process::fromShellCommandline($command);

        return ParallelProcess::create($process, self::getId());
    }

    /**
     * @param \Exen\Support\Async\Task|callable $task
     *
     * @return string
     */
    public static function encodeTask($task): string
    {
        if ($task instanceof Closure) {
            $task = new SerializableClosure($task);
        }

        return \base64_encode(closureSerialize($task));
    }

    public static function decodeTask(string $task)
    {
        return closureUnserialize(\base64_decode($task));
    }

    protected static function getId(): string
    {
        if (self::$myPid === null) {
            self::$myPid = \getmypid();
        }

        self::$currentId += 1;

        return (string) self::$currentId . (string) self::$myPid;
    }
}
