<?php /** @noinspection PhpUnused */
/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Exception\Exception;
use Exen\Support\Exception\RegexpException;
use function array_filter;
use function array_key_exists;
use function array_merge;
use function array_pop;
use function array_push;
use function array_slice;
use function compact;
use function count;
use function end;
use function explode;
use function get_called_class;
use function get_class;
use function getcwd;
use function gettype;
use function iconv;
use function implode;
use function is_array;
use function is_string;
use function min;
use function preg_match;
use function preg_replace;
use function preg_split;
use function realpath;
use function strlen;
use function strtr;
use function substr;
use function ucfirst;
use const DIRECTORY_SEPARATOR;
use const DOUBLE_DOTS;
use const DS;
use const EMPTY_STRING;
use const IS_WIN;
use const POSIX_DIR_SEP;
use const PS;
use const SINGLE_DOT;

/**
 * Class for various `Path` helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
class Path extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const PATTERN_SPLIT_DEVICE = '/^([a-zA-Z]:|[\\\\\/]{2}[^\\\\\/]+[\\\\\/]+[^\\\\\/]+)?([\\\\\/])?([\s\S]*?)$/';
    const PATTERN_SPLIT_PATH   = '/^(\\/?|)([\\s\\S]*?)((?:\\.{1,2}|[^\\/]+?|)(\\.[^.\\/]*|))(?:[\\/]*)$/';
    const PATTERN_SPLIT_TAIL   = '/^([\\s\\S]*?)((?:\\.{1,2}|[^\\\\\\/]+?|)(\\.[^.\\/\\\\]*|))(?:[\\\\\\/]*)$/';

    /**
     * Path seperator.
     *
     * @var string SEP.
     */
    const SEP       = DS;

    /**
     * Path separator.
     *
     * @var string SEPARATOR.
     */
    const SEPARATOR = DS;

    /**
     * Path delimiter.
     *
     * @var string DELIMITER.
     */
    const DELIMITER = PS;

    /**
     * Path instance.
     *
     * @var Path
     */
    private static $instance = null;

    /**
     * Path instances.
     *
     * @var Path[]
     */
    private static $instances = [];

    /**
     * Parses and canonicalizes a path and returns an object whose properties
     * represent significant elements of the `path`.
     *
     * @note The returned object will have the following properties:
     *  dir
     *  dirname
     *  path
     *  pathname
     *  root
     *  base
     *  name
     *  ext
     *  protocol
     *
     * @param string|null $path Path string.
     * @param string|null $option Option string.
     *
     * @return array|string
     * @throws RegexpException
     */
    public static function parse(?string $path = null, ?string $option = null)
    {
        $root = EMPTY_STRING;
        // $path = \strtr($path, '\\', '/');
        $path = strtr($path, '\\', '/');

        if (preg_match('@^(?:/|[a-z]:/?|[a-z]+://)@i', $path, $parts)) {
            $root = $parts[0];
            // $path = \substr($path, \strlen($root));
            $path = Str::sub($path, Str::length($root));
        }

        $path = (preg_replace('/(\/|\/\/)+/ui', DS, $path));

        $parts = [];

        // $val = \explode(\DIRECTORY_SEPARATOR, $path);
        $val = explode('/', $path);

        foreach (array_filter($val, 'strlen') as $part) {
            if (DOUBLE_DOTS == $part) {
                if (count($parts)) {
                    array_pop($parts);
                    continue;
                } elseif (!$root) {
                    continue;
                }
            } elseif (SINGLE_DOT != $part) {
                $parts[] = $part;
            }
        }

        // $path = \implode(\DS, $parts);
        $path = implode('/', $parts);
        $info = compact('root', 'path');

        // $info['dir']      = $root.\substr($path, 0, (int) \strrpos($path, '/'));
        // $info['dirname']  = $root.\substr($path, 0, (int) \strrpos($path, '/'));
        // $info['protocol'] = \strpos($root, '://') ? \substr($root, 0, -3): 'file';

        $info['dir']      = $root . Str::sub($path, 0, (int) Str::rPos($path, '/'));
        $info['dirname']  = $root . Str::sub($path, 0, (int) Str::rPos($path, '/'));
        $info['pathname'] = $root . $path;
        $info['protocol'] = Str::position($root, '://') ? Str::sub($root, 0, -3) : 'file';

        $base = (preg_split('/(\/|\/\/)+/i', $path));
        $base = end($base);

        $info['base'] = $base;

        $matches = [];

        // \preg_match('/([A-Za-z0-9]+\.[A-Za-z0-9]+)/i', $base, $matches);
        Str::pcre('preg_match', ['/([A-Za-z0-9]+\.[A-Za-z0-9]+)/i', $base, &$matches]);

        if (Lang::isNotEmpty($matches)) {
            $info['extension'] = self::extName($base);
            $info['ext'] = self::extName($base);

            $names = (preg_split('/(\.)+/i', $base));

            if (count($names) >= 3) {
                $names = array_slice($names, 0, -1);
            }

            $info['name'] = implode(SINGLE_DOT, $names);
        }

        if (Lang::isNull($option)) {
            return $info;
        }

        return array_key_exists($option, $info) ? $info[$option] : EMPTY_STRING;
    }

    /**
     * Fix directory separator.
     *
     * @param string|null $path
     * @param bool $useCleanPrefix
     *
     * @return string
     */
    public static function fixDirectorySeparator(string $path = null, bool $useCleanPrefix = false): string
    {
        /**
         * Trimming path string
         */
        // if (\EMPTY_STRING === ($path = \trim($path))) {
        if (EMPTY_STRING === ($path = Str::trim($path))) {
            return $path;
        }

        // (\/|\)+
        $path = preg_replace('/(\/|\/\/)+/', DIRECTORY_SEPARATOR, $path);
        // $path = \str_replace('\\\\', \DIRECTORY_SEPARATOR, $path);
        $path = Str::replace($path, '\\', DIRECTORY_SEPARATOR);

        if ($useCleanPrefix) {
            // $path = \DIRECTORY_SEPARATOR.\ltrim($path, \DIRECTORY_SEPARATOR);
            $path = DIRECTORY_SEPARATOR . Str::trimStart($path, DIRECTORY_SEPARATOR);
        }

        return $path;
    }

    /**
     * Join given paths.
     *
     * @param string[] ...$paths A sequence of path segments.
     *
     * @return string|null
     *
     * @throws Exception
     */
    public static function join(...$paths): ?string
    {
        if (!Lang::isStringArray($paths)) {
            throw new Exception('Arguments to Path::join() must be strings');
        }

        $retVal = null;

        if (count($paths)) {
            foreach ($paths as $path) {
                $path = preg_replace('/(\/|\/\/)+/', DS, $path);
                $retVal .= $path . DS;
            }
        }

        return self::normalize($retVal);
    }

    /**
     * Normalize a filesystem path.
     *
     * @param string|null $path path to normalize
     * @param string|null $encoding
     *
     * @return string normalized path
     * @throws RegexpException
     */
    public static function normalizePath(?string $path = null, ?string $encoding = 'UTF-8'): string
    {
        // Attempt to avoid path encoding problems.
        $path = iconv($encoding, "$encoding//IGNORE//TRANSLIT", $path);

        $path = self::fixDirectorySeparator($path);
        // $path = self::posixCompatWinPath($path);

        $path = preg_replace('|(?<=.)/+|', DIRECTORY_SEPARATOR, $path);

        // if (':' === \substr($path, 1, 1)) {
        if (':' === Str::sub($path, 1, 1)) {
            $path = ucfirst($path);
        }

        // if (self::isAbsolute($path) && \strpos($path, \SINGLE_DOT)) {
        if (self::isAbsolute($path) && Str::position($path, SINGLE_DOT)) {
            $explode = explode(DIRECTORY_SEPARATOR, $path);
            $array = [];

            foreach ($explode as $key => $value) {
                if (SINGLE_DOT == $value) {
                    continue;
                }

                if (DOUBLE_DOTS == $value) {
                    array_pop($array);
                } else {
                    $array[] = $value;
                }
            }

            $path = implode(DIRECTORY_SEPARATOR, $array);
        }

        return $path;
    }

    public static function normalize($path = null, ?string $encoding = 'UTF-8'): string
    {
        // Attempt to avoid path encoding problems.
        $path = iconv($encoding, "$encoding//IGNORE//TRANSLIT", $path);
        $path = self::fixDirectorySeparator($path);
        // $path = self::posixCompatWinPath($path);

        // Process the components
        $parts = explode(DIRECTORY_SEPARATOR, $path);

        $safe = [];

        foreach ($parts as $idx => $part) {
            if (empty($part) or (SINGLE_DOT === $part)) {
                continue;
            } elseif (DOUBLE_DOTS === $part) {
                array_pop($safe);
                continue;
            } else {
                $safe[] = $part;
            }
        }

        // Return the "clean" path
        $path = implode(DS, $safe);

        if (!preg_match('/^[A-Za-z]+:/i', $path)) {
            $path = DS . $path;
        }

        // if (!Str::endsWith(\DS, $path)) {
        //     $path .= \DS;
        // }

        $path = self::posixCompatWinPath($path);

        return $path;
    }

    public static function posixCompatWinPath($path = null): string
    {
        // $path = \str_replace('\\', \POSIX_DIR_SEP, $path);
        $path = preg_replace('/(\/|\/\/)+/iu', POSIX_DIR_SEP, $path);

        // $path = \preg_replace('`(\/|\\/)+`', \DIRECTORY_SEPARATOR, $path);

        // if (!Str::endsWith($path, '/')) {
        //     $path .= \DS;
        // }

        return $path;
    }

    /**
     * Gets basename of the given path.
     *
     * @param string|null $path
     * @param string|null $extension
     *
     * @return string|null
     */
    public static function baseName(?string $path = null, ?string $extension = null): ?string
    {
        // if (Lang::isNotNull($extension) and Lang::isNotEmpty($extension)) {
        //     return \basename($path, $extension);
        // }

        // return \basename($path);

        $result = self::splitPath($path);
        $retVal = $result[2];

        // TODO: make this comparison case-insensitive on windows?

        if ($extension && substr($retVal, -1 * strlen($extension)) === $extension) {
            $retVal = substr($retVal, 0, strlen($retVal) - strlen($extension));
        }

        return $retVal;
    }

    /**
     * Gets dirname of the given path.
     *
     * @param string|null $path
     * @param int|null $levels
     *
     * @return string
     * @noinspection PhpUnusedParameterInspection
     */
    public static function dirName(?string $path = null, ?int $levels = 1): string
    {
        // return \dirname($path, $levels);

        $result = self::splitPath($path);

        $root = $result[0];
        $dir = $result[1];

        if (!$root && !$dir) {
            // No dirname whatsoever
            return SINGLE_DOT;
        }

        if ($dir) {
            // It has a dirname, strip trailing slash
            $dir = substr($dir, 0, strlen($dir) - 1);
        }

        return $root . $dir;
    }

    /**
     * Gets extension of the given path.
     *
     * @param string|null $path
     * @param boolean $strict
     *
     * @return string
     * @noinspection PhpUnusedParameterInspection
     */
    public static function extName(?string $path = null, ?bool $strict = true): string
    {
        // if (Str::startsWith($path, \SINGLE_DOT) and $strict) {
        //     return \EMPTY_STRING;
        // } else if (Lang::isEmpty($path)) {
        //     return \EMPTY_STRING;
        // }

        // $parts = \explode(\SINGLE_DOT, $path);

        // if (\count($parts) >= 1) {
        //     return \SINGLE_DOT . \end($parts);
        // }

        $result = self::splitPath($path);
        return $result[3];
    }

    /**
     * Returns the current working directory on success, or `FALSE` on failure.
     *
     * On some Unix variants, `Path::cwd()` will return FALSE if any one of the parent directories
     * does not have the readable or search mode set, even if the current directory does.
     *
     * @see chmod() for more information on modes and permissions.
     *
     * @return string|boolean
     */
    public static function cwd()
    {
        return getcwd();
    }

    /**
     * Returns the current working directory on success, or `FALSE` on failure.
     *
     * On some Unix variants, `Path::currentWorkingDir()` will return FALSE if any one of
     * the parent directories does not have the readable or search mode set, even if the
     * current directory does.
     *
     * @see chmod() for more information on modes and permissions.
     *
     * @return string|boolean
     */
    public static function currentWorkingDir()
    {
        return self::cwd();
    }

    /**
     * Returns the current working directory on success, or `FALSE` on failure.
     *
     * On some Unix variants, `Path::currentWorkingDirectory()` will return FALSE if any one of
     * the parent directories does not have the readable or search mode set, even if the
     * current directory does.
     *
     * @see chmod() for more information on modes and permissions.
     *
     * @return string|boolean
     */
    public static function currentWorkingDirectory()
    {
        return self::cwd();
    }

    /**
     * Resolves a sequence of paths or path segments into an absolute path.
     *
     * @param string[]|string ...$paths A sequence of paths or path segments.
     *
     * @return string
     * @throws Exception
     */
    public static function resolve(...$paths): string
    {
        // throw new NotImplementedError(__METHOD__);
        // return \EMPTY_STRING;

        $resolvedPath = '';
        $resolvedAbsolute = false;

        for ($x = count($paths) - 1; $x >= -1 && !$resolvedAbsolute; --$x) {
            $path = ($x >= 0) ? $paths[$x] : getcwd();

            // Skip empty and invalid entries
            if (!is_string($path)) {
                throw new Exception('Arguments to Path::resolve() must be strings');
            } elseif (!$path) {
                continue;
            }

            $resolvedPath = $path . '/' . $resolvedPath;
            $resolvedAbsolute = substr($path, 0, 1) === '/';
        }

        // At this point the path should be resolved to a full absolute path, but
        // handle relative paths to be safe (might happen when process.cwd() fails)

        // Normalize the path
        $resolvedPath = implode('/', self::normalizeArray(preg_split('/\//', $resolvedPath), !$resolvedAbsolute));

        return (($resolvedAbsolute ? '/' : '') . $resolvedPath) ?: SINGLE_DOT;
    }

    /**
     * Returns the relative path from `from` to `to` based on the current working directory.
     * If `from` and `to` each resolve to the same path (after calling Path::resolve() on each),
     * a zero-length string is returned.
     *
     * If a zero-length string is passed as `from` or `to`, the current working directory will be
     * used instead of the zero-length strings.
     *
     * @param string|null $from
     * @param string|null $to
     *
     * @return string
     * @throws Exception
     * @example: on POSIX
     *     Path::relative('/data/orandea/test/aaa', '/data/orandea/impl/bbb');
     *     // Returns: '../../impl/bbb'
     *
     * @example: on Windows
     *     Path::relative('C:\\orandea\\test\\aaa', 'C:\\orandea\\impl\\bbb');
     *     // Returns: '..\\..\\impl\\bbb'
     *
     */
    public static function relative(?string $from = null, ?string $to = null): string
    {
        // throw new NotImplementedError(__METHOD__);
        // return \EMPTY_STRING;

        $from = substr(self::resolve($from), 1);
        $to = substr(self::resolve($to), 1);

        $trim = function ($arr) {
            $start = 0;

            for (; $start < count($arr); ++$start) {
                if ($arr[$start] !== EMPTY_STRING) {
                    break;
                }
            }

            $end = count($arr) - 1;

            for (; $end >= 0; --$end) {
                if ($arr[$end] !== EMPTY_STRING) {
                    break;
                }
            }

            if ($start > $end) {
                return [];
            }

            return array_slice($arr, $start, $end + 1);
        };

        $fromParts = $trim(preg_split('/\//', $from));
        $toParts = $trim(preg_split('/\//', $to));

        $length = min(count($fromParts), count($toParts));
        $samePartsLength = $length;

        for ($x = 0; $x < $length; ++$x) {
            if ($fromParts[$x] !== $toParts[$x]) {
                $samePartsLength = $x;
                break;
            }
        }

        $outputParts = [];

        for ($x = $samePartsLength; $x < count($fromParts); ++$x) {
            $outputParts[] = DOUBLE_DOTS;
        }

        $outputParts = array_merge($outputParts, array_slice($toParts, $samePartsLength));

        return implode('/', $outputParts);
    }

    /**
     * Returns whether the given `path` is absolute.
     *
     * @param string|null $path
     *
     * @return boolean
     * @throws RegexpException
     */
    public static function isAbsolute(string $path = null): bool
    {
        /**
         * This is definitive if true but fails if $path does not exist or contains
         * a symbolic link.
         */
        if (realpath($path) === $path) {
            return true;
        }

        if (0 === Str::length($path) or SINGLE_DOT === $path[0]) {
            return false;
        }

        // Windows allows absolute paths like this.
        if (preg_match('/^[a-zA-Z]:\\//i', $path)) {
            return true;
        }

        // A path starting with / or \ is absolute; anything else is relative.
        // return '/' === $path[0] or '\\' === $path[0];

        return self::parse($path, 'root') !== EMPTY_STRING;
    }

    /**
     * Returns whether the given `path` is relative.
     *
     * @param string|null $path
     *
     * @return boolean
     * @throws RegexpException
     */
    public static function isRelative(string $path = null): bool
    {
        return self::parse($path, 'root') === EMPTY_STRING;
    }

    /**
     * On Windows systems only, returns an equivalent namespace-prefixed `path` for the given `path`.
     * If `path` is not a string, `path` will be returned without modifications.
     *
     * This method is meaningful only on Windows system. On posix systems, the method
     * is non-operational and always returns path without modifications.
     *
     * @param string|null $path
     *
     * @return string
     * @noinspection PhpUnused
     */
    public static function toNamespacedPath(?string $path = null): string
    {
        if (IS_WIN) {
            return $path;
        }

        return $path;
    }

    /**
     * Gets path delimiter.
     *
     * @return string
     */
    public static function delimiter(): string
    {
        if (IS_WIN) {
            return ';';
        }

        return ':';
    }

    /**
     * Gets path separator.
     *
     * @return string
     */
    public static function separator(): string
    {
        if (IS_WIN) {
            return '\\';
        }

        return '/';
    }

    /**
     * @param $params
     * @return string
     * @throws Exception
     */
    public static function format($params): string
    {
        if (!is_array($params)) {
            throw new Exception("Parameter 'pathObject' must be an object, not " . gettype($params));
        }

        $root = $params['root'] ?: '';

        if (!is_string($root)) {
            throw new Exception("'pathObject.root' must be a string or undefined, not " . gettype($params['root']));
        }

        $dir = $params['dir'] ? $params['dir'] . self::separator() : '';
        $base = $params['base'] ?: '';

        return $dir . $base;
    }

    protected static function normalizeArray(?array $parts = [], bool $allowAboveRoot = false): array
    {
        $retVal = [];

        for ($x = 0; $x < count($parts); $x++) {
            $part = $parts[$x];

            // ignore empty parts
            if (!$part || $part === SINGLE_DOT) {
                continue;
            }

            if ($part === DOUBLE_DOTS) {
                if (count($retVal) && $retVal[count($retVal) - 1] !== DOUBLE_DOTS) {
                    array_pop($retVal);
                } elseif ($allowAboveRoot) {
                    array_push($retVal, DOUBLE_DOTS);
                }
            } else {
                array_push($retVal, $part);
            }
        }

        return $retVal;
    }

    protected static function normalizeUNCRoot(?string $device = null): string
    {
        $device = preg_replace('/^[\\\\\\/]+/', EMPTY_STRING, $device);
        $device = preg_replace('/[\\\\\\/]+/', '\\', $device);

        return '\\\\' . $device;
    }

    private static function splitPath(?string $path = null): array
    {
        preg_match(self::PATTERN_SPLIT_PATH, $path, $matches);
        return array_slice($matches, 1);
    }

    public static function className(): string
    {
        return get_called_class();
    }

    /**
     * Path constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
                ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
