<?php

declare(strict_types=1);

namespace Exen\Support;

/**
 * Collection class.
 *
 * @since 0.1
 */
class ExenCollection extends Container\Collection
{
}
