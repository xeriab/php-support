<?php

declare(strict_types=1);

namespace Exen\Support\Json;

use Exen\Support\Arr;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Lang;
use Exen\Support\PhpObject;
use Exen\Support\Str;
use Exen\Support\Traits\MacroableTrait;
use LogicException;
use function array_filter;
use function get_class;
use const ARRAY_FILTER_USE_BOTH;
use const SPACE_CHAR;

/**
 * Class for various `JSON` helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 *
 * @since 0.1
 */
final class Json extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const UNESCAPED_UNICODE = \JSON_UNESCAPED_UNICODE;
    const UNESCAPED_SLASHES = \JSON_UNESCAPED_SLASHES;
    const BIGINT_AS_STRING = \JSON_BIGINT_AS_STRING;
    const PRESERVE_ZERO_FRACTION = \JSON_PRESERVE_ZERO_FRACTION;
    const PRETTY_PRINT = \JSON_PRETTY_PRINT;
    const FORCE_OBJECT = \JSON_FORCE_OBJECT;
    const FORCE_ARRAY = 0b0001;
    const PRETTY = 0b0010;

    /**
     * Json instance.
     *
     * @var \Exen\Support\Json\Json
     */
    private static $instance = null;

    /**
     * Json instances.
     *
     * @var \Exen\Support\Json\Json[]
     */
    private static $instances = [];

    /**
     * Json constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * Parses a JSON file into a PHP value.
     *
     *  Usage:
     *  <code>
     *   $obj = Json::parseFile('config.json');
     *   print_r($obj);
     *  </code>
     *
     * @param string $filename The path to the JSON file to be parsed
     * @param int $options Accepts Json::FORCE_ARRAY
     *
     * @return mixed The JSON converted to a PHP value
     *
     * @throws ParseException If the file could not be read or the JSON is not valid
     */
    public static function parseFile(string $filename = null, int $options = 0)
    {
        $isFile = Lang::isFile($filename);
        $fileContent = ($isFile) ? \file_get_contents($filename) : '';

        return static::decode($fileContent, $options);
    }

    /**
     * Decodes a JSON string.
     *
     * @param string
     * @param int  accepts Json::FORCE_ARRAY
     *
     * @return mixed
     */
    public static function decode($value = null, ?int $options = 0, ?bool $sorted = false)
    {
        // $depth = 512;

        $flags = self::UNESCAPED_UNICODE |
            self::UNESCAPED_SLASHES |
            self::BIGINT_AS_STRING |
            self::PRESERVE_ZERO_FRACTION;

        if (defined('JSON_PARSER_NOTSTRICT')) {
            // $flags |= \JSON_PARSER_NOTSTRICT;
            $flags |= Lang::getConstant('JSON_PARSER_NOTSTRICT');
        }

        $forceArray = (bool)($options & static::FORCE_ARRAY);

        $flags |= $forceArray;

        $decoder = null;
        $result = null;

        try {
            if ($forceArray) {
                // $decoder = new JsonArrayDecoder($depth, $flags);
                $decoder = new JsonArrayDecoder(\JSON_MAX_DEPTH, (int)$flags);
            } else {
                // $decoder = new JsonObjectDecoder($depth, $flags);
                $decoder = new JsonObjectDecoder(\JSON_MAX_DEPTH, (int)$flags);
            }

            $result = $decoder->decode($value);
        } catch (Exception\JsonSyntaxException|Exception\JsonUnknownException|Exception\JsonControlCharacterException $ex) {
            DEBUG($ex->getMessage());
        }

        unset($decoder);

        if (Lang::isArray($result) and $sorted) {
            $result = Arr::sortRecursive($result);
        }

        return $result;
    }

    /**
     * Parses JSON into a PHP value.
     *
     *  Usage:
     *  <code>
     *   $obj = Json::parse(file_get_contents('config.json'));
     *   print_r($obj);
     *  </code>
     *
     * @param string $input A string containing JSON
     * @param int $options Accepts Json::FORCE_ARRAY
     *
     * @return mixed The JSON converted to a PHP value
     *
     * @throws ParseException If the JSON is not valid
     */
    public static function parse(?string $input = null, ?int $options = 0)
    {
        return static::decode($input, $options);
    }

    /**
     * Decodes a JSON string.
     *
     * @param string
     * @param int  accepts Json::FORCE_ARRAY
     *
     * @return mixed
     */
    public static function dump($input = null, ?int $options = 0): ?string
    {
        return static::decode($input, $options);
    }

    /**
     * Converts a PHP value to a JSON string, optionally replacing values if
     * a replacer function is specified or optionally including only the specified
     * properties if a replacer array is specified.
     *
     * @param mixed $value The value to convert to a JSON string.
     * @param mixed $replacer A function that alters the behavior of the
     *                             stringification process, or an array of String
     *                             and Number objects that serve as a whitelist
     *                             for selecting/filtering the properties of the
     *                             value object to be included in the JSON string.
     *                             If this value is null or not provided, all
     *                             properties of the object are included in the
     *                             resulting JSON string.
     * @param string|int $space A String or Number that's used to insert
     *                             white space into the output JSON string for
     *                             readability purposes. If this is a Number,
     *                             it indicates the number of space characters
     *                             to use as white space; this number is capped
     *                             at 10 (if it is greater, the value is just 10).
     *                             Values less than 1 indicate that no space
     *                             should be used. If this is a String, the string
     *                             (or the first 10 characters of the string,
     *                             if it's longer than that) is used as white space.
     *                             If this parameter is not provided (or is null),
     *                             no white space is used.
     *
     * @return string|null         Returns JSON string if the process/parsing went
     *                             well, Null if something went wrong.
     *
     * @since 0.1.0
     */
    public static function stringify(
        $value = null,
        $replacer = null,
        $space = null
    ): ?string
    {
        $result = null;
        $rs = '    ';

        if (Lang::isNotNull($replacer) and Lang::isCallableOrClosure($replacer)) {
            $result = array_filter($value, $replacer, ARRAY_FILTER_USE_BOTH);
            $result = static::encode($result);
        } else {
            $result = static::encode($value);
        }

        if (Lang::isNotNull($replacer)) {
            if (Lang::isCallableOrClosure($replacer)) {
                // $result = \array_filter($value, $replacer, \ARRAY_FILTER_USE_BOTH);
                $result = Arr::filter($value, $replacer, Arr::FILTER_USE_BOTH);
                $result = static::encode($result);
            } elseif (Lang::isArray($replacer)) {
                $callback = function ($key) use ($replacer) {
                    return (Lang::isArray($key, $replacer));
                };

                // $result = \array_filter($value, $callback, \ARRAY_FILTER_USE_KEY);
                $result = Arr::filter($value, $callback, Arr::FILTER_USE_KEY);
                $result = static::encode($result);
            } else {
                $result = static::encode($value);
            }
        }

        if (Lang::isNotNull($space)) {
            if (Lang::isString($space)) {
                $result = Str::replace($result, $rs, $space);
                // $result = Str::replaceIndents($result, 1, $space);
            } elseif (Lang::isNumeric($space)) {
                if ($space <= 10) {
                    // $ident  = str_repeat(\SPACE_CHAR, $space);
                    $ident = Str::repeat(SPACE_CHAR, $space);
                    $result = Str::replace($result, $rs, $ident);
                    // $result = Str::indent($result, $space);
                } else {
                    $result = Str::replace($result, $rs, '  ');
                    // $result = Str::indent($result, 2);
                }
            }
        }

        return $result;
    }

    /**
     * Returns the JSON representation of a value.
     *
     * @param mixed
     * @param int  accepts Json::PRETTY
     *
     * @return string|null
     */
    public static function encode($value = null, ?int $options = 0): ?string
    {
        $flags = self::UNESCAPED_UNICODE |
            self::UNESCAPED_SLASHES |
            self::BIGINT_AS_STRING |
            self::PRESERVE_ZERO_FRACTION;

        $pretty1 = ($options & static::PRETTY ? self::PRETTY_PRINT : 0);

        $USE_PRETTY_JSON = Lang::getConstant('USE_PRETTY_JSON', null, false);

        $pretty2 = $USE_PRETTY_JSON ?
            self::PRETTY_PRINT :
            0;

        $flags |= $pretty1 | $pretty2;

        $encoder = null;
        $result = null;

        try {
            $encoder = new JsonEncoder($options = $flags);
            $result = $encoder->encode($value);
        } catch (Exception\JsonUnknownException $ex) {
            // DEBUG($ex->getMessage());
        } catch (Exception\JsonControlCharacterException $ex) {
            // DEBUG($ex->getMessage());
        }

        unset($encoder);

        return $result;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }
}
