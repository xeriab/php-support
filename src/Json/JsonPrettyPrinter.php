<?php

declare(strict_types=1);

namespace Exen\Support\Json;

/**
 * Class JsonPretyPrinter
 *
 * Near mint solution for PHP < 5.4 found at
 * [Stackoverflow](http://stackoverflow.com/a/9776726)
 *
 * @package Exen\Support\Json
 */
class JsonPrettyPrinter
{
    /**
     * Indentation string.
     *
     * @var string
     */
    private $_indentationString = "\t";

    /**
     * Gets indentation string.
     *
     * @return string
     */
    public function getIndentationString()
    {
        return $this->_indentationString;
    }

    /**
     * Sets indentation string.
     *
     * @param string $indentationString Indentation string.
     *
     * @return $this
     */
    public function setIndentationString($indentationString)
    {
        $this->_indentationString = $indentationString;
        return $this;
    }

    /**
     * Pretty-print JSON string.
     *
     * @param string $jsonString JSON string.
     *
     * @return string
     */
    public function prettyPrint(string $jsonString = null)
    {
        \set_time_limit(20);
        $result = '';
        $pos = 0;
        $strLen = \mb_strlen($jsonString);
        $newLine = \PHP_EOL;
        $prevChar = '';
        $outOfQuotes = true;

        for ($x = 0; $x < $strLen; ++$x) {
            // Grab the next character in the string
            $char = \mb_substr($jsonString, $x, 1);

            if ($char == '"' && $prevChar != '\\') {
                // Are we inside a quoted string?
                $outOfQuotes = !$outOfQuotes;
            } elseif (($char == '}' || $char == ']') && $outOfQuotes) {
                // If this character is the end of an element,
                // output a new line and indent the next line
                $result .= $newLine;
                $pos--;

                for ($n = 0; $n < $pos; ++$n) {
                    $result .= $this->_indentationString;
                }
            } elseif ($outOfQuotes && false !== \mb_strpos(" \t\r\n", $char)) {
                // Eat all non-essential whitespace in the input as we do our
                // own here and it would only mess up our process
                continue;
            }

            // Add the character to the result string
            $result .= $char;

            // Always add a space after a field colon:
            if ($char == ':' && $outOfQuotes) {
                $result .= ' ';
            }

            // If the last character was the beginning of an element,
            // output a new line and indent the next line
            if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
                $result .= $newLine;

                if ($char == '{' || $char == '[') {
                    $pos++;
                }

                for ($n = 0; $n < $pos; ++$n) {
                    $result .= $this->_indentationString;
                }
            }

            $prevChar = $char;
        }

        return $result;
    }
}
