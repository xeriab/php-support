<?php

declare(strict_types=1);

namespace Exen\Support\Json;

use InvalidArgumentException;
use const PHP_VERSION_ID;

class JsonEncoder
{
    protected $depth = 512;

    protected $options = 0;

    protected $error;

    public function __construct(int $options = 0, int $depth = 512)
    {
        $this->depth = $depth;
        $this->options = $options;
        $this->error = new JsonError();
    }

    /**
     * Returns the JSON representation of a value.
     *
     * @see http://php.net/manual/en/function.json-encode.php
     *
     * @param mixed $value The value being encoded.
     *                     Can be any type except a resource.
     *
     * @return string
     */
    public function encode($value = null): string
    {
        if (is_resource($value)) {
            throw new InvalidArgumentException('Value must not be a resource.', 500);
        }

        $encoded = \json_encode(
            $value,
            $this->options,
            $this->depth
        );

        if (false === $this->error->hasError()) {
            if (PHP_VERSION_ID < 70100) {
                $encoded = \str_replace(["\xe2\x80\xa8", "\xe2\x80\xa9"], ['\u2028', '\u2029'], $encoded);
            }

            return $encoded;
        }

        $this->error->throwException();
    }
}
