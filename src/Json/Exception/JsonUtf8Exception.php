<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use InvalidArgumentException;
use Exception;

class JsonUtf8Exception extends InvalidArgumentException implements JsonException
{
    public function __construct(
        $message = 'Malformed UTF-8 characters, possibly incorrectly encoded.',
        $code = 5,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
