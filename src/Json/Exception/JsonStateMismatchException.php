<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use UnexpectedValueException;
use Exception;

class JsonStateMismatchException extends UnexpectedValueException implements JsonException
{
    public function __construct(
        $message = 'Invalid or malformed JSON.',
        $code = 2,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
