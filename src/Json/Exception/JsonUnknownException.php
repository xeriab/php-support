<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use RuntimeException;
use Exception;

class JsonUnknownException extends RuntimeException implements JsonException
{
    public function __construct(
        $message = 'Unknown error.',
        $code = 0,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
