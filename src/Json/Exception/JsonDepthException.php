<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use LengthException;
use Exception;

class JsonDepthException extends LengthException implements JsonException
{
    public function __construct(
        $message = 'The maximum stack depth has been exceeded.',
        $code = 1,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
