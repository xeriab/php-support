<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use UnexpectedValueException;
use Exception;

class JsonControlCharacterException extends UnexpectedValueException implements JsonException
{
    public function __construct(
        $message = 'Control character error, possibly incorrectly encoded.',
        $code = 3,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
