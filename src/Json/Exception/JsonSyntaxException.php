<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use RuntimeException;
use Exception;

class JsonSyntaxException extends RuntimeException implements JsonException
{
    public function __construct(
        $message = 'Syntax error.',
        $code = 4,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
