<?php

declare(strict_types=1);

namespace Exen\Support\Json\Exception;

use RuntimeException;
use Exception;

class JsonRecursionException extends RuntimeException implements JsonException
{
    public function __construct(
        $message = 'One or more recursive references in the value to be encoded.',
        $code = 6,
        Exception $previous = null
    ) {
        parent::__construct(
            $message,
            $code,
            $previous
        );
    }
}
