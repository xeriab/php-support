<?php

declare(strict_types=1);

namespace Exen\Support\Json;

class JsonArrayDecoder extends Decoder
{
    const ASSOCIATIVE = true;
}
