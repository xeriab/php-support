<?php

declare(strict_types=1);

namespace Exen\Support\Json;

class JsonObjectDecoder extends Decoder
{
    const ASSOCIATIVE = false;
}
