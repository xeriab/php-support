<?php

declare(strict_types=1);

namespace Exen\Support\Json;

class JsonError
{
    private const EXCEPTIONS = [
        \JSON_ERROR_CTRL_CHAR        => 'JsonControlCharacterException',
        \JSON_ERROR_DEPTH            => 'JsonDepthException',
        \JSON_ERROR_INF_OR_NAN       => 'JsonInfOrNanException',
        \JSON_ERROR_RECURSION        => 'JsonRecursionException',
        \JSON_ERROR_STATE_MISMATCH   => 'JsonStateMismatchException',
        \JSON_ERROR_SYNTAX           => 'JsonSyntaxException',
        \JSON_ERROR_UNSUPPORTED_TYPE => 'JsonUnsupportedTypeException',
        \JSON_ERROR_UTF8             => 'JsonUtf8Exception',
    ];

    private $error;

    /**
     * hasError.
     *
     * @return bool
     */
    public function hasError()
    {
        return \JSON_ERROR_NONE !== $this->getError();
    }

    /**
     * Gets last json error.
     *
     * @return int
     */
    public function getError()
    {
        return $this->error ?: ($this->error = \json_last_error());
    }

    public function throwException()
    {
        $exception = \array_key_exists($this->getError(), static::EXCEPTIONS)
            ? __NAMESPACE__.'\\Exception\\'.static::EXCEPTIONS[$this->getError()]
            : __NAMESPACE__.'\\Exception\\JsonUnknownException';

        throw new $exception();
    }
}
