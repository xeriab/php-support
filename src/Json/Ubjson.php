<?php

declare(strict_types=1);

namespace Exen\Support\Json;

use Exen\Support\Interfaces\Arrayable;

class Ubjson
{
    const TYPE_ARRAY  = 0;
    const TYPE_OBJECT = 1;

    //
    // Internal Constants
    //
    const DATA  = 1;
    const EOF   = 0;

    const CHAR   = 'C';
    const DOUBLE = 'D';
    const FALSE  = 'F';
    const FLOAT  = 'd';
    const INT16  = 'I';
    const INT32  = 'l';
    const INT64  = 'L';
    const INT8   = 'i';
    const NOOP   = 'N';
    const NULL   = 'Z';
    const STRING = 'S';
    const TRUE   = 'T';
    const UINT8  = 'U';

    const ARRAY_CLOSE    = ']';
    const ARRAY_OPEN     = '[';
    const HIGH_PRECISION = 'H';
    const OBJECT_CLOSE   = '}';
    const OBJECT_OPEN    = '{';

    protected $decodeType = self::TYPE_ARRAY;

    protected $offset       = 0;
    protected $source       = null;
    protected $sourceLength = null;

    protected $token = self::EOF;
    protected $tokenValue = null;

    /**
     * Constructor
     *
     * @param mixed $source Source.
     */
    protected function __construct($source = null)
    {
        $this->source = $source;

        if (\is_string($this->source)) {
            $this->sourceLength = \strlen($this->source);
        }
    }

    /**
     * Encode data into ubson format
     *
     * @param  mixed $value
     * @return string
     */
    public static function encode($value = null): string
    {
        $ubjson = new self($value);

        return $ubjson->encodeValue($value);
    }

    /**
     * @param  mixed $value
     * @return string
     */
    protected function encodeValue(&$value = null): string
    {
        $result = null;

        if (\is_object($value)) {
            $result = $this->encodeObject($value);
        } elseif (\is_array($value)) {
            $result = $this->encodeArray($value);
        } elseif (\is_int($value) || \is_float($value)) {
            $result = $this->encodeNumeric($value);
        } elseif (\is_string($value)) {
            $result = $this->encodeString($value);
        } elseif ($value === null) {
            $result = self::NULL;
        } elseif (\is_bool($value)) {
            $result = $value ? self::TRUE : self::FALSE;
        }

        return $result;
    }

    /**
     * @param array $array
     * @return string
     */
    protected function encodeArray(&$array = null): string
    {
        if (!empty($array) && (\array_keys($array) !== \range(0, \count($array) - 1))) {
            // associative array
            $result = self::OBJECT_OPEN;

            foreach ($array as $key => $value) {
                $key = (string) $key;
                $result .= $this->encodeString($key) . $this->encodeValue($value);
            }

            $result .= self::OBJECT_CLOSE;
        } else {
            // indexed array
            $result = self::ARRAY_OPEN;
            $length = \count($array);

            for ($i = 0; $i < $length; $i++) {
                $result .= $this->encodeValue($array[$i]);
            }

            $result .= self::ARRAY_CLOSE;
        }

        return $result;
    }

    /**
     * @param stdClass $object
     * @return string
     */
    protected function encodeObject(&$object = null): string
    {
        if ($object instanceof \Iterator) {
            $propCollection = (array) $object;
        } elseif ($object instanceof Arrayable) {
            $propCollection = $object->toArray();
        } else {
            $propCollection = \get_object_vars($object);
        }

        return $this->encodeArray($propCollection);
    }

    /**
     * @param string $string
     * @return string
     */
    protected function encodeString(&$string = null): string
    {
        $result = null;

        $len = \strlen($string);

        if ($len == 1) {
            $result = $prefix = self::CHAR . $string;
        } else {
            $prefix = self::STRING;

            if (\preg_match('/^[\d]+(:?\.[\d]+)?$/', $string)) {
                $prefix = self::HIGH_PRECISION;
            }

            $result = $prefix . $this->encodeNumeric(\strlen($string)) . $string;
        }

        return $result;
    }

    /**
     * @param int|float $numeric
     * @return string
     */
    protected function encodeNumeric(&$numeric = null): string
    {
        $result = null;

        if (\is_int($numeric)) {
            if (256 > $numeric) {
                if (0 < $numeric) {
                    $result = self::UINT8 . \pack('C', $numeric);
                } else {
                    $result = self::INT8 . \pack('c', $numeric);
                }
            } elseif (32768 > $numeric) {
                $result = self::INT16 . \pack('s', $numeric);
            } elseif (2147483648 > $numeric) {
                $result = self::INT32 . \pack('l', $numeric);
            }
        } elseif (\is_float($numeric)) {
            $result = self::FLOAT . \pack('f', $numeric);
        }

        return $result;
    }

    /**
     * decode ubjson format
     *
     * @param string $source
     * @param int $decodeType
     * @return mixed
     */
    public static function decode($source = null, int $decodeType = self::TYPE_ARRAY)
    {
        $ubjson = new static($source);
        $ubjson->setDecodeType($decodeType);
        $ubjson->getNextToken();

        return $ubjson->decodeValue();
    }

    public function setDecodeType(int $decodeType = self::TYPE_ARRAY)
    {
        $this->decodeType = $decodeType;
    }

    /**
     * Decode string
     *
     * @return mixed
     */
    protected function decodeValue()
    {
        $result = null;

        switch ($this->token) {
            case self::DATA:
                $result = $this->tokenValue;
                $this->getNextToken();
                break;
            case self::ARRAY_OPEN:
            case self::OBJECT_OPEN:
                $result = $this->decodeStruct();
                break;
            default:
                $result = null;
        }

        return $result;
    }

    /**
     * Get ubjson token and extract data
     *
     * @return string
     */
    protected function getNextToken()
    {
        $this->token = self::EOF;
        $this->tokenValue = null;

        if ($this->offset >= $this->sourceLength) {
            return $this->token;
        }

        $val = null;
        ++$this->offset;
        $token = $this->source{
        $this->offset - 1};
        $this->token = self::DATA;

        switch ($token) {
            case self::INT8:
                list(, $this->tokenValue) = \unpack('c', $this->read(1));
                break;
            case self::UINT8:
                list(, $this->tokenValue) = \unpack('C', $this->read(1));
                break;
            case self::INT16:
                list(, $this->tokenValue) = \unpack('s', $this->read(2));
                break;
            case self::INT32:
                list(, $this->tokenValue) = \unpack('l', $this->read(4));
                break;
                // case self::INT64:
                //     //unsupported
                //     break;
            case self::FLOAT:
                list(, $this->tokenValue) = \unpack('f', $this->read(4));
                break;
                // case self::DOUBLE:
                //     //unsupported
                //     break;
            case self::TRUE:
                $this->tokenValue = true;
                break;
            case self::FALSE:
                $this->tokenValue = false;
                break;
            case self::NULL:
                $this->tokenValue = null;
                break;
            case self::CHAR:
                $this->tokenValue = $this->read(1);
                break;
                // case self::NOOP:
                //     $this->tokenValue = null;
                //     break;
            case self::STRING:
            case self::HIGH_PRECISION:
                ++$this->offset;
                $len = 0;

                switch ($this->source{
                $this->offset - 1}) {
                    case self::INT8:
                        list(, $len) = \unpack('c', $this->read(1));
                        break;
                    case self::UINT8:
                        list(, $len) = \unpack('C', $this->read(1));
                        break;
                    case self::INT16:
                        list(, $len) = \unpack('s', $this->read(2));
                        break;
                    case self::INT32:
                        list(, $len) = \unpack('l', $this->read(4));
                        break;
                    default:
                        //unsupported
                        $this->token = null;
                }

                $this->tokenValue = '';

                if ($len) {
                    $this->tokenValue = $this->read($len);
                }

                break;
            case self::OBJECT_OPEN:
                $this->token = self::OBJECT_OPEN;
                break;
            case self::OBJECT_CLOSE:
                $this->token = self::OBJECT_CLOSE;
                break;
            case self::ARRAY_OPEN:
                $this->token = self::ARRAY_OPEN;
                break;
            case self::ARRAY_CLOSE:
                $this->token = self::ARRAY_CLOSE;
                break;
            default:
                $this->token = self::EOF;
        }

        return $this->token;
    }

    /**
     * Decode combined data from source
     *
     * @return stdClass|array
     */
    protected function decodeStruct()
    {
        $key = 0;
        $tokenOpen = $this->token;

        if ($tokenOpen == self::OBJECT_OPEN && $this->decodeType == self::TYPE_OBJECT) {
            $result = new \stdClass();
        } else {
            $result = [];
        }

        $structEnd = [self::OBJECT_CLOSE, self::ARRAY_CLOSE];

        $tokenCurrent = $this->getNextToken();

        while ($tokenCurrent && !\in_array($tokenCurrent, $structEnd)) {
            if ($tokenOpen == self::OBJECT_OPEN) {
                $key = $this->tokenValue;
                $tokenCurrent = $this->getNextToken();
            } else {
                ++$key;
            }

            $value = $this->decodeValue();
            $tokenCurrent = $this->token;

            if ($tokenOpen == self::OBJECT_OPEN && $this->decodeType == self::TYPE_OBJECT) {
                $result->$key = $value;
            } else {
                $result[$key] = $value;
            }

            if (\in_array($tokenCurrent, $structEnd)) {
                break;
            }
        }

        $this->getNextToken();

        return $result;
    }

    /**
     * Read N bytes from source string
     *
     * @param  int $bytes
     * @return mixed
     */
    protected function read($bytes = 1)
    {
        $result = \substr($this->source, $this->offset, $bytes);
        $this->offset += $bytes;

        return $result;
    }
}
