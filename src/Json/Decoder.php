<?php

declare(strict_types=1);

namespace Exen\Support\Json;

use const PHP_INT_SIZE;
use const JSON_BIGINT_AS_STRING;
use const PHP_VERSION_ID;

abstract class Decoder
{
    const ASSOCIATIVE = false;

    protected $depth = 512;

    protected $options = 0;

    protected $error;

    public function __construct(int $depth = 512, int $options = 0)
    {
        $this->depth = $depth;
        $this->options = $options;
        $this->error = new JsonError();
    }

    public function decode(string $string = null)
    {
        if (PHP_VERSION_ID < 70000) {
            if ('' === $string) {
                throw new \Exen\Support\Exception\JsonException('Syntax error');
            } elseif (!static::ASSOCIATIVE && \preg_match('#(?<=[^\\]")\\u0000(?:[^"\\]|\\.)*+"\s*+:#', $string)) {
                throw new \Exen\Support\Exception\JsonException('The decoded property name is invalid'); // fatal error when object key starts with \u0000
            } elseif (defined('JSON_C_VERSION') && !\preg_match('##u', $string)) {
                throw new \Exen\Support\Exception\JsonException('Invalid UTF-8 sequence', 5);
            } elseif (defined('JSON_C_VERSION') && \PHP_INT_SIZE === 8) {
                // Not implemented in PECL JSON-C 1.3.2 for 64bit systems
                $this->options &= ~\JSON_BIGINT_AS_STRING;
            }
        }

        $decoded = \json_decode(
            $string,
            static::ASSOCIATIVE,
            $this->depth,
            $this->options
        );

        if (false === $this->error->hasError()) {
            return $decoded;
        }

        $this->error->throwException();
    }
}
