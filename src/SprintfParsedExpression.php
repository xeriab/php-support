<?php

declare(strict_types = 1);

namespace Exen\Support;

/**
 * SprintfParsedExpression class.
 *
 * @since 0.1
 */
class SprintfParsedExpression
{
    /**
     * @var string
     */
    private $parsedFormat;

    /**
     * @var array
     */
    private $parameterMap;

    /**
     * @param string $parsedFormat
     * @param array $parameterMap
     */
    public function __construct($parsedFormat, array $parameterMap)
    {
        $this->parsedFormat = $parsedFormat;
        $this->parameterMap = $parameterMap;
    }

    /**
     * @param array $parameters
     * @param callable|null $middleware
     * @return string
     * @throws Exception
     */
    public function format(array $parameters, ?callable $middleware = null)
    {
        if (!$middleware) {
            $middleware = function ($name, callable $values, array $options) {
                return $values($name);
            };
        }

        $callback = function ($paramName) use ($parameters) {
            if (!\array_key_exists($paramName, $parameters)) {
                throw new \Exception(
                    "The '{$paramName}' parameter was in the format string but was not provided"
                );
            }

            return $parameters[$paramName];
        };

        $parsedParameters = [];

        foreach ($this->parameterMap as $paramMapping) {
            $paramName = $paramMapping['name'];
            $parsedParameters[] = \call_user_func(
                $middleware,
                $paramName,
                $callback,
                []
            );
        }

        return \vsprintf($this->parsedFormat, $parsedParameters);
    }
}
