<?php

declare(strict_types=1);

namespace Exen\Support;

/**
 * Class for various PHP Utilities.
 *
 * Just an alias for \Exen\Support\Utils class.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
final class Util extends Utils
{
    //...
}
