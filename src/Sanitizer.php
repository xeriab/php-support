<?php

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use Exen\Support\Traits\MacroableTrait;

/**
 * Class Sanitizer.
 */
class Sanitizer
{
    use MacroableTrait;

    /**
     * Entities the Multi bytes deep string.
     *
     * @param mixed $mixed  the string to detect multi bytes
     * @param bool  $entity true if want to entity the output
     *
     * @return mixed
     */
    public static function multiByteEntities($mixed, $entity = false)
    {
        static $hasIconV;
        static $limit;

        if (!isset($hasIconV)) {
            // safe resource check
            $hasIconV = \function_exists('iconv');
        }

        if (!isset($limit)) {
            $limit = @\ini_get('pcre.backtrack_limit');
            $limit = !\is_numeric($limit) ? 4096 : \abs($limit);
            // minimum regex is 512 byte
            $limit = $limit < 512 ? 512 : $limit;
            // limit into 40 KB
            $limit = $limit > 40960 ? 40960 : $limit;
        }

        if (!$hasIconV && !$entity) {
            return $mixed;
        }

        if (\is_array($mixed)) {
            foreach ($mixed as $key => $value) {
                $mixed[$key] = self::multiByteEntities($value, $entity);
            }
        } elseif (\is_object($mixed)) {
            foreach (\get_object_vars($mixed) as $key => $value) {
                $mixed->{$key} = self::multiByteEntities($value, $entity);
            }
            /**
             * Work Safe with Parse @uses @var $limit Bit
             * | 4KB data split for regex callback & safe memory usage
             * that maybe fail on very long string
             */
        } elseif (\strlen($mixed) > $limit) {
            return \implode('', self::multiByteEntities(\str_split($mixed, $limit), $entity));
        }

        if ($entity) {
            $mixed = \htmlentities(\html_entity_decode($mixed));
        }

        return $hasIconV
            ? (
                \preg_replace_callback(
                '/[\x{80}-\x{10FFFF}]/u',
                function ($match) {
                    $char = \current($match);
                    $utf = \iconv('UTF-8', 'UCS-4//IGNORE', $char);

                    return \sprintf('&#x%s;', \ltrim(\strtolower(\bin2hex($utf)), '0'));
                },
                $mixed
            ) ?: $mixed
            ): $mixed;
    }

    /* --------------------------------------------------------------------------------*
     |                              Serialize Helper                                   |
     |                                                                                 |
     | Custom From WordPress Core wp-includes/functions.php                            |
     |---------------------------------------------------------------------------------|
     */

    /**
     * Check value to find if it was serialized.
     * If $data is not an string, then returned value will always be false.
     * Serialized data is always a string.
     *
     * @param mixed $data   value to check to see if was serialized
     * @param bool  $strict Optional. Whether to be strict about the end of the string. Defaults true.
     *
     * @return bool false if not serialized and true if it was
     */
    public static function isSerialized($data, $strict = true)
    {
        /* if it isn't a string, it isn't serialized
         ------------------------------------------- */
        if (!\is_string($data) || '' == \trim($data)) {
            return false;
        }

        $data = \trim($data);

        // null && boolean
        if ('N;' == $data || 'b:0;' == $data || 'b:1;' == $data) {
            return true;
        }

        if (\strlen($data) < 4 || ':' !== $data[1]) {
            return false;
        }

        if ($strict) {
            $last_char = \substr($data, -1);
            if (';' !== $last_char && '}' !== $last_char) {
                return false;
            }
        } else {
            $semicolon = \strpos($data, ';');
            $brace = \strpos($data, '}');

            // Either ; or } must exist.
            if (false === $semicolon && false === $brace
                || false !== $semicolon && $semicolon < 3
                || false !== $brace && $brace < 4
            ) {
                return false;
            }
        }

        $token = $data[0];

        switch ($token) {
            /*
             * @noinspection PhpMissingBreakStatementInspection
             */
            case 's':
                if ($strict) {
                    if ('"' !== \substr($data, -2, 1)) {
                        return false;
                    }
                } elseif (false === \strpos($data, '"')) {
                    return false;
                }
                // or else fall through
                // no break
            case 'a':
            case 'O':
                return (bool) \preg_match("/^{$token}:[0-9]+:/s", $data);
            case 'i':
            case 'd':
                $end = $strict ? '$' : '';

                return (bool) \preg_match("/^{$token}:[0-9.E-]+;$end/", $data);
        }

        return false;
    }

    /**
     * Un-serialize value only if it was serialized.
     *
     * @param string $original maybe un-serialized original, if is needed
     *
     * @return mixed un-serialized data can be any type
     */
    public static function maybeUnserialize($original)
    {
        if (!\is_string($original) || '' == \trim($original)) {
            return $original;
        }

        /*
         * Check if serialized
         * check with trim
         */
        if (self::isSerialized($original)) {
            /*
             * use trim if possible
             * Serialized value could not start & end with white space
             */
            return @\unserialize(\trim($original));
        }

        return $original;
    }

    /**
     * Serialize data, if needed. @uses for ( un-compress serialize values )
     * This method to use safe as save data on database. Value that has been
     * Serialized will be double serialize to make sure data is stored as original.
     *
     * @param mixed $data data that might be serialized
     *
     * @return mixed A scalar data
     */
    public static function maybeSerialize($data)
    {
        if (\is_array($data) || \is_object($data)) {
            return @\serialize($data);
        }

        // Double serialization is required for backward compatibility.
        if (self::isSerialized($data, false)) {
            return \serialize($data);
        }

        return $data;
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeURL($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_URL);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeEmail($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_EMAIL);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeInteger($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeFloat($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_NUMBER_FLOAT, \FILTER_FLAG_ALLOW_FRACTION);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeTitle($value = null)
    {
        return \trim(\filter_var($value, \FILTER_SANITIZE_STRING, \FILTER_FLAG_STRIP_LOW | \FILTER_FLAG_NO_ENCODE_QUOTES));
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeString($value = null)
    {
        return \trim(\filter_var($value, \FILTER_SANITIZE_STRING, \FILTER_FLAG_STRIP_LOW | \FILTER_FLAG_NO_ENCODE_QUOTES));
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeSlug($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_STRING, \FILTER_FLAG_STRIP_LOW | \FILTER_FLAG_STRIP_HIGH | \FILTER_FLAG_NO_ENCODE_QUOTES);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeLocation($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_STRING, \FILTER_FLAG_NO_ENCODE_QUOTES);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function sanitizeDisplayName($value = null)
    {
        return \filter_var($value, \FILTER_SANITIZE_STRING, \FILTER_FLAG_NO_ENCODE_QUOTES);
    }

    /**
     *
     * @return integer
     *
     * @since 0.1
     */
    public static function integer($value = null)
    {
        return \filter_var($value, \FILTER_VALIDATE_INT);
    }

    /**
     *
     * @return float
     *
     * @since 0.1
     */
    public static function float($value = null)
    {
        return \filter_var($value, \FILTER_VALIDATE_FLOAT);
    }

    /**
     *
     * @return string
     *
     * @since 0.1
     */
    public static function boolean($value = null)
    {
        return \filter_var($value, \FILTER_VALIDATE_BOOLEAN, \FILTER_NULL_ON_FAILURE);
    }

    /**
     * Sanitize Key.
     *
     * @param string $keyName
     *
     * @return bool|string
     */
    public static function sanitizeKeyName($keyName = null)
    {
        return \is_string($keyName) && '' != \trim($keyName)
            ? \trim($keyName)
            : false;
    }

    /**
     * Instantiate this class.
     *
     * @return self
     */
    public static function instance(): self
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * Constructor.
     *
     * @throws \LogicException
     */
    protected function __construct()
    {
    }

    /**
     * Clone.
     *
     * @return self
     * @throws \LogicException
     */
    public function __clone()
    {
        throw new LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }
}
