<?php

declare(strict_types=1);

namespace Exen\Support;

use php_user_filter;

use Exen\Support\Traits\MacroableTrait;

final class Utf8Filter extends php_user_filter
{
    use MacroableTrait;

    /**
     *
     * @param $in
     * @param $out
     * @param $consumed
     * @param $closing
     *
     * @return int
     *
     * @link http://stackoverflow.com/a/3466609/372654
     */
    public function filter($in, $out, &$consumed, $closing)
    {
        while ($bucket = \stream_bucket_make_writeable($in)) {
            $bucket->data = \preg_replace(
                '/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u',
                '',
                $bucket->data
            );
            $consumed += $bucket->datalen;
            \stream_bucket_append($out, $bucket);
        }

        return \PSFS_PASS_ON;
    }
}
