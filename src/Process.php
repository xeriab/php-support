<?php

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use Exen\Support\Traits\MacroableTrait;

/**
 * Process class.
 *
 * Process is a bunch of utility methods.
 *
 * This class was originally copied from Symfony 3.
 *
 * @since 0.1.1
 */
class Process
{
    use MacroableTrait;

    /**
     * Escapes a string to be used as a shell argument.
     *
     * @param  string  $argument
     *
     * @return string
     */
    public static function escapeArgument(?string $argument = null): string
    {
        // Fix for PHP bug #43784 escapeshellarg removes % from given string
        // Fix for PHP bug #49446 escapeshellarg doesn't work on Windows
        // @see https://bugs.php.net/bug.php?id=43784
        // @see https://bugs.php.net/bug.php?id=49446
        if ('\\' === \DIRECTORY_SEPARATOR) {
            if ('' === $argument) {
                return '""';
            }

            $escapedArgument = '';
            $quote = false;

            foreach (\preg_split('/(")/', $argument, -1, \PREG_SPLIT_NO_EMPTY | \PREG_SPLIT_DELIM_CAPTURE) as $part) {
                if ('"' === $part) {
                    $escapedArgument .= '\\"';
                } elseif (self::isSurroundedBy($part, '%')) {
                    // Avoid environment variable expansion
                    $escapedArgument .= '^%"'.\substr($part, 1, -1).'"^%';
                } else {
                    // escape trailing backslash
                    if ('\\' === \substr($part, -1)) {
                        $part .= '\\';
                    }

                    $quote = true;
                    $escapedArgument .= $part;
                }
            }

            if ($quote) {
                $escapedArgument = '"'.$escapedArgument.'"';
            }

            return $escapedArgument;
        }

        return "'".\str_replace("'", "'\\''", $argument)."'";
    }

    /**
     * Is the given string surrounded by the given character?
     *
     * @param  string  $argument
     * @param  string  $char
     *
     * @return bool
     */
    protected static function isSurroundedBy(?string $argument = null, ?string $char = null): bool
    {
        return 2 < \strlen($argument) && $char === $argument[0] && $char === $argument[\strlen($argument) - 1];
    }

    /**
     * Instantiate this class.
     *
     * @return self
     */
    public static function instance(): self
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new static();
        }

        return $instance;
    }

    /**
     * Constructor.
     *
     * @throws \LogicException
     */
    protected function __construct()
    {
    }

    /**
     * Clone.
     *
     * @return self
     * @throws \LogicException
     */
    public function __clone()
    {
        throw new LogicException(
            'Class ' . \get_class($this) .
            ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }
}
