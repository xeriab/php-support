<?php

declare(strict_types=1);

namespace Exen\Support\Inflections;

use Exen\Support\Inflections;

// @codeCoverageIgnoreStart

/**
 * Norwegian Bokmal inflections.
 *
 * @param Inflections $inflect
 */
return function (Inflections $inflect) {
    $inflect
        ->plural('/$/', 'er')
        ->plural('/r$/i', 're')
        ->plural('/e$/i', 'er')

        ->singular('/er$/i', '')
        ->singular('/re$/i', 'r')

        ->irregular('konto', 'konti')

        ->uncountable(explode(' ', 'barn fjell hus'));
};

// @codeCoverageIgnoreEnd
