<?php

declare(strict_types=1);

namespace Exen\Support\Inflections;

use Exen\Support\Inflections;

// @codeCoverageIgnoreStart

/**
 * Turkish inflections.
 *
 * @param Inflections $inflect
 */
return function (Inflections $inflect) {
    $inflect
        ->plural('/([aoıu][^aoıueöiü]{0,6})$/u', '\1lar')
        ->plural('/([eöiü][^aoıueöiü]{0,6})$/u', '\1ler')

        ->singular('/l[ae]r$/i', '')

        ->irregular('ben', 'biz')
        ->irregular('sen', 'siz')
        ->irregular('o', 'onlar');
};

// @codeCoverageIgnoreEnd
