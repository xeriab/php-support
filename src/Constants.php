<?php /** @noinspection PhpUnused */
/** @noinspection SpellCheckingInspection */

declare(strict_types=1);

// Defines //////////////////////////////////////////////////////////////////

// Is the current PHP instance a CLI?
if (!defined('_IS_CLI_')) {
    define('_IS_CLI_', (PHP_SAPI === 'cli' or defined('STDIN')));
}

// Is the current PHP instance a native server?
if (!defined('_IS_CLI_SERVER_')) {
    define('_IS_CLI_SERVER_', (PHP_SAPI === 'cli-server'));
}

// Is the current PHP instance a CGI?
if (!defined('_IS_CGI_')) {
    define('_IS_CGI_', 'cgi' === substr(php_sapi_name(), 0, 3));
}

// Is it HTML output?
if (!defined('_IS_HTML_')) {
    define('_IS_HTML_', (!_IS_CLI_SERVER_ || !_IS_CLI_));
}

// Gets system endianness.
if (!defined('_ENDIANNESS_')) {
    define('_ENDIANNESS_', (pack("s", 1) === "\0\1" ? 0x00 : 0x01));
}

// Determine whether the current environment is Windows based.
if (!defined('_IS_WIN_')) {
    define('_IS_WIN_', ('win' === strtolower(substr(PHP_OS, 0, 3))));
}

// Determine whether the current environment is Linux based.
if (!defined('_IS_LINUX_')) {
    define('_IS_LINUX_', ('linux' === strtolower(substr(PHP_OS, 0, 5))));
}

// Directory separator.
if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

// Directory separator.
if (!defined('DIR_SEP')) {
    define('DIR_SEP', DS);
}

// Path separator.
if (!defined('PS')) {
    define('PS', PATH_SEPARATOR);
}

if (_IS_WIN_) {
    define('_POSIX_DIR_SEP_', '/');
} else {
    define('_POSIX_DIR_SEP_', DIRECTORY_SEPARATOR);
}

// Constants //////////////////////////////////////////////////////////////////

// const DEBUG_DUMP_AS_STRING = false;
// const DEBUG_MODE_ENABLED   = false;
// const DEBUG_MODE_FILE_INFO = false;
// const DEBUG_MODE_TIMESTAMP = false;
// const DEBUG_MODE_TITLE     = 'DEBUG';
const DOUBLE_DOTS          = '..';
const DQS                  = '"';
const EMPTY_STRING         = '';
const ENDIANNESS           = _ENDIANNESS_;
const ENDL                 = "\n";
const EOL                  = PHP_EOL;
const IS_CGI               = _IS_CGI_;
const IS_CLI               = _IS_CLI_;
const IS_CLI_SERVER        = _IS_CLI_SERVER_;
const IS_HTML              = _IS_HTML_;
const IS_LINUX             = _IS_LINUX_;
const IS_WIN               = _IS_WIN_;
const NULL_CHAR            = "\0";
const TAB_CHAR             = "\t";
const PATH_SEP             = PS;
const POSIX_DIR_SEP        = _POSIX_DIR_SEP_;
const SINGLE_DOT           = '.';
const ASTERISK             = '*';
const SPACE_CHAR           = ' ';
const WHITE_SPACE          = "\s";
const SQS                  = '\'';
const ZERO                 = 0;
const UNDERSCORE           = '_';
const DASH                 = '-';

if (!defined('NIL')) {
    define('NIL', null);
}

/**
 * Minute in seconds.
 */
const MINUTE = 60;

/**
 * Hour in seconds.
 */
const HOUR = 60 * MINUTE;

/**
 * Day in seconds.
 */
const DAY = 24 * HOUR;

/**
 * Week in seconds.
 */
const WEEK = 7 * DAY;

/**
 * Average month in seconds.
 */
const MONTH = 2629800;

/**
 * Average year in seconds.
 */
const YEAR = 31557600;

// const FALSE         = false;
// const TRUE          = true;
// const NULL          = null;

/**
 * Default inflector locale.
 */
const INFLECTOR_DEFAULT_LOCALE = 'en';

/**
 * Default JSON decoding depth.
 */
const JSON_MAX_DEPTH = 1024;
