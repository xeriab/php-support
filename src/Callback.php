<?php

declare(strict_types=1);

namespace Exen\Support;

use ReflectionException;
use Exen\Support\Exception\InvalidArgumentException;
use Exen\Support\Traits\StaticClassTrait;
use Closure;
use ReflectionFunction;
use ReflectionMethod;
use function defined;
use function func_get_args;
use function set_error_handler;
use function sprintf;
use function substr;

/**
 * PHP callable tools.
 */
final class Callback
{
    use StaticClassTrait;

    /**
     * @param null $callable
     * @param string|null $method
     * @return Closure
     * @throws InvalidArgumentException
     * @throws ReflectionException
     */
    public static function closure($callable = null, string $method = null): Closure
    {
        if (null !== $method) {
            $callable = [$callable, $method];
        } elseif (is_string($callable) && 2 === count($tmp = explode('::', $callable))) {
            $callable = $tmp;
        } elseif ($callable instanceof Closure) {
            return $callable;
        } elseif (is_object($callable)) {
            $callable = [$callable, '__invoke'];
        }

        if (is_string($callable) && function_exists($callable)) {
            return (new ReflectionFunction($callable))->getClosure();
        } elseif (is_array($callable) && method_exists($callable[0], $callable[1])) {
            return (new ReflectionMethod($callable[0], $callable[1]))->getClosure($callable[0]);
        }

        $callable = self::check($callable);

        $cb = $callable;

        return function (...$arguments) use ($cb) {
            return $cb(...$arguments);
        };
    }

    /**
     * Invokes callback.
     *
     * @param callable|null $callable
     * @param mixed ...$arguments
     * @return mixed
     *
     * @throws InvalidArgumentException|ReflectionException
     * @deprecated
     */
    public static function invoke(callable $callable = null, ...$arguments)
    {
        $callable = self::check($callable);
        return call_user_func_array($callable, $arguments);
    }

    /**
     * Invokes callback with an array of parameters.
     *
     * @param callable|null $callable
     * @param array $arguments
     * @return mixed
     *
     * @throws InvalidArgumentException|ReflectionException
     * @deprecated
     */
    public static function invokeArgs(callable $callable = null, array $arguments = [])
    {
        $callable = self::check($callable);
        return call_user_func_array($callable, $arguments);
    }

    /** @noinspection PhpUnused */
    public static function invokeArguments(callable $callable = null, array $arguments = [])
    {
        try {
            /** @noinspection PhpDeprecationInspection */
            return self::invokeArgs($callable, $arguments);
        } catch (ReflectionException | InvalidArgumentException $ex) {
            return null;
        }
    }

    /**
     * Invokes internal PHP function with own error handler.
     *
     * @param string|null $callback
     * @param array $arguments
     * @param callable|null $onError
     * @return mixed
     */
    public static function invokeSafe(string $callback = null, array $arguments = [], callable $onError = null)
    {
        $previous = set_error_handler(
            function ($severity, $message, $file) use ($onError, &$previous, $callback) {
                if ('' === $file && defined('HHVM_VERSION')) { // https://github.com/facebook/hhvm/issues/4625
                    $file = func_get_arg(5)[1]['file'];
                }

                if (__FILE__ === $file) {
                    $message = preg_replace("#^$callback\(.*?\): #", '', $message);

                    if (false !== $onError($message, $severity)) {
                        return null;
                    }
                }

                return $previous ? $previous(...func_get_args()): false;
            }
        );

        try {
            return call_user_func_array($callback, $arguments);
        } finally {
            restore_error_handler();
        }
    }

    /**
     * @param null $callable
     * @param bool $syntax
     * @return callable
     * @throws InvalidArgumentException|ReflectionException
     */
    public static function check($callable = null, bool $syntax = false): callable
    {
        if (!is_callable($callable, $syntax)) {
            throw new InvalidArgumentException(
                $syntax
                ? 'Given value is not a callable type.'
                : sprintf("Callback '%s' is not callable.", self::toString($callable))
            );
        }

        return $callable;
    }

    /**
     * @param null $callable
     * @return string
     * @throws ReflectionException
     */
    public static function toString($callable = null): string
    {
        if ($callable instanceof Closure) {
            $inner = self::unwrap($callable);

            return '{closure'.($inner instanceof Closure ? '}' : ' '.self::toString($inner).'}');
        } elseif (is_string($callable) && "\0" === $callable[0]) {
            return '{lambda}';
        } else {
            is_callable($callable, true, $textual);

            return $textual;
        }
    }

    /**
     *
     * @param null $callable
     * @return bool
     */
    public static function isStatic($callable = null): bool
    {
        return is_array($callable) ? is_string($callable[0]): is_string($callable);
    }

    /**
     * Unwraps closure created by Callback::closure().
     *
     * @param Closure|null $closure
     * @return callable
     * @throws ReflectionException
     * @internal
     */
    public static function unwrap(Closure $closure = null): callable
    {
        $refFunction = new ReflectionFunction($closure);

        if ('}' === substr($refFunction->getName(), -1)) {
            $variables = $refFunction->getStaticVariables();
            return isset($variables['_callable_']) ? $variables['_callable_'] : $closure;
        } elseif ($object = $refFunction->getClosureThis()) {
            return [$object, $refFunction->getName()];
        } elseif ($class = $refFunction->getClosureScopeClass()) {
            return [$class->getName(), $refFunction->getName()];
        } else {
            return $refFunction->getName();
        }
    }
}
