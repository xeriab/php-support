<?php

declare(strict_types=1);

use Composer\Autoload\ClassLoader;

/**
 * Attempts to load Composer's autoload.php as either a dependency or a
 * stand-alone package.
 *
 * @return bool
 */
function autoLoader(): bool
{
    $composerFiles = [
        __DIR__ . '/../../../autoload.php',  // Composer dependency
        __DIR__ . '/../vendor/autoload.php', // Stand-Alone package
    ];

    foreach ($composerFiles as $file) {
        if (is_file($file)) {
            $message = sprintf('File "%s" loaded.', realpath($file));
            syslog(SYSLOG_FACILITY | LOG_DEBUG, $message);

            /** @noinspection PhpIncludeInspection */
            @require_once $file;
            // include_once $file;

            return true;
        }
    }

    return false;
}

/** @var ClassLoader $loader */
$loader = require __DIR__ . '/../vendor/autoload.php';

if (file_exists(__DIR__ . '/../packages/autoload.php')) {
    /** @noinspection PhpIncludeInspection */
    $map = require __DIR__ . '/../packages/composer/autoload_namespaces.php';

    foreach ($map as $namespace => $p) {
        $loader->set($namespace, $p);
    }

    /** @noinspection PhpIncludeInspection */
    $map = require __DIR__ . '/../packages/composer/autoload_psr4.php';

    foreach ($map as $namespace => $p) {
        $loader->setPsr4($namespace, $p);
    }

    /** @noinspection PhpIncludeInspection */
    $classMap = require __DIR__ . '/../packages/composer/autoload_classmap.php';

    if ($classMap) {
        $loader->addClassMap($classMap);
    }

    if (file_exists(__DIR__ . '/../packages/composer/autoload_files.php')) {
        /** @noinspection PhpIncludeInspection */
        $includeFiles = require __DIR__ . '/../packages/composer/autoload_files.php';

        foreach ($includeFiles as $file) {
            /** @noinspection PhpIncludeInspection */
            require $file;
        }
    }
}

return [
    'autoLoader' => autoLoader(),
    'loader'     => $loader
];
