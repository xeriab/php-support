<?php

declare(strict_types=1);

use Composer\Autoload\ClassLoader;

/** @var ClassLoader $loader */
$loader = @require_once __DIR__ . '/../vendor/autoload.php';

return $loader;
