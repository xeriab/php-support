<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Xmlable Interface.
 */
interface Xmlable
{
    /**
     * Returns the XML string representation of the serialized object.
     *
     * param int|integer $options XML options/flags.
     */
    public function toXml();
}
