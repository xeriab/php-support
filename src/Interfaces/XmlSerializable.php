<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * XmlSerializable Interface.
 */
interface XmlSerializable
{
    /**
     * Specify data which should be serialized to XML.
     *
     * Note: This method returns data that can be serialized by Xml::encode().
     *
     * @return mixed
     */
    public function xmlSerialize();
}
