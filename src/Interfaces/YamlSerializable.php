<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * YamlSerializable Interface.
 */
interface YamlSerializable
{
    /**
     * Specify data which should be serialized to YAML.
     *
     * Note: This method returns data that can be serialized by Yaml::encode().
     *
     * @return mixed
     */
    public function yamlSerialize();
}
