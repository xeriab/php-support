<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Serializable Interface.
 */
interface Serializable extends \Serializable
{
}
