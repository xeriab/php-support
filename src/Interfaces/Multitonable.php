<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Multitonable Interface.
 */
interface Multitonable
{
    /**
     * Create a new instance if one doesn't exist.
     *
     * Once an instance has been created, or if it was already created,
     * return it.
     *
     * @return null|self
     */
    public static function getInstance(): ?self;

    /**
     * Create a new instance if one doesn't exist.
     *
     * Once an instance has been created, or if it was already created,
     * return it.
     *
     * @return null|self
     */
    public static function instance(): ?self;
}
