<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Htmlable Interface.
 */
interface Htmlable
{
    /**
     * Get the HTML string.
     *
     * @return string
     */
    public function toHtml();

    /**
     * Get the HTML string.
     *
     * @return string
     */
    public function __toString();
}
