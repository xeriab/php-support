<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Arrayable Interface.
 */
interface Arrayable
{
    /**
     * Return a representation of this object as an array.
     *
     * @param array $options Array options/flags.
     * @param int   $depth   - Optional depth parameter. May or may not be used
     *                       by the implementing object
     */
    public function toArray(array $options = null, int $depth = null);
}
