<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Objectable Interface.
 */
interface Objectable
{
    /**
     * Returns the PHP Object representation of the object.
     *
     * @param int|integer $options Options/flags.
     *
     * @access public
     *
     * @return null|string
     */
    public function toObject(int $options);
}
