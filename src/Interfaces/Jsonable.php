<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Jsonable Interface.
 */
interface Jsonable
{
    /**
     * Returns the JSON string representation of the serialized object.
     *
     * param int|integer $options JSON options/flags.
     */
    public function toJson();
}
