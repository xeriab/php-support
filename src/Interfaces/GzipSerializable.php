<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * GzipSerializable Interface.
 */
interface GzipSerializable
{
    /**
     * Specify data which should be serialized to Gzip.
     *
     * @return mixed
     */
    public function gzipSerialize();

    /**
     * Specify data which should be deserialized from Gzip.
     *
     * @return mixed
     */
    public function gzipUnserialize();
}
