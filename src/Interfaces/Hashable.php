<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Hashable Interface.
 */
interface Hashable
{
    /**
     * [equals description]
     * @param  [type] $object [description]
     * @return [type]         [description]
     */
    public function equals($object = null): bool;

    /**
     * [hashCode description]
     * @return [type] [description]
     */
    public function hashCode(): string;

    /**
     * [hashCode description]
     * @return [type] [description]
     */
    public function hash(): string;
}
