<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Yamlable Interface.
 */
interface Yamlable
{
    /**
     * Returns the YAML string representation of the serialized object.
     *
     * param int|integer $options YAML options/flags.
     */
    public function toYaml();
}
