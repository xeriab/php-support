<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * Dumpable Interface.
 */
interface Dumpable
{
    /**
     * Returns string representation of the object.
     *
     * @param int|integer $options Dump options/flags.
     */
    public function dump(int $options);
}
