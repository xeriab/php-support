<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

use Countable;
use ArrayAccess;
use IteratorAggregate;

/**
 * Dottable Interface.
 */
interface Dottable extends
    Arrayable,
    ArrayAccess,
    Countable,
    GzipSerializable,
    Hashable,
    IteratorAggregate,
    Jsonable,
    JsonSerializable,
    Serializable,
    Yamlable,
    YamlSerializable
{
}
