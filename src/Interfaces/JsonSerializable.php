<?php

declare(strict_types=1);

namespace Exen\Support\Interfaces;

/**
 * JsonSerializable Interface.
 */
interface JsonSerializable extends \JsonSerializable
{
}
