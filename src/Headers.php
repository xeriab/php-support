<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use Exen\Support\Traits\MacroableTrait;
use function get_class;
use function header;

/**
 * Class for Headers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
final class Headers
{
    use MacroableTrait;

    /**
     * Sends a 301 MOVED PERMANENTLY header.
     *
     * @return void
     */
    public static function movedPermanently(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 301 MOVED PERMANENTLY',
            true,
            301
        );

        exit();
    }

    /**
     * Sends a 302 FOUND header.
     *
     * @return void
     */
    public static function found(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 302 FOUND',
            true,
            302
        );

        exit();
    }

    /**
     * Sends a 400 BAD REQUEST header.
     *
     * @return void
     */
    public static function badRequest(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 400 BAD REQUEST',
            true,
            400
        );

        exit();
    }

    /**
     * Sends a 401 UNAUTHORIZED header.
     *
     * @return void
     */
    public static function unauthorized(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 401 UNAUTHORIZED',
            true,
            401
        );

        exit();
    }

    /**
     * Sends a 403 FORBIDDEN header.
     *
     * @return void
     */
    public static function forbidden(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 403 FORBIDDEN',
            true,
            403
        );

        exit();
    }

    /**
     * Sends a 404 NOT FOUND header.
     *
     * @return void
     */
    public static function notFound(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 404 NOT FOUND',
            true,
            404
        );

        exit();
    }

    /**
     * Sends a 408 REQUEST TIMEOUT header.
     *
     * @return void
     */
    public static function requestTimeout(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 408 REQUEST TIMEOUT',
            true,
            408
        );

        exit();
    }

    /**
     * Sends a 500 INTERNAL SERVER ERROR header.
     *
     * @return void
     */
    public static function serverError(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 500 INTERNAL SERVER ERROR',
            true,
            500
        );

        exit();
    }

    /**
     * Sends a 501 NOT IMPLEMENTED header.
     *
     * @return void
     */
    public static function notImplemented(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 501 NOT IMPLEMENTED',
            true,
            501
        );

        exit();
    }

    /**
     * Sends a 502 BAD GATEWAY header.
     *
     * @return void
     */
    public static function badGateway(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 502 BAD GATEWAY',
            true,
            502
        );

        exit();
    }

    /**
     * Sends a 503 SERVICE UNAVAILABLE header.
     *
     * @return void
     */
    public static function serviceUnavailable(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 503 SERVICE UNAVAILABLE',
            true,
            503
        );

        exit();
    }

    /**
     * Sends a 504 GATEWAY TIMEOUT header.
     *
     * @return void
     */
    public static function gatewayTimeout(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 504 GATEWAY TIMEOUT',
            true,
            504
        );

        exit();
    }

    /**
     * Sends a 422 Unprocessable Entity header.
     *
     * @return void
     */
    public static function unprocessableEntity(): void
    {
        header(
            $_SERVER['SERVER_PROTOCOL'] . ' 422 Unprocessable Entity',
            true,
            422
        );

        exit();
    }

    /**
     * Sends a Content-Type: application/json header.
     *
     * @return void
     */
    public static function jsonHeader(): void
    {
        header('Content-Type: application/json');
    }

    /**
     * Sends a Content-Type: application/pdf header.
     *
     * @return void
     */
    public static function pdfHeader(): void
    {
        header('Content-Type: application/pdf');
    }

    /**
     * Sends a Content-Type: application/xml header.
     *
     * @return void
     */
    public static function xmlHeader(): void
    {
        header('Content-Type: application/xml');
    }

    /**
     * Sends a Content-Type: application/force-download header.
     *
     * @return void
     */
    public static function downloadHeader(): void
    {
        header('Content-Type: application/force-download');
    }

    /**
     * Sends a Content-Type: text-html header.
     *
     * @return void
     */
    public static function htmlHeader(): void
    {
        header('Content-Type: text/html');
    }

    /**
     * Sends a Content-Type: text/css header.
     *
     * @return void
     */
    public static function cssHeader(): void
    {
        header('Content-Type: text/css');
    }

    /**
     * Sends a Content-Type: text/javascript header.
     *
     * @return void
     */
    public static function jsHeader(): void
    {
        header('Content-Type: text/javascript');
    }

    /**
     * Sends a Content-Type: text/plain header.
     *
     * @return void
     */
    public static function textHeader(): void
    {
        header('Content-Type: text/plain');
    }

    /**
     * Instantiate this class.
     *
     * @return self
     */
    public static function instance(): self
    {
        static $instance = null;

        if ($instance === null) {
            $instance = new Headers();
        }

        return $instance;
    }

    /**
     * Constructor.
     *
     * @throws LogicException
     */
    public function __construct()
    {
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
            ' is singleton and cannot be cloned, use instance() method'
        );
    }

    /**
     * Returns the string representation of this object.
     *
     * @return string
     *
     * @codeCoverageIgnore
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(Headers::class)['classname'],
            ]
        );
    }
}
