<?php

declare(strict_types=1);

namespace Exen\Support\Hashing\Hashids;

use InvalidArgumentException;

/**
 * This is the hashids exception class.
 *
 * @author Vincent Klaiber <hello@doubledip.se>
 */
class HashidsException extends InvalidArgumentException
{
    //
}
