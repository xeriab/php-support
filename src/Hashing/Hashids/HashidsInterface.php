<?php

declare(strict_types=1);

namespace Exen\Support\Hashing\Hashids;

/**
 * This is the hashids interface.
 *
 * @author Ivan Akimov <ivan@barreleye.com>
 * @author Vincent Klaiber <hello@doubledip.se>
 */
interface HashidsInterface
{
    /**
     * Encode parameters to generate a hash.
     *
     * @param integer|string $numbers
     *
     * @return string
     */
    public function encode(...$numbers): string;

    /**
     * Decode a hash to the original parameter values.
     *
     * @param string $hash
     *
     * @return array|integer|string
     */
    public function decode(string $hash);

    /**
     * Encode hexadecimal values and generate a hash string.
     *
     * @param string $str
     *
     * @return string
     */
    public function encodeHex(string $str): string;

    /**
     * Decode a hexadecimal hash.
     *
     * @param string $hash
     *
     * @return string
     */
    public function decodeHex(string $hash): string;
}
