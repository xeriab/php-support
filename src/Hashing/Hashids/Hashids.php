<?php /** @noinspection SpellCheckingInspection */

declare(strict_types=1);

namespace Exen\Support\Hashing\Hashids;

use Exen\Support\Hashing\Hashids\Math\Bc;
use Exen\Support\Hashing\Hashids\Math\Gmp;
use Exen\Support\Hashing\Hashids\Math\MathInterface;
use RuntimeException;
use function array_diff;
use function array_intersect;
use function array_unique;
use function ceil;
use function count;
use function ctype_digit;
use function ctype_xdigit;
use function explode;
use function extension_loaded;
use function hexdec;
use function implode;
use function is_array;
use function is_string;
use function mb_convert_encoding;
use function mb_detect_encoding;
use function mb_ord;
use function mb_strlen;
use function mb_strpos;
use function mb_substr;
use function preg_split;
use function str_replace;
use function trim;

/**
 * This is the hashids class.
 *
 * @author Ivan Akimov <ivan@barreleye.com>
 * @author Vincent Klaiber <hello@doubledip.se>
 * @author Johnson Page <jwpage@gmail.com>
 */
class Hashids implements HashidsInterface
{
    /**
     * The seps divider.
     *
     * @var float
     */
    const SEP_DIV = 3.5;

    /**
     * The guard divider.
     *
     * @var float
     */
    const GUARD_DIV = 12;

    /**
     * The alphabet string.
     *
     * @var string
     */
    protected $alphabet;

    /**
     * Shuffled alphabets, referenced by alphabet and salt.
     *
     * @var array
     */
    protected $shuffledAlphabets;

    /**
     * The seps string.
     *
     * @var string
     */
    protected $seps = 'cfhistuCFHISTU';

    /**
     * The guards string.
     *
     * @var string
     */
    protected $guards;

    /**
     * The minimum hash length.
     *
     * @var integer|null
     */
    protected $minHashLength = null;

    /**
     * The salt string.
     *
     * @var string
     */
    protected $salt = null;

    /**
     * The math class.
     *
     * @var MathInterface
     */
    protected $math;

    /**
     * Create a new hashids instance.
     *
     * @param string|null $salt
     * @param int|null $minHashLength
     * @param string|null $alphabet
     *
     * @noinspection SpellCheckingInspection
     */
    public function __construct(
        ?string $salt = '',
        ?int $minHashLength = 0,
        ?string $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    ) {
        $this->salt = mb_convert_encoding($salt, 'UTF-8', mb_detect_encoding($salt));
        $this->minHashLength = $minHashLength;
        $alphabet = mb_convert_encoding($alphabet, 'UTF-8', mb_detect_encoding($alphabet));
        $this->alphabet = implode('', array_unique($this->multiByteSplit($alphabet)));
        $this->math = $this->getMathExtension();

        if (mb_strlen($this->alphabet) < 16) {
            throw new HashidsException('Alphabet must contain at least 16 unique characters.');
        }

        if (false !== mb_strpos($this->alphabet, ' ')) {
            throw new HashidsException('Alphabet can\'t contain spaces.');
        }

        $alphabetArray = $this->multiByteSplit($this->alphabet);
        $sepsArray = $this->multiByteSplit($this->seps);
        $this->seps = implode('', array_intersect($sepsArray, $alphabetArray));
        $this->alphabet = implode('', array_diff($alphabetArray, $sepsArray));
        $this->seps = $this->shuffle($this->seps, $this->salt);

        if (!$this->seps || (mb_strlen($this->alphabet) / mb_strlen($this->seps)) > self::SEP_DIV) {
            $sepsLength = (int) ceil(mb_strlen($this->alphabet) / self::SEP_DIV);

            if ($sepsLength > mb_strlen($this->seps)) {
                $diff = $sepsLength - mb_strlen($this->seps);
                $this->seps .= mb_substr($this->alphabet, 0, $diff);
                $this->alphabet = mb_substr($this->alphabet, $diff);
            }
        }

        $this->alphabet = $this->shuffle($this->alphabet, $this->salt);
        $guardCount = (int) ceil(mb_strlen($this->alphabet) / self::GUARD_DIV);

        if (mb_strlen($this->alphabet) < 3) {
            $this->guards = mb_substr($this->seps, 0, $guardCount);
            $this->seps = mb_substr($this->seps, $guardCount);
        } else {
            $this->guards = mb_substr($this->alphabet, 0, $guardCount);
            $this->alphabet = mb_substr($this->alphabet, $guardCount);
        }
    }

    /**
     * Encode parameters to generate a hash.
     *
     * @param string|integer $numbers
     *
     * @return string
     */
    public function encode(...$numbers): string
    {
        $retVal = '';

        if (1 === count($numbers) && is_array($numbers[0])) {
            $numbers = $numbers[0];
        }

        if (!$numbers) {
            return $retVal;
        }

        foreach ($numbers as $number) {
            $isNumber = ctype_digit((string) $number);

            if (!$isNumber) {
                return $retVal;
            }
        }

        $alphabet = $this->alphabet;
        $numbersSize = count($numbers);
        $numbersHashInt = 0;

        foreach ($numbers as $x => $number) {
            $numbersHashInt += $this->math->intval($this->math->mod($number, $x + 100));
        }

        $lottery = $retVal = mb_substr($alphabet, $numbersHashInt % mb_strlen($alphabet), 1);
        foreach ($numbers as $x => $number) {
            $alphabet = $this->shuffle(
                $alphabet,
                mb_substr(
                    $lottery . $this->salt . $alphabet,
                    0,
                    mb_strlen($alphabet)
                )
            );

            $retVal .= $last = $this->hash($number, $alphabet);

            if ($x + 1 < $numbersSize) {
                $number %= (mb_ord($last, 'UTF-8') + $x);
                $sepsIndex = $this->math->intval($this->math->mod($number, mb_strlen($this->seps)));
                $retVal .= mb_substr($this->seps, $sepsIndex, 1);
            }
        }

        if (mb_strlen($retVal) < $this->minHashLength) {
            $guardIndex = ($numbersHashInt + mb_ord(mb_substr($retVal, 0, 1), 'UTF-8')) % mb_strlen($this->guards);

            $guard = mb_substr($this->guards, $guardIndex, 1);
            $retVal = $guard . $retVal;

            if (mb_strlen($retVal) < $this->minHashLength) {
                $guardIndex = ($numbersHashInt + mb_ord(mb_substr($retVal, 2, 1), 'UTF-8')) % mb_strlen($this->guards);
                $guard = mb_substr($this->guards, $guardIndex, 1);

                $retVal .= $guard;
            }
        }

        $halfLength = (int) (mb_strlen($alphabet) / 2);

        while (mb_strlen($retVal) < $this->minHashLength) {
            $alphabet = $this->shuffle($alphabet, $alphabet);
            $retVal = mb_substr($alphabet, $halfLength) . $retVal . mb_substr($alphabet, 0, $halfLength);
            $excess = mb_strlen($retVal) - $this->minHashLength;

            if ($excess > 0) {
                $retVal = mb_substr($retVal, (int) ($excess / 2), $this->minHashLength);
            }
        }

        return $retVal;
    }

    /**
     * Decode a hash to the original parameter values.
     *
     * @param string $hash
     *
     * @return array|integer|string
     */
    public function decode(string $hash)
    {
        $retVal = [];

        if (!is_string($hash) || !($hash = trim($hash))) {
            return $retVal;
        }

        $alphabet = $this->alphabet;

        $hashBreakdown = str_replace($this->multiByteSplit($this->guards), ' ', $hash);
        $hashArray = explode(' ', $hashBreakdown);

        $x = 3 === count($hashArray) || 2 === count($hashArray) ? 1 : 0;

        $hashBreakdown = $hashArray[$x];

        if ('' !== $hashBreakdown) {
            $lottery = mb_substr($hashBreakdown, 0, 1);
            $hashBreakdown = mb_substr($hashBreakdown, 1);

            $hashBreakdown = str_replace($this->multiByteSplit($this->seps), ' ', $hashBreakdown);
            $hashArray = explode(' ', $hashBreakdown);

            foreach ($hashArray as $subHash) {
                $alphabet = $this->shuffle(
                    $alphabet,
                    mb_substr(
                        $lottery . $this->salt . $alphabet,
                        0,
                        mb_strlen($alphabet)
                    )
                );

                $result = $this->unhash($subHash, $alphabet);

                if ($this->math->greaterThan($result, PHP_INT_MAX)) {
                    $retVal[] = $this->math->strval($result);
                } else {
                    $retVal[] = $this->math->intval($result);
                }
            }

            if ($this->encode($retVal) !== $hash) {
                $retVal = [];
            }
        }

        if (count($retVal) > 1) {
            return $retVal;
        }

        return $retVal[0];
    }

    /**
     * Encode hexadecimal values and generate a hash string.
     *
     * @param string|integer $str
     *
     * @return string
     */
    public function encodeHex($str): string
    {
        if (!ctype_xdigit((string) $str)) {
            return '';
        }

        $numbers = trim(chunk_split($str, 12, ' '));
        $numbers = explode(' ', $numbers);

        foreach ($numbers as $x => $number) {
            $numbers[$x] = hexdec('1' . $number);
        }

        return $this->encode(...$numbers);
    }

    /**
     * Decode a hexadecimal hash.
     *
     * @param string $hash
     *
     * @return string
     */
    public function decodeHex(string $hash): string
    {
        $ret = '';
        $numbers = $this->decode($hash);

        foreach ($numbers as $x => $number) {
            $ret .= mb_substr(dechex($number), 1);
        }

        return $ret;
    }

    /**
     * Shuffle alphabet by given salt.
     *
     * @param string $alphabet
     * @param string $salt
     *
     * @return string
     */
    protected function shuffle(string $alphabet, string $salt): string
    {
        $key = $alphabet . ' ' . $salt;

        if (isset($this->shuffledAlphabets[$key])) {
            return $this->shuffledAlphabets[$key];
        }

        $saltLength = mb_strlen($salt);
        $saltArray = $this->multiByteSplit($salt);
        if (!$saltLength) {
            return $alphabet;
        }
        $alphabetArray = $this->multiByteSplit($alphabet);
        for ($x = mb_strlen($alphabet) - 1, $v = 0, $p = 0; $x > 0; $x--, $v++) {
            $v %= $saltLength;
            $p += $int = mb_ord($saltArray[$v], 'UTF-8');
            $j = ($int + $v + $p) % $x;

            $temp = $alphabetArray[$j];
            $alphabetArray[$j] = $alphabetArray[$x];
            $alphabetArray[$x] = $temp;
        }
        $alphabet = implode('', $alphabetArray);
        $this->shuffledAlphabets[$key] = $alphabet;

        return $alphabet;
    }

    /**
     * Hash given input value.
     *
     * @param string|integer $input
     * @param string $alphabet
     *
     * @return string
     */
    protected function hash($input, string $alphabet): string
    {
        $hash = '';
        $alphabetLength = mb_strlen($alphabet);

        do {
            $hash = mb_substr($alphabet, $this->math->intval($this->math->mod($input, $alphabetLength)), 1) . $hash;

            $input = $this->math->divide($input, $alphabetLength);
        } while ($this->math->greaterThan($input, 0));

        return $hash;
    }

    /**
     * Unhash given input value.
     *
     * @param string|integer $input
     * @param string $alphabet
     *
     * @return integer
     */
    protected function unhash($input, string $alphabet): int
    {
        $number = 0;
        $inputLength = mb_strlen((string)$input);

        if ($inputLength && $alphabet) {
            $alphabetLength = mb_strlen($alphabet);
            $inputChars = $this->multiByteSplit($input);

            foreach ($inputChars as $char) {
                $position = mb_strpos($alphabet, $char);
                $number = $this->math->multiply($number, $alphabetLength);
                $number = $this->math->add($number, $position);
            }
        }

        // DD((int)$number);

        // return $number;

        $retVal = (int)$number;

        return $retVal;
    }


    /**
     * Get BC Math or GMP extension.
     *
     * @codeCoverageIgnore
     *
     * @return MathInterface
     *@throws RuntimeException
     *
     */
    protected function getMathExtension(): MathInterface
    {
        if (extension_loaded('gmp')) {
            return new Gmp();
        }

        if (extension_loaded('bcmath')) {
            return new Bc();
        }

        throw new RuntimeException('Missing BC Math or GMP extension.');
    }

    /**
     * Replace simple use of $this->multiByteSplit with multi byte string.
     *
     * @param $string
     *
     * @return array|string[]
     */
    protected function multiByteSplit($string): array
    {
        return preg_split('/(?!^)(?=.)/u', $string) ?: [];
    }
}
