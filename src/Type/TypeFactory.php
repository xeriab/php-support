<?php

declare(strict_types=1);

namespace Exen\Support\Type;

use Exception;
use ReflectionParameter;

/**
 * Class TypeFactory
 * @package \Exen\Support\Type
 */
final class TypeFactory
{
    /**
     * @param ReflectionParameter $parameter
     *
     * @return Type
     * @throws Exception
     */
    public static function reflection(ReflectionParameter $parameter): Type
    {
        if (!$parameter->hasType()) {
            throw new Exception('Parameter has no type');
        }

        if (!$parameter->getType()->isBuiltin()) {
            return new Type(Type::IS_OBJECT);
        }

        $alias = Type::alias((string) $parameter->getType());

        if ($alias === Type::NONE) {
            throw new Exception('No type found');
        }

        return new Type($alias);
    }

    /**
     * @param $expression
     *
     * @return Type
     * @throws Exception
     */
    public static function expression($expression): Type
    {
        foreach (Type::TYPE_CALLBACK as $type => $callback) {
            if ($callback($expression)) {
                return new Type($type);
            }
        }

        throw new Exception('Unknown expression: ' . $expression);
    }
}
