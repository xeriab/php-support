<?php

declare(strict_types=1);

namespace Exen\Support\Closure;

use Exception;

/**
 * Security exception class
 */
class SecurityException extends Exception
{

}
