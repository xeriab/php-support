<?php

declare(strict_types=1);

namespace Exen\Support;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidFactory;
use Ramsey\Uuid\Generator\CombGenerator;
use Ramsey\Uuid\Codec\TimestampFirstCombCodec;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Exception\RegexpException;
use Exen\Support\Exception\InvalidStateException;
use Exen\Support\Interfaces\Singletonable;

/**
 * Class for various `String` helpers.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
class Str extends PhpObject implements Singletonable
{
    use MacroableTrait;

    const TRIM_CHARACTERS = " \t\n\r\0\x0B\xC2\xA0";

    const URL_REGEXP = '%^(?:(?:https?|ftp)://)(?:\S+(?::\S*)?@|\d{1,3}(?:\.\d{1,3}){3}|' .
        '(?:(?:[a-z\d\x{00a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)(?:\.(?:[a-z\d\x{0' .
        '0a1}-\x{ffff}]+-?)*[a-z\d\x{00a1}-\x{ffff}]+)*(?:\.[a-z\x{00a1}-\x{ffff}]{2,6})' .
        ')(?::\d+)?(?:[^\s]*)?$%iu';

    const EMAIL_REGEXP = '/^([a-z0-9_.-])+@([a-z0-9_.-])+\.([a-z])+([a-z])+/i';

    /**
     * Default charset is UTF-8
     *
     * @var string
     */
    protected static string $encoding = 'UTF-32';

    /**
     * The cache of snake-cased words.
     *
     * @var array
     */
    protected static array $snakeCache = [];

    /**
     * The cache of camel-cased words.
     *
     * @var array
     */
    protected static array $camelCache = [];

    /**
     * The cache of studly-cased words.
     *
     * @var array
     */
    protected static array $studlyCache = [];

    protected static $language = null;

    /**
     * Str instance.
     *
     * @var Str|null $instance
     */
    private static ?Str $instance = null;

    /**
     * Str instances.
     *
     * @var Str[] $instances
     */
    private static array $instances = [];

    public static function detectEncoding(string $string = null)
    {
        return \mb_detect_encoding($string);
    }

    /**
     * Sets the default encoding for this class.
     *
     * @return void
     */
    public static function setEncoding(string $encodingString = 'UTF-8'): void
    {
        self::$encoding = $encodingString;

        static $encoding;

        if (null === $encoding) {
            $encoding = $encodingString;

            if ($encoding) {
                \mb_internal_encoding(self::$encoding);
            }
        }
    }

    /**
     * Checks to see if a string is utf8 encoded.
     *
     * NOTE: This function checks for 5-Byte sequences, UTF8
     *       has Bytes Sequences with a maximum length of 4.
     *
     * Written by Tony Ferrara <http://blog.ircmaxwell.com>
     *
     * @param  string $string The string to be checked
     * @return boolean
     */
    public static function seemsUtf8(string $string = null): bool
    {
        if (\function_exists('mb_check_encoding')) {
            // If mbstring is available, this is significantly faster than
            // using PHP regexps.
            return \mb_check_encoding($string, 'UTF-8');
        }

        // @codeCoverageIgnoreStart
        return static::seemsUtf8Regex($string);
        // @codeCoverageIgnoreEnd
    }

    /**
     * A non-Mbstring UTF-8 checker.
     *
     * @param  $string
     * @return bool
     */
    protected static function seemsUtf8Regex(string $string = null): bool
    {
        // Obtained from http://stackoverflow.com/a/11709412/430062 with permission.
        $regex = '/(
            [\xC0-\xC1] # Invalid UTF-8 Bytes
            | [\xF5-\xFF] # Invalid UTF-8 Bytes
            | \xE0[\x80-\x9F] # Overlong encoding of prior code point
            | \xF0[\x80-\x8F] # Overlong encoding of prior code point
            | [\xC2-\xDF](?![\x80-\xBF]) # Invalid UTF-8 Sequence Start
            | [\xE0-\xEF](?![\x80-\xBF]{2}) # Invalid UTF-8 Sequence Start
            | [\xF0-\xF4](?![\x80-\xBF]{3}) # Invalid UTF-8 Sequence Start
            | (?<=[\x0-\x7F\xF5-\xFF])[\x80-\xBF] # Invalid UTF-8 Sequence Middle
            | (?<![\xC2-\xDF]|[\xE0-\xEF]|[\xE0-\xEF][\x80-\xBF]|[\xF0-\xF4]|[\xF0-\xF4][\x80-\xBF]|[\xF0-\xF4][\x80-\xBF]{2})[\x80-\xBF] # Overlong Sequence
            | (?<=[\xE0-\xEF])[\x80-\xBF](?![\x80-\xBF]) # Short 3 byte sequence
            | (?<=[\xF0-\xF4])[\x80-\xBF](?![\x80-\xBF]{2}) # Short 4 byte sequence
            | (?<=[\xF0-\xF4][\x80-\xBF])[\x80-\xBF](?![\x80-\xBF]) # Short 4 byte sequence (2)
        )/x';

        return ! \preg_match($regex, $string);
    }

    /**
     * Check is mbstring loaded
     *
     * @return bool
     */
    private static function isMultibyteString(): bool
    {
        static $isLoaded;

        if (null === $isLoaded) {
            $isLoaded = \extension_loaded('mbstring');

            if ($isLoaded) {
                \mb_internal_encoding(self::$encoding);
            }
        }

        return $isLoaded;
    }

    /**
     * Determines whether the given `string` is unicode.
     *
     * @param string|null $string String to check.
     *
     * @return boolean
     */
    public static function isUnicode(string $string = null): bool
    {
        $encoding = static::detectEncoding($string);

        return $encoding === 'UTF-8' or
            $encoding === 'UTF-16' or
            $encoding === 'UTF-32';
    }

    /**
     * Determines whether the given `string` is ASCII.
     *
     * @param string|null $string String to check.
     *
     * @return boolean
     */
    public static function isAscii(string $string = null): bool
    {
        $encoding = static::detectEncoding($string);

        return $encoding === 'ASCII';
    }

    /**
     * Check is iconv loaded
     *
     * @return bool
     */
    public static function isIconv(): bool
    {
        static $isLoaded;

        if (null === $isLoaded) {
            $isLoaded = \extension_loaded('iconv');
        }

        return $isLoaded;
    }

    /**
     * Check is mbstring loaded
     *
     * @return bool
     */
    public static function isMbString(): bool
    {
        static $isLoaded;

        if (null === $isLoaded) {
            $isLoaded = \extension_loaded('mbstring');
        }

        return $isLoaded;
    }

    /**
     * Check is intl loaded
     *
     * @return bool
     */
    public static function isIntl(): bool
    {
        static $isLoaded;

        if (null === $isLoaded) {
            $isLoaded = \extension_loaded('intl');
        }

        return $isLoaded;
    }

    /**
     * Strip all whitespaces from the given string.
     *
     * @param string $string The string to strip
     * @return string
     * @throws InvalidStateException
     */
    public static function stripSpace(string $string): string
    {
        return static::regexReplace($string, '/\s+/', '');
    }

    public static function toBoolString($value): string
    {
        return ($value) ? 'true' : 'false';
    }

    public static function toNullString($value): string
    {
        return (is_null($value)) ? 'null' : '';
    }

    /**
     * Parse text by lines
     *
     * @param  string $text
     * @param  bool   $toAssoc
     * @return array
     */
    public static function parseLines($text, $toAssoc = true): array
    {
        $text = \htmlspecialchars_decode($text);
        $text = static::clean($text, false, false, false);

        $text = \str_replace(["\n", "\r", "\r\n", PHP_EOL], "\n", $text);
        $lines = \explode("\n", $text);

        $retVal = [];

        if (!empty($lines)) {
            foreach ($lines as $line) {
                $line = \trim($line);

                if ($line === '') {
                    continue;
                }

                if ($toAssoc) {
                    $retVal[$line] = $line;
                } else {
                    $retVal[] = $line;
                }
            }
        }

        return $retVal;
    }

    /**
     * Get unique string
     *
     * @param  string $prefix
     * @return string
     */
    public static function unique($prefix = 'unique'): string
    {
        $prefix = \rtrim(trim($prefix), '-');
        $random = \random_int(10000000, 99999999);

        $retVal = $random;

        if ($prefix) {
            $retVal = $prefix . '-' . $random;
        }

        return $retVal;
    }

    /**
     * Return the remainder of a string after a given value.
     *
     * @param string $subject
     * @param string $search
     *
     * @return string
     */
    public static function after($subject, $search)
    {
        return '' === $search ? $subject : \array_reverse(\explode($search, $subject, 2))[0];
    }

    /**
     * Get part of string
     *
     * @param  string $string
     * @param  int    $start
     * @param  int    $length
     * @return string
     */
    public static function sub($string = null, int $start = 0, int $length = 0): string
    {
        // self::$encoding = static::detectEncoding($string);

        if (0 === $length || null === $length) {
            $length = static::length($string);
        }

        if ($start < 0 && $length < 0) {
            $start += static::length($string);
        }

        if (static::isUnicode($string)) {
            return \mb_substr($string, $start, $length, static::$encoding);
        }

        // return \mb_substr($string, $start, $length, self::$encoding);

        return \substr($string, $start, $length); // @codeCoverageIgnore
    }

    public static function subReplace(string $s = null, $replacement = null, int $position = 0, int $length = 0): string
    {
        // $startString = mb_substr($original, 0, $position, 'UTF-8');
        // $endString = mb_substr($original, $position + $length, mb_strlen($original), 'UTF-8');

        $startString = static::sub($s, 0, $position);
        $endString = static::sub($s, $position + $length, static::length($s));

        return $startString . $replacement . $endString;
    }

    public static function subCount(string $haystack = null, $needles = null, int $offset = 0, int $length = 0): int
    {
        $count = 0;

        foreach ((array) $needles as $needle) {
            // if ('' !== $needle && false !== static::strPos($haystack, $needle)) {
            if ('' !== $needle) {
                $string = static::sub($haystack, $offset, $length);

                if (static::isUnicode($haystack)) {
                    $count += \mb_substr_count($string, $needle);
                } else {
                    $count += \substr_count($string, $needle);
                }
            }
        }

        return $count;
    }

    public static function count(string $haystack = null, $needles = null): int
    {
        $count = 0;

        foreach ((array) $needles as $needle) {
            // if ('' !== $needle && false !== static::strPos($haystack, $needle)) {
            if ('' !== $needle) {
                if (static::isUnicode($haystack)) {
                    $count += \mb_substr_count($haystack, $needle);
                } else {
                    $count += \substr_count($haystack, $needle);
                }
            }
        }

        return $count;
    }

    /**
     * Returns a part of UTF-8 string.
     *
     * @param string
     * @param int in characters (code points)
     * @param int in characters (code points)
     *
     * @return string
     */
    public static function substring($s, $start, $length = null): string
    {
        if (function_exists('mb_substr')) {
            return mb_substr($s, $start, $length, 'UTF-8'); // MB is much faster
        } elseif (null === $length) {
            $length = static::length($s);
        } elseif ($start < 0 && $length < 0) {
            $start += static::length($s); // unifies iconv_substr behavior with mb_substr
        }

        return iconv_substr($s, $start, $length, 'UTF-8');
    }

    /**
     * Transliterate a UTF-8 value to ASCII.
     *
     * @param string $value
     * @param string $language
     *
     * @return string
     */
    public static function ascii($value, $language = 'en')
    {
        $languageSpecific = static::languageSpecificCharsArray($language);

        if (!\is_null($languageSpecific)) {
            $value = \str_replace($languageSpecific[0], $languageSpecific[1], $value);
        }

        foreach (static::charsArray() as $key => $val) {
            $value = \str_replace($val, $key, $value);
        }

        return \preg_replace('/[^\x20-\x7E]/u', '', $value);
    }

    /**
     * Get the portion of a string before a given value.
     *
     * @param string $subject
     * @param string $search
     *
     * @return string
     */
    public static function before($subject, $search)
    {
        return '' === $search ? $subject : \explode($search, $subject)[0];
    }

    /**
     * Convert a value to camel case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function camel($value)
    {
        if (isset(static::$camelCache[$value])) {
            return static::$camelCache[$value];
        }

        return static::$camelCache[$value] = \lcfirst(static::studly($value));
    }

    /**
     * Cap a string with a single instance of a given value.
     *
     * @param string $value
     * @param string $cap
     *
     * @return string
     */
    public static function finish($value, $cap)
    {
        $quoted = \preg_quote($cap, '/');

        return \preg_replace('/(?:'.$quoted.')+$/u', '', $value).$cap;
    }

    /**
     * Determine if a given string matches a given pattern.
     *
     * @param string|array $pattern
     * @param string       $value
     *
     * @return bool
     */
    public static function is($pattern, $value)
    {
        $patterns = Arr::wrap($pattern);

        if (empty($patterns)) {
            return false;
        }

        foreach ($patterns as $pattern) {
            // If the given value is an exact match we can of course return true right
            // from the beginning. Otherwise, we will translate asterisks and do an
            // actual pattern match against the two strings to see if they match.
            if ($pattern == $value) {
                return true;
            }

            $pattern = \preg_quote($pattern, '#');

            // DEBUG($pattern);

            // Asterisks are translated into zero-or-more regular expression wildcards
            // to make it convenient to check if the strings starts with the given
            // pattern such as "library/*", making any string check convenient.
            $pattern = \str_replace('\*', '.*', $pattern);

            if (1 === \preg_match('#^'.$pattern.'\z#u', $value)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Convert a string to kebab case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function kebab($value)
    {
        return static::snake($value, '-');
    }

    /**
     * Strips whitespace.
     *
     * @param string  UTF-8 encoding
     * @param string
     * @param bool
     *
     * @return string
     */
    public static function trim(string $string = null, string $charList = self::TRIM_CHARACTERS, bool $extendedMode = false): string
    {
        if ($extendedMode) {
            $string = \trim($string, static::char(0xE3) . static::char(0x80) . static::char(0x80));
            $string = \trim($string, static::char(0xC2) . static::char(0xA0));
            $string = \trim($string);

            return $string;
        }

        $charList = \preg_quote($charList, '#');

        return static::regexReplace($string, '#^['.$charList.']+|['.$charList.']+\z#u', '');
    }

    public static function trimStart(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        // $charList = \preg_replace('([.]+)', '-', $charList);
        // $charList = \preg_quote($charList, '#');
        // $charList = \str_replace('\\-', '-', $charList);

        // $charList = '#^['.$charList.']+#m';

        // return static::regexReplace($string, $charList, '');

        return \ltrim($string, $charList);
    }

    public static function trimEnd(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        // $charList = \preg_replace('([.]+)', '-', $charList);
        // $charList = \preg_quote($charList, '#');
        // $charList = \str_replace('\\-', '-', $charList);

        // $charList = '#['.$charList.']+$#m';

        // return static::regexReplace($string, $charList, '');

        return \rtrim($string, $charList);
    }

    public static function trimLeft(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        return self::trimStart($string, $charList);
    }

    public static function trimRight(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        return self::trimEnd($string, $charList);
    }

    public static function ltrim(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        return self::trimStart($string, $charList);
    }

    public static function rtrim(string $string = null, string $charList = self::TRIM_CHARACTERS): string
    {
        return self::trimEnd($string, $charList);
    }

    /**
     * Return the length of the given string.
     *
     * @param string $string
     *
     * @return int
     */
    public static function length(string $string = null): int
    {
        if (static::isUnicode($string)) {
            return \mb_strlen($string);
        }

        return \strlen($string); // @codeCoverageIgnore
    }

    /**
     * Return the length of the given string.
     *
     * @param string $string
     *
     * @return int
     */
    public static function len(string $string = null): int
    {
        return self::length($string);
    }

    /**
     * Finds the length of common prefix of strings.
     *
     * @param string|array
     *
     * @return string
     */
    public static function findPrefix(...$strings): string
    {
        if (\is_array($strings[0])) {
            $strings = $strings[0];
        }

        $first = \array_shift($strings);

        for ($i = 0; $i < static::length($first); ++$i) {
            foreach ($strings as $s) {
                if (!isset($s[$i]) || $first[$i] !== $s[$i]) {
                    while ($i && $first[$i - 1] >= "\x80" && $first[$i] >= "\x80" && $first[$i] < "\xC0") {
                        --$i;
                    }

                    return static::sub($first, 0, $i);
                }
            }
        }

        return $first;
    }

    /**
     * Limit the number of characters in a string.
     *
     * @param string $value
     * @param int    $limit
     * @param string $end
     *
     * @return string
     */
    public static function limit($value, $limit = 100, $end = '...')
    {
        if (static::isUnicode($value)) {
            if (\mb_strwidth($value, self::$encoding) <= $limit) {
                return $value;
            }

            return \rtrim(\mb_strimwidth($value, 0, $limit, '')).$end;
        }
    }

    /**
     * Case-insensitive compares UTF-8 strings.
     *
     * @param string
     * @param string
     * @param int
     *
     * @return bool
     */
    public static function compare(string $left = null, string $right = null, int $length = null): bool
    {
        if ($length < 0) {
            $left = static::sub($left, $length, -$length);
            $right = static::sub($right, $length, -$length);
        } elseif (null !== $length) {
            $left = static::sub($left, 0, $length);
            $right = static::sub($right, 0, $length);
        }

        return static::lower($left) === static::lower($right);
    }

    /**
     * Convert the given string to lower-case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function lower(string $value = null): string
    {
        if (static::isUnicode($value)) {
            return \mb_strtolower($value);
        }

        return \strtolower($value);
    }

    /**
     * Convert first character to lower case.
     *
     * @param string  UTF-8 encoding
     *
     * @return string
     */
    public static function firstLower($s): string
    {
        return static::lower(static::sub($s, 0, 1)).static::sub($s, 1);
    }

    /**
     * Convert first character to upper case.
     *
     * @param string  UTF-8 encoding
     *
     * @return string
     */
    public static function firstUpper($s): string
    {
        return static::upper(static::sub($s, 0, 1)).static::sub($s, 1);
    }

    /**
     * Capitalize string.
     *
     * @param string  UTF-8 encoding
     *
     * @return string
     */
    public static function capitalize(string $value = null): string
    {
        if (static::isUnicode($value)) {
            return \mb_convert_case($value, \MB_CASE_TITLE);
        } elseif (static::isAscii($value)) {
            return static::upperCaseWords($value);
        }

        return $value;
    }

    /**
     * Limit the number of words in a string.
     *
     * @param string $value
     * @param int    $words
     * @param string $end
     *
     * @return string
     */
    public static function words($value, $words = 100, $end = '...'): string
    {
        \preg_match('/^\s*+(?:\S++\s*+){1,'.$words.'}/u', $value, $matches);

        if (!isset($matches[0]) || static::length($value) === static::length($matches[0])) {
            return $value;
        }

        return \rtrim($matches[0]).$end;
    }

    /**
     * Parse a Class@method style callback into class and method.
     *
     * @param string      $callback
     * @param string|null $default
     *
     * @return array
     */
    public static function parseCallback($callback, $default = null)
    {
        return static::contains($callback, '@') ? explode('@', $callback, 2): [$callback, $default];
    }

    /**
     * Ensure a string only uses ascii characters.
     *
     * @param string $str
     * @param array  $map
     *
     * @return string
     */
    public static function toExtendedAscii(string $str = null, array &$map = []): string
    {
        // Find all multi-byte characters (cf. utf-8 encoding specs).
        $matches = [];

        if (!\preg_match_all('/[\xC0-\xF7][\x80-\xBF]+/', $str, $matches)) {
            // Plain ascii string
            return $str;
        }

        // Update the encoding map with the characters not already met.
        foreach ($matches[0] as $mbc) {
            if (!isset($map[$mbc])) {
                $map[$mbc] = static::char(128 + \count($map));
            }
        }

        // Finally remap non-ascii characters.
        return \strtr($str, $map);
    }

    /**
     * Calculate the levenshtein distance between two strings.
     *
     * @param string $string1
     * @param string $string2
     *
     * @return int
     */
    public static function levenshtein(string $string1 = null, string $string2 = null): int
    {
        $charMap = [];

        $string1 = static::toExtendedAscii($string1, $charMap);
        $string2 = static::toExtendedAscii($string2, $charMap);

        return levenshtein($string1, $string2);
    }

    /**
     * firstUniChar.
     *
     * @param string $string
     *
     * @return string
     */
    public static function firstUnicodeChar(string $string = null): string
    {
        return static::sub($string, 0, 1);
    }

    /**
     * Get the plural form of an English word.
     *
     * @param string $value
     * @param int    $count
     *
     * @return string
     */
    public static function plural($value, $count = 2)
    {
        return Pluralizer::plural($value, $count);
    }

    /**
     * Generate a more truly "random" alpha-numeric string.
     *
     * @param int $length
     *
     * @return string
     */
    public static function random(int $length = 16): string
    {
        $string = '';

        while (($len = static::length($string)) < $length) {
            $size = $length - $len;
            $bytes = \random_bytes($size);
            $string .= static::sub(\str_replace(['/', '+', '='], '', \base64_encode($bytes)), 0, $size);
        }

        return $string;
    }

    /**
     * Generate readable random string
     *
     * @param  int  $length
     * @param  bool $isReadable
     * @return string
     */
    public static function randomize(?int $length = 10, ?bool $isReadable = true): string
    {
        $retVal = '';

        if ($isReadable) {
            $vowels = ['a', 'e', 'i', 'o', 'u', '0'];
            $consonants = [
                'b',
                'c',
                'd',
                'f',
                'g',
                'h',
                'j',
                'k',
                'l',
                'm',
                'n',
                'p',
                'r',
                's',
                't',
                'v',
                'w',
                'x',
                'y',
                'z',
                '1',
                '2',
                '3',
                '4',
                '5',
                '6',
                '7',
                '8',
                '9',
            ];

            $max = $length / 2;

            for ($pos = 1; $pos <= $max; ++$pos) {
                $retVal .= $consonants[\random_int(0, \count($consonants) - 1)];
                $retVal .= $vowels[\random_int(0, \count($vowels) - 1)];
            }
        } else {
            $chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

            for ($pos = 0; $pos < $length; ++$pos) {
                $retVal .= $chars[\mt_rand() % \strlen($chars)];
            }
        }

        return $retVal;
    }

    /**
     * Returns position of $offset occurence of $needle in $haystack.
     *
     * @param string
     * @param string
     * @param int  negative value means searching from the end
     *
     * @return int|false offset in characters or false if the needle was not found
     */
    public static function indexOf(string $haystack = null, string $needle = null, int $offset = 1)
    {
        $position = static::strPos($haystack, $needle, $offset);

        return false === $position
            ? false
            : static::length(static::sub($haystack, 0, $position));
    }

    /**
     * Returns position of $offset occurence of $needle in $haystack.
     *
     * @return int|false offset in bytes or false if the needle was not found
     */
    public static function position(string $haystack = null, string $needle = null, int $offset = 1)
    {
        if (!$offset) {
            return false;
        } elseif ($offset > 0) {
            if (0 === static::length($needle)) {
                return 0;
            }

            $position = 0;

            while (false !== ($position = static::strPos($haystack, $needle, $position)) && --$offset) {
                ++$position;
            }
        } else {
            $length = static::length($haystack);

            if (0 === static::length($needle)) {
                return $length;
            }

            $position = $length - 1;

            while (false !== ($position = static::rPos($haystack, $needle, $position - $length)) && ++$offset) {
                --$position;
            }
        }

        return $position;
    }

    /**
     * Returns position of $offset occurence of $needle in $haystack.
     *
     * @return int|false offset in bytes or false if the needle was not found
     */
    public static function strPos(string $haystack = null, $needle = null, int $offset = 1)
    {
        if (static::isUnicode($haystack)) {
            return \mb_strpos($haystack, $needle, $offset);
        }

        return \strpos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Find position of last occurrence of a string in a string
     *
     * @param  string $haystack
     * @param  string $needle
     * @param  int    $offset
     * @return int
     */
    public static function rPos(string $haystack = null, $needle = null, int $offset = 0)
    {
        if (static::isUnicode($haystack)) {
            return \mb_strrpos($haystack, $needle, $offset);
        }

        return \strrpos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Finds position of first occurrence of a string within another, case insensitive
     *
     * @param  string $haystack
     * @param  string $needle
     * @param  int    $offset
     * @return int
     */
    public static function iPos(string $haystack = null, $needle = null, int $offset = 0)
    {
        if (static::isUnicode($haystack)) {
            return \mb_stripos($haystack, $needle, $offset);
        }

        return \stripos($haystack, $needle, $offset); // @codeCoverageIgnore
    }

    /**
     * Finds first occurrence of a string within another
     *
     * @param  string $haystack
     * @param  string $needle
     * @param  bool   $beforeNeedle
     * @return string
     */
    public static function str(string $haystack = null, $needle = null, bool $beforeNeedle = false)
    {
        if (static::isUnicode($haystack)) {
            return \mb_strstr($haystack, $needle, $beforeNeedle);
        }

        return \strstr($haystack, $needle, $beforeNeedle); // @codeCoverageIgnore
    }

    /**
     * Finds first occurrence of a string within another, case insensitive
     *
     * @param  string $haystack
     * @param  string $needle
     * @param  bool   $beforeNeedle
     * @return string
     */
    public static function iStr(string $haystack = null, $needle = null, bool $beforeNeedle = false)
    {
        if (static::isUnicode($haystack)) {
            return \mb_stristr($haystack, $needle, $beforeNeedle);
        }

        return \stristr($haystack, $needle, $beforeNeedle); // @codeCoverageIgnore
    }

    /**
     * Strpos for array.
     *
     * @param array  $haystack
     * @param string $needle
     * @param int    $offset
     *
     * @return bool
     */
    public static function positionArray(string $haystack = null, $needle = null, int $offset = 0): bool
    {
        if (!\is_array($needle)) {
            $needle = [$needle];
        }

        foreach ($needle as $query) {
            if (false !== static::strPos($haystack, $query, $offset)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Pad a string to a certain length with another string.
     *
     * @param string  UTF-8 encoding
     * @param int
     * @param string
     *
     * @return string
     */
    public static function padLeft($s, int $length = 0, string $pad = ' '): string
    {
        $length = \max(0, $length - static::length($s));
        $padLen = static::length($pad);

        return \str_repeat($pad, (int) ($length / $padLen)).static::sub($pad, 0, $length % $padLen).$s;
    }

    /**
     * Pad a string to a certain length with another string.
     *
     * @param string  UTF-8 encoding
     * @param int
     * @param string
     *
     * @return string
     */
    public static function padRight($s, int $length = 0, string $pad = ' '): string
    {
        $length = \max(0, $length - static::length($s));
        $padLen = static::length($pad);

        return $s.\str_repeat($pad, (int) ($length / $padLen)).static::sub($pad, 0, $length % $padLen);
    }

    /**
     * Perform a regular expression search and replace.
     *
     * @param string
     * @param string|array
     * @param string|callable
     * @param int
     *
     * @return string
     */
    public static function regexReplace(string $subject = null, $pattern = null, $replacement = null, int $limit = -1): string
    {
        if (\is_object($replacement) || \is_array($replacement)) {
            if (!\is_callable($replacement, false, $textual)) {
                throw new InvalidStateException("Callback '$textual' is not callable.");
            }

            return static::pcre('preg_replace_callback', [$pattern, $replacement, $subject, $limit]);
        } elseif (null === $replacement && \is_array($pattern)) {
            $replacement = \array_values($pattern);
            $pattern = \array_keys($pattern);
        }

        return static::pcre('preg_replace', [$pattern, $replacement, $subject, $limit]);
    }

    /**
     * Perform a search and replace.
     *
     * @param string|array
     * @param string|array
     * @param string|array
     * @param int
     *
     * @return string
     */
    public static function replace($subject = null, $search = null, $replacement = null, int &$count = 0): string
    {
        // $string = null;
        $char = null;

        if (!\is_array($search) && \is_array($replacement)) {
            return false;
        }

        if (\is_array($subject)) {
            // Call mb_replace for each single string in $subject
            foreach ($subject as &$string) {
                $string = &\mb_replace($search, $replacement, $string, $char);
                $count += $char;
            }
        } elseif (\is_array($search)) {
            if (!\is_array($replacement)) {
                foreach ($search as &$string) {
                    $subject = \mb_replace($string, $replacement, $subject, $char);
                    $count += $char;
                }
            } else {
                $n = \max(\count($search), \count($replacement));

                while ($n--) {
                    $subject = \mb_replace(current($search), \current($replacement), $subject, $char);
                    $count += $char;
                    \next($search);
                    \next($replacement);
                }
            }
        } else {
            $parts = \mb_split(\preg_quote($search), $subject);
            $count = \count($parts) - 1;
            $subject = \implode($replacement, $parts);
        }

        return $subject;
    }

    public static function toArray(string $string = null): ?array
    {
        $length = static::length($string);

        while ($length) {
            $array[] = static::sub($string, 0, 1);
            $string = static::sub($string, 1, $length);
            $length = static::length($string);
        }

        return $array;
    }

    /**
     * Splits string by a regular expression or a delimiter.
     *
     * @param string
     * @param string
     * @param int
     *
     * @return array
     */
    public static function split(string $subject = null, string $delimiter = '/(?<!^)(?!$)/u', int $flags = 0): ?array
    {
        if (Lang::isRegex($delimiter)) {
            return static::pcre(
                'preg_split',
                [$delimiter, $subject, -1, $flags | \PREG_SPLIT_DELIM_CAPTURE]
            );
        } elseif (Lang::isString($delimiter)) {
            return static::explode($delimiter, $subject);
        }

        return null;
    }

    /**
     * Replace a given value in the string sequentially with an array.
     *
     * @param string $search
     * @param array  $replace
     * @param string $subject
     *
     * @return string
     */
    public static function replaceArray($search, array $replace = [], string $subject = null): string
    {
        foreach ($replace as $value) {
            $subject = static::replaceFirst($search, $value, $subject);
        }

        return $subject;
    }

    /**
     * Replace the first occurrence of a given value in the string.
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     *
     * @return string
     */
    public static function replaceFirst(string $search = null, string $replace = null, string $subject = null): string
    {
        if ('' == $search) {
            return $subject;
        }

        $position = static::strPos($subject, $search);

        if (false !== $position) {
            return static::subReplace($subject, $replace, $position, static::length($search));
        }

        return $subject;
    }

    /**
     * Replace the last occurrence of a given value in the string.
     *
     * @param string $search
     * @param string $replace
     * @param string $subject
     *
     * @return string
     */
    public static function replaceLast(string $search = null, string $replace = null, string $subject = null): string
    {
        if ('' == $search) {
            return $subject;
        }

        $position = static::strPos($subject, $search);

        if (false !== $position) {
            return static::subReplace($subject, $replace, $position, static::length($search));
        }

        return $subject;
    }

    /**
     * Begin a string with a single instance of a given value.
     *
     * @param string $value
     * @param string $prefix
     *
     * @return string
     */
    public static function start($value, $prefix)
    {
        $quoted = \preg_quote($prefix, '/');

        return $prefix.\preg_replace('/^(?:'.$quoted.')+/u', '', $value);
    }

    /**
     * Convert the given string to upper-case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function upper(string $value = null): string
    {
        if (static::isUnicode($value)) {
            return \mb_strtoupper($value);
        }

        return \strtoupper($value);
    }

    /**
     * Convert the given string to title case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function title(string $value = null): string
    {
        if (static::isUnicode($value)) {
            return \mb_convert_case($value, \MB_CASE_TITLE);
        }

        return \ucfirst($value);
    }

    /**
     * Get the singular form of an English word.
     *
     * @param string $value
     *
     * @return string
     */
    public static function singular($value)
    {
        return Pluralizer::singular($value);
    }

    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param string $title
     * @param string $separator
     * @param string $language
     *
     * @return string
     */
    public static function slug($title, $separator = '-', $language = 'en')
    {
        $title = static::ascii($title, $language);

        // Convert all dashes/underscores into separator
        $flip = '-' == $separator ? '_' : '-';

        $title = \preg_replace('!['.\preg_quote($flip).']+!u', $separator, $title);

        // Replace @ with the word 'at'
        $title = \str_replace('@', $separator.'at'.$separator, $title);

        // Remove all characters that are not the separator, letters, numbers, or whitespace.
        $title = \preg_replace('![^'.\preg_quote($separator).'\pL\pN\s]+!u', '', static::lower($title));

        // Replace all separator characters and whitespace by a single separator
        $title = \preg_replace('!['.\preg_quote($separator).'\s]+!u', $separator, $title);

        return \trim($title, $separator);
    }

    /**
     * Convert a string to snake case.
     *
     * @param string $value
     * @param string $delimiter
     *
     * @return string
     */
    public static function snake($value, $delimiter = '_')
    {
        $key = $value;

        if (isset(static::$snakeCache[$key][$delimiter])) {
            return static::$snakeCache[$key][$delimiter];
        }

        if (!\ctype_lower($value)) {
            $value = \preg_replace('/\s+/u', '', ucwords($value));

            $value = static::lower(\preg_replace('/(.)(?=[A-Z])/u', '$1'.$delimiter, $value));
        }

        return static::$snakeCache[$key][$delimiter] = $value;
    }

    /**
     * Determine if a given string contains a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    public static function contains(string $haystack = null, $needles = null): bool
    {
        if (!\is_array($needles)) {
            $needles = [$needles];
        }

        foreach ($needles as $needle) {
            if ('' !== $needle && false !== static::strPos($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a given string ends with a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    public static function endsWith(string $haystack = null, $needles = null): bool
    {
        if (!\is_array($needles)) {
            $needles = [$needles];
        }

        foreach ($needles as $needle) {
            if ('' !== $needle && static::sub($haystack, -static::length($needle)) === (string) $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if a given string starts with a given substring.
     *
     * @param string       $haystack
     * @param string|array $needles
     *
     * @return bool
     */
    public static function startsWith(string $haystack = null, $needles = null): bool
    {
        if (!\is_array($needles)) {
            $needles = [$needles];
        }

        foreach ($needles as $needle) {
            if ('' !== $needle && static::sub($haystack, 0, static::length($needle)) === (string) $needle) {
                return true;
            }
        }

        return false;
    }

    /**
     * Convert a value to studly caps case.
     *
     * @param string $value
     *
     * @return string
     */
    public static function studly($value)
    {
        $key = $value;

        if (isset(static::$studlyCache[$key])) {
            return static::$studlyCache[$key];
        }

        $value = \ucwords(\str_replace(['-', '_'], ' ', $value));

        return static::$studlyCache[$key] = \str_replace(' ', '', $value);
    }

    /**
     * Make a string's first character lowercase.
     *
     * @param string $string
     *
     * @return string
     */
    public static function lcfirst($string)
    {
        return static::lower(static::sub($string, 0, 1)).static::sub($string, 1);
    }

    /**
     * Make a string's first character uppercase.
     *
     * @param string $string
     *
     * @return string
     */
    public static function ucfirst($string)
    {
        return static::upper(static::sub($string, 0, 1)).static::sub($string, 1);
    }

    /**
     * Make a string's words uppercase.
     *
     * @param string $string
     *
     * @return string
     */
    public static function ucwords($string)
    {
        return \ucwords(\str_replace(['-', '_'], ' ', $string));
    }

    /**
     * Convert camel case to human readable format
     *
     * @param  string $input
     * @param  string $separator
     * @param  bool   $toLower   *
     * @return string
     */
    public static function splitCamelCase(string $input = null, string $separator = '_', bool $toLower = true): string
    {
        $original = $input;

        $output = \preg_replace(['/(?<=[^A-Z])([A-Z])/', '/(?<=[^0-9])([0-9])/'], '_$0', $input);
        $output = \preg_replace('#_{1,}#', $separator, $output);

        $output = \trim($output);

        if ($toLower) {
            $output = \strtolower($output);
        }

        if ('' === $output) {
            return $original;
        }

        return $output;
    }

    /**
     * Convert test name to human readable string
     *
     * @param  string $input
     * @return mixed|string
     */
    public static function testNameToHuman(string $input = null)
    {
        $original = $input;
        $input = static::getClassName($input);

        /**
         *
 * @noinspection NotOptimalRegularExpressionsInspection
*/
        if (! \preg_match('#^tests#i', $input)) {
            $input = \preg_replace('#^(test)#i', '', $input);
        }

        $input = \preg_replace('#(test)$#i', '', $input);
        $output = \preg_replace(['/(?<=[^A-Z])([A-Z])/', '/(?<=[^0-9])([0-9])/'], ' $0', $input);
        $output = \trim(\str_replace('_', ' ', $output));

        $output = \implode(
            ' ',
            \array_filter(
                \array_map(
                    function ($item) {
                        $item = \ucwords($item);
                        $item = \trim($item);

                        return $item;
                    },
                    \explode(' ', $output)
                )
            )
        );


        if (strcasecmp($original, $output) === 0) {
            return $original;
        }

        if ('' === $output) {
            return $original;
        }

        return $output;
    }

    /**
     * Generate a UUID (version 4).
     *
     * @return \Ramsey\Uuid\UuidInterface
     */
    public static function uuid4()
    {
        return Uuid::uuid4();
    }

    /**
     * Generates a universally unique identifier (UUID v4) according to RFC 4122
     * Version 4 UUIDs are pseudo-random!
     *
     * Returns Version 4 UUID format: xxxxxxxx-xxxx-4xxx-Yxxx-xxxxxxxxxxxx where x is
     * any random hex digit and Y is a random choice from 8, 9, a, or b.
     *
     * @see http://stackoverflow.com/questions/2040240/php-function-to-generate-v4-uuid
     *
     * @return string
     */
    public static function uuid(): string
    {
        return \sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            \random_int(0, 0xffff),
            \random_int(0, 0xffff),
            // 16 bits for "time_mid"
            \random_int(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            \random_int(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            \random_int(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            \random_int(0, 0xffff),
            \random_int(0, 0xffff),
            \random_int(0, 0xffff)
        );
    }

    /**
     * Generate a time-ordered UUID (version 4).
     *
     * @return \Ramsey\Uuid\UuidInterface
     */
    public static function orderedUuid()
    {
        $factory = new UuidFactory;

        $factory->setRandomGenerator(
            new CombGenerator(
                $factory->getRandomGenerator(),
                $factory->getNumberConverter()
            )
        );

        $factory->setCodec(
            new TimestampFirstCombCodec(
                $factory->getUuidBuilder()
            )
        );

        return $factory->uuid4();
    }

    /**
     * Returns the replacements for the ascii method.
     *
     * Note: Adapted from Stringy\Stringy.
     *
     * @see https://github.com/danielstjules/Stringy/blob/3.1.0/LICENSE.txt
     *
     * @return array
     */
    protected static function charsArray()
    {
        static $charsArray;

        if (isset($charsArray)) {
            return $charsArray;
        }

        return $charsArray = [
            '0' => ['°', '₀', '۰', '０'],
            '1' => ['¹', '₁', '۱', '１'],
            '2' => ['²', '₂', '۲', '２'],
            '3' => ['³', '₃', '۳', '３'],
            '4' => ['⁴', '₄', '۴', '٤', '４'],
            '5' => ['⁵', '₅', '۵', '٥', '５'],
            '6' => ['⁶', '₆', '۶', '٦', '６'],
            '7' => ['⁷', '₇', '۷', '７'],
            '8' => ['⁸', '₈', '۸', '８'],
            '9' => ['⁹', '₉', '۹', '９'],
            'a' => ['à', 'á', 'ả', 'ã', 'ạ', 'ă', 'ắ', 'ằ', 'ẳ', 'ẵ', 'ặ', 'â', 'ấ', 'ầ', 'ẩ', 'ẫ', 'ậ', 'ā', 'ą', 'å', 'α', 'ά', 'ἀ', 'ἁ', 'ἂ', 'ἃ', 'ἄ', 'ἅ', 'ἆ', 'ἇ', 'ᾀ', 'ᾁ', 'ᾂ', 'ᾃ', 'ᾄ', 'ᾅ', 'ᾆ', 'ᾇ', 'ὰ', 'ά', 'ᾰ', 'ᾱ', 'ᾲ', 'ᾳ', 'ᾴ', 'ᾶ', 'ᾷ', 'а', 'أ', 'အ', 'ာ', 'ါ', 'ǻ', 'ǎ', 'ª', 'ა', 'अ', 'ا', 'ａ', 'ä'],
            'b' => ['б', 'β', 'ب', 'ဗ', 'ბ', 'ｂ'],
            'c' => ['ç', 'ć', 'č', 'ĉ', 'ċ', 'ｃ'],
            'd' => ['ď', 'ð', 'đ', 'ƌ', 'ȡ', 'ɖ', 'ɗ', 'ᵭ', 'ᶁ', 'ᶑ', 'д', 'δ', 'د', 'ض', 'ဍ', 'ဒ', 'დ', 'ｄ'],
            'e' => ['é', 'è', 'ẻ', 'ẽ', 'ẹ', 'ê', 'ế', 'ề', 'ể', 'ễ', 'ệ', 'ë', 'ē', 'ę', 'ě', 'ĕ', 'ė', 'ε', 'έ', 'ἐ', 'ἑ', 'ἒ', 'ἓ', 'ἔ', 'ἕ', 'ὲ', 'έ', 'е', 'ё', 'э', 'є', 'ə', 'ဧ', 'ေ', 'ဲ', 'ე', 'ए', 'إ', 'ئ', 'ｅ'],
            'f' => ['ф', 'φ', 'ف', 'ƒ', 'ფ', 'ｆ'],
            'g' => ['ĝ', 'ğ', 'ġ', 'ģ', 'г', 'ґ', 'γ', 'ဂ', 'გ', 'گ', 'ｇ'],
            'h' => ['ĥ', 'ħ', 'η', 'ή', 'ح', 'ه', 'ဟ', 'ှ', 'ჰ', 'ｈ'],
            'i' => ['í', 'ì', 'ỉ', 'ĩ', 'ị', 'î', 'ï', 'ī', 'ĭ', 'į', 'ı', 'ι', 'ί', 'ϊ', 'ΐ', 'ἰ', 'ἱ', 'ἲ', 'ἳ', 'ἴ', 'ἵ', 'ἶ', 'ἷ', 'ὶ', 'ί', 'ῐ', 'ῑ', 'ῒ', 'ΐ', 'ῖ', 'ῗ', 'і', 'ї', 'и', 'ဣ', 'ိ', 'ီ', 'ည်', 'ǐ', 'ი', 'इ', 'ی', 'ｉ'],
            'j' => ['ĵ', 'ј', 'Ј', 'ჯ', 'ج', 'ｊ'],
            'k' => ['ķ', 'ĸ', 'к', 'κ', 'Ķ', 'ق', 'ك', 'က', 'კ', 'ქ', 'ک', 'ｋ'],
            'l' => ['ł', 'ľ', 'ĺ', 'ļ', 'ŀ', 'л', 'λ', 'ل', 'လ', 'ლ', 'ｌ'],
            'm' => ['м', 'μ', 'م', 'မ', 'მ', 'ｍ'],
            'n' => ['ñ', 'ń', 'ň', 'ņ', 'ŉ', 'ŋ', 'ν', 'н', 'ن', 'န', 'ნ', 'ｎ'],
            'o' => ['ó', 'ò', 'ỏ', 'õ', 'ọ', 'ô', 'ố', 'ồ', 'ổ', 'ỗ', 'ộ', 'ơ', 'ớ', 'ờ', 'ở', 'ỡ', 'ợ', 'ø', 'ō', 'ő', 'ŏ', 'ο', 'ὀ', 'ὁ', 'ὂ', 'ὃ', 'ὄ', 'ὅ', 'ὸ', 'ό', 'о', 'و', 'θ', 'ို', 'ǒ', 'ǿ', 'º', 'ო', 'ओ', 'ｏ', 'ö'],
            'p' => ['п', 'π', 'ပ', 'პ', 'پ', 'ｐ'],
            'q' => ['ყ', 'ｑ'],
            'r' => ['ŕ', 'ř', 'ŗ', 'р', 'ρ', 'ر', 'რ', 'ｒ'],
            's' => ['ś', 'š', 'ş', 'с', 'σ', 'ș', 'ς', 'س', 'ص', 'စ', 'ſ', 'ს', 'ｓ'],
            't' => ['ť', 'ţ', 'т', 'τ', 'ț', 'ت', 'ط', 'ဋ', 'တ', 'ŧ', 'თ', 'ტ', 'ｔ'],
            'u' => ['ú', 'ù', 'ủ', 'ũ', 'ụ', 'ư', 'ứ', 'ừ', 'ử', 'ữ', 'ự', 'û', 'ū', 'ů', 'ű', 'ŭ', 'ų', 'µ', 'у', 'ဉ', 'ု', 'ူ', 'ǔ', 'ǖ', 'ǘ', 'ǚ', 'ǜ', 'უ', 'उ', 'ｕ', 'ў', 'ü'],
            'v' => ['в', 'ვ', 'ϐ', 'ｖ'],
            'w' => ['ŵ', 'ω', 'ώ', 'ဝ', 'ွ', 'ｗ'],
            'x' => ['χ', 'ξ', 'ｘ'],
            'y' => ['ý', 'ỳ', 'ỷ', 'ỹ', 'ỵ', 'ÿ', 'ŷ', 'й', 'ы', 'υ', 'ϋ', 'ύ', 'ΰ', 'ي', 'ယ', 'ｙ'],
            'z' => ['ź', 'ž', 'ż', 'з', 'ζ', 'ز', 'ဇ', 'ზ', 'ｚ'],
            'aa' => ['ع', 'आ', 'آ'],
            'ae' => ['æ', 'ǽ'],
            'ai' => ['ऐ'],
            'ch' => ['ч', 'ჩ', 'ჭ', 'چ'],
            'dj' => ['ђ', 'đ'],
            'dz' => ['џ', 'ძ'],
            'ei' => ['ऍ'],
            'gh' => ['غ', 'ღ'],
            'ii' => ['ई'],
            'ij' => ['ĳ'],
            'kh' => ['х', 'خ', 'ხ'],
            'lj' => ['љ'],
            'nj' => ['њ'],
            'oe' => ['ö', 'œ', 'ؤ'],
            'oi' => ['ऑ'],
            'oii' => ['ऒ'],
            'ps' => ['ψ'],
            'sh' => ['ш', 'შ', 'ش'],
            'shch' => ['щ'],
            'ss' => ['ß'],
            'sx' => ['ŝ'],
            'th' => ['þ', 'ϑ', 'ث', 'ذ', 'ظ'],
            'ts' => ['ц', 'ც', 'წ'],
            'ue' => ['ü'],
            'uu' => ['ऊ'],
            'ya' => ['я'],
            'yu' => ['ю'],
            'zh' => ['ж', 'ჟ', 'ژ'],
            '(c)' => ['©'],
            'A' => ['Á', 'À', 'Ả', 'Ã', 'Ạ', 'Ă', 'Ắ', 'Ằ', 'Ẳ', 'Ẵ', 'Ặ', 'Â', 'Ấ', 'Ầ', 'Ẩ', 'Ẫ', 'Ậ', 'Å', 'Ā', 'Ą', 'Α', 'Ά', 'Ἀ', 'Ἁ', 'Ἂ', 'Ἃ', 'Ἄ', 'Ἅ', 'Ἆ', 'Ἇ', 'ᾈ', 'ᾉ', 'ᾊ', 'ᾋ', 'ᾌ', 'ᾍ', 'ᾎ', 'ᾏ', 'Ᾰ', 'Ᾱ', 'Ὰ', 'Ά', 'ᾼ', 'А', 'Ǻ', 'Ǎ', 'Ａ', 'Ä'],
            'B' => ['Б', 'Β', 'ब', 'Ｂ'],
            'C' => ['Ç', 'Ć', 'Č', 'Ĉ', 'Ċ', 'Ｃ'],
            'D' => ['Ď', 'Ð', 'Đ', 'Ɖ', 'Ɗ', 'Ƌ', 'ᴅ', 'ᴆ', 'Д', 'Δ', 'Ｄ'],
            'E' => ['É', 'È', 'Ẻ', 'Ẽ', 'Ẹ', 'Ê', 'Ế', 'Ề', 'Ể', 'Ễ', 'Ệ', 'Ë', 'Ē', 'Ę', 'Ě', 'Ĕ', 'Ė', 'Ε', 'Έ', 'Ἐ', 'Ἑ', 'Ἒ', 'Ἓ', 'Ἔ', 'Ἕ', 'Έ', 'Ὲ', 'Е', 'Ё', 'Э', 'Є', 'Ə', 'Ｅ'],
            'F' => ['Ф', 'Φ', 'Ｆ'],
            'G' => ['Ğ', 'Ġ', 'Ģ', 'Г', 'Ґ', 'Γ', 'Ｇ'],
            'H' => ['Η', 'Ή', 'Ħ', 'Ｈ'],
            'I' => ['Í', 'Ì', 'Ỉ', 'Ĩ', 'Ị', 'Î', 'Ï', 'Ī', 'Ĭ', 'Į', 'İ', 'Ι', 'Ί', 'Ϊ', 'Ἰ', 'Ἱ', 'Ἳ', 'Ἴ', 'Ἵ', 'Ἶ', 'Ἷ', 'Ῐ', 'Ῑ', 'Ὶ', 'Ί', 'И', 'І', 'Ї', 'Ǐ', 'ϒ', 'Ｉ'],
            'J' => ['Ｊ'],
            'K' => ['К', 'Κ', 'Ｋ'],
            'L' => ['Ĺ', 'Ł', 'Л', 'Λ', 'Ļ', 'Ľ', 'Ŀ', 'ल', 'Ｌ'],
            'M' => ['М', 'Μ', 'Ｍ'],
            'N' => ['Ń', 'Ñ', 'Ň', 'Ņ', 'Ŋ', 'Н', 'Ν', 'Ｎ'],
            'O' => ['Ó', 'Ò', 'Ỏ', 'Õ', 'Ọ', 'Ô', 'Ố', 'Ồ', 'Ổ', 'Ỗ', 'Ộ', 'Ơ', 'Ớ', 'Ờ', 'Ở', 'Ỡ', 'Ợ', 'Ø', 'Ō', 'Ő', 'Ŏ', 'Ο', 'Ό', 'Ὀ', 'Ὁ', 'Ὂ', 'Ὃ', 'Ὄ', 'Ὅ', 'Ὸ', 'Ό', 'О', 'Θ', 'Ө', 'Ǒ', 'Ǿ', 'Ｏ', 'Ö'],
            'P' => ['П', 'Π', 'Ｐ'],
            'Q' => ['Ｑ'],
            'R' => ['Ř', 'Ŕ', 'Р', 'Ρ', 'Ŗ', 'Ｒ'],
            'S' => ['Ş', 'Ŝ', 'Ș', 'Š', 'Ś', 'С', 'Σ', 'Ｓ'],
            'T' => ['Ť', 'Ţ', 'Ŧ', 'Ț', 'Т', 'Τ', 'Ｔ'],
            'U' => ['Ú', 'Ù', 'Ủ', 'Ũ', 'Ụ', 'Ư', 'Ứ', 'Ừ', 'Ử', 'Ữ', 'Ự', 'Û', 'Ū', 'Ů', 'Ű', 'Ŭ', 'Ų', 'У', 'Ǔ', 'Ǖ', 'Ǘ', 'Ǚ', 'Ǜ', 'Ｕ', 'Ў', 'Ü'],
            'V' => ['В', 'Ｖ'],
            'W' => ['Ω', 'Ώ', 'Ŵ', 'Ｗ'],
            'X' => ['Χ', 'Ξ', 'Ｘ'],
            'Y' => ['Ý', 'Ỳ', 'Ỷ', 'Ỹ', 'Ỵ', 'Ÿ', 'Ῠ', 'Ῡ', 'Ὺ', 'Ύ', 'Ы', 'Й', 'Υ', 'Ϋ', 'Ŷ', 'Ｙ'],
            'Z' => ['Ź', 'Ž', 'Ż', 'З', 'Ζ', 'Ｚ'],
            'AE' => ['Æ', 'Ǽ'],
            'Ch' => ['Ч'],
            'Dj' => ['Ђ'],
            'Dz' => ['Џ'],
            'Gx' => ['Ĝ'],
            'Hx' => ['Ĥ'],
            'Ij' => ['Ĳ'],
            'Jx' => ['Ĵ'],
            'Kh' => ['Х'],
            'Lj' => ['Љ'],
            'Nj' => ['Њ'],
            'Oe' => ['Œ'],
            'Ps' => ['Ψ'],
            'Sh' => ['Ш'],
            'Shch' => ['Щ'],
            'Ss' => ['ẞ'],
            'Th' => ['Þ'],
            'Ts' => ['Ц'],
            'Ya' => ['Я'],
            'Yu' => ['Ю'],
            'Zh' => ['Ж'],
            ' ' => ["\xC2\xA0", "\xE2\x80\x80", "\xE2\x80\x81", "\xE2\x80\x82", "\xE2\x80\x83", "\xE2\x80\x84", "\xE2\x80\x85", "\xE2\x80\x86", "\xE2\x80\x87", "\xE2\x80\x88", "\xE2\x80\x89", "\xE2\x80\x8A", "\xE2\x80\xAF", "\xE2\x81\x9F", "\xE3\x80\x80", "\xEF\xBE\xA0"],
        ];
    }

    /**
     * Returns the language specific replacements for the ascii method.
     *
     * Note: Adapted from Stringy\Stringy.
     *
     * @see https://github.com/danielstjules/Stringy/blob/3.1.0/LICENSE.txt
     *
     * @param string $language
     *
     * @return array|null
     */
    protected static function languageSpecificCharsArray($language)
    {
        static $languageSpecific;

        if (!isset($languageSpecific)) {
            $languageSpecific = [
                'bg' => [
                    ['х', 'Х', 'щ', 'Щ', 'ъ', 'Ъ', 'ь', 'Ь'],
                    ['h', 'H', 'sht', 'SHT', 'a', 'А', 'y', 'Y'],
                ],
                'de' => [
                    ['ä',  'ö',  'ü',  'Ä',  'Ö',  'Ü'],
                    ['ae', 'oe', 'ue', 'AE', 'OE', 'UE'],
                ],
            ];
        }

        return $languageSpecific[$language] ?? null;
    }

    /**
     * Checks if the string is valid for UTF-8 encoding.
     *
     * @param string  byte stream to check
     *
     * @return bool
     */
    public static function checkEncoding($s): bool
    {
        return $s === static::fixEncoding($s);
    }

    /**
     * Removes invalid code unit sequences from UTF-8 string.
     *
     * @param string  byte stream to fix
     *
     * @return string
     */
    public static function fixEncoding($s): string
    {
        // removes xD800-xDFFF, x110000 and higher
        return \htmlspecialchars_decode(
            \htmlspecialchars($s, \ENT_NOQUOTES | \ENT_IGNORE, 'UTF-8'),
            \ENT_NOQUOTES
        );
    }

    /**
     * Splits a string by a string.
     *
     * Returns an array of strings, each of which is a substring of string formed by splitting
     * it on boundaries formed by the string delimiter.
     *
     * @param string $delimiter The boundary string.
     * @param string $string    The input string.
     * @param int    $limit     If limit is set and positive, the returned array will contain a maximum of limit
     *                          elements with the last element containing the rest of string. If the limit
     *                          parameter is negative, all components except the last -limit are returned. If
     *                          the limit parameter is zero, then this is treated as 1.
     *
     * @return array
     */
    public static function explode(string $delimiter = null, string $string = null, int $limit = \PHP_INT_MAX): array
    {
        return \explode($delimiter, $string, $limit);
    }

    /**
     * Repeat the string multiple times.
     *
     * @param string $string     String to repeat
     * @param int    $multiplier Number of times to repeat the string
     *
     * @return string
     */
    public static function repeat(string $string = null, int $multiplier = 0): string
    {
        $repeated = \str_repeat($string, $multiplier);

        return $repeated;
    }

    /**
     * Get class name without namespace
     *
     * @param mixed $object
     * @param bool  $toLower
     *
     * @return mixed|string
     */
    public static function getClassName($object = null, bool $toLower = false)
    {
        $className = $object;

        if (\is_object($object)) {
            $className = \get_class($object);
        }

        $retVal = $className;

        if (static::strPos($className, '\\') !== false) {
            $className = static::explode('\\', $className);
            \reset($className);
            $retVal = \end($className);
        }

        if ($toLower) {
            $retVal = static::toLowerCase($retVal);
        }

        return $retVal;
    }

    /**
     * Get class namespace.
     *
     * @param mixed $object
     * @param bool  $toLower
     *
     * @return mixed
     */
    public static function getClassNamespace($object = null)
    {
        $className = $object;

        if (\is_object($object)) {
            $className = \get_class($object);
        }

        $retVal = $className;

        if (static::strPos($className, '\\') !== false) {
            $className = static::explode('\\', $className);
            $retVal = Arr::slice($className, 0, -1);
        }

        return $retVal;
    }

    public static function parseClassName($object = null, bool $toLower = false): array
    {
        $retVal = [
            'namespace' => static::getClassNamespace($object),
            'classname' => static::getClassName($object, $toLower),
        ];

        // if ($toObject) {
        //     $retVal = Arr::toObject($retVal);
        // }

        return $retVal;
    }

    private static function utf8(int $number = 0)
    {
        if ($number <= 0x7F) {
            return static::char($number);
        }

        if ($number <= 0x7FF) {
            return static::char(($number >> 6) + 192).static::char(($number & 63) + 128);
        }

        if ($number <= 0xFFFF) {
            return static::char(($number >> 12) + 224).static::char((($number >> 6) & 63) + 128).static::char(($number & 63) + 128);
        }

        if ($number <= 0x1FFFFF) {
            return static::char(($number >> 18) + 240).static::char((($number >> 12) & 63) + 128).static::char((($number >> 6) & 63) + 128).static::char(($number & 63) + 128);
        }

        return '';
    }

    private static function toCodePoint(string $char = null, string $encoding = 'UTF-8'): int
    {
        $string = \mb_convert_encoding($char, 'UTF-32', $encoding);
        $length = static::length($string);
        $retVal = [];

        for ($x = 0; $x < $length; ++$x) {
            $retVal[] = \hexdec(\bin2hex(static::sub($string, $x, 1)));
        }

        return \intval(\implode('', $retVal));
    }

    private static function toHex(string $string = null, string $encoding = 'UTF-8'): string
    {
        $hex = '';

        for ($x = 0; $x < static::length($string); ++$x) {
            $id = \ord(static::sub($string, $x, 1));
            $hex .= ($id <= 128) ? static::sub($string, $x, 1): static::toUnicodeCode(static::sub($string, $x, 1), $encoding);
        }

        return $hex;
    }

    private static function toUnicodeCode(string $char = null, string $encoding = 'UTF-8'): string
    {
        $string = \mb_convert_encoding($char, 'UTF-32', $encoding);
        $length = static::length($string);
        $retVal = [];

        for ($x = 0; $x < $length; ++$x) {
            $tmp = \bin2hex(static::sub($string, $x, 1));
            $tmp = \hexdec($tmp);
            $retVal[] = '\u'.static::sub($tmp, 4, 8);
        }

        return \implode('', $retVal);
    }

    /**
     * A unicode/multibyte capable 'ord'.
     *
     * @param char|string $char
     *
     * @return integer|boolean
     */
    public static function uniOrd(string $char = null)
    {
        if ($char === EMPTY_STRING) {
            return static::uniOrd("\0");
        }

        $ord0 = \ord($char[0]);

        if ($ord0 >= 0 && $ord0 <= 127) {
            return $ord0;
        }

        $ord1 = \ord($char[1]);

        if ($ord0 >= 192 && $ord0 <= 223) {
            return ($ord0 - 192) * 64 + ($ord1 - 128);
        }

        $ord2 = \ord($char[2]);

        if ($ord0 >= 224 && $ord0 <= 239) {
            return ($ord0 - 224) * 4096 + ($ord1 - 128) * 64 + ($ord2 - 128);
        }

        $ord3 = \ord($char[3]);

        if ($ord0 >= 240 && $ord0 <= 247) {
            return ($ord0 - 240) * 262144 + ($ord1 - 128) * 4096 + ($ord2 - 128) * 64 + ($ord3 - 128);
        }

        return false;
    }

    public static function ord(string $char = null, string $encoding = 'UTF-8'): int
    {
        if (static::length($char) > 1) {
            throw new \Exception('Char length is greater than 1');
        }

        // if (static::isUnicode($char)) {
        //     return \mb_ord($char);
        // }

        if (static::isUnicode($char) and static::isMbString()) {
            return \mb_ord($char);
        }

        if (static::isIntl()) {
            return \IntlChar::ord($char);
        }

        return \ord($char);
    }

    /**
     * Returns a specific character in UTF-8.
     *
     * @param int     code point (0x0 to 0xD7FF or 0xE000 to 0x10FFFF)
     *
     * @return string
     *
     * @throws \InvalidArgumentException if code point is not in valid range
     */
    public static function chr(int $code = 0): string
    {
        // if ($code < 0 || ($code >= 0xD800 && $code <= 0xDFFF) || $code > 0x10FFFF) {
        //     throw new \InvalidArgumentException('Code point must be in range 0x0 to 0xD7FF or 0xE000 to 0x10FFFF.');
        // }

        // if (static::isIconv()) {
        //     return \iconv('UTF-32BE', 'UTF-8//IGNORE', \pack('N', $code));
        //     // return \iconv('UTF-32', 'UTF-8', \pack('V', $code));
        // }

        // return \chr($code);

        return \IntlChar::chr($code);
    }

    /**
     * Returns a specific character in UTF-8.
     *
     * @param int     code point (0x0 to 0xD7FF or 0xE000 to 0x10FFFF)
     *
     * @return string
     *
     * @throws \InvalidArgumentException if code point is not in valid range
     */
    public static function char(int $code = 0): string
    {
        if (static::isUnicode()) {
            return \mb_chr($code);
        }

        return static::chr($code);
    }

    /**
     * Returns a string created by using the specified sequence of code points.
     *
     * @param int $arguments a sequence of code points
     *
     * @return string
     */
    public static function fromCodePoint(int ...$arguments): string
    {
        $retVal = '';

        if (count($arguments) > 0) {
            foreach ($arguments as $argument) {
                $retVal .= static::char($argument);
            }
        }

        return $retVal;
    }

    /**
     * Returns a string created from the specified sequence of UTF-16 code units.
     *
     * @param int $arguments A sequence of numbers that are UTF-16 code units.
     *                       The range is between 0 and 65535 (0xFFFF). Numbers greater than 0xFFFF are truncated.
     *                       No validity checks are performed.
     *
     * @return string
     */
    public static function fromCharCode(int ...$arguments): string
    {
        $retVal = '';

        if (\count($arguments) > 0) {
            foreach ($arguments as $argument) {
                $retVal .= static::char($argument);
            }
        }

        return $retVal;
    }

    /**
     * Returns a new string consisting of the single UTF-16 code unit located
     * at the specified offset into the string.
     *
     * @param string|null $string
     * @param int|int     $position
     *
     * @return string
     */
    public static function charAt(string $string = null, int $position = null): ?string
    {
        $rv = null;

        if (\is_null($position)) {
            $position = static::length($string) - 1;
        }

        $length = static::length($string);

        if ($position < $length) {
            if (static::isUnicode($string)) {
                $string = \mb_convert_encoding($string, self::$encoding, 'UTF-8');
                $length = static::length($string);

                $rv = \hexdec(\bin2hex(static::sub($string, $position, 1)));
            } else {
                $rv = static::ord($string[$position]);
            }
        }

        return ($rv) ? static::char($rv): $rv;
    }

    public static function codePointAt(string $string = null, int $position = null): int
    {
        if (\is_null($position)) {
            $position = static::length($string) - 1;
        }

        if (static::isUnicode($string)) {
            $string = \mb_convert_encoding($string, self::$encoding, 'UTF-8');
            $length = static::length($string);

            $rv = \hexdec(\bin2hex(static::sub($string, $position, 1)));
        } else {
            $rv = static::ord($string[$position]);
        }

        return $rv;
    }

    public static function charCodeAt(string $string = null, int $position = null): int
    {
        $length = 0;

        if (\is_null($position)) {
            $position = static::length($string) - 1;
        }

        if (static::isUnicode($string)) {
            $string = \mb_convert_encoding($string, self::$encoding, 'UTF-8');
            $length = static::length($string);

            $rv = \hexdec(\bin2hex(static::sub($string, $position, 1)));
        } else {
            $rv = static::ord($string[$position]);
        }

        return $rv;
    }

    public static function unicodeCodePointToUtf8($code = null): string
    {
        \is_string($code) && \sscanf($code, 'U+%x', $code);

        if ($code < 0) {
            throw new \InvalidArgumentException('Lower than 0x00.');
        }

        if ($code > 0x10FFFD) {
            throw new \InvalidArgumentException('Larger than 0x10FFFD.');
        }

        if (0xD800 <= $code && $code <= 0xDFFF) {
            throw new \InvalidArgumentException(sprintf('High and low surrogate halves are invalid unicode codepoints (U+D800 through U+DFFF, is U+%04X).', $code));
        }

        if ($code <= 0x7F) {
            return static::char($code);
        }

        if ($code <= 0x7FF) {
            return static::char(0xC0 | $code >> 6 & 0x1F).static::char(0x80 | $code & 0x3F);
        }

        if ($code <= 0xFFFF) {
            return static::char(0xE0 | $code >> 12 & 0xF).static::char(0x80 | $code >> 6 & 0x3F).static::char(0x80 | $code & 0x3F);
        }

        return static::char(0xF0 | $code >> 18 & 0x7).static::char(0x80 | $code >> 12 & 0x3F).static::char(0x80 | $code >> 6 & 0x3F).static::char(0x80 | $code & 0x3F);
    }

    /**
     * Converts to web safe characters [a-z0-9-] text.
     *
     * @param string  UTF-8 encoding
     * @param string  allowed characters
     * @param bool
     *
     * @return string
     */
    public static function webalize($s, $charList = null, bool $lower = true): string
    {
        $s = static::toAscii($s);

        if ($lower) {
            $s = static::lower($s);
        }

        $s = \preg_replace('#[^a-z0-9'.(null !== $charList ? \preg_quote($charList, '#'): '').']+#i', '-', $s);
        $s = \trim($s, '-');

        return $s;
    }

    /**
     * Truncates string to maximal length.
     *
     * @param string  UTF-8 encoding
     * @param int
     * @param string  UTF-8 encoding
     *
     * @return string
     */
    public static function truncate($s, int $maxLen = 0, $append = "\xE2\x80\xA6"): string
    {
        if (static::length($s) > $maxLen) {
            $maxLen = $maxLen - static::length($append);

            if ($maxLen < 1) {
                return $append;
            } elseif ($matches = static::match($s, '#^.{1,'.$maxLen.'}(?=[\s\x00-/:-@\[-`{-~])#us')) {
                return $matches[0].$append;
            } else {
                return static::sub($s, 0, $maxLen).$append;
            }
        }

        return $s;
    }

    /**
     * Indents the content from the left.
     *
     * @param string  UTF-8 encoding or 8-bit
     * @param int
     * @param string
     *
     * @return string
     */
    public static function indent(
        $string = '',
        int $level = 1,
        string $chars = "\t"
    ): ?string {
        if ($level > 0) {
            $string = static::regexReplace(
                $string,
                '#(?:^|[\r\n]+)(?=[^\r\n])#',
                '$0' . str_repeat($chars, $level)
            );
        }

        return $string;
    }

    /**
     * Indents the content from.
     *
     * @param string  UTF-8 encoding or 8-bit
     * @param int
     * @param string
     *
     * @return string
     */
    public static function replaceIndents(
        $string = '',
        int $level = 1,
        string $chars = "\t"
    ): ?string {
        if ($level > 0) {
            $string = static::regexReplace(
                $string,
                '#(?:^|[\s]+)(?=[^\s])#',
                '$0' . str_repeat($chars, $level)
            );
        }

        return $string;
    }

    /**
     * Performs a regular expression match.
     *
     * @param string
     * @param string
     * @param int  can be PREG_OFFSET_CAPTURE (returned in bytes)
     * @param int  offset in bytes
     *
     * @return mixed
     */
    public static function match(string $subject, string $pattern, int $flags = 0, int $offset = 0)
    {
        if ($offset > static::length($subject)) {
            return null;
        }

        $m = [];

        return static::pcre('preg_match', [$pattern, $subject, &$m, $flags, $offset])
            ? $m
            : null;
    }

    /**
     * Performs a global regular expression match.
     *
     * @param string
     * @param string
     * @param int  can be PREG_OFFSET_CAPTURE (returned in bytes); PREG_SET_ORDER is default
     * @param int  offset in bytes
     *
     * @return array
     */
    public static function matchAll(string $subject = null, string $pattern = null, int $flags = 0, int $offset = 0): ?array
    {
        if ($offset > strlen($subject)) {
            return [];
        }

        static::pcre(
            'preg_match_all',
            [
            $pattern, $subject, &$m,
            ($flags & PREG_PATTERN_ORDER) ? $flags : ($flags | PREG_SET_ORDER),
            $offset,
            ]
        );

        return $m;
    }

    /**
     * Removes special controls characters and normalizes line endings and spaces.
     *
     * @param string  UTF-8 encoding
     *
     * @return string
     */
    public static function normalize($s): string
    {
        $s = static::normalizeNewLines($s);

        // Remove control characters; leave \t + \n
        $s = \preg_replace('#[\x00-\x08\x0B-\x1F\x7F-\x9F]+#u', '', $s);

        // Right trim
        $s = \preg_replace('#[\t ]+$#m', '', $s);

        // Leading and trailing blank lines
        $s = \trim($s, "\n");

        return $s;
    }

    /**
     * Standardize line endings to unix-like.
     *
     * @param string  UTF-8 encoding or 8-bit
     *
     * @return string
     */
    public static function normalizeNewLines($s): string
    {
        return \str_replace(["\r\n", "\r"], "\n", $s);
    }

    /**
     * Replaces the format items in a specified string with the string representation of n specified objects.
     *
     * @param string $format    A composit format string
     * @param mixed  $arguments Variable number of items to format.
     *
     * @return string Returns a copy of format in which the format items have been
     *     replaced by the string representations of arg0, arg1,... argN.
     */
    public static function format(string $format = null, ...$arguments): string
    {
        $retVal = '';
        static $mode = 0x01;

        if (\count($arguments) == 1) {
            $arguments = $arguments[0];

            if (! \is_array($arguments)) {
                $arguments = [$arguments];
            }
        }

        // Translate %(name)x -> positional arguments
        $x = 0;

        foreach ($arguments as $key => $value) {
            // $value = \json_encode($value);
            // $value = static::trimStart($value, '"');
            // $value = static::trimEnd($value, '"');

            if (static::match($format, '/(\('.$key.'\))(\w)/')) {
                $mode = 0x01;
                // $format = \preg_replace('/(\('.$key.'\))(\w)/', ++$x.'$$2', $format);
                $format = static::regexReplace($format, '/(\('.$key.'\))(\w)/', ++$x.'$$2');
            } elseif (static::match($format, '/(\{'.$key.'\})/')) {
                $mode = 0x02;
                // $format = \preg_replace('/(\{'.$key.'\})/', (string) $value, $format);
                $format = static::regexReplace($format, '/(\{'.$key.'\})/', (string) $value);
            } elseif (static::match($format, '/(\%)(\w)/')) {
                $mode = 0x03;
                $format = static::regexReplace($format, '/(\%)(\w)/', '%$2');
            }
        }

        if ($mode === 0x01 or $mode === 0x02) {
            $retVal = \vsprintf($format, \array_values($arguments));
        } else {
            $retVal = \sprintf($format, ...$arguments);
        }

        return $retVal;
    }

    /**
     * Converts to ASCII.
     *
     * @param string  UTF-8 encoding
     *
     * @return string ASCII
     */
    public static function toAscii($s): string
    {
        static $transliterator = null;

        if (null === $transliterator && \class_exists('Transliterator', false)) {
            $transliterator = \Transliterator::create('Any-Latin; Latin-ASCII');
        }

        $s = \preg_replace('#[^\x09\x0A\x0D\x20-\x7E\xA0-\x{2FF}\x{370}-\x{10FFFF}]#u', '', $s);
        $s = \strtr($s, '`\'"^~?', "\x01\x02\x03\x04\x05\x06");
        $s = \str_replace(
            ["\xE2\x80\x9E", "\xE2\x80\x9C", "\xE2\x80\x9D", "\xE2\x80\x9A", "\xE2\x80\x98", "\xE2\x80\x99", "\xC2\xB0"],
            ["\x03", "\x03", "\x03", "\x02", "\x02", "\x02", "\x04"],
            $s
        );

        if (null !== $transliterator) {
            $s = $transliterator->transliterate($s);
        }

        if (\ICONV_IMPL === 'glibc') {
            $s = \str_replace(
                ["\xC2\xBB", "\xC2\xAB", "\xE2\x80\xA6", "\xE2\x84\xA2", "\xC2\xA9", "\xC2\xAE"],
                ['>>', '<<', '...', 'TM', '(c)', '(R)'],
                $s
            );

            $s = \iconv('UTF-8', 'WINDOWS-1250//TRANSLIT//IGNORE', $s);

            $s = \strtr(
                $s,
                "\xa5\xa3\xbc\x8c\xa7\x8a\xaa\x8d\x8f\x8e\xaf\xb9\xb3\xbe\x9c\x9a\xba\x9d\x9f\x9e"
                ."\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3"
                ."\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8"
                ."\xe9\xea\xeb\xec\xed\xee\xef\xf0\xf1\xf2\xf3\xf4\xf5\xf6\xf8\xf9\xfa\xfb\xfc\xfd\xfe"
                ."\x96\xa0\x8b\x97\x9b\xa6\xad\xb7",
                'ALLSSSSTZZZallssstzzzRAAAALCCCEEEEIIDDNNOOOOxRUUUUYTsraaaalccceeeeiiddnnooooruuuuyt- <->|-.'
            );

            $s = \preg_replace('#[^\x00-\x7F]++#', '', $s);
        } else {
            $s = \iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', $s);
        }

        $s = \str_replace(['`', "'", '"', '^', '~', '?'], '', $s);

        return \strtr($s, "\x01\x02\x03\x04\x05\x06", '`\'"^~?');
    }

    /**
     * Convert entities, while preserving already-encoded entities.
     *
     * @param string $string The text to be converted
     *
     * @return string
     */
    public static function htmlEntities(string $string = null, bool $preserve_encoded_entities = false): string
    {
        if ($preserve_encoded_entities) {
            // @codeCoverageIgnoreStart
            if (defined('HHVM_VERSION')) {
                $translation_table = \get_html_translation_table(\HTML_ENTITIES, \ENT_QUOTES);
            } else {
                $translation_table = \get_html_translation_table(\HTML_ENTITIES, \ENT_QUOTES, static::mbInternalEncoding());
            }
            // @codeCoverageIgnoreEnd

            $translation_table[chr(38)] = '&';
            return \preg_replace('/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/', '&amp;', \strtr($string, $translation_table));
        }

        return \htmlentities($string, \ENT_QUOTES, static::mbInternalEncoding());
    }

    /**
     * Convert >, <, ', " and & to html entities, but preserves entities that
     * are already encoded.
     *
     * @param string $string The text to be converted
     *
     * @return string
     */
    public static function htmlSpecialChars(string $string = null, bool $preserve_encoded_entities = false): string
    {
        if ($preserve_encoded_entities) {
            // @codeCoverageIgnoreStart
            if (defined('HHVM_VERSION')) {
                $translation_table = \get_html_translation_table(\HTML_SPECIALCHARS, \ENT_QUOTES);
            } else {
                $translation_table = \get_html_translation_table(\HTML_SPECIALCHARS, \ENT_QUOTES, static::mbInternalEncoding());
            }
            // @codeCoverageIgnoreEnd

            $translation_table[chr(38)] = '&';

            return \preg_replace('/&(?![A-Za-z]{0,4}\w{2,3};|#[0-9]{2,3};)/', '&amp;', \strtr($string, $translation_table));
        }

        return htmlentities($string, ENT_QUOTES, static::mbInternalEncoding());
    }

    /**
     * Generates a string of random characters.
     *
     * @throws LengthException  If $length is bigger than the available
     *                           character pool and $noDuplicateChars is
     *                           enabled
     *
     * @param  integer $length           The length of the string to
     *                                   generate
     * @param  boolean $humanFriendly    Whether or not to make the
     *                                   string human friendly by
     *                                   removing characters that can be
     *                                   confused with other characters (
     *                                   O and 0, l and 1, etc)
     * @param  boolean $includeSymbols   Whether or not to include
     *                                   symbols in the string. Can not
     *                                   be enabled if $humanFriendly is
     *                                   true
     * @param  boolean $noDuplicateChars Whether or not to only use
     *                                   characters once in the string.
     * @return string
     */
    public static function randomString(
        ?int $length = 16,
        ?bool $humanFriendly = true,
        ?bool $includeSymbols = false,
        ?bool $noDuplicateChars = false
    ): string {
        $nice_chars = 'ABCDEFGHJKLMNPQRSTUVWXYZabcdefhjkmnprstuvwxyz23456789';
        $all_an     = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';
        $symbols    = '!@#$%^&*()~_-=+{}[]|:;<>,.?/"\'\\`';
        $string     = '';

        // Determine the pool of available characters based on the given parameters
        if ($humanFriendly) {
            $pool = $nice_chars;
        } else {
            $pool = $all_an;

            if ($includeSymbols) {
                $pool .= $symbols;
            }
        }

        if (!$noDuplicateChars) {
            return \substr(\str_shuffle(\str_repeat($pool, $length)), 0, $length);
        }

        // Don't allow duplicate letters to be disabled if the length is
        // longer than the available characters
        if ($noDuplicateChars && \strlen($pool) < $length) {
            throw new \LengthException('$length exceeds the size of the pool and $noDuplicateChars is enabled');
        }

        // Convert the pool of characters into an array of characters and
        // shuffle the array
        $pool       = \str_split($pool);
        $poolLength = \count($pool);
        $rand       = \mt_rand(0, $poolLength - 1);

        // Generate our string
        for ($i = 0; $i < $length; $i++) {
            $string .= $pool[$rand];

            // Remove the character from the array to avoid duplicates
            \array_splice($pool, $rand, 1);

            // Generate a new number
            if (($poolLength - 2 - $i) > 0) {
                $rand = \mt_rand(0, $poolLength - 2 - $i);
            } else {
                $rand = 0;
            }
        }

        return $string;
    }

    /**
     * Generate secure random string of given length
     * If 'openssl_random_pseudo_bytes' is not available
     * then generate random string using default function
     *
     * Part of the Laravel Project <https://github.com/laravel/laravel>
     *
     * @param int $length length of string
     *
     * @return bool
     */
    public static function secureRandomString(?int $length = 16): string
    {
        if (\function_exists('openssl_random_pseudo_bytes')) {
            $bytes = \openssl_random_pseudo_bytes($length * 2);

            if ($bytes === false) {
                throw new \LengthException('$length is not accurate, unable to generate random string');
            }

            return \substr(
                \str_replace(
                    ['/', '+', '='],
                    '',
                    \base64_encode($bytes)
                ),
                0,
                $length
            );
        }

        // @codeCoverageIgnoreStart
        return static::randomString($length, true, false, true);
        // @codeCoverageIgnoreEnd
    }

    /**
     * Escape all HTML entities in a string.
     *
     * @param  string $string
     * @return string
     */
    public static function escape(string $string = null): string
    {
        return \htmlentities($string, \ENT_QUOTES, 'UTF-8', false);
    }

    /**
     * Wrapper to prevent errors if the user doesn't have the mbstring
     * extension installed.
     *
     * @param string $encoding
     *
     * @return string
     */
    protected static function mbInternalEncoding(string $encoding = null): string
    {
        if (\function_exists('mb_internal_encoding')) {
            return $encoding ? \mb_internal_encoding($encoding): \mb_internal_encoding();
        }

        // @codeCoverageIgnoreStart
        return 'UTF-8';
        // @codeCoverageIgnoreEnd
    }

    /**
     * Return the URL to a user's gravatar.
     *
     * @param  string  $email The email of the user
     * @param  integer $size  The size of the gravatar
     * @return string
     */
    public static function gravatar(string $email = null, int $size = 32): string
    {
        if (Lang::isHttps()) {
            $url = 'https://secure.gravatar.com/';
        } else {
            $url = 'http://www.gravatar.com/';
        }

        $url .= 'avatar/' . \md5($email) . '?s=' . (int) \abs($size);

        return $url;
    }

    /**
     * isArabicAlt.
     *
     * Check if the given string is an Arabic string/text.
     *
     * @param string $string
     *
     * @return bool
     */
    public static function isArabic(string $string = null): bool
    {
        return \preg_match('/\p{Arabic}/ui', $string) > 0;
    }

    /**
     * Check if the given string is an Englisj string/text.
     *
     * @param string $string
     *
     * @return bool
     */
    public static function isEnglish(?string $string = null): bool
    {
        return \preg_match('/\p{English}/ui', $string) > 0;
    }

    /**
     * Check if the given string contains Arabic string/text.
     *
     * @param string $string
     *
     * @return bool
     */
    public static function containsArabic(string $string = null): bool
    {
        return \preg_match('/^([\x{0600}-\x{06FF}])/ui', $string) > 0;
    }

    public static function underscoredToCamelCase(string $string = null, bool $capitalizeFirstCharacter = false): string
    {
        $str = static::toLowerCase($string);

        $str = \str_replace('_', '', \ucwords($str, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = \lcfirst($str);
        }

        return $str;
    }

    public static function underscoredToStudlyCase(string $string = null, bool $capitalizeFirstCharacter = false): string
    {
        $str = static::toLowerCase($string);

        $str = \str_replace('_', '', \ucwords($str, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = \ucfirst($str);
        }

        return $str;
    }

    public static function dashesToCamelCase(string $string = null, bool $capitalizeFirstCharacter = false): string
    {
        $str = static::toLowerCase($string);

        $str = \str_replace('-', '', \ucwords($string, '-'));

        if (!$capitalizeFirstCharacter) {
            $str = \lcfirst($str);
        }

        return $str;
    }

    public static function dashesToStudlyCase(string $string = null, bool $capitalizeFirstCharacter = false): string
    {
        $str = static::toLowerCase($string);

        $str = \str_replace('-', '', \ucwords($string, '-'));

        if (!$capitalizeFirstCharacter) {
            $str = \ucfirst($str);
        }

        return $str;
    }

    /**
     *
     */
    public static function underscorize(string $string = null): string
    {
        return static::snake($string, '_');
    }

    /**
     *
     */
    public static function toUnderscoredCase(string $string = null): string
    {
        return static::snake($string, '_');
    }

    /**
     *
     */
    public static function dasherize(string $string = null): string
    {
        return static::toKebabCase($string);
    }

    /**
     *
     */
    public static function toDashedCase(string $string = null): string
    {
        return static::toKebabCase($string);
    }

    /**
     *
     */
    public static function toLowerCase(string $string = null): string
    {
        return static::lower($string);
    }

    /**
     *
     */
    public static function toUpperCase(string $string = null): string
    {
        return static::upper($string);
    }

    /**
     *
     */
    public static function toCamelCase(string $string = null): string
    {
        return static::camel($string);
    }

    /**
     *
     */
    public static function toStudlyCase(string $string = null): string
    {
        return static::studly($string);
    }

    /**
     *
     */
    public static function toKebabCase(string $string = null): string
    {
        return static::kebab($string);
    }

    /**
     *
     */
    public static function toSnakeCase(string $string = null): string
    {
        return static::snake($string);
    }

    /**
     *
     */
    public static function toTitleCase(string $string = null): string
    {
        return static::capitalize($string);
    }

    /**
     *
     */
    public static function lowerCaseFirst(string $string = null): string
    {
        return static::lcfirst($string);
    }

    /**
     *
     */
    public static function upperCaseFirst(string $string = null): string
    {
        return static::ucfirst($string);
    }

    /**
     *
     */
    public static function upperCaseWords(string $string = null): string
    {
        return static::ucwords($string);
    }

    /**
     *
     */
    public static function generateRandomString(?int $length = 48, ?bool $isReadable = true): string
    {
        return static::randomize($length, $isReadable);
    }

    /**
     *
     */
    public static function generateRandomToken(): string
    {
        $bytes = \random_bytes(32);
        return \base_convert(\bin2hex($bytes), 16, 36);
    }

    /**
     *
     * @internal
     */
    public static function pcre(?string $callback = null, ?array $arguments = [])
    {
        static $messages = [
            6                           => 'Failed due to limited JIT stack space', // PREG_JIT_STACKLIMIT_ERROR
            \PREG_BACKTRACK_LIMIT_ERROR => 'Backtrack limit was exhausted',
            \PREG_BAD_UTF8_ERROR        => 'Malformed UTF-8 data',
            \PREG_BAD_UTF8_OFFSET_ERROR => 'Offset didn\'t correspond to the begin of a valid UTF-8 code point',
            \PREG_INTERNAL_ERROR        => 'Internal error',
            \PREG_RECURSION_LIMIT_ERROR => 'Recursion limit was exhausted',
        ];

        $retVal = Callback::invokeSafe(
            $callback,
            $arguments,
            function ($message) use ($arguments) {
                // compile-time error, not detectable by preg_last_error
                throw new RegexpException($message.' in pattern: '.\implode(' or ', (array) $arguments[0]));
            }
        );

        if (($code = \preg_last_error()) // run-time error, but preg_last_error & return code are liars
            && (null === $retVal || !\in_array($callback, ['preg_filter', 'preg_replace_callback', 'preg_replace'], true))
        ) {
            throw new RegexpException(
                (isset($messages[$code]) ? $messages[$code] : 'Unknown error')
                .' (pattern: '.\implode(' or ', (array) $arguments[0]).')',
                $code
            );
        }

        return $retVal;
    }

    /**
     * Str constructor.
     */
    public function __construct()
    {
        self::$instance = &$this;
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(): self
    {
        if (self::$instance === null) {
            self::$instance = new static();
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(): self
    {
        return self::instance();
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        throw new \LogicException(
            'Class ' . \get_class($this) .
                ' is singleton and cannot be clonned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }
}
