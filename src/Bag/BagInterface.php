<?php

declare(strict_types=1);

namespace Exen\Support\Bag;

use ArrayAccess;
use Countable;
use InvalidArgumentException;
use IteratorAggregate;
use Exen\Support\Interfaces\Arrayable;
use Exen\Support\Interfaces\Hashable;
use Exen\Support\Interfaces\Jsonable;
use Exen\Support\Interfaces\JsonSerializable;
use Exen\Support\Interfaces\Serializable;
use Exen\Support\Interfaces\Yamlable;
use Exen\Support\Interfaces\YamlSerializable;

/**
 * A bag collection interface
 *
 * @package \Exen\Support\Bag
 */
interface BagInterface extends
    Arrayable,
    ArrayAccess,
    Countable,
    Hashable,
    IteratorAggregate,
    Jsonable,
    JsonSerializable,
    YamlSerializable,
    Serializable,
    Yamlable
{

    /**
     * The Constructor
     *
     * @param array $input
     */
    public function __construct(array $input);

    /**
     * Isolates a given namespace of annotations.
     *
     * @param string $pattern namespace
     * @return Bag
     */
    public function useNamespace($pattern);

    /**
     * Performs union operations against a given Bag
     *
     * @param Bag $bag The bag to be united
     * @return Bag Bag collection with union results
     */
    public function union(BagInterface $bag);

    /**
     * Filters annotations based on a regexp
     *
     * @param string $pattern Valid regexp
     * @return Bag Bag collection with filtered results
     * @throws InvalidArgumentException          If invalid regexp is passed
     */
    public function grep($pattern);

    /**
     * Retrieve values as an array even if there's only one single value
     *
     * @param string $key A valid tag
     * @return array
     */
    public function getAsArray($key);

    /**
     * Checks if a given is declared
     *
     * @param string $key A valid tag
     * @return boolean
     */
    public function has($key);

    /**
     * Set a single value
     *
     * @param string $key a valid tag
     * @param mixed $value the param value
     * @return self
     */
    public function set($index, $value);

    /**
     * Retrieves a single value
     *
     * @param string $index A valid tag
     * @return mixed|null
     */
    public function get($index = null, $default = null);
}
