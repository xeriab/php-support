<?php

declare(strict_types=1);

namespace Exen\Support\Bag;

use function in_array;

/**
 * Bag class.
 *
 * @since 0.1
 */
class Bag extends AbstractBag
{
    public function contains($key, $value)
    {
        return in_array($value, $this->get($key, null, false));
    }
}
