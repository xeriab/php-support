<?php

declare(strict_types=1);

namespace Exen\Support\Bag;

use ArrayIterator;
use RegexGuard\Factory as RegexGuard;
use RegexIterator;
use Exen\Support\Json\Json;
use Exen\Support\Traits\HashCodeTrait;
use Exen\Support\Yaml\Yaml;
use function array_flip;
use function array_intersect_key;
use function array_key_exists;
use function array_keys;
use function array_map;
use function call_user_func;
use function count;
use function explode;
use function filter_var;
use function implode;
use function is_array;
use function is_null;
use function is_object;
use function iterator_to_array;
use function preg_quote;
use function preg_replace;
use function serialize;
use function str_replace;
use function strpos;
use function unserialize;
use const FILTER_DEFAULT;
use const FILTER_REQUIRE_ARRAY;
use const FILTER_SANITIZE_NUMBER_INT;
use const FILTER_VALIDATE_BOOLEAN;

// use Exen\Support\Arr;

/**
 * Abstract bag class.
 *
 * @since 0.1
 */
abstract class AbstractBag implements BagInterface
{
    use HashCodeTrait;

    const SEPARATOR = '/[:\.]/';

    /**
     * Associative arrays of bags
     *
     * @var array
     */
    private $attributes = [];

    /**
     * AbstractBag constructor.
     *
     * @param mixed $input
     */
    public function __construct($input = null)
    {
        $this->attributes = $this->getArrayElements($input);

        // foreach ($input as $key => &$value) {
        //     if (is_array($value)) {
        //         $value = new static($value);
        //     }

        //     $this->{$key} = $value;
        // }
    }

    /**
     * Return the given items as an array
     *
     * @param mixed $attributes
     * @return array
     */
    protected function getArrayElements($attributes = null): array
    {
        $result = [];

        if (is_array($attributes)) {
            foreach ($attributes as $key => &$value) {
                // if (is_array($value) && Arr::isAssociative($value)) {
                //     $value = new static($value);
                // } elseif (is_array($value) && !Arr::isAssociative($value)) {
                //     $value = new static($value);
                // }

                if (is_array($value)) {
                    $value = new static($value);
                }

                $result[$key] = $value;
            }

            // $result = $attributes;
        } elseif ($attributes instanceof self) {
            $result = $attributes->all();
        } else {
            $result = (array)$attributes;
        }

        return $result;
    }

    /**
     * Returns the element.
     *
     * @return array An array of attributes
     */
    public function all()
    {
        return $this->attributes;
    }

    /**
     * Isolates a given namespace of bags.
     *
     * @param string $pattern namespace
     * @param array $delimiters possible namespace delimiters to mask
     * @return AnnotationsBag
     */
    public function useNamespace($pattern, array $delimiters = ['.', '\\'])
    {
        $mask = implode('', $delimiters);
        $consumer = '(' . implode('|', array_map('preg_quote', $delimiters)) . ')';
        $namespace_pattern = '/^' . preg_quote(rtrim($pattern, $mask)) . $consumer . '/';
        $iterator = new RegexIterator(
            $this->getIterator(),
            $namespace_pattern,
            RegexIterator::REPLACE,
            RegexIterator::USE_KEY
        );

        $iterator->replacement = '';

        return new static(iterator_to_array($iterator));
    }

    /**
     *
     * @return ArrayIterator
     */
    public function getIterator(): ArrayIterator
    {
        return new ArrayIterator($this->attributes);
    }

    public function union(BagInterface $bag)
    {
        return new static($this->attributes + $bag->toArray());
    }

    public function grep($pattern)
    {
        $results = array_intersect_key(
            $this->attributes,
            array_flip(
                RegexGuard::getGuard()->grep($pattern, array_keys($this->attributes))
            )
        );

        return new static($results);
    }

    public function getAsArray($key)
    {
        if (!$this->has($key)) {
            return [];
        }

        $res = $this->attributes[$key];

        if (is_null($res)) {
            return [null];
        }

        return (array)$res;
    }

    /**
     * Returns true if the element is defined.
     *
     * @param string $key The key
     *
     * @return bool true if the parameter exists, false otherwise
     */
    public function has($key)
    {
        return array_key_exists($key, $this->attributes);
    }

    /**
     *
     * @param mixed $index
     *
     * @return bool
     */
    public function exist($index): bool
    {
        return array_key_exists($index, $this->all());
    }

    /**
     * Returns the alphabetic characters of the parameter value.
     *
     * @param string $key The parameter key
     * @param string $default The default value if the parameter key does not exist
     *
     * @return string The filtered value
     */
    public function getAlpha($key, $default = '')
    {
        return preg_replace('/[^[:alpha:]]/', '', $this->get($key, $default));
    }

    /**
     *
     * @param mixed $index
     * @param mixed $default
     *
     * @return mixed|null|self|self[]
     */
    public function &get($index = null, $default = null)
    {
        if (is_null($index)) {
            return $this->attributes;
        }

        if ($this->exists($this->attributes, $index)) {
            return $this->attributes[$index];
        }

        if (strpos($index, '.') === false) {
            return $default;
        }

        $attributes = $this->attributes;

        // D($attributes);

        foreach (explode('.', $index) as $segment) {
            if (!is_array($attributes) || !$this->exists($attributes, $segment)) {
                return $default;
            }

            $attributes = &$attributes[$segment];

            if (is_object($attributes)) {
                $attributes = $attributes->attributes;
            }
        }

        // if (is_array($attributes)) {
        //     $attributes = new static($attributes);
        // }

        return $attributes;
    }

    /**
     * Checks if the given key exists in the provided array.
     *
     * @param array $array Array to validate
     * @param int|string $key The key to look for
     *
     * @return bool
     */
    protected function exists($array, $key)
    {
        return array_key_exists($key, $array);
    }

    /**
     * Returns the alphabetic characters and digits of the parameter value.
     *
     * @param string $key The parameter key
     * @param string $default The default value if the parameter key does not exist
     *
     * @return string The filtered value
     */
    public function getAlnum($key, $default = '')
    {
        return preg_replace('/[^[:alnum:]]/', '', $this->get($key, $default));
    }

    /**
     * Returns the digits of the parameter value.
     *
     * @param string $key The parameter key
     * @param string $default The default value if the parameter key does not exist
     *
     * @return string The filtered value
     */
    public function getDigits($key, $default = '')
    {
        // we need to remove - and + because they're allowed in the filter
        return str_replace(['-', '+'], '', $this->filter($key, $default, FILTER_SANITIZE_NUMBER_INT));
    }

    /**
     * Filter key.
     *
     * @param string $key Key
     * @param mixed $default Default = null
     * @param int $filter FILTER_* constant
     * @param mixed $options Filter options
     *
     * @return mixed
     * @see http://php.net/manual/en/function.filter-var.php
     *
     */
    public function filter($key, $default = null, $filter = FILTER_DEFAULT, $options = [])
    {
        $value = $this->get($key, $default);

        // Always turn $options into an array - this allows filter_var option shortcuts.
        if (!is_array($options) && $options) {
            $options = ['flags' => $options];
        }

        // Add a convenience check for arrays.
        if (is_array($value) && !isset($options['flags'])) {
            $options['flags'] = FILTER_REQUIRE_ARRAY;
        }

        return filter_var($value, $filter, $options);
    }

    /**
     * Returns the parameter value converted to integer.
     *
     * @param string $key The parameter key
     * @param int $default The default value if the parameter key does not exist
     *
     * @return int The filtered value
     */
    public function getInt($key, $default = 0)
    {
        return (int)$this->get($key, $default);
    }

    /**
     * Returns the parameter value converted to boolean.
     *
     * @param string $key The parameter key
     * @param mixed $default The default value if the parameter key does not exist
     *
     * @return bool The filtered value
     */
    public function getBoolean($key, $default = false)
    {
        return $this->filter($key, $default, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     *
     * @return array
     */
    public function getObjectVars(): array
    {
        return call_user_func('get_object_vars', $this);
    }

    /**
     * Returns the bag keys.
     *
     * @return array An array of bag keys
     */
    public function keys()
    {
        return array_keys($this->attributes);
    }

    /**
     * Set a given key / value pair or pairs
     * if the key doesn't exist already
     *
     * @param array|int|string $keys
     * @param mixed $value
     */
    public function add($keys, $value = null)
    {
        if (is_array($keys)) {
            foreach ($keys as $key => $value) {
                $this->add($key, $value);
            }
        } elseif (is_null($this->get($keys))) {
            $this->set($keys, $value);
        }
    }

    /**
     *
     * @param mixed $index
     * @param mixed $value
     */
    public function set($index, $value)
    {
        if (is_array($value)) {
            $value = new static($value);
        }

        $this->attributes[$index] = $value;

        return $this;
    }

    /**
     * Determine if the message bag has any attributes.
     *
     * @return bool
     */
    public function any()
    {
        return $this->count() > 0;
    }

    /**
     *
     * @return int
     */
    public function count(): int
    {
        return count($this->attributes);
    }

    public function clear()
    {
        foreach ($this->attributes as $key => $value) {
            $this->remove($key);
        }
    }

    /**
     *
     * @param mixed $offset
     */
    public function remove($offset)
    {
        if ($this->offsetExists($offset)) {
            unset($this->attributes[$offset]);
        }
    }

    /**
     *
     * @param mixed $offset
     *
     * @return bool
     */
    public function offsetExists($offset): bool
    {
        return $this->has($offset);
    }

    /**
     *
     * @return string
     */
    public function serialize(): string
    {
        return serialize($this->toArray());
    }

    /**
     *
     * @return array
     */
    public function toArray(array $options = null, int $depth = null)
    {
        // $array = [];

        // foreach ($this->attributes as $key => &$value) {
        //     if ($value instanceof self) {
        //         $value = $value->toArray();
        //     }

        //     $array[$key] = &$value;
        // }

        // return $array;

        return array_map(
            function ($value) use ($options, $depth) {
                return $value instanceof Arrayable ?
                    $value->toArray($options, $depth) :
                    $value;
            },
            $this->attributes
        );
    }

    /**
     *
     * @param string $serialized
     */
    public function unserialize($serialized)
    {
        $serialized = (array)@unserialize($serialized);
        $this->replace($serialized);
    }

    /**
     *
     * @param array $values
     */
    public function replace(array $values)
    {
        foreach ($values as $key => &$value) {
            $this->set($key, $value);
        }
    }

    public function jsonSerialize()
    {
        return $this->toArray();
    }

    public function yamlSerialize()
    {
        return $this->toArray();
    }

    /**
     * {inherit doc}
     */
    public function toYaml(int $options = Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK)
    {
        return Yaml::encode($this->toArray(), $options);
    }

    /**
     *
     * @param mixed $name
     *
     * @return bool
     */
    public function __isset($name): bool
    {
        return $this->offsetExists($name);
    }

    /**
     *
     * @param mixed $name
     */
    public function __unset($name)
    {
        $this->offsetUnset($name);
    }

    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset]);
    }

    /**
     *
     * @param mixed $name
     *
     * @return mixed|self|self[]
     */
    public function &__get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     *
     * @param mixed $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    /**
     *
     * @param mixed $index
     *
     * @return mixed|self
     */
    public function &offsetGet($index)
    {
        return $this->get($index);
    }

    public function offsetSet($index, $value)
    {
        $this->set($index, $value);

        return true;
    }

    public function __toString(): string
    {
        return (string)$this->toJson();
    }

    /**
     * {inherit doc}
     */
    public function toJson(int $options = Json::PRETTY)
    {
        return Json::encode($this->toArray(), $options);
    }
}
