<?php /** @noinspection PhpUnused */

declare(strict_types=1);

namespace Exen\Support;

use ArrayIterator;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Interfaces\Dottable;
use Exen\Support\Json\Json;
use Exen\Support\Yaml\Yaml;
use Exen\Support\Arr;
use Exen\Support\Traits\HashCodeTrait;
use function array_key_exists;
use function array_map;
use function array_merge;
use function array_merge_recursive;
use function array_pop;
use function array_replace;
use function bin2hex;
use function count;
use function explode;
use function gzdecode;
use function gzencode;
use function is_null;
use function serialize;
use function sprintf;
use function unserialize;
use function var_export;
use const COUNT_RECURSIVE;
use const EMPTY_STRING;
use const SINGLE_DOT;

// use CachingIterator;

/**
 * Class Dot.
 */
class Dot extends PhpObject implements Dottable
{
    use MacroableTrait;
    use HashCodeTrait;

    const SEPARATOR = '/[:\.]/i';

    /**
     * The stored items
     *
     * @var array
     */
    protected $items = [];

    /**
     * @var bool
     */
    protected $dirty = false;

    /**
     * Create a new Config instance
     *
     * @param mixed $items
     */
    public function __construct($items = null)
    {
        $this->items = $this->getArrayItems($items);
    }

    // Protected Methods //////////////////////////////////////////////////////

    /**
     * Return the given items as an array
     *
     * @param mixed $items Items.
     *
     * @return array
     */
    protected function getArrayItems($items = null): array
    {
        // if (Lang::isArray($items)) {
        //     return $items;
        // } elseif ($items instanceof self) {
        //     return $items->all();
        // }

        // return (array) $items;

        $retVal = [];

        if (Lang::isAssociativeArray($items)) {
            foreach ($items as $key => &$value) {
                if (Lang::isArray($value) and Lang::isAssoc($items)) {
                    $value = new static($value);
                } else {
                    $value = $value;
                }

                $retVal[$key] = $value;
            }
        } else {
            $retVal[] = $items;
        }

        if ($items instanceof self) {
            $retVal = $items->all();
        } else {
            $retVal = $items;
        }

        return $retVal;
    }

    /**
     * Merges two arrays recursively. In contrast to array_merge_recursive,
     * duplicate keys are not converted to arrays but rather overwrite the
     * value in the first array with the duplicate value in the second array.
     *
     * @param array|null $array1 Initial array to merge
     * @param array|null $array2 Array to recursively merge
     *
     * @return array
     */
    protected function arrayMergeRecursiveDistinct(?array $array1 = null, ?array $array2 = null): array
    {
        $merged = &$array1;

        foreach ($array2 as $key => $value) {
            if (Lang::isArray($value) and isset($merged[$key]) and Lang::isArray($merged[$key])) {
                $merged[$key] = $this->arrayMergeRecursiveDistinct($merged[$key], $value);
            } else {
                $merged[$key] = $value;
            }
        }

        return $merged;
    }

    // Public Methods /////////////////////////////////////////////////////////

    /**
     * Recursively merge a given array or a Dot object with the given key
     * or with the whole Dot object.
     *
     * Duplicate keys are converted to arrays.
     *
     * @param array|string|self $key
     * @param array|self        $value
     */
    public function mergeRecursive($key = null, $value = []): void
    {
        if (Lang::isArray($key)) {
            $this->items = array_merge_recursive($this->items, $key);
        } elseif (Lang::isString($key)) {
            $items = (array) $this->get($key);
            $value = array_merge_recursive($items, $this->getArrayItems($value));
            $this->set($key, $value);
        } elseif ($key instanceof self) {
            $this->items = array_merge_recursive($this->items, $key->all());
        }
    }

    /**
     * Recursively merge a given array or a Dot object with the given key
     * or with the whole Dot object.
     *
     * Instead of converting duplicate keys to arrays, the value from
     * given array will replace the value in Dot object.
     *
     * @param array|string|self $key
     * @param array|self        $value
     */
    public function mergeRecursiveDistinct($key, $value = []): void
    {
        if (Lang::isArray($key)) {
            $this->items = $this->arrayMergeRecursiveDistinct($this->items, $key);
        } elseif (Lang::isString($key)) {
            $items = (array) $this->get($key);
            $value = $this->arrayMergeRecursiveDistinct($items, $this->getArrayItems($value));
            $this->set($key, $value);
        } elseif ($key instanceof self) {
            $this->items = $this->arrayMergeRecursiveDistinct($this->items, $key->all());
        }
    }

    /**
     * Set a given key / value pair or pairs
     * if the key doesn't exist already
     *
     * @param array|int|string $keys
     * @param mixed            $value
     *
     * @return self
     */
    public function add($keys = null, $value = null): self
    {
        if (Lang::isArray($keys)) {
            foreach ($keys as $key => $value) {
                $this->add($key, $value);
            }
        } elseif (Lang::isNull($this->get($keys))) {
            $this->set($keys, $value);
        }

        return $this;
    }

    /**
     * Return all the stored items
     *
     * @return array
     */
    public function all(): array
    {
        return $this->items;
    }

    /**
     * Check if a given key or keys are empty
     *
     * @param  array|int|string|null $keys
     * @return bool
     */
    public function isEmpty($keys = null): bool
    {
        if (Lang::isNull($keys)) {
            return empty($this->items);
        }

        $keys = (array) $keys;

        foreach ($keys as $key) {
            if (!empty($this->get($key))) {
                return false;
            }
        }

        return true;
    }

    /**
     * Check if a given key or keys are not empty
     *
     * @param  array|int|string|null $keys
     * @return bool
     */
    public function isNotEmpty($keys = null): bool
    {
        return (!$this->isEmpty($keys));
    }

    /**
     * Delete the contents of a given key or keys
     *
     * @param array|int|string|null $keys
     */
    public function clear($keys = null)
    {
        if (Lang::isNull($keys)) {
            $this->items = [];
            return;
        }

        $keys = (array) $keys;

        foreach ($keys as $key) {
            $this->set($key, []);
        }

        return $this;
    }

    /**
     * Delete the given key or keys
     *
     * @param array|int|string $keys
     */
    public function delete($keys = null): self
    {
        $keys = (array) $keys;

        foreach ($keys as $key) {
            if ($this->exists($this->items, $key)) {
                unset($this->items[$key]);
                continue;
            }

            $items = &$this->items;
            $segments = explode(SINGLE_DOT, $key);
            $lastSegment = array_pop($segments);

            foreach ($segments as $segment) {
                if (!isset($items[$segment]) or !Lang::isArray($items[$segment])) {
                    continue 2;
                }

                $items = &$items[$segment];
            }

            unset($items[$lastSegment]);
        }

        return $this;
    }

    /**
     *
     * @param mixed $index
     *
     * @return bool
     */
    public function exist($index = null): bool
    {
        return array_key_exists($index, $this->items);
    }

    /**
     * Checks if the given key exists in the provided array.
     *
     * @param array      $array Array to validate
     * @param int|string $key   The key to look for
     *
     * @return bool
     */
    protected function exists($array = null, $key = null): bool
    {
        return array_key_exists($key, $array);
    }

    /**
     * Return the value of the given config key.
     *
     * @param  int|string|null $key
     * @param  mixed           $default
     * @return mixed
     */
    public function &get($key = null, $default = null)
    {
        $items = $this->items;

        if (Lang::isNull($key)) {
            return $items;
        }

        // if (Arr::has($this->items, $key)) {
        //     return $this->items[$key];
        // }

        // if (\strpos($key, \SINGLE_DOT) === false) {
        //     return $default;
        // }

        // $items = $this->items;

        // foreach (\explode(\SINGLE_DOT, $key) as $segment) {
        //     if (!Lang::isArray($items) or !Arr::has($items, $segment)) {
        //         return $default;
        //     }

        //     $items = &$items[$segment];
        // }

        // return $items;

        if (Arr::exists($items, $key)) {
            return $items[$key];
        }

        if (false === Str::position($key, SINGLE_DOT)) {
            return $items[$key] ?? Lang::checkValue($default);
        }

        if (Lang::isArray($key)) {
            $return = [];

            foreach ($key as $k) {
                $return[$k] = Arr::get($items, $k, $default);
            }

            return $return;
        }

        $segments = explode(SINGLE_DOT, $key);

        foreach ($segments as $segment) {
            if (Arr::isAccessible($items) && Arr::exists($items, $segment)) {
                $items = $items[$segment];
            } else {
                return $default;
            }
        }

        return $items;
    }

    /**
     * Set a given key / value pair or pairs.
     *
     * @param array|int|string $keys
     * @param mixed $value
     * @return Dot
     */
    public function set($keys = null, $value = null): self
    {
        if (Lang::isArray($keys)) {
            if (Lang::isAssociative($keys)) {
                foreach ($keys as $key => $val) {
                    $this->set($key, $val);
                }
            }

            return $this;
        }

        $items = &$this->items;

        foreach (explode(SINGLE_DOT, $keys) as $key) {
            if (Lang::isNotSet($items, $key) or Lang::isNotArray($items[$key])) {
                $items[$key] = $value;
                // $items[$key] = [];
            }

            $item = &$items[$key];

            if ($item instanceof self) {
                $item->setDirty(true);
            }

            $items = &$item;
        }

        $items = $value;

        return $this;
    }

    /**
     * Replace all values or values within the given key with an array or Dot object.
     *
     * @param array|string|self $key
     * @param array|self $value
     * @return Dot
     */
    public function replace($key = null, $value = []): self
    {
        if (Lang::isArray($key)) {
            $this->items = array_replace($this->items, $key);
        } elseif (Lang::isString($key)) {
            $items = (array) $this->get($key);
            $value = array_replace($items, $this->getArrayItems($value));

            $this->set($key, $value);
        } elseif ($key instanceof self) {
            $this->items = array_replace($this->items, $key->all());
        }

        return $this;
    }

    /**
     * Replace all items with a given array
     *
     * @param mixed $items
     */
    public function setArray($items = null): void
    {
        $this->items = $this->getArrayItems($items);
    }

    /**
     * Replace all items with a given array as a reference
     *
     * @param array|null $items
     */
    public function setReference(?array &$items = null): void
    {
        $this->items = &$items;
    }

    /**
     * Checks if the given keys exists.
     *
     * @param array|int|string $keys
     *
     * @return bool
     */
    public function has($keys = null): bool
    {
        $keys = (array) $keys;

        if (!$this->items or $keys === []) {
            return false;
        }

        foreach ($keys as $key) {
            $items = $this->items;

            if (Arr::exists($items, $key)) {
                continue;
            }

            foreach (explode(SINGLE_DOT, $key) as $segment) {
                if (!Lang::isArray($items) or !Arr::exists($items, $segment)) {
                    return false;
                }

                $items = $items[$segment];
            }
        }

        return true;
    }

    /**
     * Merges a given array or a Config object with the given key
     * or with the whole Config object
     *
     * @param array|string|self $key
     * @param array $value
     * @param boolean $replace
     * @return Dot
     */
    public function merge($key = null, $value = null, bool $replace = false): self
    {
        // $this->items = Arr::merge($this->items, $key, $replace);
        // $this->dirty = true;

        // return $this;

        if (Lang::isArray($key)) {
            $this->items = Arr::merger($this->items, $key, $replace);
            $this->dirty = true;
        } elseif (Lang::isString($key)) {
            $items = (array) $this->get($key);
            // $value = array_merge($items, $this->getArrayItems($value));
            $value = Arr::merge($items, $this->getArrayItems($value));
            $this->set($key, $value);
        } elseif ($key instanceof self) {
            $this->items = Arr::merge($this->items, $key->all());
            $this->dirty = true;
        }

        return $this;
    }

    /**
     * Extracts values.
     *
     * @param  array $keys
     * @param  bool  $include
     *
     * @return array
     */
    public function extract($keys = null, ?bool $include = true)
    {
        return Arr::extract($this->items, $keys, $include);
    }

    /**
     * Return the value of a given key and
     * delete the key
     *
     * @param  int|string|null $key
     * @param  mixed           $default
     *
     * @return mixed
     */
    public function pull($key = null, $default = null): array
    {
        // $values = $this->get($key);
        // // Arr::pull($values, $value);
        // Arr::pull($values, $key);
        // return $this->set($key, $values);

        if (Lang::isNull($key)) {
            $value = $this->all();
            $this->clear();
            return $value;
        }

        $value = $this->get($key, $default);
        $this->delete($key);

        return $value;
    }

    /**
     * Push a given value to the end of the array
     * in a given key
     *
     * @param mixed $key
     * @param mixed $value
     */
    public function push($key = null, $value = null)
    {
        // $values = $this->get($key);
        // $values[] = $value;
        // return $this->set($key, $values);

        if (Lang::isNull($value)) {
            $this->items[] = $key;
            return;
        }

        $items = $this->get($key);

        if (Lang::isArray($items) or Lang::isNull($items)) {
            $items[] = $value;
            $this->set($key, $items);
        }

        return $this;
    }

    /**
     * Removes one or more values.
     *
     * @param array|string $keys
     *
     * @return self
     */
    public function remove($keys = null): self
    {
        Arr::rem($this->items, $keys);
        $this->dirty = true;
        return $this;
    }

    /**
     * Checks if the values are modified.
     *
     * @return bool|boolean
     */
    public function isDirty(): bool
    {
        return $this->dirty;
    }

    /**
     * @return void
     */
    public function setDirty(?bool $boolean = false): void
    {
        $this->dirty = $boolean;
    }

    /**
     * {@inheritDoc}
     */
    public function jsonSerialize()
    {
        return array_map(
            function ($value) {
                if ($value instanceof Interfaces\JsonSerializable) {
                    return $value->jsonSerialize();
                } elseif ($value instanceof Interfaces\Jsonable) {
                    return Json::decode($value->toJson(), Json::FORCE_ARRAY);
                } elseif ($value instanceof Interfaces\Arrayable) {
                    return $value->toArray();
                }

                return $value;
            },
            $this->items
        );
    }

    /**
     * {@inheritDoc}
     */
    public function yamlSerialize()
    {
        return array_map(
            function ($value) {
                if ($value instanceof Interfaces\YamlSerializable) {
                    return $value->yamlSerialize();
                } elseif ($value instanceof Interfaces\Yamlable) {
                    return Yaml::decode($value->toYaml());
                } elseif ($value instanceof Interfaces\Arrayable) {
                    return $value->toArray();
                }

                return $value;
            },
            $this->items
        );
    }

    /**
     * {@inheritDoc}
     */
    public function serialize()
    {
        return @serialize($this->items);
    }

    /**
     * {@inheritDoc}
     */
    public function unserialize($serialized)
    {
        $serialized = (array) @unserialize($serialized);
        $this->replace($serialized);
    }

    /**
     * {@inheritDoc}
     */
    public function gzipSerialize()
    {
        return gzencode(serialize($this), 1);
    }

    /**
     * {@inheritDoc}
     */
    public function gzipUnserialize($raw = null)
    {
        if (bin2hex($raw[0]) == '1f' and bin2hex($raw[1]) == '8b') {
            $raw = gzdecode($raw);
        }

        return unserialize($raw);
    }

    /**
     * {@inheritDoc}
     */
    public function toArray(?array $options = null, ?int $depth = null)
    {
        return array_map(
            function ($value) use ($options, $depth) {
                return $value instanceof Interfaces\Arrayable ? $value->toArray($options, $depth) : $value;
            },

            $this->items
        );
    }

    /**
     * {@inheritDoc}
     */
    public function toJson($key = null, ?int $options = Json::PRETTY)
    {
        if (Lang::isNotNull($key) and Lang::isNotEmpty($key) and Lang::isString($key)) {
            return Json::encode($this->get($key), $options);
        }

        $options = $key === null ? 0 : $key;

        return Json::encode($this->jsonSerialize(), $options);
    }

    /**
     * {@inheritDoc}
     */
    public function toYaml($key = null, ?int $options = Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK)
    {
        if (Lang::isNotNull($key) and Lang::isNotEmpty($key) and Lang::isString($key)) {
            return Yaml::encode($this->get($key), $options);
        }

        // $options = $key === null ? 0 : $key;

        return Yaml::encode($this->yamlSerialize(), $options);
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return $this->toJson();
    }

    /**
     * Dumps the values as php.
     *
     * @return string
     */
    public function dump(): string
    {
        $content = var_export($this->toArray(), true);

        $retVal = sprintf(
            '<?php' . ENDL . ENDL . 'declare(strict_types=1);' . ENDL . ENDL . 'return %s;' . ENDL,
            $content
        );

        return $retVal;
    }

    /**
     * Flatten an array with the given character as a key delimiter
     *
     * @param  string     $delimiter
     * @param  array|null $items
     * @param  string     $prepend
     *
     * @return array
     */
    public function flatten(
        ?string $delimiter = SINGLE_DOT,
        ?array $items = null,
        ?string $prepend = EMPTY_STRING
    ): array {
        $retVal = [];

        if (is_null($items)) {
            // $items = $this->items;
            $items = $this->toArray();
        }

        if (Lang::isAssociativeArray($items)) {
            foreach ($items as $key => &$value) {
                if (Lang::isArray($value) and Lang::isNotEmpty($value)) {
                    $retVal = array_merge(
                        $retVal,
                        $this->flatten($delimiter, $value, $prepend . $key . $delimiter)
                    );
                } else if ($value instanceof self) {
                    $retVal[$prepend . $key] = $value->flatten();
                } else {
                    $retVal[$prepend . $key] = $value;
                }
            }
        }

        return $retVal;
    }

    // Private Methods ////////////////////////////////////////////////////////

    // ArrayAccess Interface //////////////////////////////////////////////////

    /**
     * Check if a given key exists
     *
     * @param  int|string $key
     * @return bool
     */
    public function offsetExists($key)
    {
        return $this->has($key);
    }

    /**
     * Return the value of a given key
     *
     * @param  int|string $key
     * @return mixed
     */
    public function &offsetGet($key)
    {
        return $this->get($key);
    }

    /**
     * Set a given value to the given key
     *
     * @param int|string|null $key
     * @param mixed           $value
     */
    public function offsetSet($key, $value)
    {
        if (Lang::isNull($key)) {
            $this->items[] = $value;
            return;
        }

        $this->set($key, $value);
    }

    /**
     * Delete the given key
     *
     * @param int|string $key
     */
    public function offsetUnset($key)
    {
        $this->remove($key);
    }

    // IteratorAggregate Interface ////////////////////////////////////////////

    /**
     * Get an iterator for the stored items
     *
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    // Countable Interface ///////////////////////////////////////////////////////////////////////

    /**
     * Returns the number of items in a given key.
     *
     * @param  int|string|null $key
     *
     * @return int
     */
    public function count($key = null): int
    {
        return count($this->get($key), COUNT_RECURSIVE) ?? count($this->items, COUNT_RECURSIVE) ?? 0;
    }

    // Magic Methods //////////////////////////////////////////////////////////

    /**
     * {@inheritDoc}
     */
    public function __isset($name): bool
    {
        return $this->offsetExists($name);
    }

    /**
     * {@inheritDoc}
     */
    public function __set($name, $value)
    {
        $this->offsetSet($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function &__get($name)
    {
        return $this->offsetGet($name);
    }

    /**
     * {@inheritDoc}
     */
    public function __unset($name)
    {
        $this->offsetUnset($name);
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(static::class)['classname'],
            ]
        );
    }

    // /**
    //  * {@inheritDoc}
    //  */
    // public function __debugInfo()
    // {
    //     $retVal = $this->toArray();
    //     return $retVal;
    // }
}
