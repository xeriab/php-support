<?php

declare(strict_types=1);

namespace Exen\Support;

use ErrorException;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper as SymfonyCliDumper;
use function class_exists;
use function in_array;
use function var_dump;
use const PHP_SAPI;

final class Dumper
{
    /**
     * Dump a value with elegance.
     *
     * @param mixed $value
     * @throws ErrorException
     */
    public function dump($value)
    {
        $isCli = in_array(PHP_SAPI, ['cli', 'phpdbg']);

        if (class_exists(SymfonyCliDumper::class)) {
            $dumper = $isCli ? new SymfonyCliDumper() : new HtmlDumper();
            $dumper->dump((new VarCloner())->cloneVar($value));
        } else {
            var_dump($value);
        }
    }
}
