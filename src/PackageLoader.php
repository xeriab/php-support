<?php

declare(strict_types=1);

namespace Exen\Support;

use LogicException;
use Exen\Support\Interfaces\Singletonable;
use Exen\Support\Traits\MacroableTrait;
use Exen\Support\Json\Json;
use function file_exists;
use function is_array;
use function preg_match;
use function preg_quote;
use function preg_replace;
use function realpath;
use function spl_autoload_register;
use function str_replace;

/**
 * Class for PackageLoader.
 *
 * @category Utilities
 * @package  Support
 * @author   Xeriab Nabil (aka KodeBurner) <hey@xeriab.net>
 * @license  https://gitlab.com/xeriab/php-support/blob/master/LICENSE MIT
 * @link     https://gitlab.com/xeriab/php-support
 */
final class PackageLoader extends PhpObject implements Singletonable
{
    use MacroableTrait;

    /**
     * Working Path.
     *
     * @var string|null $workingPath
     */
    protected $workingPath = null;

    /**
     * PackageLoader instance.
     *
     * @var PackageLoader $instance
     */
    protected static $instance = null;

    /**
     * PackageLoader instances.
     *
     * @var PackageLoader[] $instances
     */
    protected static $instances = [];

    /**
     * Create a new PackageLoader instance.
     *
     * @param string|null $workingPath Directory/path string.
     *
     * @return void
     */
    public function __construct(?string $workingPath = null)
    {
        if (Lang::isNotEmpty($workingPath)) {
            $this->workingPath = $workingPath;
        } else {
            // workingPath = __DIR__ . DS . '..' . DS;
            $this->workingPath = realpath(__DIR__ . DS . '..' . DS);
        }

        self::$instance = &$this;
    }

    /**
     * Gets Composer's file.
     *
     * @return array|null
     */
    public function getComposerFile(): ?array
    {
        if (Filesystem::exists($this->workingPath.'/composer.json')) {
            return Json::parseFile(
                $this->workingPath . '/composer.json',
                Json::FORCE_ARRAY
            );
        }

        return null;
    }

    /**
     * Loads files and classes.
     *
     * @param string|null $workingPath Directory to load from.
     *
     * @return void
     */
    public function load(?string $workingPath = null): void
    {
        $this->workingPath = $workingPath;
        $composer = $this->getComposerFile();

        if (isset($composer['autoload']['psr-4'])) {
            $this->loadPSR4($composer['autoload']['psr-4']);
        }

        if (isset($composer['autoload']['psr-0'])) {
            $this->loadPSR0($composer['autoload']['psr-0']);
        }

        if (isset($composer['autoload']['files'])) {
            $this->loadFiles($composer['autoload']['files']);
        }
    }

    /**
     * Loads files.
     *
     * @param array|null $files Files.
     *
     * @return void
     */
    public function loadFiles(?array $files = null): void
    {
        foreach ($files as $file) {
            $fullPath = $this->workingPath.'/'.$file;

            if (file_exists($fullPath)) {
                /** @noinspection PhpIncludeInspection */
                // include_once($fullPath);
                require_once $fullPath;
            }
        }
    }

    /**
     * Loads PSR4.
     *
     * @param array $namespaces Namespaces.
     *
     * @return void
     */
    public function loadPSR4(array $namespaces): void
    {
        $this->loadPSR($namespaces, true);
    }

    /**
     * Loads PSR0.
     *
     * @param array $namespaces Namespaces.
     *
     * @return void
     */
    public function loadPSR0(array $namespaces): void
    {
        $this->loadPSR($namespaces, false);
    }

    /**
     * Loads PSR.
     *
     * @param array $namespaces Namespaces.
     * @param boolean $psr4 Is it PSR4.
     *
     * @return void
     */
    public function loadPSR(array $namespaces, bool $psr4): void
    {
        $workingPath = $this->workingPath;

        // Foreach namespace specified in the composer, load the given classes
        foreach ($namespaces as $ns => $paths) {
            if (!is_array($paths)) {
                $paths = [$paths];
            }

            spl_autoload_register(
                function ($classname) use ($ns, $paths, $workingPath, $psr4) {
                    // Check if the namespace matches the class we are
                    // looking for
                    if (preg_match('#^'. preg_quote($ns).'#', $classname)) {
                        // Remove the namespace from the file path since
                        // it's psr4
                        if ($psr4) {
                            $classname = str_replace($ns, '', $classname);
                        }

                        $filename = preg_replace(
                            '#\\\\#',
                            '/',
                            $classname
                        ).'.php';

                        foreach ($paths as $classpath) {
                            $fullPath = $this->workingPath.'/'.
                                $classpath."/$filename";

                            if (file_exists($fullPath)) {
                                /** @noinspection PhpIncludeInspection */
                                include_once $fullPath;
                            }
                        }
                    }
                }
            );
        }
    }

    /**
     * {@inheritDoc}
     */
    public static function instance(?string $workingPath = null): ?PackageLoader
    {
        if (self::$instance === null) {
            self::$instance = new PackageLoader($workingPath);
        }

        return self::$instance;
    }

    /**
     * {@inheritDoc}
     */
    public static function getInstance(?string $workingPath = null): ?PackageLoader
    {
        return self::instance($workingPath);
    }

    public function __clone()
    {
        throw new LogicException(
            'Class ' . get_class($this) .
            ' is singleton and cannot be cloned, use instance() method'
        );
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {
        return $this->toString();
    }

    /**
     * {@inheritDoc}
     */
    public function toString(): string
    {
        return xprintf(
            '<%(className)s>',
            [
                'className' => parseClassName(self::class)['classname'],
            ]
        );
    }
}
