<?php
/** @noinspection SpellCheckingInspection */
/** @noinspection JSUnresolvedVariable */
/** @noinspection BadExpressionStatementJS */
/** @noinspection JSUnresolvedFunction */
/** @noinspection PhpUnusedLocalVariableInspection */

declare(strict_types=1);

namespace Exen\Support;

use Symfony\Component\VarDumper\Dumper\HtmlDumper as SymfonyHtmlDumper;

// $DEFAULT_STYLE = <<<DEFAULT_STYLE
// background-color: #fff;
// color: #222;
// line-height: 1.2em;
// font-weight: normal;
// font-family: Monaco, Consolas, monospace;
// font-size: 12px;
// word-wrap: break-word;
// white-space: pre-wrap;
// position: relative;
// z-index: 100000;
// dir: ltr !important;
// DEFAULT_STYLE;

$DEFAULT_STYLE = 'background-color: #fff; color: #222; line-height: 1.2em; font-weight: normal;';
$DEFAULT_STYLE .= ' font-family: Monaco, Consolas, monospace; font-size: 12px; word-wrap: break-word;';
$DEFAULT_STYLE .= ' white-space: pre-wrap; position: relative; z-index: 100000; dir: ltr !important;';

// $STYLE_ARRAY = [
//     'default' => $DEFAULT_STYLE,
//     'num' => 'color:#a71d5d',
//     'const' => 'color:#795da3',
//     'str' => 'color:#df5000',
//     'cchr' => 'color:#222',
//     'note' => 'color:#a71d5d',
//     'ref' => 'color:#a0a0a0',
//     'public' => 'color:#795da3',
//     'protected' => 'color:#795da3',
//     'private' => 'color:#795da3',
//     'meta' => 'color:#b729d9',
//     'key' => 'color:#df5000',
//     'index' => 'color:#a71d5d'
// ];

final class HtmlDumper extends SymfonyHtmlDumper
{
    /**
     * @var string
     */
    protected $dumpPrefix = <<<'PREFIX'
<style>
    * .sf-dump,
    * .exen-html-dump,
    *.sf-dump,
    *.exen-html-dump,
    pre.sf-dump,
    pre.exen-html-dump {
        font-size: 12px;
        direction: ltr !important;
        text-align: left;
    }
</style>
<pre dir="ltr" class="sf-dump exen-html-dump" id="%s" data-indent-pad="%s">
PREFIX;
    protected $dumpSuffix = '</pre><script>Sfdump(%s)</script>';
    protected $dumpId = 'sf-dump';

    /**
     * Colour definitions for output.
     *
     * @var array $styles.
     */
    // phpcs: ignore
    protected $styles = [
        // 'default'   => $DEFAULT_STYLE,
        'num'       => 'color:#a71d5d',
        'const'     => 'color:#795da3',
        'str'       => 'color:#df5000',
        'cchr'      => 'color:#222',
        'note'      => 'color:#a71d5d',
        'ref'       => 'color:#a0a0a0',
        'public'    => 'color:#795da3',
        'protected' => 'color:#795da3',
        'private'   => 'color:#795da3',
        'meta'      => 'color:#b729d9',
        'key'       => 'color:#df5000',
        'index'     => 'color:#a71d5d'
    ];
}
