<?php

declare(strict_types=1);

namespace Exen\Support;

use Exen\Support\Json\Json;
use Symfony\Component\Process\Process as SymfonyProcess;
use Symfony\Component\Process\PhpExecutableFinder;

/**
 * Composer class.
 *
 * @since 0.1.1
 */
class Composer
{
    /**
     * The working path to regenerate from.
     *
     * @var string
     */
    protected $workingPath = null;

    /**
     * Create a new Composer manager instance.
     *
     * @param string|null $workingPath Working directory/path.
     *
     * @return void
     */
    public function __construct(?string $workingPath = null)
    {
        if (Lang::isNotEmpty($workingPath) and Lang::isNotNull($workingPath)) {
            $this->workingPath = $workingPath;
        } else {
            // $workingPath = __DIR__ . DS . '..' . DS;
            $this->workingPath = \realpath(__DIR__ . DS . '..' . DS);
        }
    }

    /**
     * Gets Composer's file.
     *
     * @return array
     */
    public function getComposerFile(): array
    {
        if (Filesystem::exists($this->workingPath . '/composer.json')) {
            return Json::parseFile(
                $this->workingPath . '/composer.json',
                Json::FORCE_ARRAY
            );
        }
    }

    /**
     * Regenerate the Composer autoloader files.
     *
     * @param string $extra Extra options.
     *
     * @return void
     */
    public function dumpAutoloads(?string $extra = ''): void
    {
        // $process = $this->getProcess();

        // $process->setCommasndLine(
        //     \trim($this->findComposer() . ' dump-autoload ' . $extra)
        // );

        $process = SymfonyProcess::fromShellCommandline(
            \trim($this->findComposer() . ' dump-autoload ' . $extra),
            $this->workingPath
        );

        $process->enableOutput();

        // $process->start();
        $process->run();
    }

    /**
     * Regenerate the optimized Composer autoloader files.
     *
     * @return void
     */
    public function dumpOptimized(): void
    {
        $this->dumpAutoloads('--optimize');
    }

    /**
     * Get the composer command for the environment.
     *
     * @return string
     */
    protected function findComposer(): string
    {
        if (Filesystem::exists($this->workingPath . '/composer.phar')) {
            return Process::escapeArgument(
                (new PhpExecutableFinder)->find(false)
            ) . ' composer.phar';
        }

        return 'composer';
    }

    /**
     * Get a new Symfony process instance.
     *
     * @return \Symfony\Component\Process\Process
     */
    protected function getProcess(): SymfonyProcess
    {
        return (new SymfonyProcess([], $this->workingPath))->setTimeout(null);
    }

    /**
     * Set the working path used by the class.
     *
     * @param string $path Working directory/path.
     *
     * @return self
     */
    public function setWorkingPath(?string $path = null): self
    {
        $this->workingPath = \realpath($path);
        return $this;
    }
}
